function login_rules() {
    $("#login-form").validate();

    $("#form-username").rules("add", {
        required: true,
        maxlength: 100
    });


}


$(document).ready(function() {

    login_rules(); 
    $('[data-toggle="tooltip"]').tooltip();  

});


$("#form-username").keyup(function(){
  $(this).val($(this).val().toLowerCase());
});

$("#captcha_refresh").click(function(){
  $("#captcha").load("function/recaptcha.fn.php");
});


