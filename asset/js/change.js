function login_rules() {
    $("#login-form").validate();

    $("#form_password").rules("add", {
        required: true,
        minlength: 8,
        maxlength: 40,
        atLeastOneLowercaseLetter: true,
        atLeastOneUppercaseLetter: true,
        atLeastOneNumber: true,
        atLeastOneSymbol: true        
    });

    $("#form_password_r").rules("add", {
        required: true,
        minlength: 8,
        maxlength: 40,
        equalTo: "#form_password"
    });



}


$(document).ready(function() {
    login_rules();
});



$("#form-submit").click(function(e) {
    login_rules();
});




