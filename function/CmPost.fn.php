<?php
switch ($tfFnName){
  case "like":    

    $q = "INSERT INTO cm_post_like(id_post,created_by,created_date)
            VALUES (?,?,?)";

    $param = array($tfRequest->id_post,$tfs->getUserId(),date("Y-m-d H:i:s"));
    $tfs->execute($q,$param);
    

    $q = "SELECT count(*) likes
            FROM cm_post_like
           WHERE id_post=?";
    $param = array($tfRequest->id_post);
    list($rs) = $tfs->executeQuery($q,$param);

    $response["code"]="OK";
    $response["likes"]=$rs["likes"];
    echo json_encode($response,true);
  break;
  case "unlike":
    $q = "DELETE FROM cm_post_like
           WHERE id_post=?
             AND created_by = ?";
    $param = array($tfRequest->id_post,$tfs->getUserId());
    $tfs->execute($q,$param);

    $q = "SELECT count(*) likes
            FROM cm_post_like
           WHERE id_post=?";
    $param = array($tfRequest->id_post);
    list($rs) = $tfs->executeQuery($q,$param);

    $response["code"]="OK";
    $response["likes"]=$rs["likes"];
    echo json_encode($response,true);
  break;
  case "comment":
   $q = "INSERT INTO cm_post_comment(id_post,comment,active,created_by,created_date)
            VALUES (?,?,?,?,?)";

    $param = array($tfRequest->id_post,$tfRequest->comment,"Y",$tfs->getUserId(),date("Y-m-d H:i:s"));
    $tfs->execute($q,$param);
    

    $q = "SELECT count(*) comments
            FROM cm_post_comment
           WHERE id_post=?
            AND active = 'Y'
            AND created_by != ?";
    $param = array($tfRequest->id_post,$tfs->getUserId());
    list($rs) = $tfs->executeQuery($q,$param);

    $response["other_comment"]=$rs["comments"];
    
    $q = "SELECT count(*) comments
            FROM cm_post_comment
           WHERE id_post=?
            AND active = 'Y'
            AND created_by = ?";
    $param = array($tfRequest->id_post,$tfs->getUserId());
    list($rs) = $tfs->executeQuery($q,$param);
    
    $response["code"]="OK";
    $response["mine_comment"]=$rs["comments"];
    echo json_encode($response,true);
  break;
  break;
  case "uncomment":
    $q = "UPDATE cm_post_comment
             SET active = 'N'
           WHERE id=?
             AND created_by = ?";

    $param = array($tfRequest->id_comment,$tfs->getUserId());
    $tfs->execute($q,$param);
    

    $q = "SELECT count(*) comments
            FROM cm_post_comment
           WHERE id_post=?
           AND active = 'Y'
            AND created_by != ?";
    $param = array($tfRequest->id_post,$tfs->getUserId());
    list($rs) = $tfs->executeQuery($q,$param);

    $response["other_comment"]=$rs["comments"];
    
    $q = "SELECT count(*) comments
            FROM cm_post_comment
           WHERE id_post=?
           AND active = 'Y'
            AND created_by = ?";
    $param = array($tfRequest->id_post,$tfs->getUserId());
    list($rs) = $tfs->executeQuery($q,$param);
    
    $response["code"]="OK";
    $response["mine_comment"]=$rs["comments"];
    echo json_encode($response,true);
  break;
  case "pagination":
    $aux=CmPost::dataListPage($tfs,$tfRequest->posts);

    $showIndex = explode(",", $tfRequest->posts);
    foreach ($showIndex as $i) {
        $show[]= $aux[array_search($i, array_column($aux, 'id'))];
     }

    $i=1;
    foreach ($show as $s) {
        $htmlPage.='  
                      <div>
                          <div class="card-header post">
                              <span class="badge badge-pill category-bg-'.$s["id_category"].' float-right m-2">'.$s["category"].'</span>
                              <div class="d-flex justify-content-between align-items-center">
                                  <div class="d-flex justify-content-between align-items-center">
                                      <div class="mr-2">
                                          <img class="rounded-circle post-profile-photo photo-bg-'.$i.'" src="'.$s["photo"].'" alt="">
                                      </div>
                                      <div class="ml-2">
                                          <div class="post-created-by m-0">'.$s["name"].'</div>
                                          <div class="post-business-title m-0">'.$s["business_title"].'</div>
                                          <div class="post-email-account m-0">'.$s["email_account"].'</div>
                                          <div class="post-ago mt-1">
                                              <i class="bx bx-clock-o"></i>Hace '.CmPost::timeAgo($s["created_date"]).'
                                          </div>
                                      </div>
                                  </div>
                             </div>
                          </div>
                          <div class="card-body">
                              <div class="post-title mb-2">'.$s["title"].'</div>';
                              
                         $length = strlen($s["content"]);
                   if ($length>250){
                      $htmlPage.=' <span class="card-text text-justify more">'.substr($s["content"],0,250).'...</span>
                      <span class="morecontent">
                             <span class="card-text text-justify more" style="display:none;">'.$s["content"].'</span>
                             <span class="morelink" onclick="more(this);">  Mostrar mas</span>
                          </span>';
                   }else{
                            $htmlPage.=' <span class="card-text more">'.$s["content"].'</span>';
                   }  
                   $htmlPage.='<div class="mt-2">Tags: ';     
                   $tags = explode(",",$s["tag"]);
                              foreach ($tags as $t) {
                                        $htmlPage.='<span class="badge badge-pill tag">'.$t.'</span>';
                              }  
                          
                    $htmlPage.='</div>
                                  </div>
                          <div class="card-body post">
                              <p class="feedback">';
                      if ($s["likeme"]>0){
                                $htmlPage.='<span class="like" data-id-post="'.$s["id"].'" onclick="like(this);"><i class="bx bxs-heart like"></i></span>';
                      }else{
                        $htmlPage.='<span class="no-like" data-id-post="'.$s["id"].'" onclick="like(this);"><i class="bx bx-heart"></i></span>';   
                      }      
                       if ($s["commented"]>0){
                                $htmlPage.='<span id="feedback-comment-'.$s["id"].'" data-toggle="modal" data-target="#exampleModal" class="no-comment" data-id-post="'.$s["id"].'" onclick="comment(this);"><i class="bx bxs-comment commented"></i></span>';
                      }else{
                        $htmlPage.='<span id="feedback-comment-'.$s["id"].'" data-toggle="modal" data-target="#exampleModal" class="no-comment" data-id-post="'.$s["id"].'" onclick="comment(this);"><i class="bx bx-comment"></i></span>';   
                      }   
                       $htmlPage.='   </p> 
                              <p class="likes">
                                <b id="likes-'.$s["id"].'">'.$s["likes"].' Me gusta</b>
                              </p>';
                            
                            
                            $minePostComments = CmPostComment::mineByPost($tfs,$s["id"]);
                            $postComments = CmPostComment::listByPost($tfs,$s["id"]);
                            
                            if (count($minePostComments)>0){
                              $comment = $minePostComments;
                              $mine=TRUE;
                            }else{
                              $comment = array_shift($postComments);
                              $mine=FALSE;
                            }  
                            $htmlPage.='<div id="one-comment-'.$s["id"].'">';
                            if ($mine){                              
                              foreach ($comment as $mc) {
                                $htmlPage.='<span class="post-comment text-justify"><b>'.$mc["created_by"].' </b>'.$mc["comment"].'<span class="post-ago"> - '.CmPost::timeAgo($mc["created_date"]).' </span> <a class="btn-epic-sm" data-id-post="'.$s["id"].'"data-id-comment="'.$mc["id"].'" onclick="uncomment(this);"><i class="bx bx-x"></i></a></span>';
                              }                                
                            }else{
                             $htmlPage.='<span  class="post-comment text-justify"><b>'.$comment["created_by"].' </b>'.$comment["comment"].'<span class="post-ago"> - '.CmPost::timeAgo($comment["created_date"]).'</span></span>';
                            } 
                            $htmlPage.='</div>';
                            $htmlPage.='<div id="all-comment-'.$s["id"].'" style="display:none;">';
                            foreach ($postComments as $pc) {
                              $htmlPage.='<span class="post-comment text-justify"><b>'.$pc["created_by"].' </b>'.$pc["comment"].'<span class="post-ago"> - '.CmPost::timeAgo($pc["created_date"]).'</span></span>';
                            }    
                            $htmlPage.='</div>
                                <span class="all-comment light" data-id-comment="'.$s["id"].'" onclick="allComment(this)"><span class="verb"> Mostrar </span>'.count($postComments).' comentario(s)</span>    
                          </div>
                      </div>
                  <hr>';
      $i++;
      if ($i==6){
        $i=1;
      }
    }  
  echo  $htmlPage; 
  break;
}
?>
