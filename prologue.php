  <style type="text/css">
  	.post-profile-photo{
  		width: 4.7rem;
  	}
  	.morecontent span {
    display: none;
}
.morelink {
    display: inline;
}

.bxs-heart.like {
    color: red;
}
.post-created-by{
	font-size: .75rem;
	text-transform: uppercase;
}
.post-business-title{
	font-size: .75rem;
}
.post-email-account{
	font-size: .65rem;
	font-style: italic;
}
.post-ago{
	font-size: .65rem;
}
.post-title{
	text-transform: uppercase;
}
.post-comment{
	font-size: .75rem;
	display: block;
	margin-bottom: .25rem;
}
.more-comment .morelink{
  display: inline;
}
.all-comment{
   font-size: .75rem;
   display: block;
   cursor: pointer;
}
.all-comment-hide{
	display: none;
}


.likes {
    font-size: .75rem;
    font-style: italic;
    padding: .25rem 0;
}
  </style>
  <?php 

       function timeago($date) {
	   $timestamp = strtotime($date);	
	   
	   $strTime = array("second", "minute", "hour", "day", "month", "year");
	   $length = array("60","60","24","30","12","10");

	   $currentTime = time();
	   if($currentTime >= $timestamp) {
			$diff     = time()- $timestamp;
			for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
			$diff = $diff / $length[$i];
			}

			$diff = round($diff);
			return $diff . " " . $strTime[$i] . "(s) ago ";
	   }
	}

       $postIndex=CmPost::index($tfs);
       
       //$posts=count($postIndex)-11; 
       //echo $posts."<br>";
       //echo intdiv($posts,12)."<br>";

       $posts = array_column(array_slice($postIndex, 0, 10),"id"); 
       $show=CmPost::dataListPage($tfs,implode(",",$posts));
        
       $html='  <!-- Begin dynamic content -->
				<div class="row">
				   <!-- Begin left content -->
				    <div class="col-8">'; 
       foreach ($show as $s) {
       	$html.='   <!--Begin Blog Post -->
			       <div class="card shadow mb-4">  
			            <div class="card-body">
			                <div>
			                    <div class="card-header post">
			                        <span class="badge badge-pill c3-3 float-right m-2">'.$s["category"].'</span>
			                        <div class="d-flex justify-content-between align-items-center">
			                            <div class="d-flex justify-content-between align-items-center">
			                                <div class="mr-2">
			                                    <img class="rounded-circle post-profile-photo" src="'.$s["photo"].'" alt="">
			                                </div>
			                                <div class="ml-2">
			                                    <div class="post-created-by m-0">'.$s["name"].'</div>
			                                    <div class="post-business-title text-muted m-0">'.$s["business_title"].'</div>
			                                    <div class="post-email-account text-muted m-0">'.$s["email_account"].'</div>
			                                    <div class="post-ago text-muted mt-1">
			                                        <i class="bx bx-clock-o"></i>Hace '.timeago($s["created_date"]).'
			                                    </div>
			                                </div>
			                            </div>
			                       </div>
			                    </div>
			                    <div class="card-body">
			                        <div class="post-title text-muted mb-2">'.$s["title"].'</div>
			                        <div>';
			                        $tags = explode(",",$s["tag"]);
			                        foreach ($tags as $t) {
                                        $html.='<span class="badge badge-pill c3-3">'.$t.'</span>';
			                        }
			             $html.=' </div>  
			                     <span class="card-text more">'.$s["content"].'</span>
			                    </div>
			                    <div class="card-footer post">
			                        <p>';
			                if ($s["likeme"]>0){
                                $html.='<i class="bx bxs-heart like"></i>';
			                }else{
			                	$html.='<i class="bx bx-heart"></i>';   
			                }        
			                 $html.='   <i class="bx bx-comment"></i>
			                        </p> 
			                        <p class="likes">
			                        	<b>'.$s["likes"].' Me gusta</b>
			                        </p>';
                            
                            $postComments = CmPostComment::listByPost($tfs,$s["id"]);
                            $comment = $postComments[0];
                            
                            $html.='<span id="one-comment-'.$s["id"].'" class="post-comment more-comment"><b>'.$comment["created_by"].' </b>'.$comment["comment"].'</span>
                                <div id="all-comment-'.$s["id"].'" style="display:none;">';
                            foreach ($postComments as $pc) {
                              $html.='<span class="post-comment"><b>'.$pc["created_by"].' </b>'.$pc["comment"].'</span>';
                            }    
                            $html.='</div>
                                <span class="all-comment light" data-id-comment="'.$s["id"].'"><span class="verb"> Mostrar </span>'.count($postComments).' comentario(s)</span>    
			                    </div>
			                </div>
			            </div>
			        </div>';
    }
    $html.='</div>
		    <!-- End left content --> 
		    <!-- Begin right content -->
		    <div class="col-4">
		        <!-- Search Widget -->
		        <div class="card">
		            <h5 class="card-header" ><i class="bx bx-search"></i> Buscar</h5>
		            <div class="card-body">
		                <div class="input-group">
		                    <input type="text" class="form-control" placeholder="Buscar...">
		                    <span class="input-group-append">
		                        <button class="btn btn-epic" type="button">Go!</button>
		                    </span>
		                </div>
		            </div>
		        </div>
		    </div>
		    <!-- End right content -->
		</div>  
		<!-- End dynamic content -->';
       //print_r($show);
       //
    echo $html;
  ?>

<script type="text/javascript">
	$(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 215;  // How many characters are shown by default
    var showCharComment = 115;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Mostrar mas";
    var lesstext = "Mostrar menos";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });

    $('.more-comment').each(function() {
        var content = $(this).html();
 
        if(content.length > showCharComment) {
 
            var c = content.substr(0, showCharComment);
            var h = content.substr(showCharComment, content.length - showCharComment);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
    
    $(".all-comment").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $('#one-comment-'+$(this).data('id-comment')).show();
            $('#all-comment-'+$(this).data('id-comment')).hide();
            $(this).children('span').html("Mostrar ");
            //$(this).html(moretext);
        } else {
            $(this).addClass("less");
            $('#one-comment-'+$(this).data('id-comment')).hide();
            $('#all-comment-'+$(this).data('id-comment')).show();
            $(this).children('span').html("Ocultar ");
        }
    });
    
});
</script>