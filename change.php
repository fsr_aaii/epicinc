<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" type="image/png" href="../../asset/images/shortcut.png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Guaramo</title>

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="../../vendor/bootstrap-4.1.3/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../../asset/css/login-element.css">
        <link rel="stylesheet" type="text/css" href="../../asset/css/login-style.css">
        <link rel="stylesheet" type="text/css" href="../../vendor/font-lato/latofonts.css">
        <link rel="stylesheet" type="text/css" href="../../vendor/font-lato/latostyle.css">
        <link rel="stylesheet" type="text/css" href="../../vendor/fontawesome/css/all.min.css">
        
        <script type="text/javascript" src="../../vendor/jquery-3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="../../vendor/jquery-3.3.1/jquery.validate.js"></script>
        <script type="text/javascript" src="../../vendor/jquery-3.3.1/jquery.maskedinput.js"></script>
        <script type="text/javascript" src="../../vendor/jquery-3.3.1/jquery.serializeJSON.min.js"></script>        
        <script type="text/javascript" src="../../vendor/backstretch/js/jquery.backstretch.js"></script>
        <script type="text/javascript" src="../../core/tuich-2.0/js/tuich_message.js"></script>
        <script type="text/javascript" src="../../core/tuich-2.0/js/tuich_redirect.js"></script>
        <script type="text/javascript" src="../../core/tuich-2.0/js/tuich_validate.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

    </head>

     <body>
 
          <div class="top-content">
          
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="logo col-lg-5 mx-auto text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 mx-auto form-box">
                          <div class="form-top">
                            <h3>Talent Management Tool</h3>
                            <p></p>
                            </div>
                            <div class="form-bottom">
                               <form role="form" action="<?php echo $SessionURLBase;?>change/verify" id="login-form" method="post" class="login-form">
                                 <?php echo $alert;?>  
                            
                            <div id="form_password_alert" class="alert alert-white rounded" style="display: none;" >
                              <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                                <div class="icon">
                                  <i id="form_password_alert_icon" class="fa"></i>                                  
                                </div>
                                <span id="form_password_alert_text"></span> 
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form_password">Password</label>
                                <input type="password" id="form_password" name="form_password" placeholder="Password..." class="form-password form-control" >
                                <label class="error" for="form_password"></label>
                              </div>

                              <div class="form-group">
                                <label class="sr-only" for="form_password_r">Confirm Contrase&ntilde;a</label>
                                <input type="password" id="form_password_r" name="form_password_r" placeholder="Confirm Password..." class="form_password form-control" onkeyup="encrypted_password.value=MD5(form_password_r.value);">
                                <label class="error" for="form_password_r"></label>
                              </div>
                               <div class="form-group">
                                 <div class="g-recaptcha" data-sitekey="6LdyV5MUAAAAAPWmisQORo7VgAG0KD4vQaCUifDN"></div>
                                 <label class="error" for=""></label>
                              </div>
                              <input type="hidden" id="form-username" name="form-username" value=<?php echo $tus->getSessionUserLogin();?>> 
                              <input type="hidden" id="encrypted_password" name="encrypted_password">
                              <button id="form-submit" name="form-submit" type="submit" class="btn-login">Change</button>

                              </form>                          
                          </div>
                          <div class="form-footer">
                            <div class="form-footer-left">
                            </div>  
                            <div class="form-footer-right">
                            </div>
                          </div>
                        </div> 
                    </div>
  
                </div>
            </div>
            
        </div>


     
    </body>

    <script type="text/javascript" src="../../vendor/bootstrap-4.1.3/js/bootstrap.js"></script>
    <script type="text/javascript" src="../../asset/js/custom.js"></script> 
    <script type="text/javascript" src="../../asset/js/md5.js"></script>  
    <script type="text/javascript" src="../../asset/js/change.js"></script>  
    <script type="text/javascript">
      $.backstretch("../../asset/images/login.jpg");
    </script>

</html>
