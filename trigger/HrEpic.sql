DELIMITER ;;
CREATE OR REPLACE TRIGGER HrEpic_aui_trg BEFORE INSERT ON HrEpic FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_employee";"id_position";"description";"estimated_start_date";"start_date";"completion_estimated_date";"completion_date";"id_epic_priority";"id_epic_status";"status_date";"progress";"active";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_employee,'";"',NEW.id_position,'";"',NEW.description,'";"',NEW.estimated_start_date,'";"',NEW.start_date,'";"',NEW.completion_estimated_date,'";"',NEW.completion_date,'";"',NEW.id_epic_priority,'";"',NEW.id_epic_status,'";"',NEW.status_date,'";"',NEW.progress,'";"',NEW.active,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','HrEpic',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER HrEpic_aud_trg BEFORE DELETE ON HrEpic FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_employee";"id_position";"description";"estimated_start_date";"start_date";"completion_estimated_date";"completion_date";"id_epic_priority";"id_epic_status";"status_date";"progress";"active";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_employee,'";"',OLD.id_position,'";"',OLD.description,'";"',OLD.estimated_start_date,'";"',OLD.start_date,'";"',OLD.completion_estimated_date,'";"',OLD.completion_date,'";"',OLD.id_epic_priority,'";"',OLD.id_epic_status,'";"',OLD.status_date,'";"',OLD.progress,'";"',OLD.active,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','HrEpic',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER HrEpic_auu_trg BEFORE UPDATE ON HrEpic FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_employee!=OLD.id_employee THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_employee"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_employee,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_employee,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_position!=OLD.id_position THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_position"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_position,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_position,'"');
     SET v_separator=';';
   END IF;
   IF NEW.description!=OLD.description THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"description"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.description,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.description,'"');
     SET v_separator=';';
   END IF;
   IF NEW.estimated_start_date!=OLD.estimated_start_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"estimated_start_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.estimated_start_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.estimated_start_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.start_date!=OLD.start_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"start_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.start_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.start_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.completion_estimated_date!=OLD.completion_estimated_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"completion_estimated_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.completion_estimated_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.completion_estimated_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.completion_date!=OLD.completion_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"completion_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.completion_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.completion_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_epic_priority!=OLD.id_epic_priority THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_epic_priority"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_epic_priority,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_epic_priority,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_epic_status!=OLD.id_epic_status THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_epic_status"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_epic_status,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_epic_status,'"');
     SET v_separator=';';
   END IF;
   IF NEW.status_date!=OLD.status_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"status_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.status_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.status_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.progress!=OLD.progress THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"progress"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.progress,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.progress,'"');
     SET v_separator=';';
   END IF;
   IF NEW.active!=OLD.active THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"active"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.active,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.active,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','HrEpic',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

