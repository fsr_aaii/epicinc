DELIMITER ;;
CREATE OR REPLACE TRIGGER fi_transaction_element_um_aui_trg BEFORE INSERT ON fi_transaction_element_um FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_transaction_element";"id_packeting";"id_unit_measurement";"qty";"active";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_transaction_element,'";"',NEW.id_packeting,'";"',NEW.id_unit_measurement,'";"',NEW.qty,'";"',NEW.active,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','fi_transaction_element_um',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER fi_transaction_element_um_aud_trg BEFORE DELETE ON fi_transaction_element_um FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_transaction_element";"id_packeting";"id_unit_measurement";"qty";"active";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_transaction_element,'";"',OLD.id_packeting,'";"',OLD.id_unit_measurement,'";"',OLD.qty,'";"',OLD.active,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','fi_transaction_element_um',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER fi_transaction_element_um_auu_trg BEFORE UPDATE ON fi_transaction_element_um FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_transaction_element!=OLD.id_transaction_element THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_transaction_element"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_transaction_element,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_transaction_element,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_packeting!=OLD.id_packeting THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_packeting"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_packeting,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_packeting,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_unit_measurement!=OLD.id_unit_measurement THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_unit_measurement"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_unit_measurement,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_unit_measurement,'"');
     SET v_separator=';';
   END IF;
   IF NEW.qty!=OLD.qty THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"qty"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.qty,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.qty,'"');
     SET v_separator=';';
   END IF;
   IF NEW.active!=OLD.active THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"active"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.active,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.active,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','fi_transaction_element_um',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

