DELIMITER ;;
CREATE OR REPLACE TRIGGER GlNaturalPerson_aui_trg BEFORE INSERT ON GlNaturalPerson FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"nin_type";"nin";"nin_control_digit";"first_name";"middle_name";"last_name";"second_last_name";"photo";"id_nationality_country";"id_gender";"id_birth_country";"birthplace";"birthdate";"id_marital_status";"height";"size";"weight";"is_right_handed";"id_blood_type";"is_functional_diversity";"id_study_level";"id_residence_country";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.nin_type,'";"',NEW.nin,'";"',NEW.nin_control_digit,'";"',NEW.first_name,'";"',NEW.middle_name,'";"',NEW.last_name,'";"',NEW.second_last_name,'";"',NEW.photo,'";"',NEW.id_nationality_country,'";"',NEW.id_gender,'";"',NEW.id_birth_country,'";"',NEW.birthplace,'";"',NEW.birthdate,'";"',NEW.id_marital_status,'";"',NEW.height,'";"',NEW.size,'";"',NEW.weight,'";"',NEW.is_right_handed,'";"',NEW.id_blood_type,'";"',NEW.is_functional_diversity,'";"',NEW.id_study_level,'";"',NEW.id_residence_country,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','GlNaturalPerson',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER GlNaturalPerson_aud_trg BEFORE DELETE ON GlNaturalPerson FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"nin_type";"nin";"nin_control_digit";"first_name";"middle_name";"last_name";"second_last_name";"photo";"id_nationality_country";"id_gender";"id_birth_country";"birthplace";"birthdate";"id_marital_status";"height";"size";"weight";"is_right_handed";"id_blood_type";"is_functional_diversity";"id_study_level";"id_residence_country";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.nin_type,'";"',OLD.nin,'";"',OLD.nin_control_digit,'";"',OLD.first_name,'";"',OLD.middle_name,'";"',OLD.last_name,'";"',OLD.second_last_name,'";"',OLD.photo,'";"',OLD.id_nationality_country,'";"',OLD.id_gender,'";"',OLD.id_birth_country,'";"',OLD.birthplace,'";"',OLD.birthdate,'";"',OLD.id_marital_status,'";"',OLD.height,'";"',OLD.size,'";"',OLD.weight,'";"',OLD.is_right_handed,'";"',OLD.id_blood_type,'";"',OLD.is_functional_diversity,'";"',OLD.id_study_level,'";"',OLD.id_residence_country,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','GlNaturalPerson',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER GlNaturalPerson_auu_trg BEFORE UPDATE ON GlNaturalPerson FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.nin_type!=OLD.nin_type THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"nin_type"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.nin_type,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.nin_type,'"');
     SET v_separator=';';
   END IF;
   IF NEW.nin!=OLD.nin THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"nin"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.nin,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.nin,'"');
     SET v_separator=';';
   END IF;
   IF NEW.nin_control_digit!=OLD.nin_control_digit THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"nin_control_digit"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.nin_control_digit,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.nin_control_digit,'"');
     SET v_separator=';';
   END IF;
   IF NEW.first_name!=OLD.first_name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"first_name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.first_name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.first_name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.middle_name!=OLD.middle_name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"middle_name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.middle_name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.middle_name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.last_name!=OLD.last_name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"last_name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.last_name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.last_name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.second_last_name!=OLD.second_last_name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"second_last_name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.second_last_name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.second_last_name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.photo!=OLD.photo THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"photo"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.photo,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.photo,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_nationality_country!=OLD.id_nationality_country THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_nationality_country"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_nationality_country,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_nationality_country,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_gender!=OLD.id_gender THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_gender"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_gender,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_gender,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_birth_country!=OLD.id_birth_country THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_birth_country"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_birth_country,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_birth_country,'"');
     SET v_separator=';';
   END IF;
   IF NEW.birthplace!=OLD.birthplace THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"birthplace"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.birthplace,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.birthplace,'"');
     SET v_separator=';';
   END IF;
   IF NEW.birthdate!=OLD.birthdate THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"birthdate"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.birthdate,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.birthdate,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_marital_status!=OLD.id_marital_status THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_marital_status"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_marital_status,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_marital_status,'"');
     SET v_separator=';';
   END IF;
   IF NEW.height!=OLD.height THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"height"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.height,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.height,'"');
     SET v_separator=';';
   END IF;
   IF NEW.size!=OLD.size THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"size"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.size,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.size,'"');
     SET v_separator=';';
   END IF;
   IF NEW.weight!=OLD.weight THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"weight"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.weight,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.weight,'"');
     SET v_separator=';';
   END IF;
   IF NEW.is_right_handed!=OLD.is_right_handed THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"is_right_handed"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.is_right_handed,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.is_right_handed,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_blood_type!=OLD.id_blood_type THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_blood_type"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_blood_type,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_blood_type,'"');
     SET v_separator=';';
   END IF;
   IF NEW.is_functional_diversity!=OLD.is_functional_diversity THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"is_functional_diversity"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.is_functional_diversity,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.is_functional_diversity,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_study_level!=OLD.id_study_level THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_study_level"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_study_level,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_study_level,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_residence_country!=OLD.id_residence_country THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_residence_country"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_residence_country,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_residence_country,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','GlNaturalPerson',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

