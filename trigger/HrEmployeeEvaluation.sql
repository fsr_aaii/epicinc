DELIMITER ;;
CREATE OR REPLACE TRIGGER HrEmployeeEvaluation_aui_trg BEFORE INSERT ON HrEmployeeEvaluation FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_employee";"id_position";"date";"id_evaluation_type";"evaluator_type";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_employee,'";"',NEW.id_position,'";"',NEW.date,'";"',NEW.id_evaluation_type,'";"',NEW.evaluator_type,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','HrEmployeeEvaluation',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER HrEmployeeEvaluation_aud_trg BEFORE DELETE ON HrEmployeeEvaluation FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_employee";"id_position";"date";"id_evaluation_type";"evaluator_type";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_employee,'";"',OLD.id_position,'";"',OLD.date,'";"',OLD.id_evaluation_type,'";"',OLD.evaluator_type,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','HrEmployeeEvaluation',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER HrEmployeeEvaluation_auu_trg BEFORE UPDATE ON HrEmployeeEvaluation FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_employee!=OLD.id_employee THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_employee"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_employee,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_employee,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_position!=OLD.id_position THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_position"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_position,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_position,'"');
     SET v_separator=';';
   END IF;
   IF NEW.date!=OLD.date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_evaluation_type!=OLD.id_evaluation_type THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_evaluation_type"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_evaluation_type,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_evaluation_type,'"');
     SET v_separator=';';
   END IF;
   IF NEW.evaluator_type!=OLD.evaluator_type THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"evaluator_type"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.evaluator_type,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.evaluator_type,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','HrEmployeeEvaluation',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

