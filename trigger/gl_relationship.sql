DELIMITER ;;
CREATE OR REPLACE TRIGGER gl_relationship_aui_trg BEFORE INSERT ON gl_relationship FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"name";"active";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.name,'";"',NEW.active,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','gl_relationship',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER gl_relationship_aud_trg BEFORE DELETE ON gl_relationship FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"name";"active";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.name,'";"',OLD.active,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','gl_relationship',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER gl_relationship_auu_trg BEFORE UPDATE ON gl_relationship FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.name!=OLD.name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.active!=OLD.active THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"active"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.active,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.active,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','gl_relationship',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

