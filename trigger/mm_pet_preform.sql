DELIMITER ;;
CREATE OR REPLACE TRIGGER mm_pet_preform_aui_trg BEFORE INSERT ON mm_pet_preform FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_transaction_element";"id_color";"id_polymer_closure_type";"weight";"active";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_transaction_element,'";"',NEW.id_color,'";"',NEW.id_polymer_closure_type,'";"',NEW.weight,'";"',NEW.active,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','mm_pet_preform',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER mm_pet_preform_aud_trg BEFORE DELETE ON mm_pet_preform FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_transaction_element";"id_color";"id_polymer_closure_type";"weight";"active";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_transaction_element,'";"',OLD.id_color,'";"',OLD.id_polymer_closure_type,'";"',OLD.weight,'";"',OLD.active,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','mm_pet_preform',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER mm_pet_preform_auu_trg BEFORE UPDATE ON mm_pet_preform FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_transaction_element!=OLD.id_transaction_element THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_transaction_element"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_transaction_element,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_transaction_element,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_color!=OLD.id_color THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_color"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_color,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_color,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_polymer_closure_type!=OLD.id_polymer_closure_type THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_polymer_closure_type"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_polymer_closure_type,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_polymer_closure_type,'"');
     SET v_separator=';';
   END IF;
   IF NEW.weight!=OLD.weight THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"weight"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.weight,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.weight,'"');
     SET v_separator=';';
   END IF;
   IF NEW.active!=OLD.active THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"active"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.active,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.active,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','mm_pet_preform',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

