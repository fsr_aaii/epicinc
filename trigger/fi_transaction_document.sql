DELIMITER ;;
CREATE OR REPLACE TRIGGER fi_transaction_document_aui_trg BEFORE INSERT ON fi_transaction_document FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_transaction_document_type";"id_person";"number";"id_functional_area_from";"id_functional_area_to";"issue_date";"due_date";"description";"active";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_transaction_document_type,'";"',NEW.id_person,'";"',NEW.number,'";"',NEW.id_functional_area_from,'";"',NEW.id_functional_area_to,'";"',NEW.issue_date,'";"',NEW.due_date,'";"',NEW.description,'";"',NEW.active,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','fi_transaction_document',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER fi_transaction_document_aud_trg BEFORE DELETE ON fi_transaction_document FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_transaction_document_type";"id_person";"number";"id_functional_area_from";"id_functional_area_to";"issue_date";"due_date";"description";"active";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_transaction_document_type,'";"',OLD.id_person,'";"',OLD.number,'";"',OLD.id_functional_area_from,'";"',OLD.id_functional_area_to,'";"',OLD.issue_date,'";"',OLD.due_date,'";"',OLD.description,'";"',OLD.active,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','fi_transaction_document',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER fi_transaction_document_auu_trg BEFORE UPDATE ON fi_transaction_document FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_transaction_document_type!=OLD.id_transaction_document_type THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_transaction_document_type"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_transaction_document_type,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_transaction_document_type,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_person!=OLD.id_person THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_person"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_person,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_person,'"');
     SET v_separator=';';
   END IF;
   IF NEW.number!=OLD.number THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"number"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.number,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.number,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_functional_area_from!=OLD.id_functional_area_from THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_functional_area_from"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_functional_area_from,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_functional_area_from,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_functional_area_to!=OLD.id_functional_area_to THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_functional_area_to"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_functional_area_to,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_functional_area_to,'"');
     SET v_separator=';';
   END IF;
   IF NEW.issue_date!=OLD.issue_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"issue_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.issue_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.issue_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.due_date!=OLD.due_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"due_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.due_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.due_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.description!=OLD.description THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"description"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.description,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.description,'"');
     SET v_separator=';';
   END IF;
   IF NEW.active!=OLD.active THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"active"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.active,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.active,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','fi_transaction_document',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

