DELIMITER ;;
CREATE OR REPLACE TRIGGER t_menu_task_aui_trg BEFORE INSERT ON t_menu_task FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_menu";"id_task";"sorting";"active";"tag";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_menu,'";"',NEW.id_task,'";"',NEW.sorting,'";"',NEW.active,'";"',NEW.tag,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','t_menu_task',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER t_menu_task_aud_trg BEFORE DELETE ON t_menu_task FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_menu";"id_task";"sorting";"active";"tag";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_menu,'";"',OLD.id_task,'";"',OLD.sorting,'";"',OLD.active,'";"',OLD.tag,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','t_menu_task',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER t_menu_task_auu_trg BEFORE UPDATE ON t_menu_task FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_menu!=OLD.id_menu THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_menu"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_menu,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_menu,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_task!=OLD.id_task THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_task"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_task,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_task,'"');
     SET v_separator=';';
   END IF;
   IF NEW.sorting!=OLD.sorting THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"sorting"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.sorting,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.sorting,'"');
     SET v_separator=';';
   END IF;
   IF NEW.active!=OLD.active THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"active"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.active,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.active,'"');
     SET v_separator=';';
   END IF;
   IF NEW.tag!=OLD.tag THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"tag"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.tag,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.tag,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','t_menu_task',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

