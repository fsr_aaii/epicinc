DELIMITER ;;
CREATE OR REPLACE TRIGGER sd_pricing_aui_trg BEFORE INSERT ON sd_pricing FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_transaction_element";"price";"calculation_factor";"date_from";"date_to";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_transaction_element,'";"',NEW.price,'";"',NEW.calculation_factor,'";"',NEW.date_from,'";"',NEW.date_to,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','sd_pricing',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER sd_pricing_aud_trg BEFORE DELETE ON sd_pricing FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_transaction_element";"price";"calculation_factor";"date_from";"date_to";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_transaction_element,'";"',OLD.price,'";"',OLD.calculation_factor,'";"',OLD.date_from,'";"',OLD.date_to,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','sd_pricing',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER sd_pricing_auu_trg BEFORE UPDATE ON sd_pricing FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_transaction_element!=OLD.id_transaction_element THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_transaction_element"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_transaction_element,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_transaction_element,'"');
     SET v_separator=';';
   END IF;
   IF NEW.price!=OLD.price THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"price"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.price,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.price,'"');
     SET v_separator=';';
   END IF;
   IF NEW.calculation_factor!=OLD.calculation_factor THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"calculation_factor"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.calculation_factor,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.calculation_factor,'"');
     SET v_separator=';';
   END IF;
   IF NEW.date_from!=OLD.date_from THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"date_from"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.date_from,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.date_from,'"');
     SET v_separator=';';
   END IF;
   IF NEW.date_to!=OLD.date_to THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"date_to"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.date_to,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.date_to,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','sd_pricing',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

