DELIMITER ;;
CREATE OR REPLACE TRIGGER fi_transaction_element_kpi_aui_trg BEFORE INSERT ON fi_transaction_element_kpi FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_transaction_element";"cost_price";"average_cost";"fi_transaction_element_kpicol";"qty_sales";"gross_sales";"net_sales";"last_month_demand";"monthly_demand";"ratio_days";"stock";"available_stock";"reserved_stock";"stock_to_recibe";"required_stock";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_transaction_element,'";"',NEW.cost_price,'";"',NEW.average_cost,'";"',NEW.fi_transaction_element_kpicol,'";"',NEW.qty_sales,'";"',NEW.gross_sales,'";"',NEW.net_sales,'";"',NEW.last_month_demand,'";"',NEW.monthly_demand,'";"',NEW.ratio_days,'";"',NEW.stock,'";"',NEW.available_stock,'";"',NEW.reserved_stock,'";"',NEW.stock_to_recibe,'";"',NEW.required_stock,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','fi_transaction_element_kpi',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER fi_transaction_element_kpi_aud_trg BEFORE DELETE ON fi_transaction_element_kpi FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_transaction_element";"cost_price";"average_cost";"fi_transaction_element_kpicol";"qty_sales";"gross_sales";"net_sales";"last_month_demand";"monthly_demand";"ratio_days";"stock";"available_stock";"reserved_stock";"stock_to_recibe";"required_stock";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_transaction_element,'";"',OLD.cost_price,'";"',OLD.average_cost,'";"',OLD.fi_transaction_element_kpicol,'";"',OLD.qty_sales,'";"',OLD.gross_sales,'";"',OLD.net_sales,'";"',OLD.last_month_demand,'";"',OLD.monthly_demand,'";"',OLD.ratio_days,'";"',OLD.stock,'";"',OLD.available_stock,'";"',OLD.reserved_stock,'";"',OLD.stock_to_recibe,'";"',OLD.required_stock,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','fi_transaction_element_kpi',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER fi_transaction_element_kpi_auu_trg BEFORE UPDATE ON fi_transaction_element_kpi FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_transaction_element!=OLD.id_transaction_element THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_transaction_element"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_transaction_element,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_transaction_element,'"');
     SET v_separator=';';
   END IF;
   IF NEW.cost_price!=OLD.cost_price THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"cost_price"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.cost_price,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.cost_price,'"');
     SET v_separator=';';
   END IF;
   IF NEW.average_cost!=OLD.average_cost THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"average_cost"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.average_cost,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.average_cost,'"');
     SET v_separator=';';
   END IF;
   IF NEW.fi_transaction_element_kpicol!=OLD.fi_transaction_element_kpicol THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"fi_transaction_element_kpicol"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.fi_transaction_element_kpicol,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.fi_transaction_element_kpicol,'"');
     SET v_separator=';';
   END IF;
   IF NEW.qty_sales!=OLD.qty_sales THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"qty_sales"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.qty_sales,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.qty_sales,'"');
     SET v_separator=';';
   END IF;
   IF NEW.gross_sales!=OLD.gross_sales THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"gross_sales"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.gross_sales,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.gross_sales,'"');
     SET v_separator=';';
   END IF;
   IF NEW.net_sales!=OLD.net_sales THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"net_sales"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.net_sales,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.net_sales,'"');
     SET v_separator=';';
   END IF;
   IF NEW.last_month_demand!=OLD.last_month_demand THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"last_month_demand"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.last_month_demand,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.last_month_demand,'"');
     SET v_separator=';';
   END IF;
   IF NEW.monthly_demand!=OLD.monthly_demand THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"monthly_demand"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.monthly_demand,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.monthly_demand,'"');
     SET v_separator=';';
   END IF;
   IF NEW.ratio_days!=OLD.ratio_days THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"ratio_days"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.ratio_days,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.ratio_days,'"');
     SET v_separator=';';
   END IF;
   IF NEW.stock!=OLD.stock THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"stock"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.stock,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.stock,'"');
     SET v_separator=';';
   END IF;
   IF NEW.available_stock!=OLD.available_stock THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"available_stock"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.available_stock,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.available_stock,'"');
     SET v_separator=';';
   END IF;
   IF NEW.reserved_stock!=OLD.reserved_stock THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"reserved_stock"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.reserved_stock,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.reserved_stock,'"');
     SET v_separator=';';
   END IF;
   IF NEW.stock_to_recibe!=OLD.stock_to_recibe THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"stock_to_recibe"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.stock_to_recibe,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.stock_to_recibe,'"');
     SET v_separator=';';
   END IF;
   IF NEW.required_stock!=OLD.required_stock THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"required_stock"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.required_stock,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.required_stock,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','fi_transaction_element_kpi',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

