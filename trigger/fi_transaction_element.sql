DELIMITER ;;
CREATE OR REPLACE TRIGGER fi_transaction_element_aui_trg BEFORE INSERT ON fi_transaction_element FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"name";"code";"id_transaction_element_rank";"id_transaction_type";"id_element_type";"active";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.name,'";"',NEW.code,'";"',NEW.id_transaction_element_rank,'";"',NEW.id_transaction_type,'";"',NEW.id_element_type,'";"',NEW.active,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','fi_transaction_element',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER fi_transaction_element_aud_trg BEFORE DELETE ON fi_transaction_element FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"name";"code";"id_transaction_element_rank";"id_transaction_type";"id_element_type";"active";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.name,'";"',OLD.code,'";"',OLD.id_transaction_element_rank,'";"',OLD.id_transaction_type,'";"',OLD.id_element_type,'";"',OLD.active,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','fi_transaction_element',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER fi_transaction_element_auu_trg BEFORE UPDATE ON fi_transaction_element FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.name!=OLD.name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.code!=OLD.code THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"code"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.code,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.code,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_transaction_element_rank!=OLD.id_transaction_element_rank THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_transaction_element_rank"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_transaction_element_rank,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_transaction_element_rank,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_transaction_type!=OLD.id_transaction_type THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_transaction_type"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_transaction_type,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_transaction_type,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_element_type!=OLD.id_element_type THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_element_type"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_element_type,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_element_type,'"');
     SET v_separator=';';
   END IF;
   IF NEW.active!=OLD.active THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"active"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.active,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.active,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','fi_transaction_element',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

