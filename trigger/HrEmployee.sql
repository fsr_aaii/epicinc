DELIMITER ;;
CREATE OR REPLACE TRIGGER Hremployee_aui_trg BEFORE INSERT ON Hremployee FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_person";"email_account";"hire_date";"id_development_level";"id_job_level";"id_job_level_next";"id_competence_a";"id_competence_b";"id_soft_skill_a";"id_soft_skill_b";"is_key_talent";"has_successor";"has_talent";"resignation_risk";"performace";"performace_date";"learning_agility";"learning_agility_date";"cr";"successor_name";"mobility";"area_interest";"termination_date";"id_termination_reason";"created_date";"created_by"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_person,'";"',NEW.email_account,'";"',NEW.hire_date,'";"',NEW.id_development_level,'";"',NEW.id_job_level,'";"',NEW.id_job_level_next,'";"',NEW.id_competence_a,'";"',NEW.id_competence_b,'";"',NEW.id_soft_skill_a,'";"',NEW.id_soft_skill_b,'";"',NEW.is_key_talent,'";"',NEW.has_successor,'";"',NEW.has_talent,'";"',NEW.resignation_risk,'";"',NEW.performace,'";"',NEW.performace_date,'";"',NEW.learning_agility,'";"',NEW.learning_agility_date,'";"',NEW.cr,'";"',NEW.successor_name,'";"',NEW.mobility,'";"',NEW.area_interest,'";"',NEW.termination_date,'";"',NEW.id_termination_reason,'";"',NEW.created_date,'";"',NEW.created_by,'"');
   CALL sp_audit_d('INSERT','Hremployee',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER Hremployee_aud_trg BEFORE DELETE ON Hremployee FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_person";"email_account";"hire_date";"id_development_level";"id_job_level";"id_job_level_next";"id_competence_a";"id_competence_b";"id_soft_skill_a";"id_soft_skill_b";"is_key_talent";"has_successor";"has_talent";"resignation_risk";"performace";"performace_date";"learning_agility";"learning_agility_date";"cr";"successor_name";"mobility";"area_interest";"termination_date";"id_termination_reason";"created_date";"created_by"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_person,'";"',OLD.email_account,'";"',OLD.hire_date,'";"',OLD.id_development_level,'";"',OLD.id_job_level,'";"',OLD.id_job_level_next,'";"',OLD.id_competence_a,'";"',OLD.id_competence_b,'";"',OLD.id_soft_skill_a,'";"',OLD.id_soft_skill_b,'";"',OLD.is_key_talent,'";"',OLD.has_successor,'";"',OLD.has_talent,'";"',OLD.resignation_risk,'";"',OLD.performace,'";"',OLD.performace_date,'";"',OLD.learning_agility,'";"',OLD.learning_agility_date,'";"',OLD.cr,'";"',OLD.successor_name,'";"',OLD.mobility,'";"',OLD.area_interest,'";"',OLD.termination_date,'";"',OLD.id_termination_reason,'";"',OLD.created_date,'";"',OLD.created_by,'"');
   CALL sp_audit_d('DELETE','Hremployee',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER Hremployee_auu_trg BEFORE UPDATE ON Hremployee FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_person!=OLD.id_person THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_person"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_person,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_person,'"');
     SET v_separator=';';
   END IF;
   IF NEW.email_account!=OLD.email_account THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"email_account"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.email_account,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.email_account,'"');
     SET v_separator=';';
   END IF;
   IF NEW.hire_date!=OLD.hire_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"hire_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.hire_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.hire_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_development_level!=OLD.id_development_level THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_development_level"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_development_level,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_development_level,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_job_level!=OLD.id_job_level THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_job_level"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_job_level,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_job_level,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_job_level_next!=OLD.id_job_level_next THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_job_level_next"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_job_level_next,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_job_level_next,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_competence_a!=OLD.id_competence_a THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_competence_a"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_competence_a,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_competence_a,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_competence_b!=OLD.id_competence_b THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_competence_b"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_competence_b,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_competence_b,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_soft_skill_a!=OLD.id_soft_skill_a THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_soft_skill_a"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_soft_skill_a,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_soft_skill_a,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_soft_skill_b!=OLD.id_soft_skill_b THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_soft_skill_b"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_soft_skill_b,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_soft_skill_b,'"');
     SET v_separator=';';
   END IF;
   IF NEW.is_key_talent!=OLD.is_key_talent THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"is_key_talent"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.is_key_talent,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.is_key_talent,'"');
     SET v_separator=';';
   END IF;
   IF NEW.has_successor!=OLD.has_successor THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"has_successor"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.has_successor,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.has_successor,'"');
     SET v_separator=';';
   END IF;
   IF NEW.has_talent!=OLD.has_talent THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"has_talent"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.has_talent,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.has_talent,'"');
     SET v_separator=';';
   END IF;
   IF NEW.resignation_risk!=OLD.resignation_risk THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"resignation_risk"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.resignation_risk,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.resignation_risk,'"');
     SET v_separator=';';
   END IF;
   IF NEW.performace!=OLD.performace THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"performace"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.performace,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.performace,'"');
     SET v_separator=';';
   END IF;
   IF NEW.performace_date!=OLD.performace_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"performace_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.performace_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.performace_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.learning_agility!=OLD.learning_agility THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"learning_agility"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.learning_agility,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.learning_agility,'"');
     SET v_separator=';';
   END IF;
   IF NEW.learning_agility_date!=OLD.learning_agility_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"learning_agility_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.learning_agility_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.learning_agility_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.cr!=OLD.cr THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"cr"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.cr,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.cr,'"');
     SET v_separator=';';
   END IF;
   IF NEW.successor_name!=OLD.successor_name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"successor_name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.successor_name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.successor_name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.mobility!=OLD.mobility THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"mobility"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.mobility,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.mobility,'"');
     SET v_separator=';';
   END IF;
   IF NEW.area_interest!=OLD.area_interest THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"area_interest"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.area_interest,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.area_interest,'"');
     SET v_separator=';';
   END IF;
   IF NEW.termination_date!=OLD.termination_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"termination_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.termination_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.termination_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_termination_reason!=OLD.id_termination_reason THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_termination_reason"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_termination_reason,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_termination_reason,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','Hremployee',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

