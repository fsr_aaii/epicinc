DELIMITER ;;
CREATE OR REPLACE TRIGGER HrPositionEvaluationCriteria_aui_trg BEFORE INSERT ON HrPositionEvaluationCriteria FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_position_evaluation";"id_evaluation_criteria";"value";"date";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_position_evaluation,'";"',NEW.id_evaluation_criteria,'";"',NEW.value,'";"',NEW.date,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','HrPositionEvaluationCriteria',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER HrPositionEvaluationCriteria_aud_trg BEFORE DELETE ON HrPositionEvaluationCriteria FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_position_evaluation";"id_evaluation_criteria";"value";"date";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_position_evaluation,'";"',OLD.id_evaluation_criteria,'";"',OLD.value,'";"',OLD.date,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','HrPositionEvaluationCriteria',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER HrPositionEvaluationCriteria_auu_trg BEFORE UPDATE ON HrPositionEvaluationCriteria FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_position_evaluation!=OLD.id_position_evaluation THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_position_evaluation"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_position_evaluation,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_position_evaluation,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_evaluation_criteria!=OLD.id_evaluation_criteria THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_evaluation_criteria"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_evaluation_criteria,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_evaluation_criteria,'"');
     SET v_separator=';';
   END IF;
   IF NEW.value!=OLD.value THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"value"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.value,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.value,'"');
     SET v_separator=';';
   END IF;
   IF NEW.date!=OLD.date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','HrPositionEvaluationCriteria',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

