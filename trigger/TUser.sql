DELIMITER ;;
CREATE OR REPLACE TRIGGER TUser_aui_trg BEFORE INSERT ON TUser FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"login";"photo";"active";"password";"password_date";"email_account";"name";"created_by";"created_date"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.login,'";"',NEW.photo,'";"',NEW.active,'";"',NEW.password,'";"',NEW.password_date,'";"',NEW.email_account,'";"',NEW.name,'";"',NEW.created_by,'";"',NEW.created_date,'"');
   CALL sp_audit_d('INSERT','TUser',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER TUser_aud_trg BEFORE DELETE ON TUser FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"login";"photo";"active";"password";"password_date";"email_account";"name";"created_by";"created_date"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.login,'";"',OLD.photo,'";"',OLD.active,'";"',OLD.password,'";"',OLD.password_date,'";"',OLD.email_account,'";"',OLD.name,'";"',OLD.created_by,'";"',OLD.created_date,'"');
   CALL sp_audit_d('DELETE','TUser',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER TUser_auu_trg BEFORE UPDATE ON TUser FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.login!=OLD.login THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"login"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.login,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.login,'"');
     SET v_separator=';';
   END IF;
   IF NEW.photo!=OLD.photo THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"photo"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.photo,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.photo,'"');
     SET v_separator=';';
   END IF;
   IF NEW.active!=OLD.active THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"active"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.active,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.active,'"');
     SET v_separator=';';
   END IF;
   IF NEW.password!=OLD.password THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"password"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.password,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.password,'"');
     SET v_separator=';';
   END IF;
   IF NEW.password_date!=OLD.password_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"password_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.password_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.password_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.email_account!=OLD.email_account THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"email_account"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.email_account,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.email_account,'"');
     SET v_separator=';';
   END IF;
   IF NEW.name!=OLD.name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_by!=OLD.created_by THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_by"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_by,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_by,'"');
     SET v_separator=';';
   END IF;
   IF NEW.created_date!=OLD.created_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"created_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.created_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.created_date,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','TUser',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

