DELIMITER ;;
CREATE OR REPLACE TRIGGER gl_area_code_aui_trg BEFORE INSERT ON gl_area_code FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_country";"code";"description"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_country,'";"',NEW.code,'";"',NEW.description,'"');
   CALL sp_audit_d('INSERT','gl_area_code',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER gl_area_code_aud_trg BEFORE DELETE ON gl_area_code FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_country";"code";"description"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_country,'";"',OLD.code,'";"',OLD.description,'"');
   CALL sp_audit_d('DELETE','gl_area_code',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER gl_area_code_auu_trg BEFORE UPDATE ON gl_area_code FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_country!=OLD.id_country THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_country"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_country,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_country,'"');
     SET v_separator=';';
   END IF;
   IF NEW.code!=OLD.code THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"code"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.code,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.code,'"');
     SET v_separator=';';
   END IF;
   IF NEW.description!=OLD.description THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"description"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.description,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.description,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','gl_area_code',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

