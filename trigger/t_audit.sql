DELIMITER ;;
CREATE OR REPLACE TRIGGER t_audit_aui_trg BEFORE INSERT ON t_audit FOR EACH ROW
  BEGIN

   DECLARE p_new_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_session";"ip_client";"id_user";"id_task";"user_name";"url";"audit_date";"audit_datetime";"operation";"table_name";"primary_key";"affected_columns";"old_values";"new_values"';
   SET p_new_value = CONCAT('"',NEW.id,'";"',NEW.id_session,'";"',NEW.ip_client,'";"',NEW.id_user,'";"',NEW.id_task,'";"',NEW.user_name,'";"',NEW.url,'";"',NEW.audit_date,'";"',NEW.audit_datetime,'";"',NEW.operation,'";"',NEW.table_name,'";"',NEW.primary_key,'";"',NEW.affected_columns,'";"',NEW.old_values,'";"',NEW.new_values,'"');
   CALL sp_audit_d('INSERT','t_audit',NEW.id,p_affected_columns,NULL,p_new_value);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER t_audit_aud_trg BEFORE DELETE ON t_audit FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT; 
   DECLARE p_affected_columns TEXT;
   SET p_affected_columns = '"id";"id_session";"ip_client";"id_user";"id_task";"user_name";"url";"audit_date";"audit_datetime";"operation";"table_name";"primary_key";"affected_columns";"old_values";"new_values"';
   SET p_old_value = CONCAT('"',OLD.id,'";"',OLD.id_session,'";"',OLD.ip_client,'";"',OLD.id_user,'";"',OLD.id_task,'";"',OLD.user_name,'";"',OLD.url,'";"',OLD.audit_date,'";"',OLD.audit_datetime,'";"',OLD.operation,'";"',OLD.table_name,'";"',OLD.primary_key,'";"',OLD.affected_columns,'";"',OLD.old_values,'";"',OLD.new_values,'"');
   CALL sp_audit_d('DELETE','t_audit',OLD.id,p_affected_columns,p_old_value,NULL);
  END;;
DELIMITER ;

DELIMITER ;;
CREATE OR REPLACE TRIGGER t_audit_auu_trg BEFORE UPDATE ON t_audit FOR EACH ROW
  BEGIN

   DECLARE p_old_value TEXT;
   DECLARE p_new_value TEXT;
   DECLARE p_affected_columns TEXT;
   DECLARE v_separator VARCHAR(1);
   SET v_separator = '';
   SET p_old_value = '';
   SET p_new_value = '';
   SET p_affected_columns = '';
   IF NEW.id!=OLD.id THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_session!=OLD.id_session THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_session"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_session,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_session,'"');
     SET v_separator=';';
   END IF;
   IF NEW.ip_client!=OLD.ip_client THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"ip_client"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.ip_client,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.ip_client,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_user!=OLD.id_user THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_user"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_user,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_user,'"');
     SET v_separator=';';
   END IF;
   IF NEW.id_task!=OLD.id_task THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"id_task"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.id_task,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.id_task,'"');
     SET v_separator=';';
   END IF;
   IF NEW.user_name!=OLD.user_name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"user_name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.user_name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.user_name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.url!=OLD.url THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"url"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.url,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.url,'"');
     SET v_separator=';';
   END IF;
   IF NEW.audit_date!=OLD.audit_date THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"audit_date"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.audit_date,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.audit_date,'"');
     SET v_separator=';';
   END IF;
   IF NEW.audit_datetime!=OLD.audit_datetime THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"audit_datetime"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.audit_datetime,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.audit_datetime,'"');
     SET v_separator=';';
   END IF;
   IF NEW.operation!=OLD.operation THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"operation"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.operation,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.operation,'"');
     SET v_separator=';';
   END IF;
   IF NEW.table_name!=OLD.table_name THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"table_name"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.table_name,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.table_name,'"');
     SET v_separator=';';
   END IF;
   IF NEW.primary_key!=OLD.primary_key THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"primary_key"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.primary_key,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.primary_key,'"');
     SET v_separator=';';
   END IF;
   IF NEW.affected_columns!=OLD.affected_columns THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"affected_columns"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.affected_columns,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.affected_columns,'"');
     SET v_separator=';';
   END IF;
   IF NEW.old_values!=OLD.old_values THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"old_values"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.old_values,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.old_values,'"');
     SET v_separator=';';
   END IF;
   IF NEW.new_values!=OLD.new_values THEN
     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,'"new_values"');
     SET p_new_value = CONCAT(p_new_value,v_separator,'"', NEW.new_values,'"');
     SET p_old_value = CONCAT(p_old_value,v_separator,'"', OLD.new_values,'"');
     SET v_separator=';';
   END IF;

   CALL sp_audit_d('UPDATE','t_audit',NEW.id,p_affected_columns,p_old_value,p_new_value);
  END;;
DELIMITER ;

