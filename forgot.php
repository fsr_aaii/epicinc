
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" type="image/png" href="../../asset/images/shortcut.png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Guaramo</title>

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="../../vendor/bootstrap-4.1.3/css/bootstrap.css">
         <link rel="stylesheet" type="text/css" href="../../asset/css/login-element.css">
        <link rel="stylesheet" type="text/css" href="../../asset/css/login-style.css">
        <link rel="stylesheet" type="text/css" href="../../vendor/font-lato/latofonts.css">
        <link rel="stylesheet" type="text/css" href="../../vendor/font-lato/latostyle.css">
        <link rel="stylesheet" type="text/css" href="../../vendor/fontawesome/css/all.min.css">
        
        <script type="text/javascript" src="../../vendor/jquery-3.3.1/jquery.min.js"></script>
        <script type="text/javascript" src="../../vendor/jquery-3.3.1/jquery.validate.js"></script>
        <script type="text/javascript" src="../../vendor/jquery-3.3.1/jquery.maskedinput.js"></script>
        <script type="text/javascript" src="../../vendor/jquery-3.3.1/jquery.serializeJSON.min.js"></script>
        <script type="text/javascript" src="../../vendor/backstretch/js/jquery.backstretch.js"></script>
        <script type="text/javascript" src="../../core/tuich-2.0/js/tuich_message.js"></script>
        <script type="text/javascript" src="../../core/tuich-2.0/js/tuich_redirect.js"></script>
        <script type="text/javascript" src="../../core/tuich-2.0/js/tuich_validate.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>



    </head>

     <body>

        
         <div class="top-content">
          
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="logo col-lg-5 mx-auto text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 mx-auto form-box">
                          <div class="form-top">
                            <h3>GTK</h3>
                              <p></p>
                            </div>
                            <div class="form-bottom">
                               <form role="form" action="<?php echo $SessionURLBase;?>forgot/verify" id="login-form" method="post" class="login-form">
                                <?php echo $alert;?>  

                                <div id="form_password_alert" class="alert alert-white rounded" style="display: none;" >
                                  <button type="button" data-dismiss="alert" aria-hidden="true" class="close">×</button>
                                  <div class="icon">
                                    <i id="form_password_alert_icon" class="fa"></i> 
                                  </div>
                                  <span id="form_password_alert_text"></span> 
                                </div>

                                 <div class="form-group">
                              <label class="sr-only" for="form-username">Username</label>
                                <input type="text" name="form-username" placeholder="Username..." class="form-username form-control" id="form-username" value=<?php echo $user ?>>
                                <label id="form-username-error" class="error" for="form-username"></label>
                              </div>
                               <div class="form-group">
                                 <div class="g-recaptcha" data-sitekey="6LdyV5MUAAAAAPWmisQORo7VgAG0KD4vQaCUifDN"></div>
                                 <label class="error" for=""></label>
                              </div>
                              <button id="form-submit" name="form-submit" type="submit" class="btn-login">Submit</button>

                              </form>                          
                          </div>
                          <div class="form-footer">
                            <div class="form-footer-left">
                            </div>  
                            <div class="form-footer-right">
                            </div>
                          </div>
                        </div> 
                    </div>
  
                </div>
            </div>
            
        </div>

      
    </body>
    <script type="text/javascript" src="../../vendor/bootstrap-4.1.3/js/bootstrap.js"></script>
    <script type="text/javascript" src="../../asset/js/custom.js"></script> 
    <script type="text/javascript" src="../../asset/js/md5.js"></script>  
    <script type="text/javascript" src="../../asset/js/forgot.js"></script>  
    <script type="text/javascript">
      $.backstretch("../../asset/images/login.jpg");
    </script>


</html>
