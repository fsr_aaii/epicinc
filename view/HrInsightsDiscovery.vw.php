<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_insights_discovery_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrInsightsDiscovery" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_insights_discovery_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrInsightsDiscovery" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_insights_discovery_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrInsightsDiscovery" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrInsightsDiscovery->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrInsightsDiscovery->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-8 form-frame">
    <form id="hr_insights_discovery_form" name="hr_insights_discovery_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr Insights Discovery</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_insights_discovery" name="is_hr_insights_discovery" value="'.$hrInsightsDiscovery->getInitialState().'">
         <input type="hidden" id="hr_insights_discovery_id" name="hr_insights_discovery_id" maxlength="22" value="'.$hrInsightsDiscovery->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_insights_discovery_description" class="control-label">Description:</label>
        <input type="text" id="hr_insights_discovery_description" name="hr_insights_discovery_description" class="hr_insights_discovery_description form-control"  maxlength="100"  value="'.$hrInsightsDiscovery->getDescription().'"  tabindex="1"/>
      <label for="hr_insights_discovery_description" class="error">'.$hrInsightsDiscovery->getAttrError("description").'</label>
      </div>


   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrInsightsDiscoveryRules(){
  $("#hr_insights_discovery_form").validate();
  $("#hr_insights_discovery_description").rules("add", {
    required:true,
    maxlength:100
  });

}


$(document).ready(function(){
  hrInsightsDiscoveryRules();


})
</script>