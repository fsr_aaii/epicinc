<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_employee_evaluation_criteria_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluationCriteria" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrEmployeeEvaluationCriteria->getCreatedBy()).'  on '.$hrEmployeeEvaluationCriteria->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_employee_evaluation_criteria_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluationCriteria" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_employee_evaluation_criteria_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluationCriteria" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrEmployeeEvaluationCriteria->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrEmployeeEvaluationCriteria->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-8 form-frame">
    <form id="hr_employee_evaluation_criteria_form" name="hr_employee_evaluation_criteria_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr Employee Evaluation Criteria</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_employee_evaluation_criteria" name="is_hr_employee_evaluation_criteria" value="'.$hrEmployeeEvaluationCriteria->getInitialState().'">
         <input type="hidden" id="hr_employee_evaluation_criteria_id" name="hr_employee_evaluation_criteria_id" maxlength="22" value="'.$hrEmployeeEvaluationCriteria->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_evaluation_criteria_id_employee_evaluation" class="control-label">Id Employee Evaluation:</label>
        <select  id="hr_employee_evaluation_criteria_id_employee_evaluation" name="hr_employee_evaluation_criteria_id_employee_evaluation" class="hr_employee_evaluation_criteria_id_employee_evaluation form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrEmployeeEvaluation::selectOptions($tfs),$hrEmployeeEvaluationCriteria->getIdEmployeeEvaluation()).
'      </select>
      <label for="hr_employee_evaluation_criteria_id_employee_evaluation" class="error">'.$hrEmployeeEvaluationCriteria->getAttrError("id_employee_evaluation").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_evaluation_criteria_id_evaluation_criteria" class="control-label">Id Evaluation Criteria:</label>
        <input type="text" id="hr_employee_evaluation_criteria_id_evaluation_criteria" name="hr_employee_evaluation_criteria_id_evaluation_criteria" class="hr_employee_evaluation_criteria_id_evaluation_criteria form-control"  maxlength="22"  value="'.$hrEmployeeEvaluationCriteria->getIdEvaluationCriteria().'"  tabindex="2"/>
      <label for="hr_employee_evaluation_criteria_id_evaluation_criteria" class="error">'.$hrEmployeeEvaluationCriteria->getAttrError("id_evaluation_criteria").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_evaluation_criteria_value" class="control-label">Value:</label>
        <input type="text" id="hr_employee_evaluation_criteria_value" name="hr_employee_evaluation_criteria_value" class="hr_employee_evaluation_criteria_value form-control"  maxlength="22"  value="'.$hrEmployeeEvaluationCriteria->getValue().'"  tabindex="3"/>
      <label for="hr_employee_evaluation_criteria_value" class="error">'.$hrEmployeeEvaluationCriteria->getAttrError("value").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_evaluation_criteria_date" class="control-label">Date:</label>
        <input type="text" id="hr_employee_evaluation_criteria_date" name="hr_employee_evaluation_criteria_date" class="hr_employee_evaluation_criteria_date form-control"  maxlength="22"  value="'.$hrEmployeeEvaluationCriteria->getDate().'"  tabindex="4"/>
      <label for="hr_employee_evaluation_criteria_date" class="error">'.$hrEmployeeEvaluationCriteria->getAttrError("date").'</label>
      </div>

'.$audit.'

   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrEmployeeEvaluationCriteriaRules(){
  $("#hr_employee_evaluation_criteria_form").validate();
  $("#hr_employee_evaluation_criteria_id_employee_evaluation").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_employee_evaluation_criteria_id_evaluation_criteria").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_employee_evaluation_criteria_value").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_evaluation_criteria_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#hr_employee_evaluation_criteria_created_by").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_employee_evaluation_criteria_created_date").rules("add", {
    required:true,
    maxlength:22
  });

}

$("#HrEmployeeEvaluationCriteria_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
  hrEmployeeEvaluationCriteriaRules();

  $("#HrEmployeeEvaluationCriteria_date").css('vertical-align','top');
  $("#HrEmployeeEvaluationCriteria_date").mask('y999-m9-d9');

})
</script>