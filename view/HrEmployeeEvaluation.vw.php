<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_employee_evaluation_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrEmployeeEvaluation->getCreatedBy()).'  on '.$hrEmployeeEvaluation->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_employee_evaluation_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_employee_evaluation_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrEmployeeEvaluation->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrEmployeeEvaluation->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
<div class="mx-auto  card col-lg-10 form-frame shadow mb-4">
    <form id="hr_employee_evaluation_form" name="hr_employee_evaluation_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr Employee Evaluation</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_employee_evaluation" name="is_hr_employee_evaluation" value="'.$hrEmployeeEvaluation->getInitialState().'">
         <input type="hidden" id="hr_employee_evaluation_id" name="hr_employee_evaluation_id" maxlength="22" value="'.$hrEmployeeEvaluation->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_evaluation_id_employee" class="control-label">Id Employee:</label>
        <input type="text" id="hr_employee_evaluation_id_employee" name="hr_employee_evaluation_id_employee" class="hr_employee_evaluation_id_employee form-control"  maxlength="22"  value="'.$hrEmployeeEvaluation->getIdEmployee().'"  tabindex="1"/>
      <label for="hr_employee_evaluation_id_employee" class="error">'.$hrEmployeeEvaluation->getAttrError("id_employee").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_evaluation_id_position" class="control-label">Id Position:</label>
        <input type="text" id="hr_employee_evaluation_id_position" name="hr_employee_evaluation_id_position" class="hr_employee_evaluation_id_position form-control"  maxlength="22"  value="'.$hrEmployeeEvaluation->getIdPosition().'"  tabindex="2"/>
      <label for="hr_employee_evaluation_id_position" class="error">'.$hrEmployeeEvaluation->getAttrError("id_position").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_evaluation_date" class="control-label">Date:</label>
        <input type="text" id="hr_employee_evaluation_date" name="hr_employee_evaluation_date" class="hr_employee_evaluation_date form-control"  maxlength="22"  value="'.$hrEmployeeEvaluation->getDate().'"  tabindex="3"/>
      <label for="hr_employee_evaluation_date" class="error">'.$hrEmployeeEvaluation->getAttrError("date").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_evaluation_id_evaluation_type" class="control-label">Id Evaluation Type:</label>
        <input type="text" id="hr_employee_evaluation_id_evaluation_type" name="hr_employee_evaluation_id_evaluation_type" class="hr_employee_evaluation_id_evaluation_type form-control"  maxlength="22"  value="'.$hrEmployeeEvaluation->getIdEvaluationType().'"  tabindex="4"/>
      <label for="hr_employee_evaluation_id_evaluation_type" class="error">'.$hrEmployeeEvaluation->getAttrError("id_evaluation_type").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_evaluation_evaluator_type" class="control-label">Evaluator Type:</label>
        <input type="text" id="hr_employee_evaluation_evaluator_type" name="hr_employee_evaluation_evaluator_type" class="hr_employee_evaluation_evaluator_type form-control"  maxlength="1"  value="'.$hrEmployeeEvaluation->getEvaluatorType().'"  tabindex="5"/>
      <label for="hr_employee_evaluation_evaluator_type" class="error">'.$hrEmployeeEvaluation->getAttrError("evaluator_type").'</label>
      </div>

'.$audit.'

   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrEmployeeEvaluationRules(){
  $("#hr_employee_evaluation_form").validate();
  $("#hr_employee_evaluation_id_employee").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_employee_evaluation_id_position").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_employee_evaluation_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#hr_employee_evaluation_id_evaluation_type").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_employee_evaluation_evaluator_type").rules("add", {
    required:true,
    maxlength:1
  });
  $("#hr_employee_evaluation_created_by").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_employee_evaluation_created_date").rules("add", {
    required:true,
    maxlength:22
  });

}

$("#HrEmployeeEvaluation_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
  hrEmployeeEvaluationRules();

  $("#HrEmployeeEvaluation_date").css('vertical-align','top');
  $("#HrEmployeeEvaluation_date").mask('y999-m9-d9');

})
</script>