<style type="text/css">
  .form-card{
    padding: 0;
  }


.epic-status-1{
  color:#1ABC9C;
}
.epic-status-2{
  color:#3498DB;
}
.epic-status-3{
  color:#E67E22;
}
.epic-status-4{
  color:#EA4335;
}
.epic-status-5{
  color:#95A5A6;
}
.epic-status-6{
  color:#E64B9A;
}


</style>
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_epic_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpic" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrEpic->getCreatedBy()).'  on '.$hrEpic->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_epic_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpic" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_epic_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpic" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrEpic->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrEpic->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '


  <div class="row">

  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
 
    <form id="hr_epic_form" name="hr_epic_form" method="post" onsubmit="return false" class="form-horizontal info-panel">

      <fieldset>

      <div class="col-lg-12 container title">My Epic</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_epic" name="is_hr_epic" value="'.$hrEpic->getInitialState().'">
         <input type="hidden" id="hr_epic_id" name="hr_epic_id" maxlength="22" value="'.$hrEpic->getId().'">
         <input type="hidden" id="hr_epic_id_employee" name="hr_epic_id_employee" maxlength="22" value="'.$hrEpic->getIdEmployee().'">
         <input type="hidden" id="hr_epic_id_position" name="hr_epic_id_position" maxlength="22" value="'.$hrEpic->getIdPosition().'">
         <input type="hidden" id="hr_epic_id_epic_status" name="hr_epic_id_epic_status" maxlength="22" value="'.$hrEpic->getIdEpicStatus().'">
      </div>';

  $html.= '<div class="col-lg-12 h-100 container">
        <div class="col-lg-8 container">
         <label for="hr_epic_description" class="control-label">Descripcion:</label>
         <textarea id="hr_epic_description" name="hr_epic_description" class="hr_epic_description form-control"  maxlength="200" rows="5" tabindex="3" >'.$hrEpic->getDescription().'</textarea>
        <label for="hr_epic_description" class="error">'.$hrEpic->getAttrError("description").'</label>
        </div>
        <div class="col-lg-2 my-auto container form-group">
          <div class="col-lg-7 container priority-range-container">
            <div style="height: 150px;">
              <div class="h-100 d-inline-block">
                <form class="range-field w-50">
                  <input id="hr_epic_id_epic_priority" name="hr_epic_id_epic_priority" class="hr_epic_id_epic_priority priority-'.$hrEpic->getIdEpicPriority().' border-0 vertical-lowest-first" type="range"tabindex="2" min="1" max="3" value="'.$hrEpic->getIdEpicPriority().'"/>
                </form>
              </div>
            </div>
          </div>
          <div class="col-lg-5 container priority-container">  
            <ul id="hr_epic_priority_label" class="list-group priority epl-'.$hrEpic->getIdEpicPriority().'">
              <li class="list-group-item priority-1 text-left">Alta</li>
              <li class="list-group-item priority-2 text-left">Media</li>
              <li class="list-group-item priority-3 text-left">Baja</li>
            </ul> 
          </div>  
          <label for="" class="error"></label>     
      </div>
      <div class="col-lg-2 container form-group">
        <div class="progress epic percentage status-'.$hrEpic->getIdEpicStatus().'" data-percentage="'.$hrEpic->getProgress().'">
            <span class="progress-left">
              <span class="progress-bar"></span>
            </span>
            <span class="progress-right">
              <span class="progress-bar"></span>
            </span>
            <div class="progress-value">
                
                <div class="percentage">'.$hrEpic->getProgress().'%</div>
                <div class="status">'.HrEpicStatus::description($tfs,$hrEpic->getIdEpicStatus()).'</div>
            </div>
          </div>
      </div>  
     
      <div class="col-lg-3 container">
       <label for="hr_epic_estimated_start_date" class="control-label">Estimated Start:</label>
        <input type="text" id="hr_epic_estimated_start_date" name="hr_epic_estimated_start_date" class="hr_epic_estimated_start_date form-control"  maxlength="22"  value="'.$hrEpic->getEstimatedStartDate().'"  tabindex="4"/>
      <label for="hr_epic_estimated_start_date" class="error">'.$hrEpic->getAttrError("estimated_start_date").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="hr_epic_start_date" class="control-label">Start Date:</label>
        <input type="text" id="hr_epic_start_date" name="hr_epic_start_date" class="hr_epic_start_date form-control"  maxlength="22"  value="'.$hrEpic->getStartDate().'"  tabindex="5"/>
      <label for="hr_epic_start_date" class="error">'.$hrEpic->getAttrError("start_date").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="hr_epic_completion_estimated_date" class="control-label">Completion Estimated:</label>
        <input type="text" id="hr_epic_completion_estimated_date" name="hr_epic_completion_estimated_date" class="hr_epic_completion_estimated_date form-control"  maxlength="22"  value="'.$hrEpic->getCompletionEstimatedDate().'"  tabindex="6"/>
      <label for="hr_epic_completion_estimated_date" class="error">'.$hrEpic->getAttrError("completion_estimated_date").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="hr_epic_completion_date" class="control-label">Completion Date:</label>
        <input type="text" id="hr_epic_completion_date" name="hr_epic_completion_date" class="hr_epic_completion_date form-control"  maxlength="22"  value="'.$hrEpic->getCompletionDate().'"  tabindex="7"/>
      <label for="hr_epic_completion_date" class="error">'.$hrEpic->getAttrError("completion_date").'</label>
      </div>'.$audit;  

     $html.='  <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>';

      $epicActivityList=HrEpicActivity::listByEpic($tfs, $hrEpic->getId());
      $tfData = array();
      $tfData["hr_epic_activity_id_epic"] = $hrEpic->getId();   

      if ($tfData["hr_epic_activity_id_epic"] !=""){ 
      $html.='                 <!--begin accordion-krs-->
                                      <div id="accordion-krs" class="col-lg-12 container">
                                        <div class="my-epic-profile form-card">
                                          <!--begin accordion-krs-header-->
                                          <div class="col-12 card-header" id="heading-krs">
                                            <div class="col-11 container"> 
                                              <a id="accordion-krs-a"  href="#" class="section active collapsed" data-toggle="collapse" data-target="#collapse-krs" aria-expanded="false" aria-controls="collapse-krs" data-form-action="form-action-krs">KRs</a>
                                            </div>
                                            <div class="col-1 container text-right">
                                              <a id="form-action-krs" name="form-action-krs" class="btn-epic-sm" alt="New" title="New" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpicActivity" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-action="AN" onclick="TfRequest.do(this);" style="display: none;">            
                                               <i class="bx bx-plus"></i>
                                              </a>
                                            </div>
                                          </div>
                                          <!--end accordion-krs-header-->
                                          <!--begin accordion-krs-content-->
                                          <div id="collapse-krs" class="collapsed collapse" aria-labelledby="heading-krs" data-parent="#accordion-krs">';  
              if (count($epicActivityList)==0){
                 $html.='                 No existen OKRs en este trimestre';
              }else{
                $html.='                 <div class="row epic">
                                              <div class="col-lg-2 col-container text-left">
                                              </div>
                                              <div class="col-lg-2 col-container epic-text-sm text-left">
                                                Estatus
                                              </div> 
                                              <div class="col-lg-1 col-container epic-text-sm text-center">
                                                Prioridad
                                              </div>       
                                              <div class="col-lg-6 col-container epic-text-sm text-left">
                                                Progreso
                                              </div>             
                                              <div class="col-lg-1 col-container epic-text-sm text-right">
                                              </div>      
                                          </div>    ';     
                $i=0;
                foreach ($epicActivityList as $c){
                $i++;
                $tfData = array();
                $tfData["hr_epic_activity_id"] = $c["id"];
                $html.='           <div class="row epic">
                                     <div class="col-lg-2 col-container text-left">
                                      <div class="h7 m-0">#'.$i.' Epic</div>
                                      <div class="text-muted h6">Estimado '.$c["completion_estimated_date"].'</div>
                                    </div>
                                    <div class="col-lg-2 col-container text-left">
                                      <div class="h7 m-0">
                                        <i class="bx bxs-circle epic-status-'.$c["id_epic_status"].'"></i> '.$c["status"].'
                                      </div>
                                      <div class="text-muted h6">'.$c["status_date"].'</div>
                                    </div>   
                                    <div class="col-lg-1 col-container text-left">
                                        <div class="epic-bottom">
                                           <span class="badge badge-pill c3-'.$c["id_epic_priority"].'">'.$c["priority"].'</span>
                                        </div>
                                    </div>    
                                    <div class="col-lg-6 col-container text-left">
                                      <div class="epic-text-sm m-0">'.$c["progress"].'%</div>
                                      <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: '.$c["progress"].'%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                      </div>
                                    </div>     
                                    <div class="col-lg-1 col-container text-right">
                                     <a class="btn-epic-sm" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpicActivity" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).' "onclick="TfRequest.do(this);">
                     <i class="bx bx-pencil"></i>
                   </a></div> 
                                    <div class="col-lg-12 col-container text-left">'.$c["description"].'</div>
                                </div>';
                }                

              }                           
              $html.='                   </div>
                                          <!--end accordion-krs-content-->
                                        </div>
                                      </div>
                                      <!--end accordion-krs-->'; 
              }                        

   $html.=' </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">
  function HrEpicRules(){
  $("#hr_epic_form").validate();
  $("#hr_epic_id_employee").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_epic_id_position").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_epic_description").rules("add", {
    required:true,
    maxlength:200
  });
  $("#hr_epic_estimated_start_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#hr_epic_start_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_epic_completion_estimated_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#hr_epic_completion_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_epic_id_epic_priority").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_epic_id_epic_status").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });


}



 $("#hr_epic_id_epic_priority").on('change',function(){ 
  
     $(this).removeClass (function (index, className) {
        return (className.match (/(^|\s)priority-\S+/g) || []).join(' ');
    });
     $(this).addClass("priority-"+$(this).val());

     $("#hr_epic_priority_label").removeClass (function (index, className) {
        return (className.match (/(^|\s)epl-\S+/g) || []).join(' ');
    });

     $("#hr_epic_priority_label").addClass("epl-"+$(this).val());
  });


$("#hr_epic_estimated_start_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#hr_epic_start_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#hr_epic_completion_estimated_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#hr_epic_completion_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});


   $(".section").on('click',function(){  
      if ($(this).hasClass('collapsed')){
        $('#'+$(this).data('form-action')).show();
      }else{
       $('#'+$(this).data('form-action')).hide();
      }
    });

$(document).ready(function(){
  HrEpicRules();

  $("#hr_epic_estimated_start_date").css('vertical-align','top');
  $("#hr_epic_estimated_start_date").mask('y999-m9-d9');
  $("#hr_epic_start_date").css('vertical-align','top');
  $("#hr_epic_start_date").mask('y999-m9-d9');
  $("#hr_epic_completion_estimated_date").css('vertical-align','top');
  $("#hr_epic_completion_estimated_date").mask('y999-m9-d9');
  $("#hr_epic_completion_date").css('vertical-align','top');
  $("#hr_epic_completion_date").mask('y999-m9-d9');


})
</script>