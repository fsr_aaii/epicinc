<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_epic_priority_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpicPriority" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrEpicPriority->getCreatedBy()).'  on '.$hrEpicPriority->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_epic_priority_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpicPriority" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_epic_priority_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpicPriority" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrEpicPriority->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrEpicPriority->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="hr_epic_priority_form" name="hr_epic_priority_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr Epic Priority</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_epic_priority" name="is_hr_epic_priority" value="'.$hrEpicPriority->getInitialState().'">
         <input type="hidden" id="hr_epic_priority_id" name="hr_epic_priority_id" maxlength="22" value="'.$hrEpicPriority->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_epic_priority_name" class="control-label">Name:</label>
        <input type="text" id="hr_epic_priority_name" name="hr_epic_priority_name" class="hr_epic_priority_name form-control"  maxlength="200"  value="'.$hrEpicPriority->getName().'"  tabindex="1"/>
      <label for="hr_epic_priority_name" class="error">'.$hrEpicPriority->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_epic_priority_level" class="control-label">Level:</label>
        <input type="text" id="hr_epic_priority_level" name="hr_epic_priority_level" class="hr_epic_priority_level form-control"  maxlength="22"  value="'.$hrEpicPriority->getLevel().'"  tabindex="2"/>
      <label for="hr_epic_priority_level" class="error">'.$hrEpicPriority->getAttrError("level").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_epic_priority_active" class="control-label">Active:</label>
        <select  id="hr_epic_priority_active" name="hr_epic_priority_active" class="hr_epic_priority_active form-control" tabindex="3">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($hrEpicPriority->getActive(),'Y').
'      </select>
      <label for="hr_epic_priority_active" class="error">'.$hrEpicPriority->getAttrError("active").'</label>
      </div>

'.$audit.'

   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrEpicPriorityRules(){
  $("#hr_epic_priority_form").validate();
  $("#hr_epic_priority_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#hr_epic_priority_level").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_epic_priority_active").rules("add", {
    required:true,
    maxlength:1
  });
  $("#hr_epic_priority_created_by").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_epic_priority_created_date").rules("add", {
    required:true,
    maxlength:22
  });

}


$(document).ready(function(){
  hrEpicPriorityRules();


})
</script>