<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="button" role="button" data-tf-form="#hr_employee_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployee" data-tf-action="AI" onclick="TfRequest.do(this,true);"><span>Save</span><div class="icon"><span class="far fa-save"></span></div></a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="create_by">Created  by '.TUser::description($tfs,$hrEmployee->getCreatedBy()).'  on '.$hrEmployee->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="button" role="button" data-tf-form="#hr_employee_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployee" data-tf-action="AA" onclick="TfRequest.do(this,true);"><span>Save</span><div class="icon"><span class="far fa-save"></span></div></a>';
    $buttons.='<a class="button" role="button" data-tf-form="#hr_employee_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployee" data-tf-action="AB" onclick="TfRequest.do(this,true);"><span>Delete</span><div class="icon"><span class="far fa-trash-alt"></span></div></a>';
    break;
  }
  foreach ($hrEmployee->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrEmployee->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="mx-auto card col-lg-8 form-frame shadow mb-4">
  <div class="row">
  <div class="mx-auto col-lg-12 form-frame">
    <div class="col-lg-8 title">Hr Employee</div>
    <div class="col-lg-4 action text-right">'.$buttons.'</div>
    <form id="hr_employee_form" name="hr_employee_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container">
        '.$objAlerts.'
      </div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_employee" name="is_hr_employee" value="'.$hrEmployee->getInitialState().'">
         <input type="hidden" id="hr_employee_id" name="hr_employee_id" maxlength="22" value="'.$hrEmployee->getId().'">
<input type="hidden" id="hr_employee_created_by" name="hr_employee_created_by" maxlength="22" value="'.$hrEmployee->getCreatedBy().'">
<input type="hidden" id="hr_employee_created_date" name="hr_employee_created_date" maxlength="22" value="'.$hrEmployee->getCreatedDate().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_id_person" class="control-label">Id Person:</label>
        <input type="text" id="hr_employee_id_person" name="hr_employee_id_person" class="hr_employee_id_person form-control"  maxlength="22"  value="'.$hrEmployee->getIdPerson().'"  tabindex="1"/>
      <label for="hr_employee_id_person" class="error">'.$hrEmployee->getAttrError("id_person").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_email_account" class="control-label">Email Account:</label>
        <input type="text" id="hr_employee_email_account" name="hr_employee_email_account" class="hr_employee_email_account form-control"  maxlength="200"  value="'.$hrEmployee->getEmailAccount().'"  tabindex="2"/>
      <label for="hr_employee_email_account" class="error">'.$hrEmployee->getAttrError("email_account").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_hire_date" class="control-label">Hire Date:</label>
        <input type="text" id="hr_employee_hire_date" name="hr_employee_hire_date" class="hr_employee_hire_date form-control"  maxlength="22"  value="'.$hrEmployee->getHireDate().'"  tabindex="3"/>
      <label for="hr_employee_hire_date" class="error">'.$hrEmployee->getAttrError("hire_date").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_id_development_level" class="control-label">Id Development Level:</label>
        <select  id="hr_employee_id_development_level" name="hr_employee_id_development_level" class="hr_employee_id_development_level form-control" tabindex="4">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrDevelopmentLevel::selectOptions($tfs),$hrEmployee->getIdDevelopmentLevel()).
'      </select>
      <label for="hr_employee_id_development_level" class="error">'.$hrEmployee->getAttrError("id_development_level").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_id_job_level" class="control-label">Id Job Level:</label>
        <select  id="hr_employee_id_job_level" name="hr_employee_id_job_level" class="hr_employee_id_job_level form-control" tabindex="5">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrJobLevel::selectOptions($tfs),$hrEmployee->getIdJobLevel()).
'      </select>
      <label for="hr_employee_id_job_level" class="error">'.$hrEmployee->getAttrError("id_job_level").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_id_job_level_next" class="control-label">Id Job Level Next:</label>
        <select  id="hr_employee_id_job_level_next" name="hr_employee_id_job_level_next" class="hr_employee_id_job_level_next form-control" tabindex="6">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrJobLevel::selectOptions($tfs),$hrEmployee->getIdJobLevelNext()).
'      </select>
      <label for="hr_employee_id_job_level_next" class="error">'.$hrEmployee->getAttrError("id_job_level_next").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_id_competence_a" class="control-label">Id Competence A:</label>
        <select  id="hr_employee_id_competence_a" name="hr_employee_id_competence_a" class="hr_employee_id_competence_a form-control" tabindex="7">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrCompetence::selectOptions($tfs),$hrEmployee->getIdCompetenceA()).
'      </select>
      <label for="hr_employee_id_competence_a" class="error">'.$hrEmployee->getAttrError("id_competence_a").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_id_competence_b" class="control-label">Id Competence B:</label>
        <select  id="hr_employee_id_competence_b" name="hr_employee_id_competence_b" class="hr_employee_id_competence_b form-control" tabindex="8">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrCompetence::selectOptions($tfs),$hrEmployee->getIdCompetenceB()).
'      </select>
      <label for="hr_employee_id_competence_b" class="error">'.$hrEmployee->getAttrError("id_competence_b").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_id_soft_skill_a" class="control-label">Id Soft Skill A:</label>
        <select  id="hr_employee_id_soft_skill_a" name="hr_employee_id_soft_skill_a" class="hr_employee_id_soft_skill_a form-control" tabindex="9">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrSoftSkill::selectOptions($tfs),$hrEmployee->getIdSoftSkillA()).
'      </select>
      <label for="hr_employee_id_soft_skill_a" class="error">'.$hrEmployee->getAttrError("id_soft_skill_a").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_id_soft_skill_b" class="control-label">Id Soft Skill B:</label>
        <select  id="hr_employee_id_soft_skill_b" name="hr_employee_id_soft_skill_b" class="hr_employee_id_soft_skill_b form-control" tabindex="10">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrSoftSkill::selectOptions($tfs),$hrEmployee->getIdSoftSkillB()).
'      </select>
      <label for="hr_employee_id_soft_skill_b" class="error">'.$hrEmployee->getAttrError("id_soft_skill_b").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_is_key_talent" class="control-label">Is Key Talent:</label>
        <input type="text" id="hr_employee_is_key_talent" name="hr_employee_is_key_talent" class="hr_employee_is_key_talent form-control"  maxlength="1"  value="'.$hrEmployee->getIsKeyTalent().'"  tabindex="11"/>
      <label for="hr_employee_is_key_talent" class="error">'.$hrEmployee->getAttrError("is_key_talent").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_has_successor" class="control-label">Has Successor:</label>
        <input type="text" id="hr_employee_has_successor" name="hr_employee_has_successor" class="hr_employee_has_successor form-control"  maxlength="1"  value="'.$hrEmployee->getHasSuccessor().'"  tabindex="12"/>
      <label for="hr_employee_has_successor" class="error">'.$hrEmployee->getAttrError("has_successor").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_has_talent" class="control-label">Has Talent:</label>
        <input type="text" id="hr_employee_has_talent" name="hr_employee_has_talent" class="hr_employee_has_talent form-control"  maxlength="1"  value="'.$hrEmployee->getHasTalent().'"  tabindex="13"/>
      <label for="hr_employee_has_talent" class="error">'.$hrEmployee->getAttrError("has_talent").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_resignation_risk" class="control-label">Resignation Risk:</label>
        <input type="text" id="hr_employee_resignation_risk" name="hr_employee_resignation_risk" class="hr_employee_resignation_risk form-control"  maxlength="1"  value="'.$hrEmployee->getResignationRisk().'"  tabindex="14"/>
      <label for="hr_employee_resignation_risk" class="error">'.$hrEmployee->getAttrError("resignation_risk").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_criticality" class="control-label">Criticality:</label>
        <input type="text" id="hr_employee_criticality" name="hr_employee_criticality" class="hr_employee_criticality form-control"  maxlength="22"  value="'.$hrEmployee->getCriticality().'"  tabindex="15"/>
      <label for="hr_employee_criticality" class="error">'.$hrEmployee->getAttrError("criticality").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_criticality_date" class="control-label">Criticality Date:</label>
        <input type="text" id="hr_employee_criticality_date" name="hr_employee_criticality_date" class="hr_employee_criticality_date form-control"  maxlength="22"  value="'.$hrEmployee->getCriticalityDate().'"  tabindex="16"/>
      <label for="hr_employee_criticality_date" class="error">'.$hrEmployee->getAttrError("criticality_date").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_performace" class="control-label">Performace:</label>
        <input type="text" id="hr_employee_performace" name="hr_employee_performace" class="hr_employee_performace form-control"  maxlength="22"  value="'.$hrEmployee->getPerformace().'"  tabindex="17"/>
      <label for="hr_employee_performace" class="error">'.$hrEmployee->getAttrError("performace").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_performace_date" class="control-label">Performace Date:</label>
        <input type="text" id="hr_employee_performace_date" name="hr_employee_performace_date" class="hr_employee_performace_date form-control"  maxlength="22"  value="'.$hrEmployee->getPerformaceDate().'"  tabindex="18"/>
      <label for="hr_employee_performace_date" class="error">'.$hrEmployee->getAttrError("performace_date").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_learning_agility" class="control-label">Learning Agility:</label>
        <input type="text" id="hr_employee_learning_agility" name="hr_employee_learning_agility" class="hr_employee_learning_agility form-control"  maxlength="22"  value="'.$hrEmployee->getLearningAgility().'"  tabindex="19"/>
      <label for="hr_employee_learning_agility" class="error">'.$hrEmployee->getAttrError("learning_agility").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_learning_agility_date" class="control-label">Learning Agility Date:</label>
        <input type="text" id="hr_employee_learning_agility_date" name="hr_employee_learning_agility_date" class="hr_employee_learning_agility_date form-control"  maxlength="22"  value="'.$hrEmployee->getLearningAgilityDate().'"  tabindex="20"/>
      <label for="hr_employee_learning_agility_date" class="error">'.$hrEmployee->getAttrError("learning_agility_date").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_cr" class="control-label">Cr:</label>
        <input type="text" id="hr_employee_cr" name="hr_employee_cr" class="hr_employee_cr form-control"  maxlength="1"  value="'.$hrEmployee->getCr().'"  tabindex="21"/>
      <label for="hr_employee_cr" class="error">'.$hrEmployee->getAttrError("cr").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_successor_name" class="control-label">Successor Name:</label>
        <input type="text" id="hr_employee_successor_name" name="hr_employee_successor_name" class="hr_employee_successor_name form-control"  maxlength="200"  value="'.$hrEmployee->getSuccessorName().'"  tabindex="22"/>
      <label for="hr_employee_successor_name" class="error">'.$hrEmployee->getAttrError("successor_name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_mobility" class="control-label">Mobility:</label>
        <input type="text" id="hr_employee_mobility" name="hr_employee_mobility" class="hr_employee_mobility form-control"  maxlength="4"  value="'.$hrEmployee->getMobility().'"  tabindex="23"/>
      <label for="hr_employee_mobility" class="error">'.$hrEmployee->getAttrError("mobility").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_area_interest" class="control-label">Area Interest:</label>
        <textarea id="hr_employee_area_interest" name="hr_employee_area_interest" class="hr_employee_area_interest form-control" rows="3" >'.$hrEmployee->getAreaInterest().'</textarea>
      <label for="hr_employee_area_interest" class="error">'.$hrEmployee->getAttrError("area_interest").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_termination_date" class="control-label">Termination Date:</label>
        <input type="text" id="hr_employee_termination_date" name="hr_employee_termination_date" class="hr_employee_termination_date form-control"  maxlength="22"  value="'.$hrEmployee->getTerminationDate().'"  tabindex="25"/>
      <label for="hr_employee_termination_date" class="error">'.$hrEmployee->getAttrError("termination_date").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_id_termination_reason" class="control-label">Id Termination Reason:</label>
        <input type="text" id="hr_employee_id_termination_reason" name="hr_employee_id_termination_reason" class="hr_employee_id_termination_reason form-control"  maxlength="22"  value="'.$hrEmployee->getIdTerminationReason().'"  tabindex="26"/>
      <label for="hr_employee_id_termination_reason" class="error">'.$hrEmployee->getAttrError("id_termination_reason").'</label>
      </div>

'.$audit.'

   </fieldset>
  </form>
 </div>
</div>
</div>';
  echo $html;
?>
<script type="text/javascript">
function hrEmployeeRules(){
  $("#hr_employee_form").validate();
  $("#hr_employee_id_person").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_employee_email_account").rules("add", {
    maxlength:200
  });
  $("#hr_employee_hire_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_employee_id_development_level").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_id_job_level").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_id_job_level_next").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_id_competence_a").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_id_competence_b").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_id_soft_skill_a").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_id_soft_skill_b").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_is_key_talent").rules("add", {
    required:true,
    maxlength:1
  });
  $("#hr_employee_has_successor").rules("add", {
    required:true,
    maxlength:1
  });
  $("#hr_employee_has_talent").rules("add", {
    required:true,
    maxlength:1
  });
  $("#hr_employee_resignation_risk").rules("add", {
    required:true,
    maxlength:1
  });
  $("#hr_employee_criticality").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_criticality_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_employee_performace").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_performace_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_employee_learning_agility").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_learning_agility_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_employee_cr").rules("add", {
    maxlength:1
  });
  $("#hr_employee_successor_name").rules("add", {
    maxlength:200
  });
  $("#hr_employee_mobility").rules("add", {
    maxlength:4
  });
  $("#hr_employee_area_interest").rules("add", {
    maxlength:500
  });
  $("#hr_employee_termination_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_employee_id_termination_reason").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_created_date").rules("add", {
    required:true,
    maxlength:22
  });
  $("#hr_employee_created_by").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

}

$("#HrEmployee_hire_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#HrEmployee_criticality_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#HrEmployee_performace_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#HrEmployee_learning_agility_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#HrEmployee_termination_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
  hrEmployeeRules();
  $("#HrEmployee_hire_date").css('vertical-align','top');
  $("#HrEmployee_hire_date").mask('yyyy-mm-dd');
  $("#HrEmployee_criticality_date").css('vertical-align','top');
  $("#HrEmployee_criticality_date").mask('yyyy-mm-dd');
  $("#HrEmployee_performace_date").css('vertical-align','top');
  $("#HrEmployee_performace_date").mask('yyyy-mm-dd');
  $("#HrEmployee_learning_agility_date").css('vertical-align','top');
  $("#HrEmployee_learning_agility_date").mask('yyyy-mm-dd');
  $("#HrEmployee_termination_date").css('vertical-align','top');
  $("#HrEmployee_termination_date").mask('yyyy-mm-dd');

})
</script>