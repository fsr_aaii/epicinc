<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="button" role="button" data-tf-form="#fi_element_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="fiElementType" data-tf-action="AI" onclick="TfRequest.do(this,true);"><span>Save</span><div class="icon"><span class="far fa-save"></span></div></a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="create_by">Created  by '.TUser::description($tfs,$fiElementType->getCreatedBy()).'  on '.$fiElementType->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="button" role="button" data-tf-form="#fi_element_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="fiElementType" data-tf-action="AA" onclick="TfRequest.do(this,true);"><span>Save</span><div class="icon"><span class="far fa-save"></span></div></a>';
    $buttons.='<a class="button" role="button" data-tf-form="#fi_element_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="fiElementType" data-tf-action="AB" onclick="TfRequest.do(this,true);"><span>Delete</span><div class="icon"><span class="far fa-trash-alt"></span></div></a>';
    break;
  }
  foreach ($fiElementType->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($fiElementType->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-8 form-frame">
    <div class="col-lg-8 title">Fi Element Type</div>
    <div class="col-lg-4 action text-right">'.$buttons.'</div>
    <form id="fi_element_type_form" name="fi_element_type_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container">
        '.$objAlerts.'
      </div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_fi_element_type" name="is_fi_element_type" value="'.$fiElementType->getInitialState().'">
         <input type="hidden" id="fi_element_type_id" name="fi_element_type_id" maxlength="22" value="'.$fiElementType->getId().'">
<input type="hidden" id="fi_element_type_created_by" name="fi_element_type_created_by" maxlength="22" value="'.$fiElementType->getCreatedBy().'">
<input type="hidden" id="fi_element_type_created_date" name="fi_element_type_created_date" maxlength="22" value="'.$fiElementType->getCreatedDate().'">

      </div>
      <div class="col-lg-12 container">
       <label for="fi_element_type_name" class="control-label">Name:</label>
        <input type="text" id="fi_element_type_name" name="fi_element_type_name" class="fi_element_type_name form-control"  maxlength="200"  value="'.$fiElementType->getName().'"  tabindex="1"/>
      <label for="fi_element_type_name" class="error">'.$fiElementType->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="fi_element_type_active" class="control-label">Active:</label>
        <select  id="fi_element_type_active" name="fi_element_type_active" class="_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($fiElementType->getActive(),'Y').
'      </select>
      <label for="fi_element_type_active" class="error">'.$fiElementType->getAttrError("active").'</label>
      </div>

'.$audit.'

   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function fiElementTypeRules(){
  $("#fi_element_type_form").validate();
  $("#fi_element_type_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#fi_element_type_active").rules("add", {
    required:true,
    maxlength:1
  });
  $("#fi_element_type_created_by").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#fi_element_type_created_date").rules("add", {
    required:true,
    maxlength:22
  });

}


$(document).ready(function(){
  fiElementTypeRules();

})
</script>