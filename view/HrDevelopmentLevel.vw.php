<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="button" role="button" data-tf-form="#hr_development_level_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrDevelopmentLevel" data-tf-action="AI" onclick="TfRequest.do(this,true);"><span>Save</span><div class="icon"><span class="far fa-save"></span></div></a>';
    break;
   case "AE":
   case "AB":
   case "AA":
    $buttons ='<a class="button" role="button" data-tf-form="#hr_development_level_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrDevelopmentLevel" data-tf-action="AA" onclick="TfRequest.do(this,true);"><span>Save</span><div class="icon"><span class="far fa-save"></span></div></a>';
    $buttons.='<a class="button" role="button" data-tf-form="#hr_development_level_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrDevelopmentLevel" data-tf-action="AB" onclick="TfRequest.do(this,true);"><span>Delete</span><div class="icon"><span class="far fa-trash-alt"></span></div></a>';
    break;
  }
  foreach ($hrDevelopmentLevel->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrDevelopmentLevel->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-8 form-frame">
    <div class="col-lg-8 title">Hr Development Level</div>
    <div class="col-lg-4 action text-right">'.$buttons.'</div>
    <form id="hr_development_level_form" name="hr_development_level_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container">
        '.$objAlerts.'
      </div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_development_level" name="is_hr_development_level" value="'.$hrDevelopmentLevel->getInitialState().'">
         <input type="hidden" id="hr_development_level_id" name="hr_development_level_id" maxlength="22" value="'.$hrDevelopmentLevel->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_development_level_description" class="control-label">Description:</label>
        <input type="text" id="hr_development_level_description" name="hr_development_level_description" class="hr_development_level_description form-control"  maxlength="100"  value="'.$hrDevelopmentLevel->getDescription().'"  tabindex="1"/>
      <label for="hr_development_level_description" class="error">'.$hrDevelopmentLevel->getAttrError("description").'</label>
      </div>


   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
function hrDevelopmentLevelRules(){
  $("#hr_development_level_form").validate();
  $("#hr_development_level_description").rules("add", {
    required:true,
    maxlength:100
  });

}


$(document).ready(function(){
  hrDevelopmentLevelRules();

})
