<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_tag_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmTag" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$cmTag->getCreatedBy()).'  on '.$cmTag->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_tag_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmTag" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_tag_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmTag" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($cmTag->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($cmTag->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="cm_tag_form" name="cm_tag_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Cm Tag</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_cm_tag" name="is_cm_tag" value="'.$cmTag->getInitialState().'">
         <input type="hidden" id="cm_tag_id" name="cm_tag_id" maxlength="22" value="'.$cmTag->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="cm_tag_name" class="control-label">Name:</label>
        <input type="text" id="cm_tag_name" name="cm_tag_name" class="cm_tag_name form-control"  maxlength="200"  value="'.$cmTag->getName().'"  tabindex="1"/>
      <label for="cm_tag_name" class="error">'.$cmTag->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="cm_tag_active" class="control-label">Active:</label>
        <select  id="cm_tag_active" name="cm_tag_active" class="cm_tag_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($cmTag->getActive(),'Y').
'      </select>
      <label for="cm_tag_active" class="error">'.$cmTag->getAttrError("active").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function cmTagRules(){
  $("#cm_tag_form").validate();
  $("#cm_tag_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#cm_tag_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  cmTagRules();


})
</script>