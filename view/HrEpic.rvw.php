<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8">
            <div class="col-6 title">Hr Epic</div>
             <div class="col-6 text-right action">
               <a class="button" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpic" data-tf-action="AN" onclick="TfRequest.do(this);">
                 <span>Add</span><div class="icon"><span class="far fa-plus"></span></div>
               </a>
               <a class="button" role="button" data-tf-table="#hr_epic_dt" data-tf-file="Hr Epic" onclick="TfExport.excel(this);">
                 <span>Excel</span><div class="icon"><span class="far fa-download"></span></div>
               </a>
             </div>
       <table id="hr_epic_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Id Employee</th>
             <th class="all">Id Position</th>
             <th class="all">Description</th>
             <th class="all">Estimated Start Date</th>
             <th class="all">Start Date</th>
             <th class="all">Completion Estimated Date</th>
             <th class="all">Completion Date</th>
             <th class="all">Id Epic Priority</th>
             <th class="all">Id Epic Status</th>
             <th class="all">Status Date</th>
             <th class="all">Progress</th>
             <th class="all">Active</th>
             <th class="none">Created by</th>
             <th class="none">Created date</th>
             <th class="all dt-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($hrEpicList as $row){
    $tfData["hr_epic_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["id_employee"].'</td>
            <td>'.$row["id_position"].'</td>
            <td>'.$row["description"].'</td>
            <td>'.$row["estimated_start_date"].'</td>
            <td>'.$row["start_date"].'</td>
            <td>'.$row["completion_estimated_date"].'</td>
            <td>'.$row["completion_date"].'</td>
            <td>'.$row["id_epic_priority"].'</td>
            <td>'.$row["id_epic_status"].'</td>
            <td>'.$row["status_date"].'</td>
            <td>'.$row["progress"].'</td>
            <td>'.$row["active"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="dt-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpic" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData)($tfData).'" onclick="TfRequest.do(this);">
                     <span class="far fa-pencil"></span>
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#hr_epic_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>