<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_comment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPostComment" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$cmPostComment->getCreatedBy()).'  on '.$cmPostComment->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_comment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPostComment" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_comment_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPostComment" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($cmPostComment->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($cmPostComment->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="cm_post_comment_form" name="cm_post_comment_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Cm Post Comment</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_cm_post_comment" name="is_cm_post_comment" value="'.$cmPostComment->getInitialState().'">
         <input type="hidden" id="cm_post_comment_id" name="cm_post_comment_id" maxlength="22" value="'.$cmPostComment->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="cm_post_comment_id_post" class="control-label">Id Post:</label>
        <select  id="cm_post_comment_id_post" name="cm_post_comment_id_post" class="cm_post_comment_id_post form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(CmPost::selectOptions($tfs),$cmPostComment->getIdPost()).
'      </select>
      <label for="cm_post_comment_id_post" class="error">'.$cmPostComment->getAttrError("id_post").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="cm_post_comment_comment" class="control-label">Comment:</label>
        <input type="text" id="cm_post_comment_comment" name="cm_post_comment_comment" class="cm_post_comment_comment form-control"  maxlength="280"  value="'.$cmPostComment->getComment().'"  tabindex="2"/>
      <label for="cm_post_comment_comment" class="error">'.$cmPostComment->getAttrError("comment").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="cm_post_comment_active" class="control-label">Active:</label>
        <select  id="cm_post_comment_active" name="cm_post_comment_active" class="cm_post_comment_active form-control" tabindex="3">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($cmPostComment->getActive(),'Y').
'      </select>
      <label for="cm_post_comment_active" class="error">'.$cmPostComment->getAttrError("active").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function cmPostCommentRules(){
  $("#cm_post_comment_form").validate();
  $("#cm_post_comment_id_post").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#cm_post_comment_comment").rules("add", {
    required:true,
    maxlength:280
  });
  $("#cm_post_comment_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  cmPostCommentRules();


})
</script>