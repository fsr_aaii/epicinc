<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPost" data-tf-action="AI" onclick="cmPostDo(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
     $audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$cmPost->getCreatedBy()).'  on '.$cmPost->getCreatedDate().'</span>
        </div>';
    if ($cmPost->getActive()=='N'){
      $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPost" data-tf-action="AP" onclick="cmPostDo(this,true);">Publicar</a>';
    }else{
      $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPost" data-tf-action="AR" onclick="cmPostDo(this,true);">Retirar</a>';
    }
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPost" data-tf-action="AA" onclick="cmPostDo(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button"  onclick="postPreview();" data-toggle="modal" data-target="#exampleModal">Vista Previa</a>';
    break;
  }
  foreach ($cmPost->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($cmPost->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }

  if ($cmPost->getImage()=='..' or $cmPost->getImage()==''){
    $photo='../asset/images/blank-post.png';
  }else{
    $photo=$cmPost->getImage();
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="cm_post_form" name="cm_post_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container epic-title xl mb-5">Post</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_cm_post" name="is_cm_post" value="'.$cmPost->getInitialState().'">
         <input type="hidden" id="cm_post_id" name="cm_post_id" maxlength="22" value="'.$cmPost->getId().'">
         <input type="hidden" id="cm_post_active" name="cm_post_active" maxlength="22" value="'.$cmPost->getActive().'">
         <input type="hidden" id="cm_post_image" name="cm_post_image" maxlength="500" value="'.$cmPost->getImage().'">
      </div>
      <div class="col-lg-9 card-header post col-container p-0">
         <input id="cm_post_image_path" name="cm_post_image_path" type="hidden" class="form-control" >
         <div class="button-container">           
           <img id="photo-upload" name="photo-upload" class="card-img-top cropped" src="'.$photo.'">
           <a class="btn btn-file btn-edit btn-photo-upload rounded-circle" href="#" alt="Upload">
            <i class="bx bx-camera"></i>
            <input type="file" id="cm_post_image_img" name="cm_post_image_img">
           </a>
         </div> 
      </div>
      <div class="col-lg-3 container">
      <div class="alert alert-dark ml-3" role="alert">
       La visualizaci&oacute;n actual de la imagen que seleccion&oacute; es un aproximaci&oacute;n muy cercana a como se ver&aacute; esta imagen en el post que desea publicar
       </div>
      </div>
       <div class="col-lg-9 container">
       <label for="cm_post_title" class="control-label">Titulo:</label>
        <input type="text" id="cm_post_title" name="cm_post_title" class="cm_post_title form-control"  maxlength="200"  value="'.$cmPost->getTitle().'"  tabindex="2"/>
      <label for="cm_post_title" class="error">'.$cmPost->getAttrError("title").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="cm_post_id_category" class="control-label">Categoria:</label>
        <select  id="cm_post_id_category" name="cm_post_id_category" class="cm_post_id_category form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(CmCategory::selectOptions($tfs),$cmPost->getIdCategory()).
'      </select>
      <label for="cm_post_id_category" class="error">'.$cmPost->getAttrError("id_category").'</label>
      </div>
     
      <div class="col-lg-12 container">
       <label for="cm_post_content" class="control-label">Contenido:</label>
        <textarea id="cm_post_content" name="cm_post_content" class="cm_post_content form-control" rows="5" >'.$cmPost->getContent().'</textarea>
      <label for="cm_post_content" class="error">'.$cmPost->getAttrError("content").'</label>
      </div>

      <div class="col-lg-12 container">
       <label for="cm_post_tags" class="control-label">Tags:</label>

       <input type="text"
       placeholder="..."
       value="'.$cmPost->getTags().'"
       class="form-control flexdatalist"
       data-data=\''.$tags.'\'
       data-search-in="option"
       data-visible-properties=\'["option"]\'
       data-selection-required="true"
       data-value-property="value"
       data-text-property="{option}"
       data-min-length="0"
       multiple="multiple"
       id="cm_post_tags" name="cm_post_tags">
      <label for="cm_post_tags" class="error">'.$cmPost->getAttrError("tags").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="cm_post_comment_form" name="cm_post_comment_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
          <fieldset style="padding: 0 2rem;">
            <div>


                <div class="card-header post">
                  <span class="badge badge-pill category category-bg-1 float-right m-2"></span>
                  <img id="preview-img" class="card-img-top cropped" src="">
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-lg-3 container text-center">
                      <div class="mr-2 mb-2">
                        <img class="rounded-circle post-profile-photo photo-bg-1" src="'.$profile["photo"].'" alt="">
                      </div>
                      <div class="ml-2">
                        <div class="post-created-by m-0">'.$profile["name"].'</div>
                        <div class="post-business-title m-0">'.$profile["business_title"].'</div>
                        <div class="post-email-account mt-2">'.$profile["email_account"].'</div>
                      </div>
                    </div>
                    <div class="col-lg-9 container">
                      <div class="post-title"></div>
                      <div class="post-ago">Hace '.CmPost::timeAgo($cmPost->getCreatedDate()).'</div>
                      <div class="mt-2 mb-2 tags">Tags:</div>
                      <span class="card-text text-justify more"></span>
                    </div>
                  </div>
                </div>        
                          
             </div>
          </fieldset>
        </form>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-epic-xl-primary" data-dismiss="modal">Cerrar</button>';
         if ($cmPost->getActive()=='N'){
      $html.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPost" data-tf-action="AP" onclick="publish(this,true);">Publicar</a>';
    }
       
  $html.='</div>
    </div>
  </div>
</div>
<script type="text/javascript">
    
  $("#cm_post_tags").flexdatalist({
     minLength: 0,
     toggleSelected: true,
     searchByWord: true,
     searchContain: true,
     textProperty: \'{option}\',
     valueProperty: "value",
     selectionRequired: true,
     visibleProperties: ["option"],
     searchIn: "option",
     data: \''.$tags.'\'
});
</script>';
  echo $html;
?>
<script type="text/javascript">

function cmPostDo(element){
  jConfirm('Esta seguro que desea procesar esta informaci&oacute;n?','Continuar?',function(r){
    if(r){
      if(tfValidate('#cm_post_form')){
        let url=window.location.href+"/"+window.TF_TRAIL_ID+"/view/"+element.dataset.tfTaskId+"/"+element.dataset.tfController+"/"+element.dataset.tfAction;
        let form = element.dataset.tfForm;
        let go = true;     
        let jsonObject = new Object();
        let file_data = $('#cm_post_image_img').prop('files')[0];
        var img_data = new FormData();
        img_data.append('cm_post_image_img', file_data);
        
        if (typeof form != 'undefined') {
          if ($(form).valid()){
           jsonObject = $(form).serializeJSON();   
          }else{
            go = false;
          } 
        }
        let cryptography  = new TfCryptography();         
        data = cryptography.encryptJSON(jsonObject,window.RANDOM_PUBLIC_KEY);
        img_data.append('request-data', data);

        if (go){
          $.ajax({
            data: img_data,
            url: url,    
            type: 'POST',
            dataType: 'text', 
            cache: false,
            contentType: false,
            processData: false,
            async : false,
            success:function(response){ 
              let JDATA=JSON.parse(response);
              let cryptography  = new TfCryptography();
                   $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, window.RANDOM_PUBLIC_KEY));
            }
          });
        }
      }  
    }
  })
}
function cmPostRules(){
  $("#cm_post_form").validate();
  $("#cm_post_id_category").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#cm_post_title").rules("add", {
    required:true,
    maxlength:200
  });
  $("#cm_post_content").rules("add", {
    required:true,
    maxlength:4500
  });
  $("#cm_post_active").rules("add", {
    required:true,
    maxlength:1
  });

}

function postPreview(){
  $(".post-title").html($("#cm_post_title").val());
  $(".category").removeClass('category-bg-1 category-bg-2 category-bg-3 category-bg-4 category-bg-5');
  $(".category").html($("#cm_post_id_category option:selected").html());
  $(".category").addClass('category-bg-'+$("#cm_post_id_category option:selected").val());
  $(".more").html($("#cm_post_content").val());
  $('.flexdatalist-multiple  li span.text').each(function(n,v){
     $(".tags").append('<span class="badge badge-pill tag">'+$(this).text()+'</span>');
  });

  $("#preview-img").attr("src", $("#photo-upload").attr('src'));

}

function publish(element){
  $('#exampleModal').modal('toggle');
  TfRequest.do(element,true);
}

$(document).ready(function(){
  cmPostRules();

$(document).on('change', '.btn-photo-upload :file', function() {
    var input = $(this),
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [label]);
  });

  $('.btn-photo-upload :file').on('fileselect', function(event, label) {
    var input = $(this).parents('.input-group').find(':text'),
        log = label;
        
    $('#cm_post_image_path').val(log);
      
  });
  
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#photo-upload').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
  }

    $("#cm_post_image_img").change(function(){
        readURL(this);
    }); 

})
</script>