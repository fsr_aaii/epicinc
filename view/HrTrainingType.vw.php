<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_training_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrTrainingType" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrTrainingType->getCreatedBy()).'  on '.$hrTrainingType->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_training_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrTrainingType" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_training_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrTrainingType" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrTrainingType->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrTrainingType->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="hr_training_type_form" name="hr_training_type_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr Training Type</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_training_type" name="is_hr_training_type" value="'.$hrTrainingType->getInitialState().'">
         <input type="hidden" id="hr_training_type_id" name="hr_training_type_id" maxlength="22" value="'.$hrTrainingType->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_training_type_name" class="control-label">Name:</label>
        <input type="text" id="hr_training_type_name" name="hr_training_type_name" class="hr_training_type_name form-control"  maxlength="200"  value="'.$hrTrainingType->getName().'"  tabindex="1"/>
      <label for="hr_training_type_name" class="error">'.$hrTrainingType->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_training_type_active" class="control-label">Active:</label>
        <select  id="hr_training_type_active" name="hr_training_type_active" class="hr_training_type_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($hrTrainingType->getActive(),'Y').
'      </select>
      <label for="hr_training_type_active" class="error">'.$hrTrainingType->getAttrError("active").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrTrainingTypeRules(){
  $("#hr_training_type_form").validate();
  $("#hr_training_type_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#hr_training_type_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  hrTrainingTypeRules();


})
</script>