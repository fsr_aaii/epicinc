<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8">
            <div class="col-6 title">Hr Insights Discovery</div>
             <div class="col-6 text-right action">
               <a class="button" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrInsightsDiscovery" data-tf-action="AN" onclick="TfRequest.do(this);">
                 <span>Add</span><div class="icon"><span class="far fa-plus"></span></div>
               </a>
               <a class="button" role="button" data-tf-table="#hr_insights_discovery_dt" data-tf-file="Hr Insights Discovery" onclick="TfExport.excel(this);">
                 <span>Excel</span><div class="icon"><span class="far fa-download"></span></div>
               </a>
             </div>
       <table id="hr_insights_discovery_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Description</th>
             <th class="all dt-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($hrInsightsDiscoveryList as $row){
    $tfData["hr_insights_discovery_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["description"].'</td>
                 <td class="dt-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrInsightsDiscovery" data-tf-action="AE" data-tf-data="'.tfRequest::encrypt($tfData).'"onclick="TfRequest.do(this);">
                     <span class="far fa-pencil"></span>
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#hr_insights_discovery_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>