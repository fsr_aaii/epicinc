<style type="text/css">


@keyframes click-wave {
  0% {
    height: 40px;
    width: 40px;
    opacity: 0.35;
    position: relative;
  }
  100% {
    height: 200px;
    width: 200px;
    margin-left: -80px;
    margin-top: -80px;
    opacity: 0;
  }
}

.option-input {
  -webkit-appearance: none;
  -moz-appearance: none;
  -ms-appearance: none;
  -o-appearance: none;
  appearance: none;
  position: relative;
  right: 0;
  bottom: 0;
  left: 0;
  height: 40px;
  width: 40px;
  transition: all 0.15s ease-out 0s;
  background: #d1d3e2;
  border: none;
  color: #fff;
  cursor: pointer;
  display: inline-block;
  outline: none;
  position: relative;
  z-index: 1000;
}
.option-input:hover {
  background: #E64B9A;
}
.option-input:checked {
  background: #E64B9A;
}
.option-input:checked::before {
  height: 40px;
  width: 40px;
  position: absolute;
  content: '\25CF';
  display: inline-block;
  font-size: 27px;
  text-align: center;
}
.option-input:checked::after {
  -webkit-animation: click-wave 0.65s;
  -moz-animation: click-wave 0.65s;
  animation: click-wave 0.65s;
  background: #E64B9A;
  content: '';
  display: block;
  position: relative;
  z-index: 100;
}
.option-input.radio {
  border-radius: 50%;
}
.option-input.radio::after {
  border-radius: 50%;
}


/* set range from 1 - 0 vertically (highest on top) */ .vertical-heighest-first
{
     -webkit-transform:rotate(270deg);
     -moz-transform:rotate(270deg);
     -o-transform:rotate(270deg);
     -ms-transform:rotate(270deg);
     transform:rotate(270deg);
}


/*progess*/

.progress[data-percentage="1"] .progress-right .progress-bar {
  animation: loading-1 0.5s linear forwards;
}
.progress[data-percentage="1"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="2"] .progress-right .progress-bar {
  animation: loading-2 0.5s linear forwards;
}
.progress[data-percentage="2"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="3"] .progress-right .progress-bar {
  animation: loading-3 0.5s linear forwards;
}
.progress[data-percentage="3"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="4"] .progress-right .progress-bar {
  animation: loading-4 0.5s linear forwards;
}
.progress[data-percentage="4"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="5"] .progress-right .progress-bar {
  animation: loading-5 0.5s linear forwards;
}
.progress[data-percentage="5"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="6"] .progress-right .progress-bar {
  animation: loading-6 0.5s linear forwards;
}
.progress[data-percentage="6"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="7"] .progress-right .progress-bar {
  animation: loading-7 0.5s linear forwards;
}
.progress[data-percentage="7"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="8"] .progress-right .progress-bar {
  animation: loading-8 0.5s linear forwards;
}
.progress[data-percentage="8"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="9"] .progress-right .progress-bar {
  animation: loading-9 0.5s linear forwards;
}
.progress[data-percentage="9"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="10"] .progress-right .progress-bar {
  animation: loading-10 0.5s linear forwards;
}
.progress[data-percentage="10"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="11"] .progress-right .progress-bar {
  animation: loading-11 0.5s linear forwards;
}
.progress[data-percentage="11"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="12"] .progress-right .progress-bar {
  animation: loading-12 0.5s linear forwards;
}
.progress[data-percentage="12"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="13"] .progress-right .progress-bar {
  animation: loading-13 0.5s linear forwards;
}
.progress[data-percentage="13"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="14"] .progress-right .progress-bar {
  animation: loading-14 0.5s linear forwards;
}
.progress[data-percentage="14"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="15"] .progress-right .progress-bar {
  animation: loading-15 0.5s linear forwards;
}
.progress[data-percentage="15"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="16"] .progress-right .progress-bar {
  animation: loading-16 0.5s linear forwards;
}
.progress[data-percentage="16"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="17"] .progress-right .progress-bar {
  animation: loading-17 0.5s linear forwards;
}
.progress[data-percentage="17"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="18"] .progress-right .progress-bar {
  animation: loading-18 0.5s linear forwards;
}
.progress[data-percentage="18"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="19"] .progress-right .progress-bar {
  animation: loading-19 0.5s linear forwards;
}
.progress[data-percentage="19"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="20"] .progress-right .progress-bar {
  animation: loading-20 0.5s linear forwards;
}
.progress[data-percentage="20"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="21"] .progress-right .progress-bar {
  animation: loading-21 0.5s linear forwards;
}
.progress[data-percentage="21"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="22"] .progress-right .progress-bar {
  animation: loading-22 0.5s linear forwards;
}
.progress[data-percentage="22"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="23"] .progress-right .progress-bar {
  animation: loading-23 0.5s linear forwards;
}
.progress[data-percentage="23"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="24"] .progress-right .progress-bar {
  animation: loading-24 0.5s linear forwards;
}
.progress[data-percentage="24"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="25"] .progress-right .progress-bar {
  animation: loading-25 0.5s linear forwards;
}
.progress[data-percentage="25"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="26"] .progress-right .progress-bar {
  animation: loading-26 0.5s linear forwards;
}
.progress[data-percentage="26"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="27"] .progress-right .progress-bar {
  animation: loading-27 0.5s linear forwards;
}
.progress[data-percentage="27"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="28"] .progress-right .progress-bar {
  animation: loading-28 0.5s linear forwards;
}
.progress[data-percentage="28"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="29"] .progress-right .progress-bar {
  animation: loading-29 0.5s linear forwards;
}
.progress[data-percentage="29"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="30"] .progress-right .progress-bar {
  animation: loading-30 0.5s linear forwards;
}
.progress[data-percentage="30"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="31"] .progress-right .progress-bar {
  animation: loading-31 0.5s linear forwards;
}
.progress[data-percentage="31"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="32"] .progress-right .progress-bar {
  animation: loading-32 0.5s linear forwards;
}
.progress[data-percentage="32"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="33"] .progress-right .progress-bar {
  animation: loading-33 0.5s linear forwards;
}
.progress[data-percentage="33"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="34"] .progress-right .progress-bar {
  animation: loading-34 0.5s linear forwards;
}
.progress[data-percentage="34"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="35"] .progress-right .progress-bar {
  animation: loading-35 0.5s linear forwards;
}
.progress[data-percentage="35"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="36"] .progress-right .progress-bar {
  animation: loading-36 0.5s linear forwards;
}
.progress[data-percentage="36"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="37"] .progress-right .progress-bar {
  animation: loading-37 0.5s linear forwards;
}
.progress[data-percentage="37"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="38"] .progress-right .progress-bar {
  animation: loading-38 0.5s linear forwards;
}
.progress[data-percentage="38"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="39"] .progress-right .progress-bar {
  animation: loading-39 0.5s linear forwards;
}
.progress[data-percentage="39"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="40"] .progress-right .progress-bar {
  animation: loading-40 0.5s linear forwards;
}
.progress[data-percentage="40"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="41"] .progress-right .progress-bar {
  animation: loading-41 0.5s linear forwards;
}
.progress[data-percentage="41"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="42"] .progress-right .progress-bar {
  animation: loading-42 0.5s linear forwards;
}
.progress[data-percentage="42"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="43"] .progress-right .progress-bar {
  animation: loading-43 0.5s linear forwards;
}
.progress[data-percentage="43"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="44"] .progress-right .progress-bar {
  animation: loading-44 0.5s linear forwards;
}
.progress[data-percentage="44"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="45"] .progress-right .progress-bar {
  animation: loading-45 0.5s linear forwards;
}
.progress[data-percentage="45"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="46"] .progress-right .progress-bar {
  animation: loading-46 0.5s linear forwards;
}
.progress[data-percentage="46"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="47"] .progress-right .progress-bar {
  animation: loading-47 0.5s linear forwards;
}
.progress[data-percentage="47"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="48"] .progress-right .progress-bar {
  animation: loading-48 0.5s linear forwards;
}
.progress[data-percentage="48"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="49"] .progress-right .progress-bar {
  animation: loading-49 0.5s linear forwards;
}
.progress[data-percentage="49"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="50"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="50"] .progress-left .progress-bar {
  animation: 0;
}
.progress[data-percentage="51"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="51"] .progress-left .progress-bar {
  animation: loading-1 0.5s linear forwards 0.5s;
}
.progress[data-percentage="52"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="52"] .progress-left .progress-bar {
  animation: loading-2 0.5s linear forwards 0.5s;
}
.progress[data-percentage="53"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="53"] .progress-left .progress-bar {
  animation: loading-3 0.5s linear forwards 0.5s;
}
.progress[data-percentage="54"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="54"] .progress-left .progress-bar {
  animation: loading-4 0.5s linear forwards 0.5s;
}
.progress[data-percentage="55"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="55"] .progress-left .progress-bar {
  animation: loading-5 0.5s linear forwards 0.5s;
}
.progress[data-percentage="56"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="56"] .progress-left .progress-bar {
  animation: loading-6 0.5s linear forwards 0.5s;
}
.progress[data-percentage="57"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="57"] .progress-left .progress-bar {
  animation: loading-7 0.5s linear forwards 0.5s;
}
.progress[data-percentage="58"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="58"] .progress-left .progress-bar {
  animation: loading-8 0.5s linear forwards 0.5s;
}
.progress[data-percentage="59"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="59"] .progress-left .progress-bar {
  animation: loading-9 0.5s linear forwards 0.5s;
}
.progress[data-percentage="60"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="60"] .progress-left .progress-bar {
  animation: loading-10 0.5s linear forwards 0.5s;
}
.progress[data-percentage="61"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="61"] .progress-left .progress-bar {
  animation: loading-11 0.5s linear forwards 0.5s;
}
.progress[data-percentage="62"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="62"] .progress-left .progress-bar {
  animation: loading-12 0.5s linear forwards 0.5s;
}
.progress[data-percentage="63"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="63"] .progress-left .progress-bar {
  animation: loading-13 0.5s linear forwards 0.5s;
}
.progress[data-percentage="64"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="64"] .progress-left .progress-bar {
  animation: loading-14 0.5s linear forwards 0.5s;
}
.progress[data-percentage="65"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="65"] .progress-left .progress-bar {
  animation: loading-15 0.5s linear forwards 0.5s;
}
.progress[data-percentage="66"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="66"] .progress-left .progress-bar {
  animation: loading-16 0.5s linear forwards 0.5s;
}
.progress[data-percentage="67"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="67"] .progress-left .progress-bar {
  animation: loading-17 0.5s linear forwards 0.5s;
}
.progress[data-percentage="68"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="68"] .progress-left .progress-bar {
  animation: loading-18 0.5s linear forwards 0.5s;
}
.progress[data-percentage="69"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="69"] .progress-left .progress-bar {
  animation: loading-19 0.5s linear forwards 0.5s;
}
.progress[data-percentage="70"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="70"] .progress-left .progress-bar {
  animation: loading-20 0.5s linear forwards 0.5s;
}
.progress[data-percentage="71"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="71"] .progress-left .progress-bar {
  animation: loading-21 0.5s linear forwards 0.5s;
}
.progress[data-percentage="72"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="72"] .progress-left .progress-bar {
  animation: loading-22 0.5s linear forwards 0.5s;
}
.progress[data-percentage="73"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="73"] .progress-left .progress-bar {
  animation: loading-23 0.5s linear forwards 0.5s;
}
.progress[data-percentage="74"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="74"] .progress-left .progress-bar {
  animation: loading-24 0.5s linear forwards 0.5s;
}
.progress[data-percentage="75"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="75"] .progress-left .progress-bar {
  animation: loading-25 0.5s linear forwards 0.5s;
}
.progress[data-percentage="76"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="76"] .progress-left .progress-bar {
  animation: loading-26 0.5s linear forwards 0.5s;
}
.progress[data-percentage="77"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="77"] .progress-left .progress-bar {
  animation: loading-27 0.5s linear forwards 0.5s;
}
.progress[data-percentage="78"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="78"] .progress-left .progress-bar {
  animation: loading-28 0.5s linear forwards 0.5s;
}
.progress[data-percentage="79"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="79"] .progress-left .progress-bar {
  animation: loading-29 0.5s linear forwards 0.5s;
}
.progress[data-percentage="80"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="80"] .progress-left .progress-bar {
  animation: loading-30 0.5s linear forwards 0.5s;
}
.progress[data-percentage="81"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="81"] .progress-left .progress-bar {
  animation: loading-31 0.5s linear forwards 0.5s;
}
.progress[data-percentage="82"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="82"] .progress-left .progress-bar {
  animation: loading-32 0.5s linear forwards 0.5s;
}
.progress[data-percentage="83"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="83"] .progress-left .progress-bar {
  animation: loading-33 0.5s linear forwards 0.5s;
}
.progress[data-percentage="84"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="84"] .progress-left .progress-bar {
  animation: loading-34 0.5s linear forwards 0.5s;
}
.progress[data-percentage="85"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="85"] .progress-left .progress-bar {
  animation: loading-35 0.5s linear forwards 0.5s;
}
.progress[data-percentage="86"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="86"] .progress-left .progress-bar {
  animation: loading-36 0.5s linear forwards 0.5s;
}
.progress[data-percentage="87"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="87"] .progress-left .progress-bar {
  animation: loading-37 0.5s linear forwards 0.5s;
}
.progress[data-percentage="88"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="88"] .progress-left .progress-bar {
  animation: loading-38 0.5s linear forwards 0.5s;
}
.progress[data-percentage="89"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="89"] .progress-left .progress-bar {
  animation: loading-39 0.5s linear forwards 0.5s;
}
.progress[data-percentage="90"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="90"] .progress-left .progress-bar {
  animation: loading-40 0.5s linear forwards 0.5s;
}
.progress[data-percentage="91"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="91"] .progress-left .progress-bar {
  animation: loading-41 0.5s linear forwards 0.5s;
}
.progress[data-percentage="92"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="92"] .progress-left .progress-bar {
  animation: loading-42 0.5s linear forwards 0.5s;
}
.progress[data-percentage="93"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="93"] .progress-left .progress-bar {
  animation: loading-43 0.5s linear forwards 0.5s;
}
.progress[data-percentage="94"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="94"] .progress-left .progress-bar {
  animation: loading-44 0.5s linear forwards 0.5s;
}
.progress[data-percentage="95"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="95"] .progress-left .progress-bar {
  animation: loading-45 0.5s linear forwards 0.5s;
}
.progress[data-percentage="96"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="96"] .progress-left .progress-bar {
  animation: loading-46 0.5s linear forwards 0.5s;
}
.progress[data-percentage="97"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="97"] .progress-left .progress-bar {
  animation: loading-47 0.5s linear forwards 0.5s;
}
.progress[data-percentage="98"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="98"] .progress-left .progress-bar {
  animation: loading-48 0.5s linear forwards 0.5s;
}
.progress[data-percentage="99"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="99"] .progress-left .progress-bar {
  animation: loading-49 0.5s linear forwards 0.5s;
}
.progress[data-percentage="100"] .progress-right .progress-bar {
  animation: loading-50 0.5s linear forwards;
}
.progress[data-percentage="100"] .progress-left .progress-bar {
  animation: loading-50 0.5s linear forwards 0.5s;
}
@keyframes loading-1 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(3.6);
    transform: rotate(3.6deg);
  }
}
@keyframes loading-2 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(7.2);
    transform: rotate(7.2deg);
  }
}
@keyframes loading-3 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(10.8);
    transform: rotate(10.8deg);
  }
}
@keyframes loading-4 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(14.4);
    transform: rotate(14.4deg);
  }
}
@keyframes loading-5 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(18);
    transform: rotate(18deg);
  }
}
@keyframes loading-6 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(21.6);
    transform: rotate(21.6deg);
  }
}
@keyframes loading-7 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(25.2);
    transform: rotate(25.2deg);
  }
}
@keyframes loading-8 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(28.8);
    transform: rotate(28.8deg);
  }
}
@keyframes loading-9 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(32.4);
    transform: rotate(32.4deg);
  }
}
@keyframes loading-10 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(36);
    transform: rotate(36deg);
  }
}
@keyframes loading-11 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(39.6);
    transform: rotate(39.6deg);
  }
}
@keyframes loading-12 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(43.2);
    transform: rotate(43.2deg);
  }
}
@keyframes loading-13 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(46.8);
    transform: rotate(46.8deg);
  }
}
@keyframes loading-14 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(50.4);
    transform: rotate(50.4deg);
  }
}
@keyframes loading-15 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(54);
    transform: rotate(54deg);
  }
}
@keyframes loading-16 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(57.6);
    transform: rotate(57.6deg);
  }
}
@keyframes loading-17 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(61.2);
    transform: rotate(61.2deg);
  }
}
@keyframes loading-18 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(64.8);
    transform: rotate(64.8deg);
  }
}
@keyframes loading-19 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(68.4);
    transform: rotate(68.4deg);
  }
}
@keyframes loading-20 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(72);
    transform: rotate(72deg);
  }
}
@keyframes loading-21 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(75.6);
    transform: rotate(75.6deg);
  }
}
@keyframes loading-22 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(79.2);
    transform: rotate(79.2deg);
  }
}
@keyframes loading-23 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(82.8);
    transform: rotate(82.8deg);
  }
}
@keyframes loading-24 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(86.4);
    transform: rotate(86.4deg);
  }
}
@keyframes loading-25 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(90);
    transform: rotate(90deg);
  }
}
@keyframes loading-26 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(93.6);
    transform: rotate(93.6deg);
  }
}
@keyframes loading-27 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(97.2);
    transform: rotate(97.2deg);
  }
}
@keyframes loading-28 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(100.8);
    transform: rotate(100.8deg);
  }
}
@keyframes loading-29 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(104.4);
    transform: rotate(104.4deg);
  }
}
@keyframes loading-30 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(108);
    transform: rotate(108deg);
  }
}
@keyframes loading-31 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(111.6);
    transform: rotate(111.6deg);
  }
}
@keyframes loading-32 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(115.2);
    transform: rotate(115.2deg);
  }
}
@keyframes loading-33 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(118.8);
    transform: rotate(118.8deg);
  }
}
@keyframes loading-34 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(122.4);
    transform: rotate(122.4deg);
  }
}
@keyframes loading-35 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(126);
    transform: rotate(126deg);
  }
}
@keyframes loading-36 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(129.6);
    transform: rotate(129.6deg);
  }
}
@keyframes loading-37 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(133.2);
    transform: rotate(133.2deg);
  }
}
@keyframes loading-38 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(136.8);
    transform: rotate(136.8deg);
  }
}
@keyframes loading-39 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(140.4);
    transform: rotate(140.4deg);
  }
}
@keyframes loading-40 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(144);
    transform: rotate(144deg);
  }
}
@keyframes loading-41 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(147.6);
    transform: rotate(147.6deg);
  }
}
@keyframes loading-42 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(151.2);
    transform: rotate(151.2deg);
  }
}
@keyframes loading-43 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(154.8);
    transform: rotate(154.8deg);
  }
}
@keyframes loading-44 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(158.4);
    transform: rotate(158.4deg);
  }
}
@keyframes loading-45 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(162);
    transform: rotate(162deg);
  }
}
@keyframes loading-46 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(165.6);
    transform: rotate(165.6deg);
  }
}
@keyframes loading-47 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(169.2);
    transform: rotate(169.2deg);
  }
}
@keyframes loading-48 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(172.8);
    transform: rotate(172.8deg);
  }
}
@keyframes loading-49 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(176.4);
    transform: rotate(176.4deg);
  }
}
@keyframes loading-50 {
  0% {
    -webkit-transform: rotate(0deg);
    transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(180);
    transform: rotate(180deg);
  }
}

.list-group.priority{
 margin-top: 5px;
}
.list-group.priority .list-group-item{
     margin-top: 10px;
    border: 0;
    text-align: end;
}

.vertical-heighest-first {
       height: 140px;
    width: 120px !important;

}





input[type=range]::-moz-range-thumb {
  width: 42px;
  height: 42px;
  background: #ffffff;
  border: 1.8px solid #ffffff;
  border-radius: 49px;
  cursor: pointer;
}


input[type=range] {
  width: 100%;
  margin: 5.75px 0;
  background-color: transparent;
  -webkit-appearance: none;
}
input[type=range]:focus {
  outline: none;
}
input[type=range]::-webkit-slider-runnable-track {
   background: #d1d3e2;
  border: 0;
  border-radius: 25px;
  width: 100%;
  height: 11.5px;
  cursor: pointer;
}

input[type=range]::-webkit-slider-thumb {
  margin-top: -5.75px;
  width: 23px;
  height: 23px;
  background: #334576;
  border: 10px solid #334576;
  border-radius: 50px;
  cursor: pointer;
  -webkit-appearance: none;
}

input[type=range]::-moz-range-track {
  background: rgba(255, 113, 169, 0.78);
  border: 0;
  border-radius: 25px;
  width: 100%;
  height: 11.5px;
  cursor: pointer;
}
input[type=range]::-moz-range-thumb {
  width: 23px;
  height: 23px;
  background: #878aff;
  border: 10px solid #878aff;
  border-radius: 50px;
  cursor: pointer;
}
input[type=range]::-ms-track {
  background: transparent;
  border-color: transparent;
  border-width: 5.75px 0;
  color: transparent;
  width: 100%;
  height: 11.5px;
  cursor: pointer;
}
input[type=range]::-ms-fill-lower {
  background: #ff71a9;
  border: 0;
  border-radius: 50px;
}
input[type=range]::-ms-fill-upper {
  background: rgba(255, 113, 169, 0.78);
  border: 0;
  border-radius: 50px;
}
input[type=range]::-ms-thumb {
  width: 23px;
  height: 23px;
  background: #878aff;
  border: 10px solid #878aff;
  border-radius: 50px;
  cursor: pointer;
  margin-top: 0px;
  /*Needed to keep the Edge thumb centred*/
}
input[type=range]:focus::-ms-fill-lower {
  background: rgba(255, 113, 169, 0.78);
}
input[type=range]:focus::-ms-fill-upper {
  background: #ff71a9;
}
/*TODO: Use one of the selectors from https://stackoverflow.com/a/20541859/7077589 and figure out
how to remove the virtical space around the range input in IE*/
@supports (-ms-ime-align:auto) {
  /* Pre-Chromium Edge only styles, selector taken from hhttps://stackoverflow.com/a/32202953/7077589 */
  input[type=range] {
    margin: 0;
    /*Edge starts the margin from the thumb, not the track as other browsers do*/
  }
}

.progress.percentage .progress-bar{
  color: #E64B9A;

}
.progress.percentage .progress-value{
  background:#596995;
  color: #fff;
}

.progress-value .percentage {
    position: absolute;
    width: 100%;
    margin-bottom: 10px;
}
     .progress-value .status {
    position: relative;
    padding-top: 1rem;
    font-size: .85rem;
    width: 100%;
}



.progress.percentage.status-1 .progress-bar{
  color:rgba(209, 211, 226, .8);
}
.progress.percentage.status-1 .progress-value{
  background:rgba(155, 89, 182, 1);
}

.progress.percentage.status-2 .progress-bar{
  color:rgba(209, 211, 226, .8);
}
.progress.percentage.status-2 .progress-value{
  background:rgba(26, 188, 156, 1);
}

.progress.percentage.status-3 .progress-bar{
  color:rgba(209, 211, 226, .8);
}
.progress.percentage.status-3 .progress-value{
  background:rgba(241, 196, 15, 1);
}

.progress.percentage.status-4 .progress-bar{
  color:rgba(209, 211, 226, .8);
}
.progress.percentage.status-4 .progress-value{
  background:rgba(214, 4, 4, 1);
}

.progress.percentage.status-5 .progress-bar{
  color:rgba(209, 211, 226, .8);
}
.progress.percentage.status-5 .progress-value{
  background:rgba(42, 157, 244, 1);
}

.progress.percentage.status-6 .progress-bar{
  color:rgba(209, 211, 226, .8);
}
.progress.percentage.status-6 .progress-value{
  background:rgba(65, 119, 116, 1);
}
</style>
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_epic_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpic" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrEpic->getCreatedBy()).'  on '.$hrEpic->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_epic_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpic" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_epic_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpic" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrEpic->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrEpic->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '


  <div class="row">

  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
 
    <form id="hr_epic_form" name="hr_epic_form" method="post" onsubmit="return false" class="form-horizontal info-panel">

      <fieldset>

      <div class="col-lg-12 container title">Hr Epic</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_epic" name="is_hr_epic" value="'.$hrEpic->getInitialState().'">
         <input type="hidden" id="hr_epic_id" name="hr_epic_id" maxlength="22" value="'.$hrEpic->getId().'">
         <input type="hidden" id="hr_epic_id_employee" name="hr_epic_id_employee" maxlength="22" value="'.$hrEpic->getIdEmployee().'">
         <input type="hidden" id="hr_epic_id_position" name="hr_epic_id_position" maxlength="22" value="'.$hrEpic->getIdPosition().'">
         <input type="hidden" id="hr_epic_status_date" name="hr_epic_status_date" maxlength="22" value="'.$hrEpic->getStatusDate().'">

      </div>';
  foreach (HrEpicStatus::selectOptions($tfs) as $row) {
    $html.= '<div class="col-lg-2 container text-center">
              <label class="form-check-label" for="">'.$row["option"].'</label>
              <input id="hr_epic_id_epic_status" name="hr_epic_id_epic_status" class="hr_epic_id_epic_status option-input radio" type="radio" value="'.$row["value"].'" >
              <label for="" class="error"> </label>
            </div>';
  }
  $html.= '<div class="col-lg-12 h-100 container">
        <div class="col-lg-7 container">
         <label for="hr_epic_description" class="control-label">Activity:</label>
         <textarea id="hr_epic_description" name="hr_epic_description" class="hr_epic_description form-control"  maxlength="200" rows="5" tabindex="3" >
            '.$hrEpic->getDescription().'
         </textarea>
        <label for="hr_epic_description" class="error">'.$hrEpic->getAttrError("description").'</label>
        </div>
        <div class="col-lg-3 my-auto container form-group">
          <div class="col-lg-7">
            <div style="height: 150px;">
              <div class="h-100 d-inline-block">
                <form class="range-field w-50">
                  <input id="hr_epic_id_epic_priority" name="hr_epic_id_epic_priority" class="hr_epic_id_epic_priority border-0 vertical-heighest-first" type="range"tabindex="2" min="1" max="3" value="'.$hrEpic->getIdEpicPriority().'"/>
                </form>
              </div>
            </div>
          </div>
          <div class="col-lg-5">  
            <ul class="list-group priority">
              <li class="list-group-item text-left">Alta</li>
              <li class="list-group-item text-left">Media</li>
              <li class="list-group-item text-left">Baja</li>
            </ul> 
          </div>  
          <label for="" class="error"></label>     
      </div>
      <div class="col-lg-2 container form-group">
        <div class="progress percentage status-1" data-percentage="74">
            <span class="progress-left">
              <span class="progress-bar"></span>
            </span>
            <span class="progress-right">
              <span class="progress-bar"></span>
            </span>
            <div class="progress-value">
                
                <div class="percentage">74%</div>
                <div class="status">completed</div>
            </div>
          </div>
      </div>  
     


      <div class="col-lg-3 container">
       <label for="hr_epic_estimated_start_date" class="control-label">Estimated Start:</label>
        <input type="text" id="hr_epic_estimated_start_date" name="hr_epic_estimated_start_date" class="hr_epic_estimated_start_date form-control"  maxlength="22"  value="'.$hrEpic->getEstimatedStartDate().'"  tabindex="4"/>
      <label for="hr_epic_estimated_start_date" class="error">'.$hrEpic->getAttrError("estimated_start_date").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="hr_epic_start_date" class="control-label">Start Date:</label>
        <input type="text" id="hr_epic_start_date" name="hr_epic_start_date" class="hr_epic_start_date form-control"  maxlength="22"  value="'.$hrEpic->getStartDate().'"  tabindex="5"/>
      <label for="hr_epic_start_date" class="error">'.$hrEpic->getAttrError("start_date").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="hr_epic_completion_estimated_date" class="control-label">Completion Estimated:</label>
        <input type="text" id="hr_epic_completion_estimated_date" name="hr_epic_completion_estimated_date" class="hr_epic_completion_estimated_date form-control"  maxlength="22"  value="'.$hrEpic->getCompletionEstimatedDate().'"  tabindex="6"/>
      <label for="hr_epic_completion_estimated_date" class="error">'.$hrEpic->getAttrError("completion_estimated_date").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="hr_epic_completion_date" class="control-label">Completion Date:</label>
        <input type="text" id="hr_epic_completion_date" name="hr_epic_completion_date" class="hr_epic_completion_date form-control"  maxlength="22"  value="'.$hrEpic->getCompletionDate().'"  tabindex="7"/>
      <label for="hr_epic_completion_date" class="error">'.$hrEpic->getAttrError("completion_date").'</label>
      </div>'.$audit.'

   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrEpicRules(){
  $("#hr_epic_form").validate();
  $("#hr_epic_id_employee").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_epic_id_position").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_epic_description").rules("add", {
    required:true,
    maxlength:200
  });
  $("#hr_epic_estimated_start_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#hr_epic_start_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_epic_completion_estimated_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#hr_epic_completion_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_epic_id_epic_priority").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_epic_id_epic_status").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_epic_status_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });


}


$("#hr_epic_estimated_start_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#hr_epic_start_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#hr_epic_completion_estimated_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#hr_epic_completion_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});


$(document).ready(function(){
  hrEpicRules();

  $("#hr_epic_estimated_start_date").css('vertical-align','top');
  $("#hr_epic_estimated_start_date").mask('y999-m9-d9');
  $("#hr_epic_start_date").css('vertical-align','top');
  $("#hr_epic_start_date").mask('y999-m9-d9');
  $("#hr_epic_completion_estimated_date").css('vertical-align','top');
  $("#hr_epic_completion_estimated_date").mask('y999-m9-d9');
  $("#hr_epic_completion_date").css('vertical-align','top');
  $("#hr_epic_completion_date").mask('y999-m9-d9');


})
</script>