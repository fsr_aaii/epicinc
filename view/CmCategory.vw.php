<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_category_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="mCategory" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$cmCategory->getCreatedBy()).'  on '.$cmCategory->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_category_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmCategory" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_category_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmCategory" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($cmCategory->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($cmCategory->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="cm_category_form" name="cm_category_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Cm Category</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_cm_category" name="is_cm_category" value="'.$cmCategory->getInitialState().'">
         <input type="hidden" id="cm_category_id" name="cm_category_id" maxlength="22" value="'.$cmCategory->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="cm_category_name" class="control-label">Name:</label>
        <input type="text" id="cm_category_name" name="cm_category_name" class="cm_category_name form-control"  maxlength="200"  value="'.$cmCategory->getName().'"  tabindex="1"/>
      <label for="cm_category_name" class="error">'.$cmCategory->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="cm_category_active" class="control-label">Active:</label>
        <select  id="cm_category_active" name="cm_category_active" class="cm_category_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($cmCategory->getActive(),'Y').
'      </select>
      <label for="cm_category_active" class="error">'.$cmCategory->getAttrError("active").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function cmCategoryRules(){
  $("#cm_category_form").validate();
  $("#cm_category_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#cm_category_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  cmCategoryRules();


})
</script>