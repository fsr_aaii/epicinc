<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_position_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPosition" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrPosition->getCreatedBy()).'  on '.$hrPosition->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_position_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPosition" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_position_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPosition" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrPosition->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrPosition->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="hr_position_form" name="hr_position_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr Position</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_position" name="is_hr_position" value="'.$hrPosition->getInitialState().'">
         <input type="hidden" id="hr_position_id" name="hr_position_id" maxlength="22" value="'.$hrPosition->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_position_id_cost_center" class="control-label">Id Cost Center:</label>
        <input type="text" id="hr_position_id_cost_center" name="hr_position_id_cost_center" class="hr_position_id_cost_center form-control"  maxlength="22"  value="'.$hrPosition->getIdCostCenter().'"  tabindex="1"/>
      <label for="hr_position_id_cost_center" class="error">'.$hrPosition->getAttrError("id_cost_center").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_position_id_business_title" class="control-label">Id Business Title:</label>
        <input type="text" id="hr_position_id_business_title" name="hr_position_id_business_title" class="hr_position_id_business_title form-control"  maxlength="22"  value="'.$hrPosition->getIdBusinessTitle().'"  tabindex="2"/>
      <label for="hr_position_id_business_title" class="error">'.$hrPosition->getAttrError("id_business_title").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_position_criticality" class="control-label">Criticality:</label>
        <input type="text" id="hr_position_criticality" name="hr_position_criticality" class="hr_position_criticality form-control"  maxlength="22"  value="'.$hrPosition->getCriticality().'"  tabindex="3"/>
      <label for="hr_position_criticality" class="error">'.$hrPosition->getAttrError("criticality").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_position_leadership_bench" class="control-label">Leadership Bench:</label>
        <input type="text" id="hr_position_leadership_bench" name="hr_position_leadership_bench" class="hr_position_leadership_bench form-control"  maxlength="22"  value="'.$hrPosition->getLeadershipBench().'"  tabindex="4"/>
      <label for="hr_position_leadership_bench" class="error">'.$hrPosition->getAttrError("leadership_bench").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_position_evaluation_date" class="control-label">Evaluation Date:</label>
        <input type="text" id="hr_position_evaluation_date" name="hr_position_evaluation_date" class="hr_position_evaluation_date form-control"  maxlength="22"  value="'.$hrPosition->getEvaluationDate().'"  tabindex="5"/>
      <label for="hr_position_evaluation_date" class="error">'.$hrPosition->getAttrError("evaluation_date").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrPositionRules(){
  $("#hr_position_form").validate();
  $("#hr_position_id_cost_center").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_position_id_business_title").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_position_criticality").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_position_leadership_bench").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_position_evaluation_date").rules("add", {
    isDate:true,
    maxlength:22
  });

}

$("#hr_position_evaluation_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
  hrPositionRules();

  $("#hr_position_evaluation_date").css('vertical-align','top');
  $("#hr_position_evaluation_date").mask('y999-m9-d9');

})
</script>