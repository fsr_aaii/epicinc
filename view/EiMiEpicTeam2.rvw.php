<style type="text/css">

.badge.c3-3{
  color: #fff;
    background-color:#FF7F27;
}

.badge.c3-2{
  color: #fff;
    background-color:#FBE12B;
}
.badge.c3-1{
  color: #fff;
    background-color:#A7C85B;
}
.badge.c3-{
  color: #fff;
    background-color:#858796;
}



</style>
<?php

$html=' <!-- Begin dynamic content --> 
<div class="card shadow mb-4">
  <div class="col-lg-12 my-epic-profile  col-container">    
    <div class="row">      
      <div class="col-lg-12 form-frame">   
        <!--begin team-->';
        
        $learningLevel= Array('Deep'=>'It <b>refers to a tendency or strong preference or fixed type of situation</b>, actions or knowledge this person <b>is able to interact and perform well</b>.It speaks about a type of behaviours in which a person <b>could express fixed and limited preferences on challenges</b>',
                   'Balanced'=>'It <b>refers to wider perspective on situations in</b> which a person can interact and perform well, but still has some situations that are not fully likely to work well.Limitations might be known, that <b>person might be stretching him/herself</b> to expand areas of impact.It might also be also a natural style.',
                   'Broad'=>'It <b>refers to a person who is able to navigate well</b> a variety of situations, and <b>still interact effectively and perform well</b>.<br>The tendency is even to proactively look for situations that are requiring to take that intelligence into another level.There is a tendency towards <b>curiosity for diverse challenges with resilience</b>.<br><b>It could be natural or adapted.</b>');

        $html2=''; 

       
        $quarter=array_fill(1,4,array());
        $currentQuarter=HrEmployee::currentQuarter($tfs);

        foreach  ($quarter as $key => $value) {
           if ($key<$currentQuarter){
            $quarter[$key]["tab"]='ro';
           }elseif ($key>$currentQuarter) {
             $quarter[$key]["tab"]='disabled'; 
           }else{
             $quarter[$key]["tab"]='enable';
             $quarter[$key]["pane"]='active show';
             $quarter[$key]["accordion"]='active';
           } 
        }  

        foreach ($hrEmployeeList as $row) {
          $tfData = array();
          $tfData["hr_employee_id"] = $row["id_employee"];
          $tfData["gl_natural_person_id"] = $row["id_person"];
          $tenure='';
          $in_role='';

          if ($row["tenure_year"]>0){
            $tenure.=$row["tenure_year"].'y ';
            $month = $row["tenure_month"]-($row["tenure_year"]*12);
            $tenure.=$month.'m';
          }elseif ($row["tenure_month"]>0) {
            $tenure.=$row["tenure_month"].'m';
          }elseif ($row["tenure_day"]>0) {
            $tenure.=$row["tenure_day"].'d';
          }else{
            $tenure.='NYA';
          }
          if ($row["in_role_year"]>0){
            $in_role.=$row["in_role_year"].'y ';
            $month = $row["in_role_month"]-($row["in_role_year"]*12);
            $in_role.=$month.'m';
          }elseif ($row["in_role_month"]>0) {
            $in_role.=$row["in_role_month"].'m';
          }elseif ($row["in_role_day"]>0) {
            $in_role.=$row["in_role_day"].'d';
          }else{
            $in_role.='NYA';
          }
           
          $health=array();
          if($row["criticality_level"]=='Critical' and $row["is_key_talent"]!='Y' ){
            $health[]='Search for a Key Talent to fill this position.';
          }
          if($row["criticality_level"]=='Critical' and $row["is_key_talent"]=='Y' and  in_array($row["resignation_risk"],array("M","H"))){
            $health[]='Talk to your team member to reduce the flight risk and ensure that you have a successor for the position.';
          }
          if($row["criticality_level"]=='Critical' and $row["has_successor"]!='Y' ){
            $health[]='Look for successors or develop successors.';
          }
          if($row["criticality_level"]!='Critical' and $row["is_key_talent"]=='Y' ){
            $health[]='Try to move this team member to a critical position.';
          }
          if($row["learning_agility_level"]=='Deep'){
            $health[]='Develop this employee for a position in the same area or develop the people agility in the employee if he/she will become a people manager.';
          }
          if($row["learning_agility_level"]=='Broad'){
            $health[]='Develop this employee for a leadership position.';
          }

          if($row["development_moment"]=='Preparation/Expansion'){
            $health[]='Assign a learning experience to close the gaps. Remember to create a growth plan based on the 70.20.10 learning model.';
          }
          if($row["development_moment"]=='Ready'){
            $health[]='Make sure the employee has a promotion, lateral move, assignment or any other growth action.';
          }
          if($row["performace_level"]=='Exceed' and  in_array($row["resignation_risk"],array("M","H"))){
            $health[]='Talk to your employee and try to reduce the flight risk.';
          }
          if($row["development_moment"]=='Learning'){
            $health[]='Make sure your employee has defined SMART goals and has a GROWTH PLAN in order to reduce the learning curve.';
          }
          $health_html='';
          foreach ($health as $r){
            $health_html='<p class="dashboard-list">'.$r.'<p>';
          }  
   
          $managerLAGroupDetails=HrEmployeeEvaluation::groupDetails($tfs,$row["id_employee"],$row["learning_agility_date"],'M'); 
          $employeeLAGroupDetails=HrEmployeeEvaluation::groupDetails($tfs,$row["id_employee"],$row["self_learning_agility_date"],'E'); 
          
          $go=false;
          $total=0;
          $qty=0;
          $chartLabels="";
          $chartData="";
          foreach ($managerLAGroupDetails as $r){
            $total+=$r["total"];
            $qty+=$r["qty"];
            $chartLabels.="'".$r["name"]."',"; 
            $chartData.="'".round($r["total"]/$r["qty"],2)."',"; 
          } 
          
          $datasets="";
          if ($qty!=0){
            $labels=$chartLabels;
            $datasets.="{label: 'Manager',
                              backgroundColor: 'rgba(69, 116, 210, 0.5)',
                              borderColor: 'rgba(69, 116, 210, 0.8)',
                              pointBackgroundColor: 'rgba(69, 116, 210, 1.0)',
                              data: [".$chartData."]},"; 
            $go=true;                  
          }
          
          if (count($managerLAGroupDetails)==count($employeeLAGroupDetails) OR !$go){
            $total=0;
            $qty=0;
            $chartLabels="";
            $chartData="";
            foreach ($employeeLAGroupDetails as $r){
              $total+=$r["total"];
              $qty+=$r["qty"];
              $chartLabels.="'".$r["name"]."',"; 
              $chartData.="'".round($r["total"]/$r["qty"],2)."',"; 
            }  

            if ($qty!=0){
              $labels=$chartLabels;
              $datasets.="{label: 'Employee',
                                backgroundColor: 'rgba(230, 75, 154, 0.5)',
                                borderColor: 'rgba(230, 75, 154, 0.8)',
                                pointBackgroundColor: 'rgba(230, 75, 154, 1.0)',
                                data: [".$chartData."]},"; 
              $go=true;                  
            }
          }  
          
          
          
          if ($go){            
            $script.="var ctx = document.getElementById('la_group_details_".$row["id_employee"]."').getContext('2d');

                       var myChart = new Chart(ctx, {
                        type: 'radar',
                        data: { labels: [".$labels."],
                                datasets: [".$datasets."]
                              },
                        options: {
                      legend: {
                        position: 'bottom',
                      },
                      title: {
                        display: false
                      },
                      scale: {
                        ticks: {
                          beginAtZero: true
                        }
                      }
                    }
                    });";
          }  

          
          $html.='<div id="epic-profile-'.$row["id_employee"].'" class="col-4 coaching" data-show-profile="team-epic-profile-'.$row["id_employee"].'" data-id-employee="'.$row["id_employee"].'">
            <div class="d-flex align-items-center">
                <div class="mr-2">
                    <img class="rounded-circle" width="45" src="'.$row["photo"].'" alt="">
                </div>
                <div class="ml-2">
                    <div class="epic h2 m-0">'.$row["name"].'</div>
                    <div class="epic h4 m-0 text-muted">'.$row["business_title"].'</div>
                    <div class="epic h6 m-0 text-muted">'.$row["email_account"].'</div>
                </div>
            </div>
          </div>'; 

          $html2.='<div id="team-epic-profile-'.$row["id_employee"].'" id="team-epic-profile-'.$row["id_employee"].'" class="col-lg-12 team-epic-profile col-container" style="display: none;">
                  <div class="row">
                    <div class="col-lg-2 col-container text-center">
                      <img class="img-profile rounded-circle" src="'.$row["photo"].'">
                    </div>
                    <div class="col-lg-3 data-col">
                      <div class="col-12 container">
                        <span class="name-profile">'.$row["name"].'</span> 
                      </div>
                      <div class="col-12 container">
                        <span class="business-title-profile">'.$row["business_title"].'</span>
                      </div>
                      <div class="col-lg-12 container">
                        <span class="data-text">'.$row["development_moment"].' to '.$row["job_level_next"].'</span>
                      </div>
                      <div class="col-lg-12 container">
                        <span class="data-text">Tenure: '.$tenure.' In Role: '.$in_role.'</span>
                      </div>
                      <div class="col-lg-12 container">
                        <span class="dashboard data-text far"> Flight Risk: '.$row["resignation_risk"].' Id: '.$row["id_employee"].'</span>
                      </div>
                    </div>
                    <div class="col-lg-2 container progress-circle text-center">
                      <div class="progress epic">
                        <span class="progress-left">
                          <span class="progress-bar empty"></span>
                          <span class="progress-bar lv3-'.$row["id_criticality_level"].' c3-'.$row["id_criticality_level"].'"></span>
                        </span>
                        <span class="progress-right">
                          <span class="progress-bar empty"></span>
                          <span class="progress-bar lv3-'.$row["id_criticality_level"].' c3-'.$row["id_criticality_level"].'"></span>
                        </span>
                        <div class="progress-value">
                          <p class="primary">'.$row["criticality_level"].'</p>
                          <p class="secondary"> </p>
                        </div>
                </div>
                      <div class="progress-label">Criticality</div>
                    </div>
                    <div class="col-lg-2 container progress-circle text-center">
                     <div class="progress epic">
                        <span class="progress-left">
                          <span class="progress-bar empty"></span>
                          <span class="progress-bar vs lv3-'.$row["id_self_performance_level"].' c3-'.$row["id_performance_level"].'"></span>
                          <span class="progress-bar lv3-'.$row["id_performance_level"].' c3-'.$row["id_performance_level"].'"></span>
                        </span>
                        <span class="progress-right">
                           <span class="progress-bar empty"></span>
                          <span class="progress-bar vs lv3-'.$row["id_self_performance_level"].' c3-'.$row["id_performance_level"].'"></span>
                          <span class="progress-bar lv3-'.$row["id_performance_level"].' c3-'.$row["id_performance_level"].'"></span>
                        </span>
                        <div class="progress-value">
                          <p class="primary">'.$row["performance_level"].'</p>
                          <p class="secondary">'.$row["self_performance_level"].'</p>
                        </div>
                      </div>
                      <div class="progress-label">Performace</div>
                    </div>
                    <div class="col-lg-2 container progress-circle text-center">
                      <div class="progress epic">
                        <span class="progress-left">
                          <span class="progress-bar empty"></span>
                          <span class="progress-bar vs lv3-'.$row["id_self_learning_agility_level"].' c3-'.$row["id_learning_agility_level"].'"></span>
                          <span class="progress-bar lv3-'.$row["id_learning_agility_level"].' c3-'.$row["id_learning_agility_level"].'"></span>
                          
                        </span>
                        <span class="progress-right">
                          <span class="progress-bar empty"></span>
                          <span class="progress-bar vs lv3-'.$row["id_self_learning_agility_level"].' c3-'.$row["id_learning_agility_level"].'"></span>
                          <span class="progress-bar lv3-'.$row["id_learning_agility_level"].' c3-'.$row["id_learning_agility_level"].'"></span>
                          
                        </span>
                        <div class="progress-value">
                          <p class="primary">'.$row["learning_agility_level"].'</p>
                          <p class="secondary">'.$row["self_learning_agility_level"].'</p>
                        </div>
                      </div>
                      <div class="progress-label">Learning Agility</div>
                    </div>
                    <div class="col-lg-1 data-col">

                      <div class="col-12 container">
                        <a class="btn btn-epic-2 xl rounded-circle" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployee" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'"onclick="TfRequest.do(this);">
                     <i class="bx bx-edit-alt"></i>
                   </a>
                       </div>
                     </div> 
                     <div class="row team-epic-profile-extra">      
                    <div class="col-lg-12 form-frame">
                      <div class="col-lg-6 container">
                        <h7 style="color: #334576;">'.$row["learning_agility_level"].'</h7>
                        <p>'.$learningLevel[$row["learning_agility_level"]].'</p>
                        <h7 style="color: #334576;">Health</h7>
                        '.$health_html.'
                      </div>
                      <div class="col-lg-6 container text-center">
                       <canvas id="la_group_details_'.$row["id_employee"].'" class="col-12" ></canvas>
                      </div>  
                    </div>
                  </div> 

                  <!---Begin tabs QUARTER-->
                  <div class="container tab-accordion">
                    <ul id="tabs" class="nav nav-tabs" role="tablist">
                        <li class="nav-item '.$quarter[1]["tab"].'">
                            <a id="tab-QI-'.$row["id_employee"].'" name="tab-QI-'.$row["id_employee"].'" href="#pane-QI-'.$row["id_employee"].'" class="nav-link quarter" data-toggle="tab" role="tab">Trimestre I - %</a>
                        </li>
                        <li class="nav-item '.$quarter[2]["tab"].'">
                            <a id="tab-QII-'.$row["id_employee"].'" href="#pane-QII-'.$row["id_employee"].'" class="nav-link quarter" data-toggle="tab" role="tab">Trimestre II - %</a>
                        </li>
                        <li class="nav-item '.$quarter[3]["tab"].'">
                            <a id="tab-QIII-'.$row["id_employee"].'" href="#pane-QIII-'.$row["id_employee"].'" class="nav-link quarter active" data-toggle="tab" role="tab">Trimestre III - %</a>
                        </li>
                        <li class="nav-item '.$quarter[4]["tab"].'" >
                            <a id="tab-QIV-'.$row["id_employee"].'" href="#pane-QIV-'.$row["id_employee"].'" class="nav-link quarter" data-toggle="tab" role="tab" >Trimestre IV </a>
                        </li>
                    </ul> 
                    <!---Begin tabs content-->
                    <div id="content-'.$row["id_employee"].'" class="tab-content" role="tablist">
                      <!---Begin tabs content QUARTER I-->
                      <div id="pane-QI-'.$row["id_employee"].'" class="card tab-pane fade '.$quarter[1]["pane"].'" role="tabpanel" aria-labelledby="tab-QI'.$row["id_employee"].'">
                          <div class="card-header" role="tab" id="heading-QI-'.$row["id_employee"].'">
                              <h5 class="mb-0">
                                 <a data-toggle="collapse" href="#collapse-QI-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-QI-'.$row["id_employee"].'">
                                      Trimestre I - %
                                  </a>
                              </h5>
                          </div>
                          <div id="collapse-QI-'.$row["id_employee"].'" class="collapse" data-parent="#content-'.$row["id_employee"].'" role="tabpanel" aria-labelledby="heading-QI-'.$row["id_employee"].'">
                             <div class="card-body">
                                <div class="row">';
          if ($quarter[1]["tab"]!='disabled'){
            $html2.='                 QI-'.$row["id_employee"];  
          }
          $html2.='             </div>
                             </div>     
                          </div>
                      </div>
                      <!---End tabs content QUARTER I--> 
                      <!---Begin tabs content QUARTER II-->
                      <div id="pane-QII-'.$row["id_employee"].'" class="card tab-pane fade '.$quarter[2]["pane"].'" role="tabpanel" aria-labelledby="tab-QII'.$row["id_employee"].'">
                          <div class="card-header" role="tab" id="heading-QII-'.$row["id_employee"].'">
                              <h5 class="mb-0">
                                 <a data-toggle="collapse" href="#collapse-QII-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-QII-'.$row["id_employee"].'">
                                      Trimestre II - %
                                  </a>
                              </h5>
                          </div>
                          <div id="collapse-QII-'.$row["id_employee"].'" class="collapse" data-parent="#content-'.$row["id_employee"].'" role="tabpanel" aria-labelledby="heading-QII-'.$row["id_employee"].'">
                             <div class="card-body">
                                <div class="row">';

          if ($quarter[2]["tab"]!='disabled'){
            $html2.='                 QII-'.$row["id_employee"];  
          }
          $html2.='             </div>
                             </div>     
                          </div>
                      </div>
                      <!---End tabs content QUARTER II-->  
                      <!---Begin tabs content QUARTER III-->
                      <div id="pane-QIII-'.$row["id_employee"].'" class="card tab-pane fade '.$quarter[3]["pane"].'" role="tabpanel" aria-labelledby="tab-QIII'.$row["id_employee"].'">
                          <div class="card-header" role="tab" id="heading-QIII-'.$row["id_employee"].'">
                              <h5 class="mb-0">
                                 <a data-toggle="collapse" href="#collapse-QIII-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-QIII-'.$row["id_employee"].'">
                                      Trimestre III - %
                                  </a>
                              </h5>
                          </div>
                          <div id="collapse-QIII-'.$row["id_employee"].'" class="collapse" data-parent="#content-'.$row["id_employee"].'" role="tabpanel" aria-labelledby="heading-QIII-'.$row["id_employee"].'">
                             <div class="card-body">
                                <div class="row">';
          if ($quarter[3]["tab"]!='disabled'){
            
            $criticalityEvaluationList=HrPositionEvaluation::listByPositionQuarter($tfs,$row["id_position"],3);
            $learningEvaluationList=HrEmployeeEvaluation::listByEmployeeQuarter($tfs,3,$row["id_employee"],'M',3);
            $performanceEvaluationList=HrEmployeeEvaluation::listByEmployeeQuarter($tfs,4,$row["id_employee"],'M',3);
            $epicList=HrEpic::listByEmployeeQuarter($tfs,$row["id_employee"],3);

            $tfData = array();
            $tfData["hr_position_evaluation_id_position"] = $row["id_position"];

            $html2.='                 <!--begin accordion-criticality-->
                                      <div id="accordion-criticality-'.$row["id_employee"].'" class="col-lg-12 container">
                                        <!--begin my-epic-profile card-->
                                        <div class="my-epic-profile card">
                                          <!--begin accordion-criticality-header-->
                                          <div class="col-12 card-header" id="heading-criticality-'.$row["id_employee"].'">
                                            <div class="col-11 container"> 
                                              <a id="accordion-criticality-a-'.$row["id_employee"].'" href="#" class="section '.$quarter[3]["accordion"].' collapsed" data-toggle="collapse" data-target="#collapse-criticality-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-criticality-'.$row["id_employee"].'" data-form-action="form-action-criticality-'.$row["id_employee"].'"> Evalua esta posicion</a>
                                            </div>
                                            <div class="col-1 container text-right">';
            if ($quarter[3]["accordion"]=='active'){                                
              $html2.='                       <a id="form-action-criticality-'.$row["id_employee"].'" name="form-action-criticality-'.$row["id_employee"].'" class="btn-epic-sm" alt="New" title="New" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPositionEvaluation" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-action="AI" onclick="TfRequest.do(this);" style="display: none;">            
                                                 <i class="bx bx-plus"></i>
                                                </a>';
            } 
            $html2.='                      </div>
                                          </div>
                                          <!--end accordion-criticality-header-->
                                          <!--begin accordion-criticality-content-->
                                          <div id="collapse-criticality-'.$row["id_employee"].'" class="collapsed collapse" aria-labelledby="heading-criticality-'.$row["id_employee"].'" data-parent="#accordion-criticality-'.$row["id_employee"].'">';  
              // BEGIN if count
              if (count($criticalityEvaluationList)==0){
                 $html2.='                 No existen evaluaciones en este trimestre';
              }else{
                $html2.='                  <!--Begin row epic-->
                                            <div class="row epic">
                                              <div class="col-lg-3 col-container text-left">
                                              </div>
                                              <div class="col-lg-4 col-container epic-text-sm text-left">
                                                Criticality
                                              </div> 
                                              <div class="col-lg-4 col-container epic-text-sm text-left">
                                                Leadership Bank
                                              </div>                              
                                              <div class="col-lg-1 col-container epic-text-sm text-right">
                                              </div>
                                          </div>
                                          <!--End row epic-->   ';     
                $i=0;
                // BEGIN foreach
                foreach ($criticalityEvaluationList as $c){
                  $i++;
                  $tfData = array();
                  $tfData["hr_position_evaluation_id"] = $c["id"];
                  $html2.='         <!--Begin row epic-->
                                    <div class="row epic">
                                      <div class="col-lg-3 col-container text-left">
                                        <div class="h7 m-0">#'.$i.' Test</div>
                                        <div class="text-muted h6">'.$c["created_date"].'</div>
                                      </div>
                                      <div class="col-lg-4 col-container text-left">
                                        <div class="epic-bottom">
                                          <span class="badge badge-pill badge-secondary">'.$c["criticality"].'</span>
                                          <span class="badge badge-pill c3-'.$c["id_criticality_level"].'">'.$c["criticality_level"].'</span>
                                        </div>
                                      </div>                  
                                      <div class="col-lg-4 col-container text-left">
                                        <div class="epic-bottom">
                                          <span class="badge badge-pill badge-secondary">'.$c["leadership"].'</span>
                                          <span class="badge badge-pill c3-'.$c["id_leadership_level"].'">'.$c["leadership_level"].'</span>
                                        </div>
                                      </div>             
                                      <div class="col-lg-1 col-container text-right">';
                        // BEGIN if quarter
                        if ($quarter[3]["accordion"]=='active'){                                
                           $html2.='              <a class="btn-epic-sm" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPositionEvaluation" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).' "onclick="TfRequest.do(this);">
                                 <i class="bx bx-pencil"></i>
                               </a>';
                        }  
                        // END if quarter
                        $html2.='               </div>
                                            </div>
                                            <!--End row epic-->';
                }   
                // END foreach                           
              }    
              // END if count
              $html2.='                    </div>
                                          <!--end accordion-criticality-content-->
                                        </div>
                                        <!--end my-epic-profile card-->
                                      </div>
                                      <!--end accordion-criticality-->';   

 

              $html2.='                 <!--begin accordion-learning-->
                                      <div id="accordion-learning-'.$row["id_employee"].'" class="col-lg-12 container">
                                        <div class="my-epic-profile card">
                                          <!--begin accordion-learning-header-->
                                          <div class="col-12 card-header" id="heading-learning-'.$row["id_employee"].'">
                                            <div class="col-11 container"> 
                                              <a id="accordion-learning-a-'.$row["id_employee"].'" href="#" class="section '.$quarter[3]["accordion"].' collapsed" data-toggle="collapse" data-target="#collapse-learning-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-learning-'.$row["id_employee"].'" data-form-action="form-action-learning-'.$row["id_employee"].'"> Evalua su learning</a>
                                            </div>
                                            <div class="col-1 container text-right">';
            if ($quarter[3]["accordion"]=='active'){     
              $tfData = array();
              $tfData["hr_employee_evaluation_id_employee"] = $row["id_employee"];    
              $tfData["hr_employee_evaluation_id_position"] = $row["id_position"];   
              $tfData["hr_employee_evaluation_id_evaluation_type"] = 3;     
              $tfData["hr_employee_evaluation_evaluator_type"] = 'M';                            
              $html2.='                       <a id="form-action-learning-'.$row["id_employee"].'" name="form-action-learning-'.$row["id_employee"].'" class="btn-epic-sm" alt="New" title="New" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-action="AI" onclick="TfRequest.do(this);" style="display: none;">            
                                               <i class="bx bx-plus"></i>
                                              </a>';
            } 
            $html2.='                      </div>
                                          </div>
                                          <!--end accordion-learning-header-->
                                          <!--begin accordion-learning-content-->
                                          <div id="collapse-learning-'.$row["id_employee"].'" class="collapsed collapse" aria-labelledby="heading-learning-'.$row["id_employee"].'" data-parent="#accordion-learning-'.$row["id_employee"].'">';  
              if (count($learningEvaluationList)==0){
                 $html2.='                 No existen evaluaciones en este trimestre';
              }else{
                $html2.='                 <div class="row epic">
                                              <div class="col-lg-3 col-container text-left">
                                              </div>
                                              <div class="col-lg-8 col-container epic-text-sm text-left">
                                                Learning Agility
                                              </div>                             
                                              <div class="col-lg-1 col-container epic-text-sm text-right">
                                              </div>
                                          </div>    ';     
                $i=0;
                foreach ($learningEvaluationList as $c){
                $i++;
                $tfData = array();
                $tfData["hr_employee_evaluation_id"] = $c["id"];
                $html2.='           <div class="row epic">
                                     <div class="col-lg-3 col-container text-left">
                                      <div class="h7 m-0">#'.$i.' Test</div>
                                      <div class="text-muted h6">'.$c["created_date"].'</div>
                                    </div>
                                    <div class="col-lg-8 col-container text-left">
                                      <div class="epic-bottom">
                                        <span class="badge badge-pill badge-secondary">'.$c["result"].'</span>
                                        <span class="badge badge-pill c3-'.$c["id_level"].'">'.$c["level"].'</span>
                                      </div>
                                    </div>              
                                    <div class="col-lg-1 col-container text-right">';
                  if ($quarter[3]["accordion"]=='active'){                                
                    $html2.='              <a class="btn-epic-sm" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).' "onclick="TfRequest.do(this);">
                     <i class="bx bx-pencil"></i>
                   </a>';
                  }         
                  $html2.='         </div> 
                                </div>';
                }                

              }                           
              $html2.='                   </div>
                                          <!--end accordion-learning-content-->
                                        </div>
                                      </div>
                                      <!--end accordion-learning-->';    

              $html2.='                 <!--begin accordion-performance-->
                                      <div id="accordion-performance-'.$row["id_employee"].'" class="col-lg-12 container">
                                        <div class="my-epic-profile card">
                                          <!--begin accordion-performance-header-->
                                          <div class="col-12 card-header" id="heading-performance-'.$row["id_employee"].'">
                                            <div class="col-11 container"> 
                                              <a id="accordion-performance-a-'.$row["id_employee"].'" href="#" class="section '.$quarter[3]["accordion"].' collapsed" data-toggle="collapse" data-target="#collapse-performance-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-performance-'.$row["id_employee"].'" data-form-action="form-action-performance-'.$row["id_employee"].'"> Evalua su performance</a>
                                            </div>
                                            <div class="col-1 container text-right">';
            if ($quarter[3]["accordion"]=='active'){     
              $tfData = array();
              $tfData["hr_employee_evaluation_id_employee"] = $row["id_employee"];    
              $tfData["hr_employee_evaluation_id_position"] = $row["id_position"];   
              $tfData["hr_employee_evaluation_id_evaluation_type"] = 4;     
              $tfData["hr_employee_evaluation_evaluator_type"] = 'M';                            
              $html2.='                       <a id="form-action-performance-'.$row["id_employee"].'" name="form-action-performance-'.$row["id_employee"].'" class="btn-epic-sm" alt="New" title="New" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-action="AI" onclick="TfRequest.do(this);" style="display: none;">            
                                               <i class="bx bx-plus"></i>
                                              </a>';
            } 
            $html2.='                      </div>
                                          </div>
                                          <!--end accordion-performance-header-->
                                          <!--begin accordion-performance-content-->
                                          <div id="collapse-performance-'.$row["id_employee"].'" class="collapsed collapse" aria-labelledby="heading-performance-'.$row["id_employee"].'" data-parent="#accordion-performance-'.$row["id_employee"].'">';  
              if (count($performanceEvaluationList)==0){
                 $html2.='                 No existen evaluaciones en este trimestre';
              }else{
                $html2.='                 <div class="row epic">
                                              <div class="col-lg-3 col-container text-left">
                                              </div>
                                              <div class="col-lg-8 col-container epic-text-sm text-left">
                                                performance
                                              </div>                             
                                              <div class="col-lg-1 col-container epic-text-sm text-right">
                                              </div>
                                          </div>    ';     
                $i=0;
                foreach ($performanceEvaluationList as $c){
                $i++;
                $tfData = array();
                $tfData["hr_employee_evaluation_id"] = $c["id"];
                $html2.='           <div class="row epic">
                                     <div class="col-lg-3 col-container text-left">
                                      <div class="h7 m-0">#'.$i.' Test</div>
                                      <div class="text-muted h6">'.$c["created_date"].'</div>
                                    </div>
                                    <div class="col-lg-8 col-container text-left">
                                      <div class="epic-bottom">
                                        <span class="badge badge-pill badge-secondary">'.$c["result"].'</span>
                                        <span class="badge badge-pill c3-'.$c["id_level"].'">'.$c["level"].'</span>
                                      </div>
                                    </div>              
                                    <div class="col-lg-1 col-container text-right">';
                  if ($quarter[3]["accordion"]=='active'){                                
                    $html2.='              <a class="btn-epic-sm" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).' "onclick="TfRequest.do(this);">
                     <i class="bx bx-pencil"></i>
                   </a>';
                  }         
                  $html2.='         </div> 
                                </div>';
                }                

              }                           
              $html2.='                   </div>
                                          <!--end accordion-performance-content-->
                                        </div>
                                      </div>
                                      <!--end accordion-performance-->';       
              
              $html2.='                 <!--begin accordion-okr-->
                                      <div id="accordion-okr-'.$row["id_employee"].'" class="col-lg-12 container">
                                        <div class="my-epic-profile card">
                                          <!--begin accordion-okr-header-->
                                          <div class="col-12 card-header" id="heading-okr-'.$row["id_employee"].'">
                                            <div class="col-11 container"> 
                                              <a id="accordion-okr-a-'.$row["id_employee"].'"  href="#" class="section '.$quarter[3]["accordion"].' collapsed" data-toggle="collapse" data-target="#collapse-okr-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-okr-'.$row["id_employee"].'" data-form-action="form-action-okr-'.$row["id_employee"].'"> Mira sus OKRs</a>
                                            </div>
                                            <div class="col-1 container text-right">';
            if ($quarter[3]["accordion"]=='active'){     
              $tfData = array();
              $tfData["hr_epic_id_employee"] = $row["id_employee"];    
              $tfData["hr_epic_id_position"] = $row["id_position"];                 
              $html2.='                       <a id="form-action-okr-'.$row["id_employee"].'" name="form-action-okr-'.$row["id_employee"].'" class="btn-epic-sm" alt="New" title="New" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpic" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-action="AN" onclick="TfRequest.do(this);" style="display: none;">            
                                               <i class="bx bx-plus"></i>
                                              </a>';
            } 
            $html2.='                      </div>
                                          </div>
                                          <!--end accordion-okr-header-->
                                          <!--begin accordion-okr-content-->
                                          <div id="collapse-okr-'.$row["id_employee"].'" class="collapsed collapse" aria-labelledby="heading-okr-'.$row["id_employee"].'" data-parent="#accordion-okr-'.$row["id_employee"].'">';  
              if (count($epicList)==0){
                 $html2.='                 No existen OKRs en este trimestre';
              }else{
                $html2.='                 <div class="row epic">
                                              <div class="col-lg-2 col-container text-left">
                                              </div>
                                              <div class="col-lg-2 col-container epic-text-sm text-left">
                                                Estatus
                                              </div> 
                                              <div class="col-lg-1 col-container epic-text-sm text-center">
                                                Prioridad
                                              </div>                  
                                              <div class="col-lg-1 col-container epic-text-sm text-center">
                                                Actividades  
                                              </div>       
                                              <div class="col-lg-5 col-container epic-text-sm text-left">
                                                Progreso
                                              </div>             
                                              <div class="col-lg-1 col-container epic-text-sm text-right">
                                              </div>      
                                          </div>    ';     
                $i=0;
                foreach ($epicList as $c){
                $i++;
                $tfData = array();
                $tfData["hr_epic_id"] = $c["id"];
                $html2.='           <div class="row epic">
                                     <div class="col-lg-2 col-container text-left">
                                      <div class="h7 m-0">#'.$i.' Epic</div>
                                      <div class="text-muted h6">Estimado '.$c["completion_estimated_date"].'</div>
                                    </div>
                                    <div class="col-lg-2 col-container text-left">
                                      <div class="h7 m-0">
                                        <i class="bx bxs-circle" style="color:#1cc88a; font-size: 0.9rem;"></i> '.$c["status"].'
                                      </div>
                                      <div class="text-muted h6">'.$c["status_date"].'</div>
                                    </div>   
                                    <div class="col-lg-1 col-container text-left">
                                        <div class="epic-bottom">
                                           <span class="badge badge-pill c3-'.$c["id_epic_priority"].'">'.$c["priority"].'</span>
                                        </div>
                                    </div>    
                                    <div class="col-lg-1 col-container text-center">
                                      <div class="epic-bottom">
                                        <span class="badge badge-success">'.$c["activities"].'</span>
                                      </div>  
                                    </div>  
                                    <div class="col-lg-5 col-container text-left">
                                      <div class="epic-text-sm m-0">'.$c["progress"].'%</div>
                                      <div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: '.$c["progress"].'%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                      </div>
                                    </div>     
                                    <div class="col-lg-1 col-container text-right">';
                  if ($quarter[3]["accordion"]=='active'){                                
                    $html2.='              <a class="btn-epic-sm" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpic" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).' "onclick="TfRequest.do(this);">
                     <i class="bx bx-pencil"></i>
                   </a>';
                  }         
                  $html2.='         </div> 
                                    <div class="col-lg-12 col-container text-left">'.$c["description"].'</div>
                                </div>';
                }                

              }                           
              $html2.='                   </div>
                                          <!--end accordion-okr-content-->
                                        </div>
                                      </div>
                                      <!--end accordion-okr-->';                                    
                                  
          }
          $html2.='             </div>
                             </div>     
                          </div>
                      </div>
                      <!---End tabs content QUARTER III-->  
                      <!---Begin tabs content QUARTER IV-->
                      <div id="pane-QIV-'.$row["id_employee"].'" class="card tab-pane fade '.$quarter[4]["pane"].'" role="tabpanel" aria-labelledby="tab-QIV'.$row["id_employee"].'">
                          <div class="card-header" role="tab" id="heading-QIV-'.$row["id_employee"].'">
                              <h5 class="mb-0">
                                 <a data-toggle="collapse" href="#collapse-QIV-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-QIV-'.$row["id_employee"].'">
                                      Trimestre IV - %
                                  </a>
                              </h5>
                          </div>
                          <div id="collapse-QIV-'.$row["id_employee"].'" class="collapse" data-parent="#content-'.$row["id_employee"].'" role="tabpanel" aria-labelledby="heading-QIV-'.$row["id_employee"].'">
                             <div class="card-body">
                                <div class="row">';
          if ($quarter[4]["tab"]!='disabled'){
            

            $html2.='                 QIV-'.$row["id_employee"];  
          }
          $html2.='             </div>
                             </div>     
                          </div>
                      </div>
                      <!---End tabs content QUARTER IV-->     
                    </div>
                    <!---End tabs content-->
                  </div> 
                   <!---End tabs QUARTER-->
              
              </div>
            </div>';


        }
        $html.=$html2;
        unset($html2);
        $html.='<!--end team-->
        </div>
      </div>
    </div>
  </div>';

  echo $html;
  echo '<script type="text/javascript">'.$script.'</script>'; 
                     
?>
<script type="text/javascript">
  Chart.defaults.global.legend.position = 'bottom';
            Chart.defaults.global.legend.labels.usePointStyle = true;
            Chart.defaults.global.legend.labels.boxWidth = 15;
            Chart.defaults.global.tooltips.backgroundColor = '#000';
            Chart.defaults.global.defaultFontColor = '#000';



var colors = [
  ['rgba(101, 187, 166, 1)', 'rgba(101, 187, 166, 0.4)'],
  ['rgba(108, 185, 189, 1)', 'rgba(108, 185, 189, 0.4)'],
  ['rgba(128, 153, 186, 1)', 'rgba(128, 153, 186, 0.4)'],
  ['rgba(143, 140, 189, 1)', 'rgba(143, 140, 189, 0.4)'],
  ['rgba(159, 119, 184, 1)', 'rgba(159, 119, 184, 0.4)'],
  ['rgba(166, 103, 160, 1)', 'rgba(166, 103, 160, 0.4)'],
  ['rgba(181, 94, 120, 1)', 'rgba(181, 94, 120, 0.4)'],
  ['rgba(197, 77, 94, 1)', 'rgba(197, 77, 94, 0.4)'],
  ['rgba(162, 76, 80, 1)', 'rgba(162, 76, 80, 0.4)'],
  ['rgba(199, 123, 74, 1)', 'rgba(199, 123, 74, 0.4)'],
  ['rgba(200, 139, 35, 1)', 'rgba(200, 139, 35, 0.4)'],
  ['rgba(127,141,42, 1)', 'rgba(127,141,42, 0.4)'],
  ['rgba(61,131,742, 1)', 'rgba(61,131,74, 0.4)'],
  ['rgba(0,114,100, 1)', 'rgba(0,114,100, 0.4)'],  
  ['rgba(47,72,88, 1)', 'rgba(47,72,88, 0.4)'],
  ['rgba(2,94,105, 1)', 'rgba(2,94,105, 0.4)'],
  ['rgba(72,90,115, 1)', 'rgba(72,90,115, 0.4)'],
  ['rgba(103,106,139, 1)', 'rgba(103,106,139, 0.4)'],
  ['rgba(140,122,159, 1)', 'rgba(140,122,159, 0.4)'],
  ['rgba(79,138,174, 1)', 'rgba(79,138,174, 0.4)'],
  ['rgba(218,155,183, 1)', 'rgba(218,155,183, 0.4)']

];


var pieColors = [
  colors[0][0],
  colors[3][0],
  colors[6][0],
  colors[9][0],
  colors[12][0],
  colors[15][0],
  colors[18][0],
  colors[2][0],
  colors[5][0],
  colors[8][0],
  colors[11][0],
  colors[14][0],
  colors[17][0],
  colors[20][0],
  colors[1][0],
  colors[4][0],
  colors[7][0],
  colors[10][0],
  colors[13][0],
  colors[16][0],
  colors[19][0]
];

 
  $(".coaching").on('click',function(){  
      $(".coaching").removeClass('active');
      $(this).addClass('active');
      $(".team-epic-profile").hide();
      $('#'+$(this).data('show-profile')).show();
      
      sessionStorage.setItem('profile', $(this).attr('id'));
      sessionStorage.setItem('quarter', $("a[id*='-"+$(this).data('id-employee')+"'].quarter.active").attr('id'));
      sessionStorage.setItem('section', 'null');

      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: ($('#'+$(this).data('show-profile')).offset().top-90)
      }, 1000, 'easeInOutExpo');


  });

  $(".quarter").on('click',function(){ 
     sessionStorage.setItem('quarter', $(this).attr('id'));
     sessionStorage.setItem('section', 'null');
  });

  function scrollFocus(){
    var profile= sessionStorage.getItem('profile');
    var quarter= sessionStorage.getItem('quarter');
    var section= sessionStorage.getItem('section');
    if (profile != 'null') {
       $('#'+profile).click();       
       if (quarter != 'null') {
          $('#'+quarter).click();
          $('html, body').stop().animate({scrollTop: ($('#'+quarter).offset().top-360)}, 1000, 'easeInOutExpo');
       if (section != 'null') {
          $('#'+section).click();
          $('html, body').stop().animate({scrollTop: ($('#'+section).offset().top)}, 1000, 'easeInOutExpo');
       } 
       } 
    }
  }


   $(".section.active").on('click',function(){  
      sessionStorage.setItem('section', $(this).attr('id'));
      if ($(this).hasClass('collapsed')){
        $('#'+$(this).data('form-action')).show();
      }else{
       $('#'+$(this).data('form-action')).hide();
      }
    });
    
    $(document).ready(function(){
         scrollFocus();
    });

</script>