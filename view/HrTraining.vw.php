<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_training_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrTraining" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrTraining->getCreatedBy()).'  on '.$hrTraining->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_training_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrTraining" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_training_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrTraining" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrTraining->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrTraining->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="hr_training_form" name="hr_training_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr Training</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_training" name="is_hr_training" value="'.$hrTraining->getInitialState().'">
         <input type="hidden" id="hr_training_id" name="hr_training_id" maxlength="22" value="'.$hrTraining->getId().'">
         <input type="hidden" id="hr_training_id_employee" name="hr_training_id_employee" maxlength="22" value="'.$hrTraining->getIdEmployee().'">
         <input type="hidden" id="hr_training_id_position" name="hr_training_id_position" maxlength="22" value="'.$hrTraining->getIdPosition().'">
         <input type="hidden" id="hr_training_id_training_type" name="hr_training_id_training_type" maxlength="22" value="'.$hrTraining->getIdTrainingType().'">
      </div>
      
      <div class="col-lg-11 container">
         <label for="hr_training_name" class="control-label">Name:</label>
          <input type="text" id="hr_training_name" name="hr_training_name" class="hr_training_name form-control"  maxlength="50"  value="'.$hrTraining->getName().'"  tabindex="4"/>
        <label for="hr_training_name" class="error">'.$hrTraining->getAttrError("name").'</label>
        </div>
     
      <div class="col-lg-1 container">
         <label for="hr_training_hour" class="control-label">Hours:</label>
          <input type="number" id="hr_training_hour" name="hr_training_hour" class="hr_training_hour form-control"  maxlength="3"  value="'.$hrTraining->getHour().'"  min="0" tabindex="4"/>
        <label for="hr_training_hour" class="error">'.$hrTraining->getAttrError("hour").'</label>
        </div>
            <div class="col-lg-12 h-100 container">
        <div class="col-lg-8 container">
         <label for="hr_training_description" class="control-label">Description:</label>
         <textarea id="hr_training_description" name="hr_training_description" class="hr_training_description form-control"  maxlength="200" rows="5" tabindex="3" >'.$hrTraining->getDescription().'</textarea>
        <label for="hr_training_description" class="error">'.$hrTraining->getAttrError("description").'</label>
        </div>
        <div class="col-lg-2 my-auto container form-group">
          <div class="col-lg-7 container priority-range-container">
            <div style="height: 150px;">
              <div class="h-100 d-inline-block">
                <form class="range-field w-50">
                  <input id="hr_training_id_epic_priority" name="hr_training_id_epic_priority" class="hr_training_id_epic_priority priority-'.$hrTraining->getIdEpicPriority().' border-0 vertical-lowest-first" type="range"tabindex="2" min="1" max="3" value="'.$hrTraining->getIdEpicPriority().'"/>
                </form>
              </div>
            </div>
          </div>
          <div class="col-lg-5 container priority-container">  
            <ul id="hr_training_priority_label" class="list-group priority epl-'.$hrTraining->getIdEpicPriority().'">
              <li class="list-group-item priority-1 text-left">Alta</li>
              <li class="list-group-item priority-2 text-left">Media</li>
              <li class="list-group-item priority-3 text-left">Baja</li>
            </ul> 
          </div>  
          <label for="" class="error"></label>     
        </div>
        <div class="col-lg-2 container form-group">
          <div class="progress-input-container">
            <ul class="list-group">
              <li class="list-group-item text-left">
               <a class="btn rounded-circle" onclick="hr_training_progress_up();">
                  <i class="bx bx-plus"></i>
                </a>
              </li>
              <li class="list-group-item percentage text-left">
                 <input type="number" min="0" max="100" id="hr_training_progress" name="hr_training_progress" class="hr_training_progress form-control" style="display:none;" maxlength="22"  value="'.$hrTraining->getProgress().'"  tabindex="10"/>
                 <div id="hr_training_progress_label" name="hr_training_progress_label" class="circle-progress">
                      '.$hrTraining->getProgress().'%
                </div>
              </li>
              <li class="list-group-item text-left">
                   <a class="btn rounded-circle" onclick="hr_training_progress_down();">
                             <i class="bx bx-minus"></i>
                    </a>
              </li>
            </ul> 
          </div>
          <label for="" class="error"></label>
      </div>

      <div class="col-lg-6 container">
       <label for="hr_training_estimated_start_date" class="control-label">Estimated Start Date:</label>
        <input type="text" id="hr_training_estimated_start_date" name="hr_training_estimated_start_date" class="hr_training_estimated_start_date form-control"  maxlength="22"  value="'.$hrTraining->getEstimatedStartDate().'"  tabindex="6"/>
      <label for="hr_training_estimated_start_date" class="error">'.$hrTraining->getAttrError("estimated_start_date").'</label>
      </div>

      <div class="col-lg-6 container">
       <label for="hr_training_start_date" class="control-label">Start Date:</label>
        <input type="text" id="hr_training_start_date" name="hr_training_start_date" class="hr_training_start_date form-control"  maxlength="22"  value="'.$hrTraining->getStartDate().'"  tabindex="7"/>
      <label for="hr_training_start_date" class="error">'.$hrTraining->getAttrError("start_date").'</label>
      </div>
      <div class="col-lg-6 container">
       <label for="hr_training_completion_estimated_date" class="control-label">Completion Estimated Date:</label>
        <input type="text" id="hr_training_completion_estimated_date" name="hr_training_completion_estimated_date" class="hr_training_completion_estimated_date form-control"  maxlength="22"  value="'.$hrTraining->getCompletionEstimatedDate().'"  tabindex="8"/>
      <label for="hr_training_completion_estimated_date" class="error">'.$hrTraining->getAttrError("completion_estimated_date").'</label>
      </div>
      <div class="col-lg-6 container">
       <label for="hr_training_completion_date" class="control-label">Completion Date:</label>
        <input type="text" id="hr_training_completion_date" name="hr_training_completion_date" class="hr_training_completion_date form-control"  maxlength="22"  value="'.$hrTraining->getCompletionDate().'"  tabindex="9"/>
      <label for="hr_training_completion_date" class="error">'.$hrTraining->getAttrError("completion_date").'</label>
      </div>';

      
      foreach (HrEpicStatus::selectOptions($tfs) as $row) {
        if ($hrTraining->getIdEpicStatus()==$row["value"]){
          $checked='checked';
        }else{
          $checked=''; 
        }
        $html.= '<div class="col-lg-2 container text-center">
              <label class="form-check-label" for="">'.$row["option"].'</label>
              <input id="hr_training_id_epic_status" name="hr_training_id_epic_status" class="hr_training_id_epic_status option-input radio" type="radio" value="'.$row["value"].'" '.$checked.'>
              <label for="" class="error"> </label>
            </div>';
      }
    

$html.=$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrTrainingRules(){
  $("#hr_training_form").validate();
  $("#hr_training_name").rules("add", {
    required:true,
    maxlength:50
  });
  $("#hr_training_description").rules("add", {
    required:true,
    maxlength:500
  });
  $("#hr_training_hour").rules("add", {
    number:true,
    maxlength:3
  });
  $("#hr_training_estimated_start_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#hr_training_start_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_training_completion_estimated_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#hr_training_completion_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_training_id_epic_status").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_training_progress").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
 

}


$("#hr_training_estimated_start_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#hr_training_start_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#hr_training_completion_estimated_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#hr_training_completion_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});


$("#hr_training_id_epic_priority").on('change',function(){ 
  
     $(this).removeClass (function (index, className) {
        return (className.match (/(^|\s)priority-\S+/g) || []).join(' ');
    });
     $(this).addClass("priority-"+$(this).val());

     $("#hr_training_priority_label").removeClass (function (index, className) {
        return (className.match (/(^|\s)epl-\S+/g) || []).join(' ');
    });

     $("#hr_training_priority_label").addClass("epl-"+$(this).val());
  });

function hr_training_progress_up(){
   document.getElementById('hr_training_progress').stepUp(10);
  $("#hr_training_progress_label").html($("#hr_training_progress").val()+'%');
}
function hr_training_progress_down(){
   document.getElementById('hr_training_progress').stepDown(10);
  $("#hr_training_progress_label").html($("#hr_training_progress").val()+'%');
}

$(document).ready(function(){
  hrTrainingRules();

  $("#hr_training_estimated_start_date").css('vertical-align','top');
  $("#hr_training_estimated_start_date").mask('y999-m9-d9');
  $("#hr_training_start_date").css('vertical-align','top');
  $("#hr_training_start_date").mask('y999-m9-d9');
  $("#hr_training_completion_estimated_date").css('vertical-align','top');
  $("#hr_training_completion_estimated_date").mask('y999-m9-d9');
  $("#hr_training_completion_date").css('vertical-align','top');
  $("#hr_training_completion_date").mask('y999-m9-d9');

})
</script>