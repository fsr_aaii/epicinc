<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AE":
   case "AB":
   case "AA":
    $audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrPositionEvaluation->getCreatedBy()).'  on '.$hrPositionEvaluation->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_position_evaluation_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPositionEvaluation" data-tf-action="AA" onclick="hrPositionEvaluationDo(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_position_evaluation_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPositionEvaluation" data-tf-action="AB" onclick="hrPositionEvaluationDo(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrPositionEvaluation->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrPositionEvaluation->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }

  $range = array(1,2,3,4,5,6);
  $html = '<div class="row">
<div class="mx-auto  card col-lg-10 form-frame shadow mb-4">
    <form id="hr_position_evaluation_form" name="hr_position_evaluation_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Critical positions identification tool</div>
      <div class="col-lg-12 container subtitle"><b>Position: </b>'.$hrPositionEvaluation->getBusinessTitleDesc().'</div>
      <div class="col-lg-12 container information">Please rank the follows criterias on a scale 1-6 (1: very low, 6 very high) to estimate the position criticality</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_position_evaluation" name="is_hr_position_evaluation" value="'.$hrPositionEvaluation->getInitialState().'">
         <input type="hidden" id="hr_position_evaluation_id" name="hr_position_evaluation_id" maxlength="22" value="'.$hrPositionEvaluation->getId().'">
      </div>';
  
  $i=0;    
  foreach ($hrPositionEvaluationCriteriaList as $row){
      $i++;
        if ($type!=$row['type']){
          /*$html.='<div class="col-12 text-left evaluation-type">
                    <span>'.$row['type'].'</span>
                  </div>';*/
          $type=$row['type'];        
        }
        if ($group!=$row['group']){
          $html.='<div class="col-12 text-left evaluation-group">
                    <span>'.$row['group'].'</span>
                  </div>';    
          $group=$row['group'];        
        }
        $html.='<div class="col-12 text-left evaluation-criteria">
                  <span id="hr_position_evaluation_criteria_label_'.$row['id'].'">'.$i.'.- '.$row['criteria'].'</span>
                </div>                
                <div id="hr_position_evaluation_criteria_box_'.$row['id'].'" name="hr_position_evaluation_criteria_box_'.$row['id'].'" class="col-12 container hr_position_evaluation_criteria_box evaluation_box" data-radio-id="hr_position_evaluation_criteria_'.$row['id'].'" data-label-id="hr_position_evaluation_criteria_label_'.$row['id'].'">';
                
        foreach ($range as $r) {

          if($r==$row["value"]){
            $checked="checked";
          }else{
            $checked="";
          }          
          $html.='<div class="col-2 container">
                   <label for="" class="form-check-label">'.$r.'</label>
                   <input type="radio" class="hr_position_evaluation_criteria option-input radio" name="hr_position_evaluation_criteria_'.$row['id'].'" value="'.$r.'" '.$checked.' data-label-id="hr_position_evaluation_criteria_label_'.$row['id'].'">
                  </div>';
          
        }  
        $html.='</div>';
    }

  $html.= $audit.'<div class="col-lg-12 container text-right mb-4 mt-4">'.$buttons.'</div>

   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">

  function positionEvaluationCriteriaValidate(){
  var a;
  var stop;
  stop=false;
  a=$("#hr_position_evaluation_form").valid();
  $('.hr_position_evaluation_criteria_box').each(function () {       
     var rval=$('input:radio[name='+$(this).data('radio-id')+']:checked').val();
     if (typeof rval == 'undefined') {
      $('#'+$(this).data('label-id')).addClass("error");
      if (!stop){
        stop=true;
        $('#'+$(this).data('label-id')).focus();
        

        $("body,html").animate(
      {
        scrollTop:$('#'+$(this).data('label-id')).offset().top-180
        },
        800 //speed
    );
      }
     } 
  });

  if (stop){
    return false;
  }else{
    return a;
  } 

};

function hrPositionEvaluationDo(element,confirm=false){
    if (confirm){
       jConfirm('Are you sure you want to process this information?','Continue?',function(r){
           if(r){
            if(positionEvaluationCriteriaValidate()){ 
             TfRequest.post(element);
            }
           }
       });
    }else{
          TfRequest.post(element);
    }
  }

$(document).ready(function(){
  
  $('.hr_position_evaluation_criteria').click(function () {
   $('#'+$(this).data('label-id')).removeClass("error");        
  });

})

</script>