<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_like_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPostLike" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$cmPostLike->getCreatedBy()).'  on '.$cmPostLike->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_like_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPostLike" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_like_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPostLike" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($cmPostLike->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($cmPostLike->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="cm_post_like_form" name="cm_post_like_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Cm Post Like</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_cm_post_like" name="is_cm_post_like" value="'.$cmPostLike->getInitialState().'">
         <input type="hidden" id="cm_post_like_id" name="cm_post_like_id" maxlength="22" value="'.$cmPostLike->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="cm_post_like_id_post" class="control-label">Id Post:</label>
        <select  id="cm_post_like_id_post" name="cm_post_like_id_post" class="cm_post_like_id_post form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(CmPost::selectOptions($tfs),$cmPostLike->getIdPost()).
'      </select>
      <label for="cm_post_like_id_post" class="error">'.$cmPostLike->getAttrError("id_post").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function cmPostLikeRules(){
  $("#cm_post_like_form").validate();
  $("#cm_post_like_id_post").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

}


$(document).ready(function(){
  cmPostLikeRules();
  $("#cm_post_like_id_post").select2({
    minimumResultsForSearch: -1
  });


})
</script>