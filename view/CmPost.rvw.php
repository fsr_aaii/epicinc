<style type="text/css">
 .dataTables_wrapper .dataTables_paginate {
    float: left;
    text-align: left;
    padding-top: 0.25em;
}
 table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>th:first-child:before {
    top: 1.4rem;
    left: 4px;
    height: 14px;
    width: 14px;
    display: block;
    position: absolute;
    color: #276EF1;
    border: 0;
    border-radius: 0;
    box-shadow: none;
    box-sizing: content-box;
    text-align: center;
    text-indent: 0 !important;
    font-family: 'Font Awesome 5 Pro';
    line-height: 14px;
    content: "\f105";
    background-color: transparent;
    -webkit-transition: all 0.25s ease;
    transition: all 0.25s ease;
}
table.dataTable.dtr-inline.collapsed>tbody>tr.parent>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr.parent>th:first-child:before {
    content: "\f107";
}


.action{
  padding: 0;
}
</style>
<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
             <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
             <div style="padding: 2rem;">
            <div class="col-6 epic-title xl">Post</div>
             <div class="col-6 text-right action">
               <a class="btn btn-epic-2 xl rounded-circle" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPost" data-tf-action="AN" onclick="TfRequest.do(this);">
                 <i class="bx bx-plus"></i>
               </a>


               <a class="btn btn-epic-2 xl rounded-circle" role="button" data-tf-table="#cm_post_dt" data-tf-file="Cm Post" onclick="TfExport.excel(this);">
                 <i class="bx bx-download"></i>
               </a>
             </div>
       <table id="cm_post_dt" class="table table-striped dt-responsive nowrap" style="width:100%">
         <thead>
          <tr>
             <th class="all">Fecha</th>
             <th class="all">Titulo</th>
             <th class="none">Categoria</th>
             <th class="all">Likes</th>
             <th class="all">Commentarios</th>
             <th class="none">Publicado</th>
             <th class="all dt-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($cmPostList as $row){
    $tfData["cm_post_id"] = $row["id"];
    $html.='   <tr>
            <td>'.TfWidget::dateFormat($row["created_date"]).'</td>
            <td>'.$row["title"].'</td>
            <td>'.$row["category"].'</td>
            <td>'.$row["likes"].'</td>
            <td>'.$row["comment"].'</td>
            <td>'.$row["active"].'</td>
            
                 <td class="dt-right">
                   <a class="btn-epic-sm" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPost" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'" onclick="TfRequest.do(this);">
                     <i class="bx bx-pencil"></i>
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
          </div>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#cm_post_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>