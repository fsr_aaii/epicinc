<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_position_evaluation_criteria_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPositionEvaluationCriteria" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrPositionEvaluationCriteria->getCreatedBy()).'  on '.$hrPositionEvaluationCriteria->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_position_evaluation_criteria_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPositionEvaluationCriteria" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_position_evaluation_criteria_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPositionEvaluationCriteria" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrPositionEvaluationCriteria->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrPositionEvaluationCriteria->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-8 form-frame">
    <form id="hr_position_evaluation_criteria_form" name="hr_position_evaluation_criteria_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr Position Evaluation Criteria</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_position_evaluation_criteria" name="is_hr_position_evaluation_criteria" value="'.$hrPositionEvaluationCriteria->getInitialState().'">
         <input type="hidden" id="hr_position_evaluation_criteria_id" name="hr_position_evaluation_criteria_id" maxlength="22" value="'.$hrPositionEvaluationCriteria->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_position_evaluation_criteria_id_position_evaluation" class="control-label">Id Position Evaluation:</label>
        <select  id="hr_position_evaluation_criteria_id_position_evaluation" name="hr_position_evaluation_criteria_id_position_evaluation" class="hr_position_evaluation_criteria_id_position_evaluation form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrPositionEvaluation::selectOptions($tfs),$hrPositionEvaluationCriteria->getIdPositionEvaluation()).
'      </select>
      <label for="hr_position_evaluation_criteria_id_position_evaluation" class="error">'.$hrPositionEvaluationCriteria->getAttrError("id_position_evaluation").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_position_evaluation_criteria_id_evaluation_criteria" class="control-label">Id Evaluation Criteria:</label>
        <select  id="hr_position_evaluation_criteria_id_evaluation_criteria" name="hr_position_evaluation_criteria_id_evaluation_criteria" class="hr_position_evaluation_criteria_id_evaluation_criteria form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrEvaluationCriteria::selectOptions($tfs),$hrPositionEvaluationCriteria->getIdEvaluationCriteria()).
'      </select>
      <label for="hr_position_evaluation_criteria_id_evaluation_criteria" class="error">'.$hrPositionEvaluationCriteria->getAttrError("id_evaluation_criteria").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_position_evaluation_criteria_value" class="control-label">Value:</label>
        <input type="text" id="hr_position_evaluation_criteria_value" name="hr_position_evaluation_criteria_value" class="hr_position_evaluation_criteria_value form-control"  maxlength="22"  value="'.$hrPositionEvaluationCriteria->getValue().'"  tabindex="3"/>
      <label for="hr_position_evaluation_criteria_value" class="error">'.$hrPositionEvaluationCriteria->getAttrError("value").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_position_evaluation_criteria_date" class="control-label">Date:</label>
        <input type="text" id="hr_position_evaluation_criteria_date" name="hr_position_evaluation_criteria_date" class="hr_position_evaluation_criteria_date form-control"  maxlength="22"  value="'.$hrPositionEvaluationCriteria->getDate().'"  tabindex="4"/>
      <label for="hr_position_evaluation_criteria_date" class="error">'.$hrPositionEvaluationCriteria->getAttrError("date").'</label>
      </div>

'.$audit.'

   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrPositionEvaluationCriteriaRules(){
  $("#hr_position_evaluation_criteria_form").validate();
  $("#hr_position_evaluation_criteria_id_position_evaluation").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_position_evaluation_criteria_id_evaluation_criteria").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_position_evaluation_criteria_value").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_position_evaluation_criteria_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#hr_position_evaluation_criteria_created_by").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_position_evaluation_criteria_created_date").rules("add", {
    required:true,
    maxlength:22
  });

}

$("#HrPositionEvaluationCriteria_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
  hrPositionEvaluationCriteriaRules();

  $("#HrPositionEvaluationCriteria_date").css('vertical-align','top');
  $("#HrPositionEvaluationCriteria_date").mask('y999-m9-d9');

})
</script>