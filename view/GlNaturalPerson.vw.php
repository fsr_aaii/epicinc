<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#gl_natural_person_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlNaturalPerson" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$glNaturalPerson->getCreatedBy()).'  on '.$glNaturalPerson->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#gl_natural_person_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlNaturalPerson" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-primary" role="button" data-tf-form="#gl_natural_person_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="GlNaturalPerson" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($glNaturalPerson->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($glNaturalPerson->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-8 form-frame">
    <form id="gl_natural_person_form" name="gl_natural_person_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Gl Natural Person</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_gl_natural_person" name="is_gl_natural_person" value="'.$glNaturalPerson->getInitialState().'">
         <input type="hidden" id="gl_natural_person_id" name="gl_natural_person_id" maxlength="22" value="'.$glNaturalPerson->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_nin_type" class="control-label">Nin Type:</label>
        <input type="text" id="gl_natural_person_nin_type" name="gl_natural_person_nin_type" class="gl_natural_person_nin_type form-control"  maxlength="1"  value="'.$glNaturalPerson->getNinType().'"  tabindex="1"/>
      <label for="gl_natural_person_nin_type" class="error">'.$glNaturalPerson->getAttrError("nin_type").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_nin" class="control-label">Nin:</label>
        <input type="text" id="gl_natural_person_nin" name="gl_natural_person_nin" class="gl_natural_person_nin form-control"  maxlength="22"  value="'.$glNaturalPerson->getNin().'"  tabindex="2"/>
      <label for="gl_natural_person_nin" class="error">'.$glNaturalPerson->getAttrError("nin").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_nin_control_digit" class="control-label">Nin Control Digit:</label>
        <input type="text" id="gl_natural_person_nin_control_digit" name="gl_natural_person_nin_control_digit" class="gl_natural_person_nin_control_digit form-control"  maxlength="22"  value="'.$glNaturalPerson->getNinControlDigit().'"  tabindex="3"/>
      <label for="gl_natural_person_nin_control_digit" class="error">'.$glNaturalPerson->getAttrError("nin_control_digit").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_first_name" class="control-label">First Name:</label>
        <input type="text" id="gl_natural_person_first_name" name="gl_natural_person_first_name" class="gl_natural_person_first_name form-control"  maxlength="50"  value="'.$glNaturalPerson->getFirstName().'"  tabindex="4"/>
      <label for="gl_natural_person_first_name" class="error">'.$glNaturalPerson->getAttrError("first_name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_middle_name" class="control-label">Middle Name:</label>
        <input type="text" id="gl_natural_person_middle_name" name="gl_natural_person_middle_name" class="gl_natural_person_middle_name form-control"  maxlength="50"  value="'.$glNaturalPerson->getMiddleName().'"  tabindex="5"/>
      <label for="gl_natural_person_middle_name" class="error">'.$glNaturalPerson->getAttrError("middle_name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_last_name" class="control-label">Last Name:</label>
        <input type="text" id="gl_natural_person_last_name" name="gl_natural_person_last_name" class="gl_natural_person_last_name form-control"  maxlength="50"  value="'.$glNaturalPerson->getLastName().'"  tabindex="6"/>
      <label for="gl_natural_person_last_name" class="error">'.$glNaturalPerson->getAttrError("last_name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_second_last_name" class="control-label">Second Last Name:</label>
        <input type="text" id="gl_natural_person_second_last_name" name="gl_natural_person_second_last_name" class="gl_natural_person_second_last_name form-control"  maxlength="50"  value="'.$glNaturalPerson->getSecondLastName().'"  tabindex="7"/>
      <label for="gl_natural_person_second_last_name" class="error">'.$glNaturalPerson->getAttrError("second_last_name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_photo" class="control-label">Photo:</label>
        <input type="text" id="gl_natural_person_photo" name="gl_natural_person_photo" class="gl_natural_person_photo form-control"  maxlength="400"  value="'.$glNaturalPerson->getPhoto().'"  tabindex="8"/>
      <label for="gl_natural_person_photo" class="error">'.$glNaturalPerson->getAttrError("photo").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_id_nationality_country" class="control-label">Id Nationality Country:</label>
        <input type="text" id="gl_natural_person_id_nationality_country" name="gl_natural_person_id_nationality_country" class="gl_natural_person_id_nationality_country form-control"  maxlength="2"  value="'.$glNaturalPerson->getIdNationalityCountry().'"  tabindex="9"/>
      <label for="gl_natural_person_id_nationality_country" class="error">'.$glNaturalPerson->getAttrError("id_nationality_country").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_id_gender" class="control-label">Id Gender:</label>
        <input type="text" id="gl_natural_person_id_gender" name="gl_natural_person_id_gender" class="gl_natural_person_id_gender form-control"  maxlength="1"  value="'.$glNaturalPerson->getIdGender().'"  tabindex="10"/>
      <label for="gl_natural_person_id_gender" class="error">'.$glNaturalPerson->getAttrError("id_gender").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_id_birth_country" class="control-label">Id Birth Country:</label>
        <input type="text" id="gl_natural_person_id_birth_country" name="gl_natural_person_id_birth_country" class="gl_natural_person_id_birth_country form-control"  maxlength="2"  value="'.$glNaturalPerson->getIdBirthCountry().'"  tabindex="11"/>
      <label for="gl_natural_person_id_birth_country" class="error">'.$glNaturalPerson->getAttrError("id_birth_country").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_birthplace" class="control-label">Birthplace:</label>
        <input type="text" id="gl_natural_person_birthplace" name="gl_natural_person_birthplace" class="gl_natural_person_birthplace form-control"  maxlength="40"  value="'.$glNaturalPerson->getBirthplace().'"  tabindex="12"/>
      <label for="gl_natural_person_birthplace" class="error">'.$glNaturalPerson->getAttrError("birthplace").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_birthdate" class="control-label">Birthdate:</label>
        <input type="text" id="gl_natural_person_birthdate" name="gl_natural_person_birthdate" class="gl_natural_person_birthdate form-control"  maxlength="22"  value="'.$glNaturalPerson->getBirthdate().'"  tabindex="13"/>
      <label for="gl_natural_person_birthdate" class="error">'.$glNaturalPerson->getAttrError("birthdate").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_id_marital_status" class="control-label">Id Marital Status:</label>
        <input type="text" id="gl_natural_person_id_marital_status" name="gl_natural_person_id_marital_status" class="gl_natural_person_id_marital_status form-control"  maxlength="1"  value="'.$glNaturalPerson->getIdMaritalStatus().'"  tabindex="14"/>
      <label for="gl_natural_person_id_marital_status" class="error">'.$glNaturalPerson->getAttrError("id_marital_status").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_height" class="control-label">Height:</label>
        <input type="text" id="gl_natural_person_height" name="gl_natural_person_height" class="gl_natural_person_height form-control"  maxlength="22"  value="'.$glNaturalPerson->getHeight().'"  tabindex="15"/>
      <label for="gl_natural_person_height" class="error">'.$glNaturalPerson->getAttrError("height").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_size" class="control-label">Size:</label>
        <input type="text" id="gl_natural_person_size" name="gl_natural_person_size" class="gl_natural_person_size form-control"  maxlength="5"  value="'.$glNaturalPerson->getSize().'"  tabindex="16"/>
      <label for="gl_natural_person_size" class="error">'.$glNaturalPerson->getAttrError("size").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_weight" class="control-label">Weight:</label>
        <input type="text" id="gl_natural_person_weight" name="gl_natural_person_weight" class="gl_natural_person_weight form-control"  maxlength="22"  value="'.$glNaturalPerson->getWeight().'"  tabindex="17"/>
      <label for="gl_natural_person_weight" class="error">'.$glNaturalPerson->getAttrError("weight").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_is_right_handed" class="control-label">Is Right Handed:</label>
        <input type="text" id="gl_natural_person_is_right_handed" name="gl_natural_person_is_right_handed" class="gl_natural_person_is_right_handed form-control"  maxlength="1"  value="'.$glNaturalPerson->getIsRightHanded().'"  tabindex="18"/>
      <label for="gl_natural_person_is_right_handed" class="error">'.$glNaturalPerson->getAttrError("is_right_handed").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_id_blood_type" class="control-label">Id Blood Type:</label>
        <input type="text" id="gl_natural_person_id_blood_type" name="gl_natural_person_id_blood_type" class="gl_natural_person_id_blood_type form-control"  maxlength="22"  value="'.$glNaturalPerson->getIdBloodType().'"  tabindex="19"/>
      <label for="gl_natural_person_id_blood_type" class="error">'.$glNaturalPerson->getAttrError("id_blood_type").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_is_functional_diversity" class="control-label">Is Functional Diversity:</label>
        <input type="text" id="gl_natural_person_is_functional_diversity" name="gl_natural_person_is_functional_diversity" class="gl_natural_person_is_functional_diversity form-control"  maxlength="1"  value="'.$glNaturalPerson->getIsFunctionalDiversity().'"  tabindex="20"/>
      <label for="gl_natural_person_is_functional_diversity" class="error">'.$glNaturalPerson->getAttrError("is_functional_diversity").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_id_study_level" class="control-label">Id Study Level:</label>
        <input type="text" id="gl_natural_person_id_study_level" name="gl_natural_person_id_study_level" class="gl_natural_person_id_study_level form-control"  maxlength="4"  value="'.$glNaturalPerson->getIdStudyLevel().'"  tabindex="21"/>
      <label for="gl_natural_person_id_study_level" class="error">'.$glNaturalPerson->getAttrError("id_study_level").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="gl_natural_person_id_residence_country" class="control-label">Id Residence Country:</label>
        <input type="text" id="gl_natural_person_id_residence_country" name="gl_natural_person_id_residence_country" class="gl_natural_person_id_residence_country form-control"  maxlength="2"  value="'.$glNaturalPerson->getIdResidenceCountry().'"  tabindex="22"/>
      <label for="gl_natural_person_id_residence_country" class="error">'.$glNaturalPerson->getAttrError("id_residence_country").'</label>
      </div>

'.$audit.'

   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function glNaturalPersonRules(){
  $("#gl_natural_person_form").validate();
  $("#gl_natural_person_nin_type").rules("add", {
    required:true,
    maxlength:1
  });
  $("#gl_natural_person_nin").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#gl_natural_person_nin_control_digit").rules("add", {
    number:true,
    maxlength:22
  });
  $("#gl_natural_person_first_name").rules("add", {
    maxlength:50
  });
  $("#gl_natural_person_middle_name").rules("add", {
    maxlength:50
  });
  $("#gl_natural_person_last_name").rules("add", {
    maxlength:50
  });
  $("#gl_natural_person_second_last_name").rules("add", {
    maxlength:50
  });
  $("#gl_natural_person_photo").rules("add", {
    maxlength:400
  });
  $("#gl_natural_person_id_nationality_country").rules("add", {
    maxlength:2
  });
  $("#gl_natural_person_id_gender").rules("add", {
    maxlength:1
  });
  $("#gl_natural_person_id_birth_country").rules("add", {
    maxlength:2
  });
  $("#gl_natural_person_birthplace").rules("add", {
    maxlength:40
  });
  $("#gl_natural_person_birthdate").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#gl_natural_person_id_marital_status").rules("add", {
    maxlength:1
  });
  $("#gl_natural_person_height").rules("add", {
    number:true,
    maxlength:22
  });
  $("#gl_natural_person_size").rules("add", {
    maxlength:5
  });
  $("#gl_natural_person_weight").rules("add", {
    number:true,
    maxlength:22
  });
  $("#gl_natural_person_is_right_handed").rules("add", {
    maxlength:1
  });
  $("#gl_natural_person_id_blood_type").rules("add", {
    number:true,
    maxlength:22
  });
  $("#gl_natural_person_is_functional_diversity").rules("add", {
    maxlength:1
  });
  $("#gl_natural_person_id_study_level").rules("add", {
    maxlength:4
  });
  $("#gl_natural_person_id_residence_country").rules("add", {
    maxlength:2
  });
  $("#gl_natural_person_created_by").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#gl_natural_person_created_date").rules("add", {
    required:true,
    maxlength:22
  });

}

$("#GlNaturalPerson_birthdate").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
  glNaturalPersonRules();

  $("#GlNaturalPerson_birthdate").css('vertical-align','top');
  $("#GlNaturalPerson_birthdate").mask('y999-m9-d9');

})
</script>