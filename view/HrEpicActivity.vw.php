<style type="text/css">

</style>
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_epic_activity_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpicActivity" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrEpicActivity->getCreatedBy()).'  on '.$hrEpicActivity->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_epic_activity_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpicActivity" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_epic_activity_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpicActivity" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrEpicActivity->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrEpicActivity->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="hr_epic_activity_form" name="hr_epic_activity_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Key Results</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_epic_activity" name="is_hr_epic_activity" value="'.$hrEpicActivity->getInitialState().'">
         <input type="hidden" id="hr_epic_activity_id" name="hr_epic_activity_id" maxlength="22" value="'.$hrEpicActivity->getId().'">
         <input type="hidden" id="hr_epic_activity_id_epic" name="hr_epic_activity_id_epic" maxlength="22" value="'.$hrEpicActivity->getIdEpic().'">
      </div>
        

      <div class="col-lg-12 h-100 container">
        <div class="col-lg-8 container">
         <label for="hr_epic_activity_description" class="control-label">Description:</label>
         <textarea id="hr_epic_activity_description" name="hr_epic_activity_description" class="hr_epic_activity_description form-control"  maxlength="200" rows="5" tabindex="3" >'.$hrEpicActivity->getDescription().'</textarea>
        <label for="hr_epic_activity_description" class="error">'.$hrEpicActivity->getAttrError("description").'</label>
        </div>
        <div class="col-lg-2 my-auto container form-group">
          <div class="col-lg-7 container priority-range-container">
            <div style="height: 150px;">
              <div class="h-100 d-inline-block">
                <form class="range-field w-50">
                  <input id="hr_epic_activity_id_epic_priority" name="hr_epic_activity_id_epic_priority" class="hr_epic_activity_id_epic_priority priority-'.$hrEpicActivity->getIdEpicPriority().' border-0 vertical-lowest-first" type="range"tabindex="2" min="1" max="3" value="'.$hrEpicActivity->getIdEpicPriority().'"/>
                </form>
              </div>
            </div>
          </div>
          <div class="col-lg-5 container priority-container">  
            <ul id="hr_epic_activity_priority_label" class="list-group priority epl-'.$hrEpicActivity->getIdEpicPriority().'">
              <li class="list-group-item priority-1 text-left">Alta</li>
              <li class="list-group-item priority-2 text-left">Media</li>
              <li class="list-group-item priority-3 text-left">Baja</li>
            </ul> 
          </div>  
          <label for="" class="error"></label>     
        </div>
        <div class="col-lg-2 container form-group">
          <div class="progress-input-container">
            <ul class="list-group">
              <li class="list-group-item text-left">
               <a class="btn rounded-circle" onclick="hr_epic_activity_progress_up();">
                  <i class="bx bx-plus"></i>
                </a>
              </li>
              <li class="list-group-item text-left">
                 <input type="number" min="0" max="100" id="hr_epic_activity_progress" name="hr_epic_activity_progress" class="hr_epic_activity_progress form-control" style="display:none;" maxlength="22"  value="'.$hrEpicActivity->getProgress().'"  tabindex="10"/>
                 <div id="hr_epic_activity_progress_label" name="hr_epic_activity_progress_label" class="circle-progress">
                      '.$hrEpicActivity->getProgress().'%
                </div>
              </li>
              <li class="list-group-item text-left">
                   <a class="btn rounded-circle" onclick="hr_epic_activity_progress_down();">
                             <i class="bx bx-minus"></i>
                    </a>
              </li>
            </ul> 
          </div>
          <label for="" class="error"></label>
      </div>


      </div>  
        <div class="col-lg-3 container">
         <label for="hr_epic_activity_estimated_start_date" class="control-label">Estimated Start:</label>
          <input type="text" id="hr_epic_activity_estimated_start_date" name="hr_epic_activity_estimated_start_date" class="hr_epic_activity_estimated_start_date form-control"  maxlength="22"  value="'.$hrEpicActivity->getEstimatedStartDate().'"  tabindex="3"/>
        <label for="hr_epic_activity_estimated_start_date" class="error">'.$hrEpicActivity->getAttrError("estimated_start_date").'</label>
        </div>
      <div class="col-lg-3 container">
       <label for="hr_epic_activity_start_date" class="control-label">Start Date:</label>
        <input type="text" id="hr_epic_activity_start_date" name="hr_epic_activity_start_date" class="hr_epic_activity_start_date form-control"  maxlength="22"  value="'.$hrEpicActivity->getStartDate().'"  tabindex="4"/>
      <label for="hr_epic_activity_start_date" class="error">'.$hrEpicActivity->getAttrError("start_date").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="hr_epic_activity_completion_estimated_date" class="control-label">Completion Estimated:</label>
        <input type="text" id="hr_epic_activity_completion_estimated_date" name="hr_epic_activity_completion_estimated_date" class="hr_epic_activity_completion_estimated_date form-control"  maxlength="22"  value="'.$hrEpicActivity->getCompletionEstimatedDate().'"  tabindex="5"/>
      <label for="hr_epic_activity_completion_estimated_date" class="error">'.$hrEpicActivity->getAttrError("completion_estimated_date").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="hr_epic_activity_completion_date" class="control-label">Completion Date:</label>
        <input type="text" id="hr_epic_activity_completion_date" name="hr_epic_activity_completion_date" class="hr_epic_activity_completion_date form-control"  maxlength="22"  value="'.$hrEpicActivity->getCompletionDate().'"  tabindex="6"/>
      <label for="hr_epic_activity_completion_date" class="error">'.$hrEpicActivity->getAttrError("completion_date").'</label>
      </div>';

      
      foreach (HrEpicStatus::selectOptions($tfs) as $row) {
        if ($hrEpicActivity->getIdEpicStatus()==$row["value"]){
          $checked='checked';
        }else{
          $checked=''; 
        }
        $html.= '<div class="col-lg-2 container text-center">
              <label class="form-check-label" for="">'.$row["option"].'</label>
              <input id="hr_epic_activity_id_epic_status" name="hr_epic_activity_id_epic_status" class="hr_epic_activity_id_epic_status option-input radio" type="radio" value="'.$row["value"].'" '.$checked.'>
              <label for="" class="error"> </label>
            </div>';
      }
    

$html.= $audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrEpicActivityRules(){
  $("#hr_epic_activity_form").validate();
  $("#hr_epic_activity_id_epic").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_epic_activity_description").rules("add", {
    required:true,
    maxlength:200
  });
  $("#hr_epic_activity_estimated_start_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#hr_epic_activity_start_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_epic_activity_completion_estimated_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#hr_epic_activity_completion_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_epic_activity_id_epic_priority").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_epic_activity_id_epic_status").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

  $("#hr_epic_activity_progress").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });

}

$("#hr_epic_activity_estimated_start_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#hr_epic_activity_start_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#hr_epic_activity_completion_estimated_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});
$("#hr_epic_activity_completion_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});


 $("#hr_epic_activity_id_epic_priority").on('change',function(){ 
  
     $(this).removeClass (function (index, className) {
        return (className.match (/(^|\s)priority-\S+/g) || []).join(' ');
    });
     $(this).addClass("priority-"+$(this).val());

     $("#hr_epic_activity_priority_label").removeClass (function (index, className) {
        return (className.match (/(^|\s)epl-\S+/g) || []).join(' ');
    });

     $("#hr_epic_activity_priority_label").addClass("epl-"+$(this).val());
  });

function hr_epic_activity_progress_up(){
   document.getElementById('hr_epic_activity_progress').stepUp(10);
  $("#hr_epic_activity_progress_label").html($("#hr_epic_activity_progress").val()+'%');
}
function hr_epic_activity_progress_down(){
   document.getElementById('hr_epic_activity_progress').stepDown(10);
  $("#hr_epic_activity_progress_label").html($("#hr_epic_activity_progress").val()+'%');
}
$(document).ready(function(){
  hrEpicActivityRules();

  $("#hr_epic_activity_estimated_start_date").css('vertical-align','top');
  $("#hr_epic_activity_estimated_start_date").mask('y999-m9-d9');
  $("#hr_epic_activity_start_date").css('vertical-align','top');
  $("#hr_epic_activity_start_date").mask('y999-m9-d9');
  $("#hr_epic_activity_completion_estimated_date").css('vertical-align','top');
  $("#hr_epic_activity_completion_estimated_date").mask('y999-m9-d9');
  $("#hr_epic_activity_completion_date").css('vertical-align','top');
  $("#hr_epic_activity_completion_date").mask('y999-m9-d9');

})
</script>