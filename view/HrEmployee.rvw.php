<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8">
            <div class="col-6 title">Hr Employee</div>
             <div class="col-6 text-right action">
               <a class="button" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployee" data-tf-action="AN" onclick="TfRequest.do(this);">
                 <span>Add</span><div class="icon"><span class="far fa-plus"></span></div>
               </a>
               <a class="button" role="button" data-tf-table="#hr_employee_dt" data-tf-file="Hr Employee" onclick="TfExport.excel(this);">
                 <span>Excel</span><div class="icon"><span class="far fa-download"></span></div>
               </a>
             </div>
       <table id="hr_employee_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Id Person</th>
             <th class="all">Email Account</th>
             <th class="all">Hire Date</th>
             <th class="all">Id Development Level</th>
             <th class="all">Id Job Level</th>
             <th class="all">Id Job Level Next</th>
             <th class="all">Id Competence A</th>
             <th class="all">Id Competence B</th>
             <th class="all">Id Soft Skill A</th>
             <th class="all">Id Soft Skill B</th>
             <th class="all">Is Key Talent</th>
             <th class="all">Has Successor</th>
             <th class="all">Has Talent</th>
             <th class="all">Resignation Risk</th>
             <th class="all">Criticality</th>
             <th class="all">Criticality Date</th>
             <th class="all">Performace</th>
             <th class="all">Performace Date</th>
             <th class="all">Learning Agility</th>
             <th class="all">Learning Agility Date</th>
             <th class="all">Cr</th>
             <th class="all">Successor Name</th>
             <th class="all">Mobility</th>
             <th class="all">Area Interest</th>
             <th class="all">Termination Date</th>
             <th class="all">Id Termination Reason</th>
             <th class="none">Created date</th>
             <th class="none">Created by</th>
             <th class="all dt-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($hrEmployeeList as $row){
    $tfData["hr_employee_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["id_person"].'</td>
            <td>'.$row["email_account"].'</td>
            <td>'.$row["hire_date"].'</td>
            <td>'.$row["id_development_level"].'</td>
            <td>'.$row["id_job_level"].'</td>
            <td>'.$row["id_job_level_next"].'</td>
            <td>'.$row["id_competence_a"].'</td>
            <td>'.$row["id_competence_b"].'</td>
            <td>'.$row["id_soft_skill_a"].'</td>
            <td>'.$row["id_soft_skill_b"].'</td>
            <td>'.$row["is_key_talent"].'</td>
            <td>'.$row["has_successor"].'</td>
            <td>'.$row["has_talent"].'</td>
            <td>'.$row["resignation_risk"].'</td>
            <td>'.$row["criticality"].'</td>
            <td>'.$row["criticality_date"].'</td>
            <td>'.$row["performace"].'</td>
            <td>'.$row["performace_date"].'</td>
            <td>'.$row["learning_agility"].'</td>
            <td>'.$row["learning_agility_date"].'</td>
            <td>'.$row["cr"].'</td>
            <td>'.$row["successor_name"].'</td>
            <td>'.$row["mobility"].'</td>
            <td>'.$row["area_interest"].'</td>
            <td>'.$row["termination_date"].'</td>
            <td>'.$row["id_termination_reason"].'</td>
            <td>'.$row["created_date"].'</td>
            <td>'.$row["created_by"].'</td>
                 <td class="dt-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployee" data-tf-action="AE" data-tf-data="'.tfRequest::encrypt($tfData).'"onclick="TfRequest.do(this);">
                     <span class="far fa-pencil"></span>
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
$(document).ready(function() {
  $("#hr_employee_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
