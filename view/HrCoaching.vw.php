<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_coaching_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrCoaching" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrCoaching->getCreatedBy()).'  on '.$hrCoaching->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_coaching_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrCoaching" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_coaching_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrCoaching" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrCoaching->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrCoaching->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="hr_coaching_form" name="hr_coaching_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr Coaching</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_coaching" name="is_hr_coaching" value="'.$hrCoaching->getInitialState().'">
         <input type="hidden" id="hr_coaching_id" name="hr_coaching_id" maxlength="22" value="'.$hrCoaching->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_coaching_id_employee" class="control-label">Id Employee:</label>
        <select  id="hr_coaching_id_employee" name="hr_coaching_id_employee" class="hr_coaching_id_employee form-control" tabindex="1">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrEmployee::selectOptions($tfs),$hrCoaching->getIdEmployee()).
'      </select>
      <label for="hr_coaching_id_employee" class="error">'.$hrCoaching->getAttrError("id_employee").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_coaching_id_coaching" class="control-label">Id Coaching:</label>
        <select  id="hr_coaching_id_coaching" name="hr_coaching_id_coaching" class="hr_coaching_id_coaching form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(GlPerson::selectOptions($tfs),$hrCoaching->getIdCoaching()).
'      </select>
      <label for="hr_coaching_id_coaching" class="error">'.$hrCoaching->getAttrError("id_coaching").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_coaching_active" class="control-label">Active:</label>
        <select  id="hr_coaching_active" name="hr_coaching_active" class="hr_coaching_active form-control" tabindex="3">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($hrCoaching->getActive(),'Y').
'      </select>
      <label for="hr_coaching_active" class="error">'.$hrCoaching->getAttrError("active").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrCoachingRules(){
  $("#hr_coaching_form").validate();
  $("#hr_coaching_id_employee").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_coaching_id_coaching").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_coaching_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  hrCoachingRules();


})
</script>