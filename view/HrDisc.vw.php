<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_disc_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrDisc" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrDisc->getCreatedBy()).'  on '.$hrDisc->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_disc_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrDisc" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_disc_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrDisc" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrDisc->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrDisc->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-8 form-frame">
    <form id="hr_disc_form" name="hr_disc_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr Disc</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_disc" name="is_hr_disc" value="'.$hrDisc->getInitialState().'">
         <input type="hidden" id="hr_disc_id" name="hr_disc_id" maxlength="22" value="'.$hrDisc->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_disc_name" class="control-label">Name:</label>
        <input type="text" id="hr_disc_name" name="hr_disc_name" class="hr_disc_name form-control"  maxlength="200"  value="'.$hrDisc->getName().'"  tabindex="1"/>
      <label for="hr_disc_name" class="error">'.$hrDisc->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_disc_active" class="control-label">Active:</label>
        <select  id="hr_disc_active" name="hr_disc_active" class="hr_disc_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($hrDisc->getActive(),'Y').
'      </select>
      <label for="hr_disc_active" class="error">'.$hrDisc->getAttrError("active").'</label>
      </div>

'.$audit.'

   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrDiscRules(){
  $("#hr_disc_form").validate();
  $("#hr_disc_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#hr_disc_active").rules("add", {
    required:true,
    maxlength:1
  });
  $("#hr_disc_created_by").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_disc_created_date").rules("add", {
    required:true,
    maxlength:22
  });

}


$(document).ready(function(){
  hrDiscRules();


})
</script>