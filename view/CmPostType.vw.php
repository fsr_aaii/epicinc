<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPostType" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$cmPostType->getCreatedBy()).'  on '.$cmPostType->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPostType" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#cm_post_type_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="CmPostType" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($cmPostType->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($cmPostType->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="cm_post_type_form" name="cm_post_type_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Cm Post Type</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_cm_post_type" name="is_cm_post_type" value="'.$cmPostType->getInitialState().'">
         <input type="hidden" id="cm_post_type_id" name="cm_post_type_id" maxlength="22" value="'.$cmPostType->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="cm_post_type_name" class="control-label">Name:</label>
        <input type="text" id="cm_post_type_name" name="cm_post_type_name" class="cm_post_type_name form-control"  maxlength="200"  value="'.$cmPostType->getName().'"  tabindex="1"/>
      <label for="cm_post_type_name" class="error">'.$cmPostType->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="cm_post_type_active" class="control-label">Active:</label>
        <select  id="cm_post_type_active" name="cm_post_type_active" class="cm_post_type_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($cmPostType->getActive(),'Y').
'      </select>
      <label for="cm_post_type_active" class="error">'.$cmPostType->getAttrError("active").'</label>
      </div>

'.$audit.'

     <div class="col-lg-12 container text-right keypad">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function cmPostTypeRules(){
  $("#cm_post_type_form").validate();
  $("#cm_post_type_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#cm_post_type_active").rules("add", {
    required:true,
    maxlength:1
  });

}


$(document).ready(function(){
  cmPostTypeRules();


})
</script>