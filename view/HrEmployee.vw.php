
<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
    case "AE":
   case "AB":
   case "AA":
   case "AR":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrEmployee->getCreatedBy()).'  on '.$hrEmployee->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_employee_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployee" data-tf-action="AA" onclick="hrEmployeeDo(this);"> Guardar </a>';
   break;
  }
  foreach ($hrEmployee->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrEmployee->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }

$risk= Array (0=>Array("value" =>"N","option"=>"None"),
                1=>Array("value" =>"L","option"=>"Low"),
                2=>Array("value" =>"M","option"=>"Medium"),
                3=>Array("value" =>"H","option"=>"High"));

$mobility= Array (0=>Array("value" =>"C","option"=>"Country"),
                1=>Array("value" =>"IR","option"=>"International (Region)"),
                2=>Array("value" =>"IOR","option"=>"International (Other Region)"),
                3=>Array("value" =>"NM","option"=>"No Mobility"));

  if ($glNaturalPerson->getPhoto()=='..' or $glNaturalPerson->getPhoto()==''){
    $photo='../asset/images/blank-user.jpg';
  }else{
    $photo=$glNaturalPerson->getPhoto();
  }

$html = ' <div class="row">
<div class="mx-auto  card col-lg-10 form-frame shadow mb-4">
   
   <form id="hr_employee_form" name="hr_employee_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Employee</div>
      <div class="col-lg-12 container">
        '.$objAlerts.'
      </div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_employee" name="is_hr_employee" value="'.$hrEmployee->getInitialState().'">
         <input type="hidden" id="hr_employee_id" name="hr_employee_id" maxlength="22" value="'.$hrEmployee->getId().'">
         <input type="hidden" id="gl_natural_person_id" name="gl_natural_person_id" maxlength="22" value="'.$glNaturalPerson->getId().'">
         <input type="hidden" id="gl_natural_person_photo" name="gl_natural_person_photo" maxlength="400" value="'.$glNaturalPerson->getPhoto().'">
      </div>
      <div class="col-lg-2 col-container">
         <label for=""  class="control-label">Photo:</label>
         <input id="gl_natural_person_photo_path" name="gl_natural_person_photo_path" type="hidden" class="form-control" >
         <div class="button-container">           
           <img id="photo-upload" name="photo-upload" class="img-profile" src="'.$photo.'">
           <a class="btn btn-file btn-edit btn-photo-upload rounded-circle" href="#" alt="Upload">
            <i class="bx bx-camera"></i>
            <input type="file" id="gl_natural_person_photo_img" name="gl_natural_person_photo_img">
           </a>
         </div> 
      </div>
      <div class="col-lg-10 col-container">
    
           <div class="col-lg-3 container">
             <label for="hr_employee_hire_date" class="control-label">Hire Date:</label>
              <input type="text" id="hr_employee_hire_date" name="hr_employee_hire_date" class="hr_employee_hire_date form-control"  maxlength="22"  value="'.$hrEmployee->getHireDate().'"  tabindex="3"/>
            <label for="hr_employee_hire_date" class="error">'.$hrEmployee->getAttrError("hire_date").'</label>
            </div>

            <div class="col-lg-5 container">
             <label for="hr_employee_id_development_level" class="control-label">Development Level:</label>
              <select  id="hr_employee_id_development_level" name="hr_employee_id_development_level" class="hr_employee_id_development_level form-control select-css" tabindex="4">
            <option value="">None</option>'.
            TfWidget::selectStructure(HrDevelopmentLevel::selectOptions($tfs),$hrEmployee->getIdDevelopmentLevel()).
      '      </select>
            <label for="hr_employee_id_development_level" class="error">'.$hrEmployee->getAttrError("id_development_level").'</label>
            </div>
            <div class="col-lg-2 container">
             <label for="hr_employee_id_job_level" class="control-label">Level:</label>
              <select  id="hr_employee_id_job_level" name="hr_employee_id_job_level" class="hr_employee_id_job_level form-control" tabindex="5">
            <option value="">Select a option</option>'.
            TfWidget::selectStructure(HrJobLevel::selectOptions($tfs),$hrEmployee->getIdJobLevel()).
      '      </select>
            <label for="hr_employee_id_job_level" class="error">'.$hrEmployee->getAttrError("id_job_level").'</label>
            </div>
            <div class="col-lg-2 container">
             <label for="hr_employee_id_job_level_next" class="control-label">Level Next:</label>
              <select  id="hr_employee_id_job_level_next" name="hr_employee_id_job_level_next" class="hr_employee_id_job_level_next form-control" tabindex="6">
            <option value="">Select a option</option>'.
            TfWidget::selectStructure(HrJobLevel::selectOptions($tfs),$hrEmployee->getIdJobLevelNext()).
      '      </select>
            <label for="hr_employee_id_job_level_next" class="error">'.$hrEmployee->getAttrError("id_job_level_next").'</label>
            </div>

                  <div class="col-lg-3 container">
       <label for="hr_employee_resignation_risk" class="control-label"> Flight Risk:</label>
        <select  id="hr_employee_resignation_risk" name="hr_employee_resignation_risk" class="hr_employee_resignation_risk form-control" tabindex="14">
       <option value="">Select a option</option>'.
        TfWidget::selectStructure($risk,$hrEmployee->getResignationRisk()).
     '</select>
      <label for="hr_employee_resignation_risk" class="error">'.$hrEmployee->getAttrError("resignation_risk").'</label>
      </div>
      <div class="col-lg-3 container">
       <label for="hr_employee_has_successor" class="control-label">Has Successor:</label>
        <select  id="hr_employee_has_successor" name="hr_employee_has_successor" class="hr_employee_has_successor form-control" tabindex="12">
       <option value="">Select a option</option>'.
        TfWidget::selectStructureYN($hrEmployee->getHasSuccessor(),'N').
     '</select>
      <label for="hr_employee_has_successor" class="error">'.$hrEmployee->getAttrError("has_successor").'</label>
      </div>
      <div class="col-lg-6 container">
       <label for="hr_employee_mobility" class="control-label">Mobility:</label>
        <select  id="hr_employee_mobility" name="hr_employee_mobility" class="hr_employee_mobility form-control" tabindex="23">
       <option value="">Select...</option>'.
        tfWidget::selectStructure($mobility,$hrEmployee->getMobility()).
     '</select>
      <label for="hr_employee_mobility" class="error">'.$hrEmployee->getAttrError("mobility").'</label>
      </div>

        </div>
     
      <div class="col-lg-6 container">
       <label for="hr_employee_id_competence_a" class="control-label">Competence A:</label>
        <select  id="hr_employee_id_competence_a" name="hr_employee_id_competence_a" class="hr_employee_id_competence_a form-control" tabindex="7">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrCompetence::selectOptions($tfs),$hrEmployee->getIdCompetenceA()).
'      </select>
      <label for="hr_employee_id_competence_a" class="error">'.$hrEmployee->getAttrError("id_competence_a").'</label>
      </div>
      <div class="col-lg-6 container">
       <label for="hr_employee_id_competence_b" class="control-label">Competence B:</label>
        <select  id="hr_employee_id_competence_b" name="hr_employee_id_competence_b" class="hr_employee_id_competence_b form-control" tabindex="8">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrCompetence::selectOptions($tfs),$hrEmployee->getIdCompetenceB()).
'      </select>
      <label for="hr_employee_id_competence_b" class="error">'.$hrEmployee->getAttrError("id_competence_b").'</label>
      </div>
      <div class="col-lg-6 container">
       <label for="hr_employee_id_soft_skill_a" class="control-label">Soft Skill A:</label>
        <select  id="hr_employee_id_soft_skill_a" name="hr_employee_id_soft_skill_a" class="hr_employee_id_soft_skill_a form-control" tabindex="9">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrSoftSkill::selectOptions($tfs),$hrEmployee->getIdSoftSkillA()).
'      </select>
      <label for="hr_employee_id_soft_skill_a" class="error">'.$hrEmployee->getAttrError("id_soft_skill_a").'</label>
      </div>
      <div class="col-lg-6 container">
       <label for="hr_employee_id_soft_skill_b" class="control-label">Soft Skill B:</label>
        <select  id="hr_employee_id_soft_skill_b" name="hr_employee_id_soft_skill_b" class="hr_employee_id_soft_skill_b form-control" tabindex="10">
      <option value="">Select a option</option>'.
      TfWidget::selectStructure(HrSoftSkill::selectOptions($tfs),$hrEmployee->getIdSoftSkillB()).
'      </select>
      <label for="hr_employee_id_soft_skill_b" class="error">'.$hrEmployee->getAttrError("id_soft_skill_b").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_employee_area_interest" class="control-label">Area Interest:</label>
        <textarea id="hr_employee_area_interest" name="hr_employee_area_interest" class="hr_employee_area_interest form-control" rows="3" >'.$hrEmployee->getAreaInterest().'</textarea>
      <label for="hr_employee_area_interest" class="error">'.$hrEmployee->getAttrError("area_interest").'</label>
      </div>
      <div class="col-lg-2 container">
       <label for="hr_employee_termination_date" class="control-label">Termination Date:</label>
        <input type="text" id="hr_employee_termination_date" name="hr_employee_termination_date" class="hr_employee_termination_date form-control"  maxlength="22"  value="'.$hrEmployee->getTerminationDate().'"  tabindex="25"/>
      <label for="hr_employee_termination_date" class="error">'.$hrEmployee->getAttrError("termination_date").'</label>
      </div>
      <div class="col-lg-4 container">
       <label for="hr_employee_id_termination_reason" class="control-label">Termination Reason:</label>
        <input type="text" id="hr_employee_id_termination_reason" name="hr_employee_id_termination_reason" class="hr_employee_id_termination_reason form-control"  maxlength="22"  value="'.$hrEmployee->getIdTerminationReason().'"  tabindex="26"/>
      <label for="hr_employee_id_termination_reason" class="error">'.$hrEmployee->getAttrError("id_termination_reason").'</label>
      </div>
      '.$audit.'
      <div class="col-lg-12 container text-right mb-4 mt-4">'.$buttons.'</div>
   </fieldset>
  </form>
</div>
</div>';
echo str_replace(array("\r", "\n"), '', $html);

 ?> 

 <script type="text/javascript">

  function hrEmployeeDo(element){
  jConfirm('Are you sure you want to process this information?','Continue?',function(r){
    if(r){
      if(tfValidate('#hr_employee_form')){
        let url=window.location.href+"/"+window.TF_TRAIL_ID+"/view/"+element.dataset.tfTaskId+"/"+element.dataset.tfController+"/"+element.dataset.tfAction;
        let form = element.dataset.tfForm;
        let go = true;     
        let jsonObject = new Object();
        let file_data = $('#gl_natural_person_photo_img').prop('files')[0];
        var img_data = new FormData();
        img_data.append('gl_natural_person_photo_img', file_data);
        
        if (typeof form != 'undefined') {
          if ($(form).valid()){
           jsonObject = $(form).serializeJSON();   
          }else{
            go = false;
          } 
        }
        let cryptography  = new TfCryptography();         
        data = cryptography.encryptJSON(jsonObject,window.RANDOM_PUBLIC_KEY);
        img_data.append('request-data', data);

        if (go){
          $.ajax({
            data: img_data,
            url: url,    
            type: 'POST',
            dataType: 'text', 
            cache: false,
            contentType: false,
            processData: false,
            async : false,
            success:function(response){ 
              let JDATA=JSON.parse(response);
              let cryptography  = new TfCryptography();
                   $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, window.RANDOM_PUBLIC_KEY));
            }
          });
        }
      }  
    }
  })
}

   function hrEmployeeRules(){
  $("#hr_employee_form").validate();
   $("#hr_employee_hire_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_employee_id_development_level").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_id_job_level").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_id_job_level_next").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_id_competence_a").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_id_competence_b").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_id_soft_skill_a").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_id_soft_skill_b").rules("add", {
    number:true,
    maxlength:22
  });
  $("#hr_employee_has_successor").rules("add", {
    required:true,
    maxlength:1
  });
  $("#hr_employee_resignation_risk").rules("add", {
    required:true,
    maxlength:1
  });
  $("#hr_employee_mobility").rules("add", {
    maxlength:4
  });
  $("#hr_employee_area_interest").rules("add", {
    maxlength:500
  });
  $("#hr_employee_termination_date").rules("add", {
    isDate:true,
    maxlength:22
  });
  $("#hr_employee_id_termination_reason").rules("add", {
    number:true,
    maxlength:22
  });

}

$("#hr_employee_hire_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$("#hr_employee_termination_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function() {
  hrEmployeeRules();
  $('#hr_employee_id_development_level').select2({
    minimumResultsForSearch: -1
  });
  $('#hr_employee_id_job_level').select2({
    minimumResultsForSearch: -1
  });
  $('#hr_employee_id_job_level_next').select2({
    minimumResultsForSearch: -1
  });
  $('#hr_employee_id_competence_a').select2({
    minimumResultsForSearch: -1
  });
  $('#hr_employee_id_competence_b').select2({
    minimumResultsForSearch: -1
  });
  $('#hr_employee_id_soft_skill_a').select2({
    minimumResultsForSearch: -1
  });
  $('#hr_employee_id_soft_skill_b').select2({
    minimumResultsForSearch: -1
  });
  $('#hr_employee_has_successor').select2({
    minimumResultsForSearch: -1
  });
  $('#hr_employee_resignation_risk').select2({
    minimumResultsForSearch: -1
  });
  $('#hr_employee_mobility').select2({
    minimumResultsForSearch: -1
  });
     
  $("#hr_employee_hire_date").css('vertical-align','top');
  $("#hr_employee_hire_date").mask('y999-m9-d9');
  $("#hr_employee_termination_date").css('vertical-align','top');
  $("#hr_employee_termination_date").mask('y999-m9-d9');


  $(document).on('change', '.btn-photo-upload :file', function() {
    var input = $(this),
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [label]);
  });

  $('.btn-photo-upload :file').on('fileselect', function(event, label) {
    var input = $(this).parents('.input-group').find(':text'),
        log = label;
        
    $('#gl_natural_person_photo_path').val(log);
      
  });
  
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#photo-upload').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
  }

    $("#gl_natural_person_photo_img").change(function(){
        readURL(this);
    }); 
 
});





 </script>