<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8">
            <div class="col-6 title">Hr Job Level</div>
             <div class="col-6 text-right action">
               <a class="button" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrJobLevel" data-tf-action="AN" onclick="TfRequest.do(this);">
                 <span>Add</span><div class="icon"><span class="far fa-plus"></span></div>
               </a>
               <a class="button" role="button" data-tf-table="#hr_job_level_dt" data-tf-file="Hr Job Level" onclick="TfExport.excel(this);">
                 <span>Excel</span><div class="icon"><span class="far fa-download"></span></div>
               </a>
             </div>
       <table id="hr_job_level_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Description</th>
             <th class="all dt-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($hrJobLevelList as $row){
    $tfData["hr_job_level_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["description"].'</td>
                 <td class="dt-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrJobLevel" data-tf-action="AE" data-tf-data="'.tfRequest::encrypt($tfData).'"onclick="TfRequest.do(this);">
                     <span class="far fa-pencil"></span>
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
$(document).ready(function() {
  $("#hr_job_level_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
