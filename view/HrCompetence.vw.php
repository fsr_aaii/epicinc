<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="button" role="button" data-tf-form="#hr_competence_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrCompetence" data-tf-action="AI" onclick="TfRequest.do(this,true);"><span>Save</span><div class="icon"><span class="far fa-save"></span></div></a>';
    break;
   case "AE":
   case "AB":
   case "AA":
    $buttons ='<a class="button" role="button" data-tf-form="#hr_competence_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrCompetence" data-tf-action="AA" onclick="TfRequest.do(this,true);"><span>Save</span><div class="icon"><span class="far fa-save"></span></div></a>';
    $buttons.='<a class="button" role="button" data-tf-form="#hr_competence_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrCompetence" data-tf-action="AB" onclick="TfRequest.do(this,true);"><span>Delete</span><div class="icon"><span class="far fa-trash-alt"></span></div></a>';
    break;
  }
  foreach ($hrCompetence->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrCompetence->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-8 form-frame">
    <div class="col-lg-8 title">Hr Competence</div>
    <div class="col-lg-4 action text-right">'.$buttons.'</div>
    <form id="hr_competence_form" name="hr_competence_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container">
        '.$objAlerts.'
      </div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_competence" name="is_hr_competence" value="'.$hrCompetence->getInitialState().'">
         <input type="hidden" id="hr_competence_id" name="hr_competence_id" maxlength="22" value="'.$hrCompetence->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_competence_description" class="control-label">Description:</label>
        <input type="text" id="hr_competence_description" name="hr_competence_description" class="hr_competence_description form-control"  maxlength="100"  value="'.$hrCompetence->getDescription().'"  tabindex="1"/>
      <label for="hr_competence_description" class="error">'.$hrCompetence->getAttrError("description").'</label>
      </div>


   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
function hrCompetenceRules(){
  $("#hr_competence_form").validate();
  $("#hr_competence_description").rules("add", {
    required:true,
    maxlength:100
  });

}


$(document).ready(function(){
  hrCompetenceRules();

})
