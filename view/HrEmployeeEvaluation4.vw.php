<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AE":
   case "AB":
   case "AA":
     $audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrEmployeeEvaluation->getCreatedBy()).'  on '.$hrEmployeeEvaluation->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_employee_evaluation_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-action="AA" onclick="hrEmployeeEvaluationDo(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#hr_employee_evaluation_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-action="AB" onclick="hrEmployeeEvaluationDo(this,false);">Borrar</a>';
    break;
  }
  $range = array(1,2,3);
  $label = array ('1'=>'Below','2'=>'Meets','3'=>'Exceeds');

  foreach ($hrEmployeeEvaluation->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrEmployeeEvaluation->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto  card col-lg-10 form-frame shadow mb-4">
    <form id="hr_employee_evaluation_form" name="hr_employee_evaluation_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
       <div class="col-lg-12 title">PERF evaluation</div>
    <div class="col-lg-12 subtitle">'.$hrEmployeeEvaluation->getEmployeeName().'</div>
    <div class="col-lg-12 information">Please  rank the following on a scale 1-3. (1: Below, 2 Meets, 3:Exceeds) to estimate the employee PERF</div>   
      <div class="col-lg-12 container">
        '.$objAlerts.'
      </div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_employee_evaluation" name="is_hr_employee_evaluation" value="'.$hrEmployeeEvaluation->getInitialState().'">
         <input type="hidden" id="hr_employee_evaluation_id" name="hr_employee_evaluation_id" class="hr_employee_evaluation_id" maxlength="22" value="'.$hrEmployeeEvaluation->getId().'">
      </div>';
      $type='';
      $group='';
      $i=0;
      foreach ($hrEmployeeEvaluationCriteriaList as $row){
        $i++;
        if ($type!=$row['type']){
          /*$html.='<div class="col-12 text-left evaluation-type">
                    <span>'.$row['type'].'</span>
                  </div>';*/
          $type=$row['type'];        
        }
        if ($group!=$row['group']){
          $html.='<div class="col-12 text-left evaluation-group">
                    <span>'.$row['group'].'</span>
                  </div>';    
          $group=$row['group'];        
        }
        $html.='<div class="col-12 text-left evaluation-criteria">
                  <span id="hr_employee_evaluation_criteria_label_'.$row['id'].'">'.$i.' '.$row['criteria'].'</span>
                </div>                
                <div id="hr_employee_evaluation_criteria_box_'.$row['id'].'" name="hr_employee_evaluation_criteria_box_'.$row['id'].'" class="col-12 container hr_employee_evaluation_criteria_box evaluation_box" data-radio-id="hr_employee_evaluation_criteria_'.$row['id'].'" data-label-id="hr_employee_evaluation_criteria_label_'.$row['id'].'">';
                
        foreach ($range as $r) {

          if ($r==$row["value"]){
            $checked="checked";
          }else{
            $checked="";
          }          
          $html.='<div class="col-4 container">
                   <label for="" class="form-check-label">'.$label[$r].'</label>
                   <input type="radio" class="hr_employee_evaluation_criteria  option-input radio" name="hr_employee_evaluation_criteria_'.$row['id'].'" value="'.$r.'" '.$checked.' data-label-id="hr_employee_evaluation_criteria_label_'.$row['id'].'">
                  </div>';
          
        }  
        $html.='</div>';
    }
      

     $html.= $audit.'<div class="col-lg-12 container text-right mb-4 mt-4">'.$buttons.'</div>
   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">

  function employeeEvaluationCriteriaValidate(){
  var a;
  var stop;
  stop=false;
  a=$("#hr_employee_evaluation_form").valid();
  $('.hr_employee_evaluation_criteria_box').each(function () {       
     var rval=$('input:radio[name='+$(this).data('radio-id')+']:checked').val();
     if (typeof rval == 'undefined') {
      $('#'+$(this).data('label-id')).addClass("error");
      if (!stop){
        stop=true;
        $('#'+$(this).data('label-id')).focus();
        

        $("body,html").animate(
      {
        scrollTop:$('#'+$(this).data('label-id')).offset().top-180
        },
        800 //speed
    );
      }
     } 
  });

  if (stop){
    return false;
  }else{
    return a;
  } 

};

function hrEmployeeEvaluationDo(element,confirm=false){
    if (confirm){
       jConfirm('Are you sure you want to process this information?','Continue?',function(r){
           if(r){
            if(employeeEvaluationCriteriaValidate()){ 
             TfRequest.post(element);
            }
           }
       });
    }else{
          TfRequest.post(element);
    }
  }

$(document).ready(function(){
  
  $('.hr_employee_evaluation_criteria').click(function () {
   $('#'+$(this).data('label-id')).removeClass("error");        
  });

})

</script>

