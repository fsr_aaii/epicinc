<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="button" role="button" data-tf-form="#t_user_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="tUser" data-tf-action="AI" onclick="TfRequest.do(this,true);"><span>Save</span><div class="icon"><span class="far fa-save"></span></div></a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="create_by">Created  by '.t_user::description($tfs,$tUser->getCreated_By()).'  on '.$tUser->getCreated_Date().'</span>
        </div>';
    $buttons ='<a class="button" role="button" data-tf-form="#t_user_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="tUser" data-tf-action="AA" onclick="TfRequest.do(this,true);"><span>Save</span><div class="icon"><span class="far fa-save"></span></div></a>';
    $buttons ='<a class="button" role="button" data-tf-form="#t_user_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="tUser" data-tf-action="AI" onclick="TfRequest.do(this,true);"><span>Delete</span><div class="icon"><span class="far fa-trash-alt"></span></div></a>';
    break;
  }
  foreach ($tUser->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($tUser->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto col-lg-8 form-frame">
    <div class="col-lg-8 title">'.$viewTitle.'</div>
    <div class="col-lg-4 action text-right">'.$buttons.'</div>
    <form id="t_user_form" name="t_user_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container">
        '.$objAlerts.'
      </div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_t_user" name="is_t_user" value="'.$tUser->getInitialState().'">
         <input type="hidden" id="t_user_id" name="t_user_id" maxlength="22" value="'.$tUser->getId().'">
<input type="hidden" id="t_user_created_by" name="t_user_created_by" maxlength="22" value="'.$tUser->getCreatedBy().'">
<input type="hidden" id="t_user_created_date" name="t_user_created_date" maxlength="22" value="'.$tUser->getCreatedDate().'">

      </div>
      <div class="col-lg-12 container">
       <label for="t_user_login" class="control-label">Login:</label>
        <input type="text" id="t_user_login" name="t_user_login" class="t_user_login form-control"  maxlength="30"  value="'.$tUser->getLogin().'"  tabindex="1"/>
      <label for="t_user_login" class="error">'.$tUser->getAttrError("login").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="t_user_photo" class="control-label">Photo:</label>
        <textarea id="t_user_photo" name="t_user_photo" class="t_user_photo form-control" rows="3" >'.$tUser->getPhoto().'</textarea>
      <label for="t_user_photo" class="error">'.$tUser->getAttrError("photo").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="t_user_active" class="control-label">Active:</label>
        <select  id="t_user_active" name="t_user_active" class="_active form-control" tabindex="3">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($tUser->getActive(),'Y').
'      </select>
      <label for="t_user_active" class="error">'.$tUser->getAttrError("active").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="t_user_password" class="control-label">Password:</label>
        <textarea id="t_user_password" name="t_user_password" class="t_user_password form-control" rows="3" >'.$tUser->getPassword().'</textarea>
      <label for="t_user_password" class="error">'.$tUser->getAttrError("password").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="t_user_password_date" class="control-label">Password Date:</label>
        <input type="text" id="t_user_password_date" name="t_user_password_date" class="t_user_password_date form-control"  maxlength="22"  value="'.$tUser->getPasswordDate().'"  tabindex="5"/>
      <label for="t_user_password_date" class="error">'.$tUser->getAttrError("password_date").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="t_user_email_account" class="control-label">Email Account:</label>
        <textarea id="t_user_email_account" name="t_user_email_account" class="t_user_email_account form-control" rows="3" >'.$tUser->getEmailAccount().'</textarea>
      <label for="t_user_email_account" class="error">'.$tUser->getAttrError("email_account").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="t_user_name" class="control-label">Name:</label>
        <input type="text" id="t_user_name" name="t_user_name" class="t_user_name form-control"  maxlength="200"  value="'.$tUser->getName().'"  tabindex="7"/>
      <label for="t_user_name" class="error">'.$tUser->getAttrError("name").'</label>
      </div>

'.$audit.'

   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
function tUserRules(){
  $("#t_user_form").validate();
  $("#t_user_login").rules("add", {
    required:true,
    maxlength:30
  });
  $("#t_user_photo").rules("add", {
    required:true,
    maxlength:500
  });
  $("#t_user_active").rules("add", {
    required:true,
    maxlength:1
  });
  $("#t_user_password").rules("add", {
    required:true,
    maxlength:500
  });
  $("#t_user_password_date").rules("add", {
    required:true,
    isDate:true,
    maxlength:22
  });
  $("#t_user_email_account").rules("add", {
    maxlength:500
  });
  $("#t_user_name").rules("add", {
    maxlength:200
  });
  $("#t_user_created_by").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#t_user_created_date").rules("add", {
    required:true,
    maxlength:22
  });

}

$("#TUser_password_date").datepicker({
  uiLibrary: 'bootstrap4',
  format: 'yyyy-mm-dd',
  }).on('change', function(e) {
  $(this).valid();
});

$(document).ready(function(){
  tUserRules();
  $("#TUser_password_date").css('vertical-align','top');
  $("#TUser_password_date").mask('yyyy-mm-dd');

})
