<?php
  if (!is_object($tfs)){
    throw new TfException("The server cannot process your request",7102,400);
  }

  $html='<div class="row">
           <div class="mx-auto col-lg-8">
            <div class="col-6 title">Hr Employee Evaluation</div>
             <div class="col-6 text-right action">
               <a class="button" role="button" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-action="AN" onclick="TfRequest.do(this);">
                 <span>Add</span><div class="icon"><span class="far fa-plus"></span></div>
               </a>
               <a class="button" role="button" data-tf-table="#hr_employee_evaluation_dt" data-tf-file="Hr Employee Evaluation" onclick="TfExport.excel(this);">
                 <span>Excel</span><div class="icon"><span class="far fa-download"></span></div>
               </a>
             </div>
       <table id="hr_employee_evaluation_dt" class="display responsive" style="width:100%">
         <thead>
          <tr>
             <th class="all">Id Employee</th>
             <th class="all">Id Position</th>
             <th class="all">Date</th>
             <th class="all">Id Evaluation Type</th>
             <th class="all">Evaluator Type</th>
             <th class="none">Created by</th>
             <th class="none">Created date</th>
             <th class="all dt-right"></th>
           </tr>
         </thead>
         <tbody>';
   foreach ($hrEmployeeEvaluationList as $row){
    $tfData["hr_employee_evaluation_id"] = $row["id"];
    $html.='   <tr>
            <td>'.$row["id_employee"].'</td>
            <td>'.$row["id_position"].'</td>
            <td>'.$row["date"].'</td>
            <td>'.$row["id_evaluation_type"].'</td>
            <td>'.$row["evaluator_type"].'</td>
            <td>'.$row["created_by"].'</td>
            <td>'.$row["created_date"].'</td>
                 <td class="dt-right">
                   <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-action="AE" data-tf-data="'.tfRequest::encrypt($tfData).'"onclick="TfRequest.do(this);">
                     <span class="far fa-pencil"></span>
                   </a>
                 </td>';  
   }
   $html.='</tr>
           </tbody>
          </table>
         </div>
        </div>';
 echo $html;
?>
<script type="text/javascript">$(document).ready(function() {
  $("#hr_employee_evaluation_dt").DataTable({
    pageLength: 8,
    info:false,
    paging:true,
    lengthChange:false,
    dom: 'frtip',
    columnDefs: [
      { width: "3em", targets: -1 },
      { orderable: false, targets: -1 }
    ],
  });
});
</script>