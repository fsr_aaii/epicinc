  <style type="text/css">
  	.post-profile-photo{
  		width: 4.7rem;
  	}

.morecontent .more {
    display: none;
}

.more-comment{
	display: none;
}
.morelink {
    display: inline;
    color: #E64B9A;
    cursor: pointer;

}

.post-created-by{
	font-size: .75rem;
	text-transform: uppercase;
}
.post-business-title{
	font-size: .75rem;
}
.post-email-account{
	font-size: .75rem;
	
}
.post-ago{
	font-size: .65rem;
  font-style: italic;
  color: rgba(45, 41, 38, 0.6);
}
.post-title{
  text-transform: uppercase;
	font-style: 1rem;
  color: #000;
}
.post-comment{
	font-size: .75rem;
	display: block;
	margin-bottom: .25rem;
}
.more-comment .morelink{
  display: inline;
}
.all-comment{
   font-size: .75rem;
   display: block;
   cursor: pointer;
}
.all-comment-hide{
	display: none;
}


.likes {
    font-size: .75rem;
    font-style: italic;
    padding: .25rem 0;
    margin-bottom: 0;
}

 .post-page.active{
       display: block;
     }
     .post-page{
       display: none;
     }

     .more, .more-comment, .morecontent{
     	font-size: .75rem;
     }
.post .feedback{
	font-size: 1.25rem;
	margin-bottom: 0;
}    
.feedback .like{
	color:#EA4335;
} 
.feedback .commented{
  color:#16A085;
} 
.modal {
	top:6rem;
	z-index: 99999;
}

.card-header{
  color: #2D2926;
}

.epic-input-group [role=right-icon] {
    cursor: pointer;
}

.epic-input-group .input-group-append .btn {
    padding: 0;
}

.badge-pill.tag {
    border: 1px solid #696969;
    font-weight: 400;
    margin-right: 5px;
}
  </style>
  <?php 
 
	  $htmlPage='<div id="post-page-1" class="post-page full active">';
    
    foreach ($show as $s) {
       	$htmlPage.='   <!--Begin Blog Post -->
			       <div class="card shadow mb-4">  
			            <div class="card-body">
			                <div>
			                    <div class="card-header post">
			                        <span class="badge badge-pill c3-3 float-right m-2">'.$s["category"].'</span>
			                        <div class="d-flex justify-content-between align-items-center">
			                            <div class="d-flex justify-content-between align-items-center">
			                                <div class="mr-2">
			                                    <img class="rounded-circle post-profile-photo" src="'.$s["photo"].'" alt="">
			                                </div>
			                                <div class="ml-2">
			                                    <div class="post-created-by m-0">'.$s["name"].'</div>
			                                    <div class="post-business-title m-0">'.$s["business_title"].'</div>
			                                    <div class="post-email-account m-0">'.$s["email_account"].'</div>
			                                    <div class="post-ago mt-1">
			                                        <i class="bx bx-clock-o"></i>Hace '.CmPost::timeAgo($s["created_date"]).'
			                                    </div>
			                                </div>
			                            </div>
			                       </div>
			                    </div>
			                    <div class="card-body">
			                        <div class="post-title mb-2">'.$s["title"].'</div>';
			                        
                         $length = strlen($s["content"]);
			             if ($length>225){
			                $htmlPage.=' <span class="card-text more">'.substr($s["content"],0,225).'...</span>
			                <span class="morecontent">
                             <span class="card-text more">'.$s["content"].'</span>
                             <span class="morelink" onclick="more(this);">  Mostrar mas</span>
                          </span>';
			             }else{
                            $htmlPage.=' <span class="card-text more">'.$s["content"].'</span>';
			             }  
                   $htmlPage.='<div class="mt-2">';     
                   $tags = explode(",",$s["tag"]);
                              foreach ($tags as $t) {
                                        $htmlPage.='<span class="badge badge-pill tag">'.$t.'</span>';
                              }  
                          
			              $htmlPage.='</div>
                                  </div>
			                    <div class="card-body post">
			                        <p class="feedback">';
			                if ($s["likeme"]>0){
                                $htmlPage.='<span class="like" data-id-post="'.$s["id"].'" onclick="like(this);"><i class="bx bxs-heart like"></i></span>';
			                }else{
			                	$htmlPage.='<span class="no-like" data-id-post="'.$s["id"].'" onclick="like(this);"><i class="bx bx-heart"></i></span>';   
			                }      
                       if ($s["commented"]>0){
                                $htmlPage.='<span id="feedback-comment-'.$s["id"].'" data-toggle="modal" data-target="#exampleModal" class="no-comment" data-id-post="'.$s["id"].'" onclick="comment(this);"><i class="bx bxs-comment commented"></i></span>';
                      }else{
                        $htmlPage.='<span id="feedback-comment-'.$s["id"].'" data-toggle="modal" data-target="#exampleModal" class="no-comment" data-id-post="'.$s["id"].'" onclick="comment(this);"><i class="bx bx-comment"></i></span>';   
                      }   
			                 $htmlPage.='   </p> 
			                        <p class="likes">
			                        	<b id="likes-'.$s["id"].'">'.$s["likes"].' Me gusta</b>
			                        </p>';
                            
                            
                            $minePostComments = CmPostComment::mineByPost($tfs,$s["id"]);
                            $postComments = CmPostComment::listByPost($tfs,$s["id"]);
                            
                            if (count($minePostComments)>0){
                              $comment = $minePostComments;
                              $mine=TRUE;
                            }else{
                              $comment = array_shift($postComments);
                              $mine=FALSE;
                            }  
                            $htmlPage.='<div id="one-comment-'.$s["id"].'">';
                            if ($mine){                              
                              foreach ($comment as $mc) {
                                $htmlPage.='<span class="post-comment"><b>'.$mc["created_by"].' </b>'.$mc["comment"].'<span class="post-ago"> - '.CmPost::timeAgo($mc["created_date"]).' </span> <a class="btn-epic-sm" data-id-post="'.$s["id"].'"data-id-comment="'.$mc["id"].'" onclick="uncomment(this);"><i class="bx bx-x"></i></a></span>';
                              }                                
                            }else{
                             $htmlPage.='<span  class="post-comment"><b>'.$comment["created_by"].' </b>'.$comment["comment"].'<span class="post-ago"> - '.CmPost::timeAgo($comment["created_date"]).'</span></span>';
                            } 
                            $htmlPage.='</div>';
                            $htmlPage.='<div id="all-comment-'.$s["id"].'" style="display:none;">';
                            foreach ($postComments as $pc) {
                              $htmlPage.='<span class="post-comment"><b>'.$pc["created_by"].' </b>'.$pc["comment"].'<span class="post-ago"> - '.CmPost::timeAgo($pc["created_date"]).'</span></span>';
                            }    
                            $htmlPage.='</div>
                                <span class="all-comment light" data-id-comment="'.$s["id"].'" onclick="allComment(this)"><span class="verb"> Mostrar </span>'.count($postComments).' comentario(s)</span>    
			                    </div>
			                </div>
			            </div>
			        </div>';
    }
    $htmlPage.='</div>';

     for ($x = 1; $x <= ($pages-1); $x++) {
	  $pagination[$x+1]= implode(",",array_column(array_slice($postIndex,(5*$x), 5),"id"));
	  $htmlPage.='<div id="post-page-'.($x+1).'" class="post-page"></div>';
	}	
	if ($pageExtra>0){
     $pagination[$x+1]= implode(",",array_column(array_slice($postIndex,(5*$x), $pageExtra),"id"));
     $htmlPage.='<div id="post-page-'.($x+1).'" class="post-page"></div>';

	}
    
    $paginationJSON = json_encode($pagination,true);


    $postJs='<script type="text/javascript">

  sessionStorage.setItem("username", "'.$tfs->getUserName().'");
	$(function() {

	
    $("#light-pagination").pagination({
        items: '.$postLength.',
        itemsOnPage: 5,
        useAnchors:false,
        cssStyle: "light-theme",
        onPageClick : function(pageNumber){
        	let index = '.$paginationJSON.';	
            $(".post-page").removeClass("active");
            if (!$("#post-page-"+pageNumber).hasClass("full")){
              let JDATA = new Object();
              JDATA.tfTaskId = 81;
              JDATA.tfController = "CmPost"; 
              JDATA.tfFnName = "pagination";	
              JDATA.tfData = new Object();
              JDATA.tfData.posts=index[pageNumber];
              $("#post-page-"+pageNumber).html(TfRequest.fn(JDATA));
              $("#post-page-"+pageNumber).addClass("full");
            }  
            
            $("#post-page-"+pageNumber).addClass("active");
          }
	    });
	});
    </script>'; 

    //$postPage = array_column(array_slice($postIndex, 0, 10),"id"); 
   

   // $show=CmPost::dataListPage($tfs,implode(",",$posts));
        
    $html='  <!-- Begin dynamic content -->
			 <div class="row">
			   <!-- Begin left content -->
			    <div class="col-8">
			    '.$htmlPage.'
			    <div id="light-pagination" class="pagination light-theme simple-pagination"></div>
			    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="cm_post_comment_form" name="cm_post_comment_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
          <fieldset style="padding: 0 2rem;">
            <div class="col-lg-12 container">
             <input type="hidden" id="cm_post_comment_id_post" name="cm_post_comment_id_post" maxlength="22">
             <label for="cm_post_comment_comment" class="control-label">Comenta:</label>
              <textarea id="cm_post_comment_comment" name="cm_post_comment_comment" class="cm_post_comment_comment form-control" rows="4" ></textarea>
            <label for="cm_post_comment_comment" class="error"></label>
            </div>
          </fieldset>
        </form>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-epic-xl-primary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-epic-xl-primary" onclick="toPost();">Publicar</button>
      </div>
    </div>
  </div>
</div> '; 
       
     
    $html.='</div>
		    <!-- End left content --> 
		    <!-- Begin right content -->
		    <div class="col-4">
		        <!-- Search Widget -->
		         <div class="card shadow mb-4"> 
		            <div class="card-body">
                    <div class="col-lg-12 container">
                      <div role="wrapper" class="epic-input-group input-group">
                        <input type="text" id="cm_post_search" name="cm_post_search" class="cm_post_search form-control" maxlength="22" value="" tabindex="1" placeholder="Buscar..." style="vertical-align: top;">
                        <span class="input-group-append" role="right-icon">
                          <button class="btn btn-outline-secondary border-left-0" type="button">
                            <i class="bx bx-search"></i>
                          </button>
                        </span>
                      </div>
                    </div>		
                    <div class="col-lg-12 container">        
  		                <ul class="list-group list-group-flush">
                        <li class="list-group-item">Cras justo odio</li>
                        <li class="list-group-item">Dapibus ac facilisis in</li>
                        <li class="list-group-item">Morbi leo risus</li>
                        <li class="list-group-item">Porta ac consectetur ac</li>
                        <li class="list-group-item">Vestibulum at eros</li>
                      </ul>
                    </div>  
                </div>

		        </div>
		    
        </div>
		    <!-- End right content -->
		</div>  
		<!-- End dynamic content -->';
       //print_r($show);
       //
    echo $html.$postJs;
  ?>

<script type="text/javascript">

function cmPostCommentRules(){
  $("#cm_post_comment_form").validate();

  $("#cm_post_comment_comment").rules("add", {
    required:true,
    maxlength:280
  });

}

function toPost(){
    if ($("#cm_post_comment_form").valid()){
      let JDATA = new Object();
      JDATA.tfTaskId = 81;
      JDATA.tfController = "CmPost"; 
      JDATA.tfFnName = "comment";  
      JDATA.tfData = new Object();
      JDATA.tfData.id_post=$("#cm_post_comment_id_post").val();
      JDATA.tfData.comment=$("#cm_post_comment_comment").val();
      let response = TfRequest.fn(JDATA);
      var JRESP=JSON.parse(response);         
      if (JRESP.code=='OK'){
         if (JRESP.mine_comment==1){
          $("#all-comment-"+JDATA.tfData.id_post).prepend($( "#one-comment-"+JDATA.tfData.id_post).html());  
          $( "#one-comment-"+JDATA.tfData.id_post).html('<span class="post-comment"><b>'+sessionStorage.getItem("username")+'</b> '+JDATA.tfData.comment+'<span class="post-ago"> - Hace un instante</span></span>' ); 
          $("#feedback-comment-"+JDATA.tfData.id_post+" i").removeClass("bx-comment");
          $("#feedback-comment-"+JDATA.tfData.id_post+" i").addClass("bxs-comment");
          $("#feedback-comment-"+JDATA.tfData.id_post+" i").addClass("commented");

          
        }else{
          $( "#one-comment-"+JDATA.tfData.id_post).prepend('<span class="post-comment"><b>'+sessionStorage.getItem("username")+' </b>'+JDATA.tfData.comment+'<span class="post-ago"> - Hace un instante</span></span>' );  
        } 
        $("#cm_post_comment_id_post").val("");
        $("#cm_post_comment_comment").val("");
        $('#exampleModal').modal('hide');
      }else{
        console.log("NO OK");
      }
    } 
}


$(document).ready(function(){
  cmPostCommentRules();

})
</script>