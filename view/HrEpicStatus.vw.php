<?php
  if (!is_object($tfs)){
    header("Location: ../403.html");
    exit();
  }

  switch ($tfRequestAction){
   case "AN":
   case "AI":
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_epic_status_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpicStatus" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>';
    break;
   case "AE":
   case "AB":
   case "AA":
$audit='<div class="col-lg-12 container">
          <span class="created_by">Created  by '.TUser::description($tfs,$hrEpicStatus->getCreatedBy()).'  on '.$hrEpicStatus->getCreatedDate().'</span>
        </div>';
    $buttons ='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_epic_status_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpicStatus" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>';
    $buttons.='<a class="btn btn-epic-primary" role="button" data-tf-form="#hr_epic_status_form" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpicStatus" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>';
    break;
  }
  foreach ($hrEpicStatus->getObjError() as $oe) {
    $objAlerts.=TfWidget::alertDangerTemplate($oe);
  }
  foreach ($hrEpicStatus->getObjMsg() as $om) {
    $objAlerts.=TfWidget::alertSuccessTemplate($om);
  }
  $html = '<div class="row">
  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">
    <form id="hr_epic_status_form" name="hr_epic_status_form" method="post" onsubmit="return false" class="form-horizontal info-panel">
      <fieldset>
      <div class="col-lg-12 container title">Hr Epic Status</div>
      <div class="col-lg-12 container">'.$objAlerts.'</div>
      <div class="col-lg-12 container">
         <input type="hidden" id="is_hr_epic_status" name="is_hr_epic_status" value="'.$hrEpicStatus->getInitialState().'">
         <input type="hidden" id="hr_epic_status_id" name="hr_epic_status_id" maxlength="22" value="'.$hrEpicStatus->getId().'">

      </div>
      <div class="col-lg-12 container">
       <label for="hr_epic_status_name" class="control-label">Name:</label>
        <input type="text" id="hr_epic_status_name" name="hr_epic_status_name" class="hr_epic_status_name form-control"  maxlength="200"  value="'.$hrEpicStatus->getName().'"  tabindex="1"/>
      <label for="hr_epic_status_name" class="error">'.$hrEpicStatus->getAttrError("name").'</label>
      </div>
      <div class="col-lg-12 container">
       <label for="hr_epic_status_active" class="control-label">Active:</label>
        <select  id="hr_epic_status_active" name="hr_epic_status_active" class="hr_epic_status_active form-control" tabindex="2">
      <option value="">Select a option</option>'.
      TfWidget::selectStructureYN($hrEpicStatus->getActive(),'Y').
'      </select>
      <label for="hr_epic_status_active" class="error">'.$hrEpicStatus->getAttrError("active").'</label>
      </div>

'.$audit.'

   </fieldset>
  </form>
 </div>
</div>';
  echo $html;
?>
<script type="text/javascript">function hrEpicStatusRules(){
  $("#hr_epic_status_form").validate();
  $("#hr_epic_status_name").rules("add", {
    required:true,
    maxlength:200
  });
  $("#hr_epic_status_active").rules("add", {
    required:true,
    maxlength:1
  });
  $("#hr_epic_status_created_by").rules("add", {
    required:true,
    number:true,
    maxlength:22
  });
  $("#hr_epic_status_created_date").rules("add", {
    required:true,
    maxlength:22
  });

}


$(document).ready(function(){
  hrEpicStatusRules();


})
</script>