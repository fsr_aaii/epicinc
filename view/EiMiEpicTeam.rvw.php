<style type="text/css">
 .modal{
  z-index: 9999;
  top:92px;
 }

 .modal-content {
    border: 0;
    border-radius: 0;
}
.bg-okrs{
  background-color: #8E44AD !important;
}
.bg-on-job{
  background-color: #187BCD !important;
}
.bg-self-training{
  background-color: #1ABC9C !important;
}
.bg-critical{
  background-color: #F39C12 !important;
}
.bg-learning{
  background-color: #FF437F !important;
}
.bg-perfomance{
  background-color: #D60404 !important;
}


</style>
<?php

$html=' <!-- Begin dynamic content --> 
<div class="card shadow mb-4">
  <div class="col-lg-12 my-epic-profile  col-container">    
    <div class="row">      
      <div class="col-lg-12 form-frame">   
        <!--begin team-->';
        
        $learningLevel= Array('Deep'=>'It <b>refers to a tendency or strong preference or fixed type of situation</b>, actions or knowledge this person <b>is able to interact and perform well</b>.It speaks about a type of behaviours in which a person <b>could express fixed and limited preferences on challenges</b>',
                   'Balanced'=>'It <b>refers to wider perspective on situations in</b> which a person can interact and perform well, but still has some situations that are not fully likely to work well.Limitations might be known, that <b>person might be stretching him/herself</b> to expand areas of impact.It might also be also a natural style.',
                   'Broad'=>'It <b>refers to a person who is able to navigate well</b> a variety of situations, and <b>still interact effectively and perform well</b>.<br>The tendency is even to proactively look for situations that are requiring to take that intelligence into another level.There is a tendency towards <b>curiosity for diverse challenges with resilience</b>.<br><b>It could be natural or adapted.</b>');

        $html2=''; 
        $profile='';
        
        $trimestre = array('1' => 'I', '2' => 'II', '3' => 'III', '4' => 'IV');
       
        $quarter=array_fill(1,4,array());
        $currentQuarter=HrEmployee::currentQuarter($tfs);

        foreach  ($quarter as $key => $value) {
           if ($key<$currentQuarter){
            $quarter[$key]["tab"]='ro';
           }elseif ($key>$currentQuarter) {
             $quarter[$key]["tab"]='disabled'; 
           }else{
             $quarter[$key]["link"]='active';
             $quarter[$key]["tab"]='enable';
             $quarter[$key]["pane"]='active show';
             $quarter[$key]["accordion"]='active';
           } 
        }  
         
        //Begin foreach hrEmployeeList
        foreach ($hrEmployeeList as $row) {
          $tfData = array();
          $tfData["hr_employee_id"] = $row["id_employee"];
          $tfData["gl_natural_person_id"] = $row["id_person"];
          $tenure='';
          $in_role='';

          if ($row["tenure_year"]>0){
            $tenure.=$row["tenure_year"].'y ';
            $month = $row["tenure_month"]-($row["tenure_year"]*12);
            $tenure.=$month.'m';
          }elseif ($row["tenure_month"]>0) {
            $tenure.=$row["tenure_month"].'m';
          }elseif ($row["tenure_day"]>0) {
            $tenure.=$row["tenure_day"].'d';
          }else{
            $tenure.='NYA';
          }
          if ($row["in_role_year"]>0){
            $in_role.=$row["in_role_year"].'y ';
            $month = $row["in_role_month"]-($row["in_role_year"]*12);
            $in_role.=$month.'m';
          }elseif ($row["in_role_month"]>0) {
            $in_role.=$row["in_role_month"].'m';
          }elseif ($row["in_role_day"]>0) {
            $in_role.=$row["in_role_day"].'d';
          }else{
            $in_role.='NYA';
          }
           
          $health=array();
          if($row["criticality_level"]=='Critical' and $row["is_key_talent"]!='Y' ){
            $health[]='Search for a Key Talent to fill this position.';
          }
          if($row["criticality_level"]=='Critical' and $row["is_key_talent"]=='Y' and  in_array($row["resignation_risk"],array("M","H"))){
            $health[]='Talk to your team member to reduce the flight risk and ensure that you have a successor for the position.';
          }
          if($row["criticality_level"]=='Critical' and $row["has_successor"]!='Y' ){
            $health[]='Look for successors or develop successors.';
          }
          if($row["criticality_level"]!='Critical' and $row["is_key_talent"]=='Y' ){
            $health[]='Try to move this team member to a critical position.';
          }
          if($row["learning_agility_level"]=='Deep'){
            $health[]='Develop this employee for a position in the same area or develop the people agility in the employee if he/she will become a people manager.';
          }
          if($row["learning_agility_level"]=='Broad'){
            $health[]='Develop this employee for a leadership position.';
          }

          if($row["development_moment"]=='Preparation/Expansion'){
            $health[]='Assign a learning experience to close the gaps. Remember to create a growth plan based on the 70.20.10 learning model.';
          }
          if($row["development_moment"]=='Ready'){
            $health[]='Make sure the employee has a promotion, lateral move, assignment or any other growth action.';
          }
          if($row["performace_level"]=='Exceed' and  in_array($row["resignation_risk"],array("M","H"))){
            $health[]='Talk to your employee and try to reduce the flight risk.';
          }
          if($row["development_moment"]=='Learning'){
            $health[]='Make sure your employee has defined SMART goals and has a GROWTH PLAN in order to reduce the learning curve.';
          }
          $health_html='';
          foreach ($health as $r){
            $health_html='<p class="dashboard-list">'.$r.'<p>';
          }  
   
          $managerLAGroupDetails=HrEmployeeEvaluation::groupDetails($tfs,$row["id_employee"],$row["learning_agility_date"],'M'); 
          $employeeLAGroupDetails=HrEmployeeEvaluation::groupDetails($tfs,$row["id_employee"],$row["self_learning_agility_date"],'E'); 
          
          $go=false;
          $total=0;
          $qty=0;
          $chartLabels="";
          $chartData="";
          foreach ($managerLAGroupDetails as $r){
            $total+=$r["total"];
            $qty+=$r["qty"];
            $chartLabels.="'".$r["name"]."',"; 
            $chartData.="'".round($r["total"]/$r["qty"],2)."',"; 
          } 
          
          $datasets="";
          if ($qty!=0){
            $labels=$chartLabels;
            $datasets.="{label: 'Manager',
                              backgroundColor: 'rgba(142, 68, 173, 0.5)',
                              borderColor: 'rgba(142, 68, 173, 0.8)',
                              pointBackgroundColor: 'rgba(142, 68, 173, 1.0)',
                              data: [".$chartData."]},"; 
            $go=true;                  
          }
          
          if (count($managerLAGroupDetails)==count($employeeLAGroupDetails) OR !$go){
            $total=0;
            $qty=0;
            $chartLabels="";
            $chartData="";
            foreach ($employeeLAGroupDetails as $r){
              $total+=$r["total"];
              $qty+=$r["qty"];
              $chartLabels.="'".$r["name"]."',"; 
              $chartData.="'".round($r["total"]/$r["qty"],2)."',"; 
            }  

            if ($qty!=0){
              $labels=$chartLabels;
              $datasets.="{label: 'Employee',
                                backgroundColor: 'rgba(26, 188, 156, 0.5)',
                                borderColor: 'rgba(26, 188, 156, 0.8)',
                                pointBackgroundColor: 'rgba(26, 188, 156, 1.0)',
                                data: [".$chartData."]},"; 
              $go=true;                  
            }
          }  
        
          if ($go){            
            $script.="var ctx = document.getElementById('la_group_details_".$row["id_employee"]."').getContext('2d');

                       var myChart = new Chart(ctx, {
                        type: 'radar',
                        data: { labels: [".$labels."],
                                datasets: [".$datasets."]
                              },
                        options: {
                      legend: {
                        position: 'bottom',
                      },
                      title: {
                        display: false
                      },
                      scale: {
                        ticks: {
                          beginAtZero: true
                        }
                      }
                    }
                    });";
          }  

          
          $html.='<div id="epic-profile-'.$row["id_employee"].'" class="col-4 coaching" data-show-profile="team-epic-profile-'.$row["id_employee"].'" data-id-employee="'.$row["id_employee"].'">
            <div class="d-flex align-items-center">
                <div class="mr-2">
                    <img class="rounded-circle" width="45" src="'.$row["photo"].'" alt="">
                </div>
                <div class="ml-2">
                    <div class="epic h2 m-0">'.$row["name"].'</div>
                    <div class="epic h4 m-0 text-muted">'.$row["business_title"].'</div>
                    <div class="epic h6 m-0 text-muted">'.$row["email_account"].'</div>
                </div>
            </div>
          </div>'; 
          

          $profile.=' <!---Begin team-epic-profile-->
          <div id="team-epic-profile-'.$row["id_employee"].'" id="team-epic-profile-'.$row["id_employee"].'" class="col-lg-12 team-epic-profile col-container" style="display: none;">
                  <!---Begin team-epic-profile row-->
                  <div class="row">
                    <div class="col-lg-2 col-container text-center">
                      <img class="img-profile rounded-circle" src="'.$row["photo"].'">
                    </div>
                    <div class="col-lg-3 data-col">
                      <div class="col-12 container">
                        <span class="name-profile">'.$row["name"].'</span> 
                      </div>
                      <div class="col-12 container">
                        <span class="business-title-profile">'.$row["business_title"].'</span>
                      </div>
                      <div class="col-lg-12 container">
                        <span class="data-text">'.$row["development_moment"].' to '.$row["job_level_next"].'</span>
                      </div>
                      <div class="col-lg-12 container">
                        <span class="data-text">Tenure: '.$tenure.' In Role: '.$in_role.'</span>
                      </div>
                      <div class="col-lg-12 container">
                        <span class="dashboard data-text far"> Flight Risk: '.$row["resignation_risk"].'</span>
                      </div>
                    </div>
                    <div class="col-lg-2 container progress-circle text-center">
                      <div class="progress epic">
                        <span class="progress-left">
                          <span class="progress-bar empty"></span>
                          <span class="progress-bar lv3-'.$row["id_criticality_level"].' c3-'.$row["id_criticality_level"].'"></span>
                        </span>
                        <span class="progress-right">
                          <span class="progress-bar empty"></span>
                          <span class="progress-bar lv3-'.$row["id_criticality_level"].' c3-'.$row["id_criticality_level"].'"></span>
                        </span>
                        <div class="progress-value">
                          <p class="primary">'.$row["criticality_level"].'</p>
                          <p class="secondary"> </p>
                        </div>
                      </div>
                      <div class="progress-label">Criticality</div>
                    </div>
                    <div class="col-lg-2 container progress-circle text-center">
                     <div class="progress epic">
                        <span class="progress-left">
                          <span class="progress-bar empty"></span>
                          <span class="progress-bar vs lv3-'.$row["id_self_performance_level"].' c3-'.$row["id_performance_level"].'"></span>
                          <span class="progress-bar lv3-'.$row["id_performance_level"].' c3-'.$row["id_performance_level"].'"></span>
                        </span>
                        <span class="progress-right">
                           <span class="progress-bar empty"></span>
                          <span class="progress-bar vs lv3-'.$row["id_self_performance_level"].' c3-'.$row["id_performance_level"].'"></span>
                          <span class="progress-bar lv3-'.$row["id_performance_level"].' c3-'.$row["id_performance_level"].'"></span>
                        </span>
                        <div class="progress-value">
                          <p class="primary">'.$row["performance_level"].'</p>
                          <p class="secondary">'.$row["self_performance_level"].'</p>
                        </div>
                      </div>
                      <div class="progress-label">Performace</div>
                    </div>
                    <div class="col-lg-2 container progress-circle text-center">
                      <div class="progress epic">
                        <span class="progress-left">
                          <span class="progress-bar empty"></span>
                          <span class="progress-bar vs lv3-'.$row["id_self_learning_agility_level"].' c3-'.$row["id_learning_agility_level"].'"></span>
                          <span class="progress-bar lv3-'.$row["id_learning_agility_level"].' c3-'.$row["id_learning_agility_level"].'"></span>
                        </span>
                        <span class="progress-right">
                          <span class="progress-bar empty"></span>
                          <span class="progress-bar vs lv3-'.$row["id_self_learning_agility_level"].' c3-'.$row["id_learning_agility_level"].'"></span>
                          <span class="progress-bar lv3-'.$row["id_learning_agility_level"].' c3-'.$row["id_learning_agility_level"].'"></span>
                        </span>
                        <div class="progress-value">
                          <p class="primary">'.$row["learning_agility_level"].'</p>
                          <p class="secondary">'.$row["self_learning_agility_level"].'</p>
                        </div>
                      </div>
                      <div class="progress-label">Learning Agility</div>
                    </div>
                    <div class="col-lg-1 data-col">
                      <div class="col-12 container">
                        <a class="btn btn-epic-2 xl rounded-circle" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployee" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).'"onclick="TfRequest.do(this);">
                           <i class="bx bx-edit-alt"></i>
                        </a>
                       </div>
                    </div>
                      
                    <!---Begin team-epic-profile-extra-->
                    <div class="row team-epic-profile-extra">  
                      <div class="col-lg-12 form-frame">
                        <div class="col-lg-6 container">
                          <h7 style="color: #334576;">'.$row["learning_agility_level"].'</h7>
                          <p>'.$learningLevel[$row["learning_agility_level"]].'</p>
                          <h7 style="color: #334576;">Health</h7>
                          '.$health_html.'
                        </div>
                        <div class="col-lg-6 container text-center">
                           <canvas id="la_group_details_'.$row["id_employee"].'" class="col-12" ></canvas>
                        </div>  
                      </div>
                    </div>  
                    <!---End team-epic-profile-extra-->
                    <!---Begin tabs QUARTER-->
                    <div class="container tab-accordion">
                      <ul id="tabs" class="nav nav-tabs" role="tablist">';
                    //Begin x 1..4  
                    
                  for ($x = 1; $x <= 4; $x++) {
                    $quarterProgress[$x] = TfWidget::amount(HrEpic::progressByEmployeeQuarter($tfs,$row["id_employee"],$x));
                    $profile.='<li class="nav-item '.$quarter[$x]["tab"].'">
                            <a id="tab-Q'.$x.'-'.$row["id_employee"].'" name="tab-Q'.$x.'-'.$row["id_employee"].'" href="#pane-Q'.$x.'-'.$row["id_employee"].'" class="nav-link quarte '.$quarter[$x]["link"].'" data-toggle="tab" role="tab">Trimestre '.$trimestre[$x].' - '.$quarterProgress[$x].'%</a>
                        </li>';
                  } 
                  //End x 1..4     

      $profile.='  </ul> 
                      <!---Begin tabs content-->
                      <div id="content-'.$row["id_employee"].'" class="tab-content" role="tablist">';
      //Begin x 1..4
      for ($x = 1; $x <= 4; $x++) {                 
        $profile.='     <!---Begin pane QUARTER '.$trimestre[$x].'-->
                        <div id="pane-Q'.$x.'-'.$row["id_employee"].'" class="card tab-pane fade '.$quarter[$x]["pane"].'" role="tabpanel" aria-labelledby="tab-Q'.$x.'-'.$row["id_employee"].'">
                          <!---Begin card-header QUARTER '.$trimestre[$x].'-->
                          <div class="card-header" role="tab" id="heading-Q'.$x.'-'.$row["id_employee"].'">
                            <h5 class="mb-0">
                              <a data-toggle="collapse" href="#collapse-Q'.$x.'-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-Q'.$x.'-'.$row["id_employee"].'">
                                    Trimestre '.$trimestre[$x].' - '.$quarterProgress[$x].'%
                              </a>
                            </h5>
                          </div>
                          <!---End card-header QUARTER '.$trimestre[$x].'-->
                          <!---Begin collapse QUARTER '.$trimestre[$x].'-->
                          <div id="collapse-Q'.$x.'-'.$row["id_employee"].'" class="collapse" data-parent="#content-'.$row["id_employee"].'" role="tabpanel" aria-labelledby="heading-Q'.$x.'-'.$row["id_employee"].'">
                            <!---Begin card-body QUARTER '.$trimestre[$x].'-->
                            <div class="card-body">
                              <!---Begin card-body row QUARTER '.$trimestre[$x].'-->
                              <div class="row">';           
        //Begin IF quarter tab disabled          
        if ($quarter[$x]["tab"]!='disabled'){
          
          $criticalityEvaluationList=HrPositionEvaluation::listByPositionQuarter($tfs,$row["id_position"],$x);
          $learningEvaluationList=HrEmployeeEvaluation::listByEmployeeQuarter($tfs,3,$row["id_employee"],'M',$x);
          $performanceEvaluationList=HrEmployeeEvaluation::listByEmployeeQuarter($tfs,4,$row["id_employee"],'M',$x);
          $epicList=HrEpic::listByEmployeeQuarter($tfs,$row["id_employee"],$x);
          $coachingList=HrCoaching::listByEmployeeQuarter($tfs,$row["id_employee"],$x);
          $onTheJobList=HrTraining::listByEmployeeQuarter($tfs,$row["id_employee"],1,$x);
          $selfTrainingList=HrTraining::listByEmployeeQuarter($tfs,$row["id_employee"],2,$x);


          //$tfData = array();
          //$tfData["hr_position_evaluation_id_position"] = $row["id_position"];
          
          if ($quarter[$x]["pane"]=='active show'){
            $collapseOKR = "show";
            $expandedOKR = "true";
            $accordionOKR = "";
          }else{
            $collapseOKR = "";
            $expandedOKR = "false";
            $accordionOKR = "collapsed";
          }
          $profile.='           <!--begin accordion-okr-->
                                <div id="accordion-okr-Q'.$x.'-'.$row["id_employee"].'" class="col-lg-12 container">
                                  <div class="my-epic-profile card">
                                    <!--begin accordion-okr-header-->
                                    <div class="col-12 card-header" id="heading-okr-Q'.$x.'-'.$row["id_employee"].'">
                                      <div class="col-11 container"> 
                                        <a id="accordion-okr-a-Q'.$x.'-'.$row["id_employee"].'"  href="#" class="section '.$quarter[$x]["accordion"].' '.$accordionOKR.'" data-toggle="collapse" data-target="#collapse-okr-Q'.$x.'-'.$row["id_employee"].'" aria-expanded="'.$expandedOKR.'" aria-controls="collapse-okr-Q'.$x.'-'.$row["id_employee"].'" data-form-action="form-action-okr-Q'.$x.'-'.$row["id_employee"].'"> Mira sus OKRs</a>
                                      </div>
                                      <div class="col-1 container text-right">';
          if ($quarter[$x]["accordion"]=='active'){     
            $tfData = array();
            $tfData["hr_epic_id_employee"] = $row["id_employee"];    
            $tfData["hr_epic_id_position"] = $row["id_position"];                 
            $profile.='                       <a id="form-action-okr-Q'.$x.'-'.$row["id_employee"].'" name="form-action-okr-Q'.$x.'-'.$row["id_employee"].'" class="btn-epic-sm" alt="New" title="New" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpic" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-action="AN" onclick="TfRequest.do(this);">            
                                             <i class="bx bx-plus"></i>
                                            </a>';
          } 
          $profile.='                      </div>
                                        </div>
                                        <!--end accordion-okr-header-->
                                        <!--begin accordion-okr-content-->
                                        <div id="collapse-okr-Q'.$x.'-'.$row["id_employee"].'" class="collapsed collapse '.$collapseOKR.'" aria-labelledby="heading-okr-Q'.$x.'-'.$row["id_employee"].'" data-parent="#accordion-okr-Q'.$x.'-'.$row["id_employee"].'">';  
          if (count($epicList)==0){
             $profile.='                 No existen OKRs en este trimestre';
          }else{
            $profile.='                 <div class="row epic">
                                          <div class="col-lg-2 col-container text-left">
                                          </div>
                                          <div class="col-lg-2 col-container epic-text-sm text-left">
                                            Estatus
                                          </div> 
                                          <div class="col-lg-1 col-container epic-text-sm text-center">
                                            Prioridad
                                          </div>                  
                                          <div class="col-lg-1 col-container epic-text-sm text-center">
                                            Actividades  
                                          </div>       
                                          <div class="col-lg-5 col-container epic-text-sm text-left">
                                            Progreso
                                          </div>             
                                          <div class="col-lg-1 col-container epic-text-sm text-right">
                                          </div>      
                                      </div>    ';     
            $i=0;
            foreach ($epicList as $c){
              $i++;
              $tfData = array();
              $tfData["hr_epic_id"] = $c["id"];
              $profile.='           <div class="row epic">
                                   <div class="col-lg-2 col-container text-left">
                                    <div class="h7 m-0">#'.$i.' Epic</div>
                                    <div class="text-muted h6">Estimado '.$c["completion_estimated_date"].'</div>
                                  </div>
                                  <div class="col-lg-2 col-container text-left">
                                    <div class="h7 m-0">
                                      <i class="bx bxs-circle" style="color:#1cc88a; font-size: 0.9rem;"></i> '.$c["status"].'
                                    </div>
                                    <div class="text-muted h6">'.$c["status_date"].'</div>
                                  </div>   
                                  <div class="col-lg-1 col-container text-left">
                                      <div class="epic-bottom">
                                         <span class="badge badge-pill c3-'.$c["id_epic_priority"].'">'.$c["priority"].'</span>
                                      </div>
                                  </div>    
                                  <div class="col-lg-1 col-container text-center">
                                    <div class="epic-bottom">
                                      <span class="badge badge-success">'.$c["activities"].'</span>
                                    </div>  
                                  </div>  
                                  <div class="col-lg-5 col-container text-left">
                                    <div class="epic-text-sm m-0">'.$c["progress"].'%</div>
                                    <div class="progress">
                                      <div class="progress-bar bg-okrs" role="progressbar" style="width: '.$c["progress"].'%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                      </div>
                                    </div>
                                  </div>     
                                  <div class="col-lg-1 col-container text-right">';
            if ($quarter[$x]["accordion"]=='active'){                                
              $profile.='              <a class="btn-epic-sm" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEpic" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).' "onclick="TfRequest.do(this);">
               <i class="bx bx-pencil"></i>
             </a>';
            }         
            $profile.='         </div> 
                              <div class="col-lg-12 col-container text-left">'.$c["description"].'</div>
                          </div>';

          }                

         }                           
         $profile.='                   </div>
                                      <!--end accordion-okr-content-->
                                    </div>
                                  </div>
                                  <!--end accordion-okr-->'; 
         
          $profile.='           <!--begin accordion-onthejob-->
                                <div id="accordion-onthejob-Q'.$x.'-'.$row["id_employee"].'" class="col-lg-12 container">
                                  <div class="my-epic-profile card">
                                    <!--begin accordion-onthejob-header-->
                                    <div class="col-12 card-header" id="heading-onthejob-Q'.$x.'-'.$row["id_employee"].'">
                                      <div class="col-11 container"> 
                                        <a id="accordion-onthejob-a-Q'.$x.'-'.$row["id_employee"].'"  href="#" class="section '.$quarter[$x]["accordion"].' collapsed" data-toggle="collapse" data-target="#collapse-onthejob-Q'.$x.'-'.$row["id_employee"].'" aria-expanded="false" aria-controls="collapse-onthejob-Q'.$x.'-'.$row["id_employee"].'" data-form-action="form-action-onthejob-Q'.$x.'-'.$row["id_employee"].'"> Training on the job</a>
                                      </div>
                                      <div class="col-1 container text-right">';
          if ($quarter[$x]["accordion"]=='active'){     
            $tfData = array();
            $tfData["hr_training_id_employee"] = $row["id_employee"];    
            $tfData["hr_training_id_position"] = $row["id_position"];  
            $tfData["hr_training_id_training_type"] = 1;         

            $profile.='                       <a id="form-action-onthejob-Q'.$x.'-'.$row["id_employee"].'" name="form-action-onthejob-Q'.$x.'-'.$row["id_employee"].'" class="btn-epic-sm" alt="New" title="New" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrTraining" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-action="AN" onclick="TfRequest.do(this);" style="display: none;">            
                                             <i class="bx bx-plus"></i>
                                            </a>';
          } 
          $profile.='                      </div>
                                        </div>
                                        <!--end accordion-onthejob-header-->
                                        <!--begin accordion-onthejob-content-->
                                        <div id="collapse-onthejob-Q'.$x.'-'.$row["id_employee"].'" class="collapsed collapse" aria-labelledby="heading-onthejob-Q'.$x.'-'.$row["id_employee"].'" data-parent="#accordion-onthejob-Q'.$x.'-'.$row["id_employee"].'">';  

          if (count($onTheJobList)==0){
             $profile.='                 No existen training en este trimestre';
          }else{
            $profile.='                 <div class="row epic">
                                          <div class="col-lg-2 col-container text-left">
                                          </div>
                                          <div class="col-lg-2 col-container epic-text-sm text-left">
                                            Estatus
                                          </div> 
                                          <div class="col-lg-1 col-container epic-text-sm text-center">
                                            Prioridad
                                          </div>                  
                                          <div class="col-lg-1 col-container epic-text-sm text-center">
                                            Horas  
                                          </div>       
                                          <div class="col-lg-5 col-container epic-text-sm text-left">
                                            Progreso
                                          </div>             
                                          <div class="col-lg-1 col-container epic-text-sm text-right">
                                          </div>      
                                      </div>    ';     
            $i=0;
            foreach ($onTheJobList as $c){
              $i++;
              $tfData = array();
              $tfData["hr_training_id"] = $c["id"];
              $profile.='           <div class="row epic">
                                   <div class="col-lg-2 col-container text-left">
                                    <div class="h7 m-0">#'.$i.' '.$c["name"].'</div>
                                    <div class="text-muted h6">Estimado '.$c["completion_estimated_date"].'</div>
                                  </div>
                                  <div class="col-lg-2 col-container text-left">
                                    <div class="h7 m-0">
                                      <i class="bx bxs-circle" style="color:#1cc88a; font-size: 0.9rem;"></i> '.$c["status"].'
                                    </div>
                                    <div class="text-muted h6">'.$c["status_date"].'</div>
                                  </div>   
                                  <div class="col-lg-1 col-container text-left">
                                      <div class="epic-bottom">
                                         <span class="badge badge-pill c3-'.$c["id_epic_priority"].'">'.$c["priority"].'</span>
                                      </div>
                                  </div>    
                                  <div class="col-lg-1 col-container text-center">
                                    <div class="epic-bottom">
                                      <span class="badge badge-success">'.$c["hour"].'</span>
                                    </div>  
                                  </div>  
                                  <div class="col-lg-5 col-container text-left">
                                    <div class="epic-text-sm m-0">'.$c["progress"].'%</div>
                                    <div class="progress">
                                      <div class="progress-bar bg-on-job" role="progressbar" style="width: '.$c["progress"].'%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                      </div>
                                    </div>
                                  </div>     
                                  <div class="col-lg-1 col-container text-right">';
            if ($quarter[$x]["accordion"]=='active'){                                
              $profile.='              <a class="btn-epic-sm" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrTraining" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).' "onclick="TfRequest.do(this);">
               <i class="bx bx-pencil"></i>
             </a>';
            }         
            $profile.='         </div> 
                              <div class="col-lg-12 col-container text-left">'.$c["description"].'</div>
                          </div>';

          }                

         }                           
         $profile.='                   </div>
                                      <!--end accordion-onthejob-content-->
                                    </div>
                                  </div>
                                  <!--end accordion-onthejob-->';  
         $profile.='           <!--begin accordion-selftraining-->
                                <div id="accordion-selftraining-Q'.$x.'-'.$row["id_employee"].'" class="col-lg-12 container">
                                  <div class="my-epic-profile card">
                                    <!--begin accordion-selftraining-header-->
                                    <div class="col-12 card-header" id="heading-selftraining-Q'.$x.'-'.$row["id_employee"].'">
                                      <div class="col-11 container"> 
                                        <a id="accordion-selftraining-a-Q'.$x.'-'.$row["id_employee"].'"  href="#" class="section '.$quarter[$x]["accordion"].' collapsed" data-toggle="collapse" data-target="#collapse-selftraining-Q'.$x.'-'.$row["id_employee"].'" aria-expanded="false" aria-controls="collapse-selftraining-Q'.$x.'-'.$row["id_employee"].'" data-form-action="form-action-selftraining-Q'.$x.'-'.$row["id_employee"].'"> Self Training </a>
                                      </div>
                                      <div class="col-1 container text-right">';
          if ($quarter[$x]["accordion"]=='active'){     
            $tfData = array();
            $tfData["hr_training_id_employee"] = $row["id_employee"];    
            $tfData["hr_training_id_position"] = $row["id_position"];  
            $tfData["hr_training_id_training_type"] = 2;         

            $profile.='                       <a id="form-action-selftraining-Q'.$x.'-'.$row["id_employee"].'" name="form-action-selftraining-Q'.$x.'-'.$row["id_employee"].'" class="btn-epic-sm" alt="New" title="New" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrTraining" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-action="AN" onclick="TfRequest.do(this);" style="display: none;">            
                                             <i class="bx bx-plus"></i>
                                            </a>';
          } 
          $profile.='                      </div>
                                        </div>
                                        <!--end accordion-selftraining-header-->
                                        <!--begin accordion-selftraining-content-->
                                        <div id="collapse-selftraining-Q'.$x.'-'.$row["id_employee"].'" class="collapsed collapse" aria-labelledby="heading-selftraining-Q'.$x.'-'.$row["id_employee"].'" data-parent="#accordion-selftraining-Q'.$x.'-'.$row["id_employee"].'">';  

          if (count($selfTrainingList)==0){
             $profile.='                 No existen training en este trimestre';
          }else{
            $profile.='                 <div class="row epic">
                                          <div class="col-lg-2 col-container text-left">
                                          </div>
                                          <div class="col-lg-2 col-container epic-text-sm text-left">
                                            Estatus
                                          </div> 
                                          <div class="col-lg-1 col-container epic-text-sm text-center">
                                            Prioridad
                                          </div>                  
                                          <div class="col-lg-1 col-container epic-text-sm text-center">
                                            Horas  
                                          </div>       
                                          <div class="col-lg-5 col-container epic-text-sm text-left">
                                            Progreso
                                          </div>             
                                          <div class="col-lg-1 col-container epic-text-sm text-right">
                                          </div>      
                                      </div>    ';     
            $i=0;
            foreach ($selfTrainingList as $c){
              $i++;
              $tfData = array();
              $tfData["hr_training_id"] = $c["id"];
              $profile.='           <div class="row epic">
                                   <div class="col-lg-2 col-container text-left">
                                    <div class="h7 m-0">#'.$i.' '.$c["name"].'</div>
                                    <div class="text-muted h6">Estimado '.$c["completion_estimated_date"].'</div>
                                  </div>
                                  <div class="col-lg-2 col-container text-left">
                                    <div class="h7 m-0">
                                      <i class="bx bxs-circle" style="color:#1cc88a; font-size: 0.9rem;"></i> '.$c["status"].'
                                    </div>
                                    <div class="text-muted h6">'.$c["status_date"].'</div>
                                  </div>   
                                  <div class="col-lg-1 col-container text-left">
                                      <div class="epic-bottom">
                                         <span class="badge badge-pill c3-'.$c["id_epic_priority"].'">'.$c["priority"].'</span>
                                      </div>
                                  </div>    
                                  <div class="col-lg-1 col-container text-center">
                                    <div class="epic-bottom">
                                      <span class="badge badge-success">'.$c["hour"].'</span>
                                    </div>  
                                  </div>  
                                  <div class="col-lg-5 col-container text-left">
                                    <div class="epic-text-sm m-0">'.$c["progress"].'%</div>
                                    <div class="progress">
                                      <div class="progress-bar bg-self-training" role="progressbar" style="width: '.$c["progress"].'%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                      </div>
                                    </div>
                                  </div>     
                                  <div class="col-lg-1 col-container text-right">';
            if ($quarter[$x]["accordion"]=='active'){                                
              $profile.='              <a class="btn-epic-sm" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrTraining" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).' "onclick="TfRequest.do(this);">
               <i class="bx bx-pencil"></i>
             </a>';
            }         
            $profile.='         </div> 
                              <div class="col-lg-12 col-container text-left">'.$c["description"].'</div>
                          </div>';

          }                

         }                           
         $profile.='                   </div>
                                      <!--end accordion-selftraining-content-->
                                    </div>
                                  </div>
                                  <!--end accordion-selftraining-->';                           
        /* $profile.='                 <!--begin accordion-coaching-->
                                                <div id="accordion-coaching-Q'.$x.'-'.$row["id_employee"].'" class="col-lg-12 container">
                                                  <!--begin my-epic-profile card-->
                                                  <div class="my-epic-profile card">
                                                    <!--begin accordion-coaching-header-->
                                                    <div class="col-12 card-header" id="heading-coaching-Q'.$x.'-'.$row["id_employee"].'">
                                                      <div class="col-11 container"> 
                                                        <a id="accordion-coaching-a-Q'.$x.'-'.$row["id_employee"].'" href="#" class="section '.$quarter[$x]["accordion"].' collapsed" data-toggle="collapse" data-target="#collapse-coaching-Q'.$x.'-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-coaching-Q'.$x.'-'.$row["id_employee"].'" data-form-action="form-action-coaching-Q'.$x.'-'.$row["id_employee"].'"> Mentoring & Coaching</a>
                                                      </div>
                                                      <div class="col-1 container text-right">';
                      if ($quarter[$x]["accordion"]=='active'){   
                        $tfData = array();
                      $tfData["hr_coaching_id_employee"] = $row["id_employee"];                              
                        $profile.='                       <a id="form-action-coaching-Q'.$x.'-'.$row["id_employee"].'" name="form-action-coaching-Q'.$x.'-'.$row["id_employee"].'" class="btn-epic-sm btn-coaching-create" alt="New" title="New" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrCoaching" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-action="AI" style="display: none;">            
                                                           <i class="bx bx-plus"></i>
                                                          </a>';
                      } 
                      $profile.='                      </div>
                                                    </div>
                                                    <!--end accordion-coaching-header-->
                                                    <!--begin accordion-coaching-content-->
                                                    <div id="collapse-coaching-Q'.$x.'-'.$row["id_employee"].'" class="collapsed collapse" aria-labelledby="heading-coaching-Q'.$x.'-'.$row["id_employee"].'" data-parent="#accordion-coaching-Q'.$x.'-'.$row["id_employee"].'">';  
                      // BEGIN if count
                      if (count($coachingList)==0){
                         $profile.='                 No se han asignado Coaching en este trimestre';
                      }else{
                            
                        $i=0;
                        // BEGIN foreach
                        foreach ($coachingList as $c){
                          $i++;
                          $tfData = array();
                          $tfData["hr_coaching_id"] = $c["id"];
                          $profile.=' <!--Begin row coaching-->
                                       <div class="col-4 coaching">
                                          <div class="d-flex justify-content-between align-items-center">
                                            <div class="mr-2">
                                              <img class="rounded-circle" width="45" src="'.$c["photo"].'" alt="">
                                            </div>
                                            <div class="ml-2">
                                              <div class="epic h2 m-0">'.$c["name"].'</div>
                                              <div class="epic h4 m-0 text-muted">'.$c["business_title"].'</div>
                                              <div class="epic h6 m-0 text-muted">'.$c["email_account"].'</div>
                                            </div>
                                            <div class="actions">';
                          // BEGIN if quarter
                          if ($quarter[$x]["accordion"]=='active'){                                
                             $profile.='              <i class="bx bx-message-square-detail"></i>
                                               <i class="bx bx-calendar-event"></i>
                                               <i class="bx bx-trash-alt"></i>';
                          }  
                          // END if quarter
                          $profile.='            </div>
                                                </div>
                                              </div>
                                              <!--End row coaching-->';
                        
                        }   
                        // END foreach                           
                      }    
                        // END if count
                      $profile.='                    </div>
                                                  <!--end accordion-coaching-content-->
                                                </div>
                                                <!--end my-epic-profile card-->
                                              </div>
                                              <!--end accordion-coaching-->';*/
                       
         $profile.='                 <!--begin accordion-criticality-->
                                                <div id="accordion-criticality-Q'.$x.'-'.$row["id_employee"].'" class="col-lg-12 container">
                                                  <!--begin my-epic-profile card-->
                                                  <div class="my-epic-profile card">
                                                    <!--begin accordion-criticality-header-->
                                                    <div class="col-12 card-header" id="heading-criticality-Q'.$x.'-'.$row["id_employee"].'">
                                                      <div class="col-11 container"> 
                                                        <a id="accordion-criticality-a-Q'.$x.'-'.$row["id_employee"].'" href="#" class="section '.$quarter[$x]["accordion"].' collapsed" data-toggle="collapse" data-target="#collapse-criticality-Q'.$x.'-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-criticality-Q'.$x.'-'.$row["id_employee"].'" data-form-action="form-action-criticality-Q'.$x.'-'.$row["id_employee"].'"> Evalua esta posicion</a>
                                                      </div>
                                                      <div class="col-1 container text-right">';
                      if ($quarter[$x]["accordion"]=='active'){   
                        $tfData = array();
                      $tfData["hr_position_evaluation_id_position"] = $row["id_position"];                              
                        $profile.='                       <a id="form-action-criticality-Q'.$x.'-'.$row["id_employee"].'" name="form-action-criticality-Q'.$x.'-'.$row["id_employee"].'" class="btn-epic-sm" alt="New" title="New" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPositionEvaluation" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-action="AI" onclick="TfRequest.do(this);" style="display: none;">            
                                                           <i class="bx bx-plus"></i>
                                                          </a>';
                      } 
                      $profile.='                      </div>
                                                    </div>
                                                    <!--end accordion-criticality-header-->
                                                    <!--begin accordion-criticality-content-->
                                                    <div id="collapse-criticality-Q'.$x.'-'.$row["id_employee"].'" class="collapsed collapse" aria-labelledby="heading-criticality-Q'.$x.'-'.$row["id_employee"].'" data-parent="#accordion-criticality-Q'.$x.'-'.$row["id_employee"].'">';  
                      // BEGIN if count
                      if (count($criticalityEvaluationList)==0){
                         $profile.='                 No existen evaluaciones en este trimestre';
                      }else{
                        $profile.='                  <!--Begin row epic-->
                                                    <div class="row epic">
                                                      <div class="col-lg-3 col-container text-left">
                                                      </div>
                                                      <div class="col-lg-4 col-container epic-text-sm text-left">
                                                        Criticality
                                                      </div> 
                                                      <div class="col-lg-4 col-container epic-text-sm text-left">
                                                        Leadership Bank
                                                      </div>                              
                                                      <div class="col-lg-1 col-container epic-text-sm text-right">
                                                      </div>
                                                  </div>
                                                  <!--End row epic-->   ';     
                        $i=0;
                        // BEGIN foreach
                        foreach ($criticalityEvaluationList as $c){
                          $i++;
                          $tfData = array();
                          $tfData["hr_position_evaluation_id"] = $c["id"];
                          $profile.='         <!--Begin row epic-->
                                            <div class="row epic">
                                              <div class="col-lg-3 col-container text-left">
                                                <div class="h7 m-0">#'.$i.' Test</div>
                                                <div class="text-muted h6">'.$c["created_date"].'</div>
                                              </div>
                                              <div class="col-lg-4 col-container text-left">
                                                <div class="epic-bottom">
                                                  <span class="badge badge-pill badge-secondary">'.$c["criticality"].'</span>
                                                  <span class="badge badge-pill c3-'.$c["id_criticality_level"].'">'.$c["criticality_level"].'</span>
                                                </div>
                                              </div>                  
                                              <div class="col-lg-4 col-container text-left">
                                                <div class="epic-bottom">
                                                  <span class="badge badge-pill badge-secondary">'.$c["leadership"].'</span>
                                                  <span class="badge badge-pill c3-'.$c["id_leadership_level"].'">'.$c["leadership_level"].'</span>
                                                </div>
                                              </div>             
                                        <div class="col-lg-1 col-container text-right">';
                          // BEGIN if quarter
                          if ($quarter[$x]["accordion"]=='active'){                                
                             $profile.='              <a class="btn-epic-sm" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPositionEvaluation" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).' "onclick="TfRequest.do(this);">
                                   <i class="bx bx-pencil"></i>
                                 </a>';
                          }  
                          // END if quarter
                          $profile.='               </div>
                                              </div>
                                              <!--End row epic-->';
                        
                        }   
                        // END foreach                           
                      }    
                        // END if count
                      $profile.='                    </div>
                                                  <!--end accordion-criticality-content-->
                                                </div>
                                                <!--end my-epic-profile card-->
                                              </div>
                                              <!--end accordion-criticality-->';   

           

                      $profile.='                 <!--begin accordion-learning-->
                                              <div id="accordion-learning-Q'.$x.'-'.$row["id_employee"].'" class="col-lg-12 container">
                                                <div class="my-epic-profile card">
                                                  <!--begin accordion-learning-header-->
                                                  <div class="col-12 card-header" id="heading-learning-Q'.$x.'-'.$row["id_employee"].'">
                                                    <div class="col-11 container"> 
                                                      <a id="accordion-learning-a-Q'.$x.'-'.$row["id_employee"].'" href="#" class="section '.$quarter[$x]["accordion"].' collapsed" data-toggle="collapse" data-target="#collapse-learning-Q'.$x.'-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-learning-Q'.$x.'-'.$row["id_employee"].'" data-form-action="form-action-learning-Q'.$x.'-'.$row["id_employee"].'"> Evalua su learning</a>
                                                    </div>
                                                    <div class="col-1 container text-right">';
                      if ($quarter[$x]["accordion"]=='active'){     
                        $tfData = array();
                        $tfData["hr_employee_evaluation_id_employee"] = $row["id_employee"];    
                        $tfData["hr_employee_evaluation_id_position"] = $row["id_position"];   
                        $tfData["hr_employee_evaluation_id_evaluation_type"] = 3;     
                        $tfData["hr_employee_evaluation_evaluator_type"] = 'M';                            
                        $profile.='                       <a id="form-action-learning-Q'.$x.'-'.$row["id_employee"].'" name="form-action-learning-Q'.$x.'-'.$row["id_employee"].'" class="btn-epic-sm" alt="New" title="New" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-action="AI" onclick="TfRequest.do(this);" style="display: none;">            
                                                         <i class="bx bx-plus"></i>
                                                        </a>';
                      } 
                      $profile.='                      </div>
                                                    </div>
                                                    <!--end accordion-learning-header-->
                                                    <!--begin accordion-learning-content-->
                                                    <div id="collapse-learning-Q'.$x.'-'.$row["id_employee"].'" class="collapsed collapse" aria-labelledby="heading-learning-Q'.$x.'-'.$row["id_employee"].'" data-parent="#accordion-learning-Q'.$x.'-'.$row["id_employee"].'">';  
                      if (count($learningEvaluationList)==0){
                         $profile.='                 No existen evaluaciones en este trimestre';
                      }else{
                        $profile.='                 <div class="row epic">
                                                      <div class="col-lg-3 col-container text-left">
                                                      </div>
                                                      <div class="col-lg-8 col-container epic-text-sm text-left">
                                                        Learning Agility
                                                      </div>                             
                                                      <div class="col-lg-1 col-container epic-text-sm text-right">
                                                      </div>
                                                  </div>    ';     
                        $i=0;
                        foreach ($learningEvaluationList as $c){
                          $i++;
                          $tfData = array();
                          $tfData["hr_employee_evaluation_id"] = $c["id"];
                          $profile.='           <div class="row epic">
                                               <div class="col-lg-3 col-container text-left">
                                                <div class="h7 m-0">#'.$i.' Test</div>
                                                <div class="text-muted h6">'.$c["created_date"].'</div>
                                              </div>
                                              <div class="col-lg-8 col-container text-left">
                                                <div class="epic-bottom">
                                                  <span class="badge badge-pill badge-secondary">'.$c["result"].'</span>
                                                  <span class="badge badge-pill c3-'.$c["id_level"].'">'.$c["level"].'</span>
                                                </div>
                                              </div>              
                                              <div class="col-lg-1 col-container text-right">';
                            if ($quarter[$x]["accordion"]=='active'){                                
                              $profile.='              <a class="btn-epic-sm" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).' "onclick="TfRequest.do(this);">
                               <i class="bx bx-pencil"></i>
                             </a>';
                            }         
                            $profile.='         </div> 
                                          </div>';

                        }                

                      }                           
                      $profile.='                   </div>
                                                  <!--end accordion-learning-content-->
                                                </div>
                                              </div>
                                              <!--end accordion-learning-->';    

                      $profile.='                 <!--begin accordion-performance-->
                                              <div id="accordion-performance-Q'.$x.'-'.$row["id_employee"].'" class="col-lg-12 container">
                                                <div class="my-epic-profile card">
                                                  <!--begin accordion-performance-header-->
                                                  <div class="col-12 card-header" id="heading-performance-Q'.$x.'-'.$row["id_employee"].'">
                                                    <div class="col-11 container"> 
                                                      <a id="accordion-performance-a-Q'.$x.'-'.$row["id_employee"].'" href="#" class="section '.$quarter[$x]["accordion"].' collapsed" data-toggle="collapse" data-target="#collapse-performance-Q'.$x.'-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-performance-Q'.$x.'-'.$row["id_employee"].'" data-form-action="form-action-performance-Q'.$x.'-'.$row["id_employee"].'"> Evalua su performance</a>
                                                    </div>
                                                    <div class="col-1 container text-right">';
                      if ($quarter[$x]["accordion"]=='active'){     
                        $tfData = array();
                        $tfData["hr_employee_evaluation_id_employee"] = $row["id_employee"];    
                        $tfData["hr_employee_evaluation_id_position"] = $row["id_position"];   
                        $tfData["hr_employee_evaluation_id_evaluation_type"] = 4;     
                        $tfData["hr_employee_evaluation_evaluator_type"] = 'M';                            
                        $profile.='                       <a id="form-action-performance-Q'.$x.'-'.$row["id_employee"].'" name="form-action-performance-Q'.$x.'-'.$row["id_employee"].'" class="btn-epic-sm" alt="New" title="New" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-action="AI" onclick="TfRequest.do(this);" style="display: none;">            
                                                         <i class="bx bx-plus"></i>
                                                        </a>';
                      } 
                      $profile.='                      </div>
                                                    </div>
                                                    <!--end accordion-performance-header-->
                                                    <!--begin accordion-performance-content-->
                                                    <div id="collapse-performance-Q'.$x.'-'.$row["id_employee"].'" class="collapsed collapse" aria-labelledby="heading-performance-Q'.$x.'-'.$row["id_employee"].'" data-parent="#accordion-performance-Q'.$x.'-'.$row["id_employee"].'">';  
                      if (count($performanceEvaluationList)==0){
                         $profile.='                 No existen evaluaciones en este trimestre';
                      }else{
                        $profile.='                 <div class="row epic">
                                                      <div class="col-lg-3 col-container text-left">
                                                      </div>
                                                      <div class="col-lg-8 col-container epic-text-sm text-left">
                                                        performance
                                                      </div>                             
                                                      <div class="col-lg-1 col-container epic-text-sm text-right">
                                                      </div>
                                                  </div>    ';     
                        $i=0;
                        foreach ($performanceEvaluationList as $c){
                          $i++;
                          $tfData = array();
                          $tfData["hr_employee_evaluation_id"] = $c["id"];
                          $profile.='           <div class="row epic">
                                             <div class="col-lg-3 col-container text-left">
                                              <div class="h7 m-0">#'.$i.' Test</div>
                                              <div class="text-muted h6">'.$c["created_date"].'</div>
                                            </div>
                                            <div class="col-lg-8 col-container text-left">
                                              <div class="epic-bottom">
                                                <span class="badge badge-pill badge-secondary">'.$c["result"].'</span>
                                                <span class="badge badge-pill c3-'.$c["id_level"].'">'.$c["level"].'</span>
                                              </div>
                                            </div>              
                                            <div class="col-lg-1 col-container text-right">';
                          if ($quarter[$x]["accordion"]=='active'){                                
                            $profile.='              <a class="btn-epic-sm" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrEmployeeEvaluation" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).' "onclick="TfRequest.do(this);">
                             <i class="bx bx-pencil"></i>
                           </a>';
                          }         
                          $profile.='         </div> 
                                        </div>';
                        }                

                      }                           
                      $profile.='                   </div>
                                                  <!--end accordion-performance-content-->
                                                </div>
                                              </div>
                                              <!--end accordion-performance-->';       
                      

        } 
        //END IF quarter tab disabled  
        $profile.='           </div>
                              <!---End card-body row QUARTER '.$trimestre[$x].'-->
                            </div>
                            <!---End card-body QUARTER '.$trimestre[$x].'-->  
                          </div>
                          <!---End collapse QUARTER '.$trimestre[$x].'-->    
                        </div>
                        <!---End pane QUARTER '.$trimestre[$x].'-->';
      }          
      //End x 1..4        
      $profile.='     </div> 
                      <!---End tabs content-->
                    </div> 
                    <!---End tabs QUARTER-->  
                  </div>
                  <!---End team-epic-profile row-->
                </div>
                <!---End team-epic-profile-->'; 



      }
      //Begin foreach hrEmployeeList
      $html.=$profile;
      unset($profile);
      $html.='<!--end team-->
        </div>
      </div>
    </div>
  </div>


  <!-- Modal -->
<div class="modal fade dashboard" id="coaching-modal" tabindex="-1" role="dialog" aria-labelledby="coaching-modal-label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content shadow">
            <div class="input-group">
              <input type="text"
                     placeholder="¿Qué quieres hacer hoy?"
                     class="flexdatalist-json form-control bg-light border-0 small"
                     data-data=\''.$coachingJSON.'\'
                     data-visible-properties=\'["name", "business_title", "email_account"]\'
                     data-selection-required="true"
                     data-focus-first-result="true"
                     data-min-length="1"
                     data-value-property="id"
                     data-text-property="{name}, {business_title}, {email_account}"
                     data-search-contain="true"
                     id="menus"
                     name="menus">
            </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- End Modal -->';

  echo $html;
  echo '<script type="text/javascript">'.$script.'</script>'; 
                     
?>
<script type="text/javascript">
  Chart.defaults.global.legend.position = 'bottom';
            Chart.defaults.global.legend.labels.usePointStyle = true;
            Chart.defaults.global.legend.labels.boxWidth = 15;
            Chart.defaults.global.tooltips.backgroundColor = '#000';
            Chart.defaults.global.defaultFontColor = '#000';



 
  $(".coaching").on('click',function(){  
      $(".coaching").removeClass('active');
      $(this).addClass('active');
      $(".team-epic-profile").hide();
      $('#'+$(this).data('show-profile')).show();
      
      sessionStorage.setItem('profile', $(this).attr('id'));
      sessionStorage.setItem('quarter', $("a[id*='-"+$(this).data('id-employee')+"'].quarter.active").attr('id'));
      sessionStorage.setItem('section', 'null');

      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: ($('#'+$(this).data('show-profile')).offset().top-90)
      }, 1000, 'easeInOutExpo');


  });

  $(".quarter").on('click',function(){ 
     sessionStorage.setItem('quarter', $(this).attr('id'));
     sessionStorage.setItem('section', 'null');
  });

 $(".btn-coaching-create").on('click',function(){ 
    $('#coaching-modal').modal('show');
 });
  function scrollFocus(){
    var profile= sessionStorage.getItem('profile');
    var quarter= sessionStorage.getItem('quarter');
    var section= sessionStorage.getItem('section');
    if (profile != 'null') {
       $('#'+profile).click();       
       if (quarter != 'null') {
          $('#'+quarter).click();
          $('html, body').stop().animate({scrollTop: ($('#'+quarter).offset().top-360)}, 1000, 'easeInOutExpo');
       if (section != 'null') {
          $('#'+section).click();
          $('html, body').stop().animate({scrollTop: ($('#'+section).offset().top)}, 1000, 'easeInOutExpo');
       } 
       } 
    }
  }


   $(".section.active").on('click',function(){  
      sessionStorage.setItem('section', $(this).attr('id'));
      if ($(this).hasClass('collapsed')){
        $('#'+$(this).data('form-action')).show();
      }else{
       $('#'+$(this).data('form-action')).hide();
      }
    });
    
    $('#menus').flexdatalist({
     searchContain: true,
     textProperty: '{name}, {business_title}, {email_account}',
     valueProperty: '*',
     minLength: 1,
     focusFirstResult: true,
     selectionRequired: true,
     visibleProperties: ["name","business_title","email_account"],
     searchIn: ["name","business_title","email_account"],
     searchByWord: true
    });

    $('#menus').on('change:flexdatalist', function(event, set, options) {
      console.log(set.value);
      console.log(set.text);

       $('#menus').val("");
       $('#coaching-modal').modal('hide');
    
    });
    $(document).ready(function(){
         scrollFocus();
    });

</script>