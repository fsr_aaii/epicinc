<?php
  class HrPositionBase extends TfEntity {
    protected $id;
    protected $id_cost_center;
    protected $id_business_title;
    protected $criticality;
    protected $leadership_bench;
    protected $evaluation_date;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="hr_position";
  }

  private function getAll(){

    $q="SELECT id,
               id_cost_center,
               id_business_title,
               criticality,
               leadership_bench,
               evaluation_date,
               created_by,
               created_date
          FROM hr_position
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_cost_center=$rs["id_cost_center"];
    $this->id_business_title=$rs["id_business_title"];
    $this->criticality=$rs["criticality"];
    $this->leadership_bench=$rs["leadership_bench"];
    $this->evaluation_date=$rs["evaluation_date"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->hr_position_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_hr_position){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_hr_position; 
    }

    if ($tfRequest->exist("hr_position_id_cost_center")){
      $this->id_cost_center=$tfRequest->hr_position_id_cost_center;
    }
    if ($tfRequest->exist("hr_position_id_business_title")){
      $this->id_business_title=$tfRequest->hr_position_id_business_title;
    }
    if ($tfRequest->exist("hr_position_criticality")){
      $this->criticality=$tfRequest->hr_position_criticality;
    }
    if ($tfRequest->exist("hr_position_leadership_bench")){
      $this->leadership_bench=$tfRequest->hr_position_leadership_bench;
    }
    if ($tfRequest->exist("hr_position_evaluation_date")){
      $this->evaluation_date=$tfRequest->hr_position_evaluation_date;
    }
    if ($tfRequest->exist("hr_position_created_by")){
      $this->created_by=$tfRequest->hr_position_created_by;
    }
    if ($tfRequest->exist("hr_position_created_date")){
      $this->created_date=$tfRequest->hr_position_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_cost_center"]=array("type"=>"number",
                                  "value"=>$this->id_cost_center,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_business_title"]=array("type"=>"number",
                                  "value"=>$this->id_business_title,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["criticality"]=array("type"=>"number",
                                  "value"=>$this->criticality,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["leadership_bench"]=array("type"=>"number",
                                  "value"=>$this->leadership_bench,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["evaluation_date"]=array("type"=>"date",
                                  "value"=>$this->evaluation_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdCostCenter($value){
  $this->id_cost_center=$value;
  }
  public function getIdCostCenter(){
  return $this->id_cost_center;
  }
  public function setIdBusinessTitle($value){
  $this->id_business_title=$value;
  }
  public function getIdBusinessTitle(){
  return $this->id_business_title;
  }
  public function setCriticality($value){
  $this->criticality=$value;
  }
  public function getCriticality(){
  return $this->criticality;
  }
  public function setLeadershipBench($value){
  $this->leadership_bench=$value;
  }
  public function getLeadershipBench(){
  return $this->leadership_bench;
  }
  public function setEvaluationDate($value){
  $this->evaluation_date=$value;
  }
  public function getEvaluationDate(){
  return $this->evaluation_date;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO hr_position(id,
                               id_cost_center,
                               id_business_title,
                               criticality,
                               leadership_bench,
                               evaluation_date,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_cost_center==''?NULL:$this->id_cost_center,
                     $this->id_business_title==''?NULL:$this->id_business_title,
                     $this->criticality==''?NULL:$this->criticality,
                     $this->leadership_bench==''?NULL:$this->leadership_bench,
                     $this->evaluation_date==''?NULL:$this->evaluation_date,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_cost_center!= $rs["id_cost_center"]){
          if ($this->updateable["id_cost_center"]){
            $set.=$set_aux."id_cost_center=?";
            $set_aux=",";
            $param[]=$this->id_cost_center==''?NULL:$this->id_cost_center;
          }else{
            $this->objError[]="The field (id_cost_center) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_business_title!= $rs["id_business_title"]){
          if ($this->updateable["id_business_title"]){
            $set.=$set_aux."id_business_title=?";
            $set_aux=",";
            $param[]=$this->id_business_title==''?NULL:$this->id_business_title;
          }else{
            $this->objError[]="The field (id_business_title) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->criticality!= $rs["criticality"]){
          if ($this->updateable["criticality"]){
            $set.=$set_aux."criticality=?";
            $set_aux=",";
            $param[]=$this->criticality==''?NULL:$this->criticality;
          }else{
            $this->objError[]="The field (criticality) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->leadership_bench!= $rs["leadership_bench"]){
          if ($this->updateable["leadership_bench"]){
            $set.=$set_aux."leadership_bench=?";
            $set_aux=",";
            $param[]=$this->leadership_bench==''?NULL:$this->leadership_bench;
          }else{
            $this->objError[]="The field (leadership_bench) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->evaluation_date!= $rs["evaluation_date"]){
          if ($this->updateable["evaluation_date"]){
            $set.=$set_aux."evaluation_date=?";
            $set_aux=",";
            $param[]=$this->evaluation_date==''?NULL:$this->evaluation_date;
          }else{
            $this->objError[]="The field (evaluation_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE hr_position ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM hr_position
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
