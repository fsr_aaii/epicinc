<?php
  class CmPostCommentBase extends TfEntity {
    protected $id;
    protected $id_post;
    protected $comment;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="cm_post_comment";
  }

  private function getAll(){

    $q="SELECT id,
               id_post,
               comment,
               active,
               created_by,
               created_date
          FROM cm_post_comment
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_post=$rs["id_post"];
    $this->comment=$rs["comment"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->cm_post_comment_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_cm_post_comment){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_cm_post_comment; 
    }

    if ($tfRequest->exist("cm_post_comment_id_post")){
      $this->id_post=$tfRequest->cm_post_comment_id_post;
    }
    if ($tfRequest->exist("cm_post_comment_comment")){
      $this->comment=$tfRequest->cm_post_comment_comment;
    }
    if ($tfRequest->exist("cm_post_comment_active")){
      $this->active=$tfRequest->cm_post_comment_active;
    }
    if ($tfRequest->exist("cm_post_comment_created_by")){
      $this->created_by=$tfRequest->cm_post_comment_created_by;
    }
    if ($tfRequest->exist("cm_post_comment_created_date")){
      $this->created_date=$tfRequest->cm_post_comment_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_post"]=array("type"=>"number",
                                  "value"=>$this->id_post,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["comment"]=array("type"=>"string",
                                  "value"=>$this->comment,
                                  "length"=>280,
                                  "required"=>true);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdPost($value){
  $this->id_post=$value;
  }
  public function getIdPost(){
  return $this->id_post;
  }
  public function setComment($value){
  $this->comment=$value;
  }
  public function getComment(){
  return $this->comment;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO cm_post_comment(id,
                               id_post,
                               comment,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_post==''?NULL:$this->id_post,
                     $this->comment==''?NULL:$this->comment,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_post!= $rs["id_post"]){
          if ($this->updateable["id_post"]){
            $set.=$set_aux."id_post=?";
            $set_aux=",";
            $param[]=$this->id_post==''?NULL:$this->id_post;
          }else{
            $this->objError[]="The field (id_post) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->comment!= $rs["comment"]){
          if ($this->updateable["comment"]){
            $set.=$set_aux."comment=?";
            $set_aux=",";
            $param[]=$this->comment==''?NULL:$this->comment;
          }else{
            $this->objError[]="The field (comment) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE cm_post_comment ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM cm_post_comment
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
