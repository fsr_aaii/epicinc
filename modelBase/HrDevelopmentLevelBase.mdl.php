<?php
  class HrDevelopmentLevelBase extends TfEntity {
    protected $id;
    protected $description;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="hr_development_level";
  }

  private function getAll(){

    $q="SELECT id,
               description
          FROM hr_development_level
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->description=$rs["description"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->hr_development_level_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_hr_development_level){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_hr_development_level; 
    }

    if ($tfRequest->exist("hr_development_level_description")){
      $this->description=$tfRequest->hr_development_level_description;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["description"]=array("type"=>"string",
                                  "value"=>$this->description,
                                  "length"=>100,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setDescription($value){
  $this->description=$value;
  }
  public function getDescription(){
  return $this->description;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO hr_development_level(id,
                               description)
            VALUES (?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->description==''?NULL:$this->description);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->description!= $rs["description"]){
          if ($this->updateable["description"]){
            $set.=$set_aux."description=?";
            $set_aux=",";
            $param[]=$this->description==''?NULL:$this->description;
          }else{
            $this->objError[]="The field (description) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE hr_development_level ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM hr_development_level
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
