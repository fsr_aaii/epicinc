<?php
  class GlNaturalPersonBase extends TfEntity {
    protected $id;
    protected $nin_type;
    protected $nin;
    protected $nin_control_digit;
    protected $first_name;
    protected $middle_name;
    protected $last_name;
    protected $second_last_name;
    protected $photo;
    protected $id_nationality_country;
    protected $id_gender;
    protected $id_birth_country;
    protected $birthplace;
    protected $birthdate;
    protected $id_marital_status;
    protected $height;
    protected $size;
    protected $weight;
    protected $is_right_handed;
    protected $id_blood_type;
    protected $is_functional_diversity;
    protected $id_study_level;
    protected $id_residence_country;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="gl_natural_person";
  }

  private function getAll(){

    $q="SELECT id,
               nin_type,
               nin,
               nin_control_digit,
               first_name,
               middle_name,
               last_name,
               second_last_name,
               photo,
               id_nationality_country,
               id_gender,
               id_birth_country,
               birthplace,
               birthdate,
               id_marital_status,
               height,
               size,
               weight,
               is_right_handed,
               id_blood_type,
               is_functional_diversity,
               id_study_level,
               id_residence_country,
               created_by,
               created_date
          FROM gl_natural_person
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->nin_type=$rs["nin_type"];
    $this->nin=$rs["nin"];
    $this->nin_control_digit=$rs["nin_control_digit"];
    $this->first_name=$rs["first_name"];
    $this->middle_name=$rs["middle_name"];
    $this->last_name=$rs["last_name"];
    $this->second_last_name=$rs["second_last_name"];
    $this->photo=$rs["photo"];
    $this->id_nationality_country=$rs["id_nationality_country"];
    $this->id_gender=$rs["id_gender"];
    $this->id_birth_country=$rs["id_birth_country"];
    $this->birthplace=$rs["birthplace"];
    $this->birthdate=$rs["birthdate"];
    $this->id_marital_status=$rs["id_marital_status"];
    $this->height=$rs["height"];
    $this->size=$rs["size"];
    $this->weight=$rs["weight"];
    $this->is_right_handed=$rs["is_right_handed"];
    $this->id_blood_type=$rs["id_blood_type"];
    $this->is_functional_diversity=$rs["is_functional_diversity"];
    $this->id_study_level=$rs["id_study_level"];
    $this->id_residence_country=$rs["id_residence_country"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->gl_natural_person_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_gl_natural_person){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_gl_natural_person; 
    }

    if ($tfRequest->exist("gl_natural_person_nin_type")){
      $this->nin_type=$tfRequest->gl_natural_person_nin_type;
    }
    if ($tfRequest->exist("gl_natural_person_nin")){
      $this->nin=$tfRequest->gl_natural_person_nin;
    }
    if ($tfRequest->exist("gl_natural_person_nin_control_digit")){
      $this->nin_control_digit=$tfRequest->gl_natural_person_nin_control_digit;
    }
    if ($tfRequest->exist("gl_natural_person_first_name")){
      $this->first_name=$tfRequest->gl_natural_person_first_name;
    }
    if ($tfRequest->exist("gl_natural_person_middle_name")){
      $this->middle_name=$tfRequest->gl_natural_person_middle_name;
    }
    if ($tfRequest->exist("gl_natural_person_last_name")){
      $this->last_name=$tfRequest->gl_natural_person_last_name;
    }
    if ($tfRequest->exist("gl_natural_person_second_last_name")){
      $this->second_last_name=$tfRequest->gl_natural_person_second_last_name;
    }
    if ($tfRequest->exist("gl_natural_person_photo")){
      $this->photo=$tfRequest->gl_natural_person_photo;
    }
    if ($tfRequest->exist("gl_natural_person_id_nationality_country")){
      $this->id_nationality_country=$tfRequest->gl_natural_person_id_nationality_country;
    }
    if ($tfRequest->exist("gl_natural_person_id_gender")){
      $this->id_gender=$tfRequest->gl_natural_person_id_gender;
    }
    if ($tfRequest->exist("gl_natural_person_id_birth_country")){
      $this->id_birth_country=$tfRequest->gl_natural_person_id_birth_country;
    }
    if ($tfRequest->exist("gl_natural_person_birthplace")){
      $this->birthplace=$tfRequest->gl_natural_person_birthplace;
    }
    if ($tfRequest->exist("gl_natural_person_birthdate")){
      $this->birthdate=$tfRequest->gl_natural_person_birthdate;
    }
    if ($tfRequest->exist("gl_natural_person_id_marital_status")){
      $this->id_marital_status=$tfRequest->gl_natural_person_id_marital_status;
    }
    if ($tfRequest->exist("gl_natural_person_height")){
      $this->height=$tfRequest->gl_natural_person_height;
    }
    if ($tfRequest->exist("gl_natural_person_size")){
      $this->size=$tfRequest->gl_natural_person_size;
    }
    if ($tfRequest->exist("gl_natural_person_weight")){
      $this->weight=$tfRequest->gl_natural_person_weight;
    }
    if ($tfRequest->exist("gl_natural_person_is_right_handed")){
      $this->is_right_handed=$tfRequest->gl_natural_person_is_right_handed;
    }
    if ($tfRequest->exist("gl_natural_person_id_blood_type")){
      $this->id_blood_type=$tfRequest->gl_natural_person_id_blood_type;
    }
    if ($tfRequest->exist("gl_natural_person_is_functional_diversity")){
      $this->is_functional_diversity=$tfRequest->gl_natural_person_is_functional_diversity;
    }
    if ($tfRequest->exist("gl_natural_person_id_study_level")){
      $this->id_study_level=$tfRequest->gl_natural_person_id_study_level;
    }
    if ($tfRequest->exist("gl_natural_person_id_residence_country")){
      $this->id_residence_country=$tfRequest->gl_natural_person_id_residence_country;
    }
    if ($tfRequest->exist("gl_natural_person_created_by")){
      $this->created_by=$tfRequest->gl_natural_person_created_by;
    }
    if ($tfRequest->exist("gl_natural_person_created_date")){
      $this->created_date=$tfRequest->gl_natural_person_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["nin_type"]=array("type"=>"string",
                                  "value"=>$this->nin_type,
                                  "length"=>1,
                                  "required"=>false);
    $this->validation["nin"]=array("type"=>"number",
                                  "value"=>$this->nin,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["nin_control_digit"]=array("type"=>"number",
                                  "value"=>$this->nin_control_digit,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["first_name"]=array("type"=>"string",
                                  "value"=>$this->first_name,
                                  "length"=>50,
                                  "required"=>false);
    $this->validation["middle_name"]=array("type"=>"string",
                                  "value"=>$this->middle_name,
                                  "length"=>50,
                                  "required"=>false);
    $this->validation["last_name"]=array("type"=>"string",
                                  "value"=>$this->last_name,
                                  "length"=>50,
                                  "required"=>false);
    $this->validation["second_last_name"]=array("type"=>"string",
                                  "value"=>$this->second_last_name,
                                  "length"=>50,
                                  "required"=>false);
    $this->validation["photo"]=array("type"=>"string",
                                  "value"=>$this->photo,
                                  "length"=>400,
                                  "required"=>false);
    $this->validation["id_nationality_country"]=array("type"=>"string",
                                  "value"=>$this->id_nationality_country,
                                  "length"=>2,
                                  "required"=>false);
    $this->validation["id_gender"]=array("type"=>"string",
                                  "value"=>$this->id_gender,
                                  "length"=>1,
                                  "required"=>false);
    $this->validation["id_birth_country"]=array("type"=>"string",
                                  "value"=>$this->id_birth_country,
                                  "length"=>2,
                                  "required"=>false);
    $this->validation["birthplace"]=array("type"=>"string",
                                  "value"=>$this->birthplace,
                                  "length"=>40,
                                  "required"=>false);
    $this->validation["birthdate"]=array("type"=>"date",
                                  "value"=>$this->birthdate,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_marital_status"]=array("type"=>"string",
                                  "value"=>$this->id_marital_status,
                                  "length"=>1,
                                  "required"=>false);
    $this->validation["height"]=array("type"=>"number",
                                  "value"=>$this->height,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["size"]=array("type"=>"string",
                                  "value"=>$this->size,
                                  "length"=>5,
                                  "required"=>false);
    $this->validation["weight"]=array("type"=>"number",
                                  "value"=>$this->weight,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["is_right_handed"]=array("type"=>"string",
                                  "value"=>$this->is_right_handed,
                                  "length"=>1,
                                  "required"=>false);
    $this->validation["id_blood_type"]=array("type"=>"number",
                                  "value"=>$this->id_blood_type,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["is_functional_diversity"]=array("type"=>"string",
                                  "value"=>$this->is_functional_diversity,
                                  "length"=>1,
                                  "required"=>false);
    $this->validation["id_study_level"]=array("type"=>"string",
                                  "value"=>$this->id_study_level,
                                  "length"=>4,
                                  "required"=>false);
    $this->validation["id_residence_country"]=array("type"=>"string",
                                  "value"=>$this->id_residence_country,
                                  "length"=>2,
                                  "required"=>false);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setNinType($value){
  $this->nin_type=$value;
  }
  public function getNinType(){
  return $this->nin_type;
  }
  public function setNin($value){
  $this->nin=$value;
  }
  public function getNin(){
  return $this->nin;
  }
  public function setNinControlDigit($value){
  $this->nin_control_digit=$value;
  }
  public function getNinControlDigit(){
  return $this->nin_control_digit;
  }
  public function setFirstName($value){
  $this->first_name=$value;
  }
  public function getFirstName(){
  return $this->first_name;
  }
  public function setMiddleName($value){
  $this->middle_name=$value;
  }
  public function getMiddleName(){
  return $this->middle_name;
  }
  public function setLastName($value){
  $this->last_name=$value;
  }
  public function getLastName(){
  return $this->last_name;
  }
  public function setSecondLastName($value){
  $this->second_last_name=$value;
  }
  public function getSecondLastName(){
  return $this->second_last_name;
  }
  public function setPhoto($value){
  $this->photo=$value;
  }
  public function getPhoto(){
  return $this->photo;
  }
  public function setIdNationalityCountry($value){
  $this->id_nationality_country=$value;
  }
  public function getIdNationalityCountry(){
  return $this->id_nationality_country;
  }
  public function setIdGender($value){
  $this->id_gender=$value;
  }
  public function getIdGender(){
  return $this->id_gender;
  }
  public function setIdBirthCountry($value){
  $this->id_birth_country=$value;
  }
  public function getIdBirthCountry(){
  return $this->id_birth_country;
  }
  public function setBirthplace($value){
  $this->birthplace=$value;
  }
  public function getBirthplace(){
  return $this->birthplace;
  }
  public function setBirthdate($value){
  $this->birthdate=$value;
  }
  public function getBirthdate(){
  return $this->birthdate;
  }
  public function setIdMaritalStatus($value){
  $this->id_marital_status=$value;
  }
  public function getIdMaritalStatus(){
  return $this->id_marital_status;
  }
  public function setHeight($value){
  $this->height=$value;
  }
  public function getHeight(){
  return $this->height;
  }
  public function setSize($value){
  $this->size=$value;
  }
  public function getSize(){
  return $this->size;
  }
  public function setWeight($value){
  $this->weight=$value;
  }
  public function getWeight(){
  return $this->weight;
  }
  public function setIsRightHanded($value){
  $this->is_right_handed=$value;
  }
  public function getIsRightHanded(){
  return $this->is_right_handed;
  }
  public function setIdBloodType($value){
  $this->id_blood_type=$value;
  }
  public function getIdBloodType(){
  return $this->id_blood_type;
  }
  public function setIsFunctionalDiversity($value){
  $this->is_functional_diversity=$value;
  }
  public function getIsFunctionalDiversity(){
  return $this->is_functional_diversity;
  }
  public function setIdStudyLevel($value){
  $this->id_study_level=$value;
  }
  public function getIdStudyLevel(){
  return $this->id_study_level;
  }
  public function setIdResidenceCountry($value){
  $this->id_residence_country=$value;
  }
  public function getIdResidenceCountry(){
  return $this->id_residence_country;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO gl_natural_person(id,
                               nin_type,
                               nin,
                               nin_control_digit,
                               first_name,
                               middle_name,
                               last_name,
                               second_last_name,
                               photo,
                               id_nationality_country,
                               id_gender,
                               id_birth_country,
                               birthplace,
                               birthdate,
                               id_marital_status,
                               height,
                               size,
                               weight,
                               is_right_handed,
                               id_blood_type,
                               is_functional_diversity,
                               id_study_level,
                               id_residence_country,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->nin_type==''?NULL:$this->nin_type,
                     $this->nin==''?NULL:$this->nin,
                     $this->nin_control_digit==''?NULL:$this->nin_control_digit,
                     $this->first_name==''?NULL:$this->first_name,
                     $this->middle_name==''?NULL:$this->middle_name,
                     $this->last_name==''?NULL:$this->last_name,
                     $this->second_last_name==''?NULL:$this->second_last_name,
                     $this->photo==''?NULL:$this->photo,
                     $this->id_nationality_country==''?NULL:$this->id_nationality_country,
                     $this->id_gender==''?NULL:$this->id_gender,
                     $this->id_birth_country==''?NULL:$this->id_birth_country,
                     $this->birthplace==''?NULL:$this->birthplace,
                     $this->birthdate==''?NULL:$this->birthdate,
                     $this->id_marital_status==''?NULL:$this->id_marital_status,
                     $this->height==''?NULL:$this->height,
                     $this->size==''?NULL:$this->size,
                     $this->weight==''?NULL:$this->weight,
                     $this->is_right_handed==''?NULL:$this->is_right_handed,
                     $this->id_blood_type==''?NULL:$this->id_blood_type,
                     $this->is_functional_diversity==''?NULL:$this->is_functional_diversity,
                     $this->id_study_level==''?NULL:$this->id_study_level,
                     $this->id_residence_country==''?NULL:$this->id_residence_country,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->nin_type!= $rs["nin_type"]){
          if ($this->updateable["nin_type"]){
            $set.=$set_aux."nin_type=?";
            $set_aux=",";
            $param[]=$this->nin_type==''?NULL:$this->nin_type;
          }else{
            $this->objError[]="The field (nin_type) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->nin!= $rs["nin"]){
          if ($this->updateable["nin"]){
            $set.=$set_aux."nin=?";
            $set_aux=",";
            $param[]=$this->nin==''?NULL:$this->nin;
          }else{
            $this->objError[]="The field (nin) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->nin_control_digit!= $rs["nin_control_digit"]){
          if ($this->updateable["nin_control_digit"]){
            $set.=$set_aux."nin_control_digit=?";
            $set_aux=",";
            $param[]=$this->nin_control_digit==''?NULL:$this->nin_control_digit;
          }else{
            $this->objError[]="The field (nin_control_digit) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->first_name!= $rs["first_name"]){
          if ($this->updateable["first_name"]){
            $set.=$set_aux."first_name=?";
            $set_aux=",";
            $param[]=$this->first_name==''?NULL:$this->first_name;
          }else{
            $this->objError[]="The field (first_name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->middle_name!= $rs["middle_name"]){
          if ($this->updateable["middle_name"]){
            $set.=$set_aux."middle_name=?";
            $set_aux=",";
            $param[]=$this->middle_name==''?NULL:$this->middle_name;
          }else{
            $this->objError[]="The field (middle_name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->last_name!= $rs["last_name"]){
          if ($this->updateable["last_name"]){
            $set.=$set_aux."last_name=?";
            $set_aux=",";
            $param[]=$this->last_name==''?NULL:$this->last_name;
          }else{
            $this->objError[]="The field (last_name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->second_last_name!= $rs["second_last_name"]){
          if ($this->updateable["second_last_name"]){
            $set.=$set_aux."second_last_name=?";
            $set_aux=",";
            $param[]=$this->second_last_name==''?NULL:$this->second_last_name;
          }else{
            $this->objError[]="The field (second_last_name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->photo!= $rs["photo"]){
          if ($this->updateable["photo"]){
            $set.=$set_aux."photo=?";
            $set_aux=",";
            $param[]=$this->photo==''?NULL:$this->photo;
          }else{
            $this->objError[]="The field (photo) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_nationality_country!= $rs["id_nationality_country"]){
          if ($this->updateable["id_nationality_country"]){
            $set.=$set_aux."id_nationality_country=?";
            $set_aux=",";
            $param[]=$this->id_nationality_country==''?NULL:$this->id_nationality_country;
          }else{
            $this->objError[]="The field (id_nationality_country) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_gender!= $rs["id_gender"]){
          if ($this->updateable["id_gender"]){
            $set.=$set_aux."id_gender=?";
            $set_aux=",";
            $param[]=$this->id_gender==''?NULL:$this->id_gender;
          }else{
            $this->objError[]="The field (id_gender) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_birth_country!= $rs["id_birth_country"]){
          if ($this->updateable["id_birth_country"]){
            $set.=$set_aux."id_birth_country=?";
            $set_aux=",";
            $param[]=$this->id_birth_country==''?NULL:$this->id_birth_country;
          }else{
            $this->objError[]="The field (id_birth_country) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->birthplace!= $rs["birthplace"]){
          if ($this->updateable["birthplace"]){
            $set.=$set_aux."birthplace=?";
            $set_aux=",";
            $param[]=$this->birthplace==''?NULL:$this->birthplace;
          }else{
            $this->objError[]="The field (birthplace) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->birthdate!= $rs["birthdate"]){
          if ($this->updateable["birthdate"]){
            $set.=$set_aux."birthdate=?";
            $set_aux=",";
            $param[]=$this->birthdate==''?NULL:$this->birthdate;
          }else{
            $this->objError[]="The field (birthdate) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_marital_status!= $rs["id_marital_status"]){
          if ($this->updateable["id_marital_status"]){
            $set.=$set_aux."id_marital_status=?";
            $set_aux=",";
            $param[]=$this->id_marital_status==''?NULL:$this->id_marital_status;
          }else{
            $this->objError[]="The field (id_marital_status) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->height!= $rs["height"]){
          if ($this->updateable["height"]){
            $set.=$set_aux."height=?";
            $set_aux=",";
            $param[]=$this->height==''?NULL:$this->height;
          }else{
            $this->objError[]="The field (height) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->size!= $rs["size"]){
          if ($this->updateable["size"]){
            $set.=$set_aux."size=?";
            $set_aux=",";
            $param[]=$this->size==''?NULL:$this->size;
          }else{
            $this->objError[]="The field (size) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->weight!= $rs["weight"]){
          if ($this->updateable["weight"]){
            $set.=$set_aux."weight=?";
            $set_aux=",";
            $param[]=$this->weight==''?NULL:$this->weight;
          }else{
            $this->objError[]="The field (weight) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->is_right_handed!= $rs["is_right_handed"]){
          if ($this->updateable["is_right_handed"]){
            $set.=$set_aux."is_right_handed=?";
            $set_aux=",";
            $param[]=$this->is_right_handed==''?NULL:$this->is_right_handed;
          }else{
            $this->objError[]="The field (is_right_handed) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_blood_type!= $rs["id_blood_type"]){
          if ($this->updateable["id_blood_type"]){
            $set.=$set_aux."id_blood_type=?";
            $set_aux=",";
            $param[]=$this->id_blood_type==''?NULL:$this->id_blood_type;
          }else{
            $this->objError[]="The field (id_blood_type) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->is_functional_diversity!= $rs["is_functional_diversity"]){
          if ($this->updateable["is_functional_diversity"]){
            $set.=$set_aux."is_functional_diversity=?";
            $set_aux=",";
            $param[]=$this->is_functional_diversity==''?NULL:$this->is_functional_diversity;
          }else{
            $this->objError[]="The field (is_functional_diversity) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_study_level!= $rs["id_study_level"]){
          if ($this->updateable["id_study_level"]){
            $set.=$set_aux."id_study_level=?";
            $set_aux=",";
            $param[]=$this->id_study_level==''?NULL:$this->id_study_level;
          }else{
            $this->objError[]="The field (id_study_level) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_residence_country!= $rs["id_residence_country"]){
          if ($this->updateable["id_residence_country"]){
            $set.=$set_aux."id_residence_country=?";
            $set_aux=",";
            $param[]=$this->id_residence_country==''?NULL:$this->id_residence_country;
          }else{
            $this->objError[]="The field (id_residence_country) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE gl_natural_person ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM gl_natural_person
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
