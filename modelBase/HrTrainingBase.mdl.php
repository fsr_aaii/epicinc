<?php
  class HrTrainingBase extends TfEntity {
    protected $id;
    protected $id_employee;
    protected $id_position;
    protected $id_training_type;
    protected $name;
    protected $description;
    protected $hour;
    protected $estimated_start_date;
    protected $start_date;
    protected $completion_estimated_date;
    protected $completion_date;
    protected $id_epic_priority;
    protected $id_epic_status;
    protected $status_date;
    protected $progress;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="hr_training";
  }

  private function getAll(){

    $q="SELECT id,
               id_employee,
               id_position,
               id_training_type,
               name,
               description,
               hour,
               estimated_start_date,
               start_date,
               completion_estimated_date,
               completion_date,
               id_epic_priority,
               id_epic_status,
               status_date,
               progress,
               active,
               created_by,
               created_date
          FROM hr_training
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_employee=$rs["id_employee"];
    $this->id_position=$rs["id_position"];
    $this->id_training_type=$rs["id_training_type"];
    $this->name=$rs["name"];
    $this->description=$rs["description"];
    $this->hour=$rs["hour"];
    $this->estimated_start_date=$rs["estimated_start_date"];
    $this->start_date=$rs["start_date"];
    $this->completion_estimated_date=$rs["completion_estimated_date"];
    $this->completion_date=$rs["completion_date"];
    $this->id_epic_priority=$rs["id_epic_priority"];
    $this->id_epic_status=$rs["id_epic_status"];
    $this->status_date=$rs["status_date"];
    $this->progress=$rs["progress"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->hr_training_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_hr_training){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_hr_training; 
    }

    if ($tfRequest->exist("hr_training_id_employee")){
      $this->id_employee=$tfRequest->hr_training_id_employee;
    }
    if ($tfRequest->exist("hr_training_id_position")){
      $this->id_position=$tfRequest->hr_training_id_position;
    }
    if ($tfRequest->exist("hr_training_id_training_type")){
      $this->id_training_type=$tfRequest->hr_training_id_training_type;
    }
    if ($tfRequest->exist("hr_training_name")){
      $this->name=$tfRequest->hr_training_name;
    }
    if ($tfRequest->exist("hr_training_description")){
      $this->description=$tfRequest->hr_training_description;
    }
    if ($tfRequest->exist("hr_training_hour")){
      $this->hour=$tfRequest->hr_training_hour;
    }
    if ($tfRequest->exist("hr_training_estimated_start_date")){
      $this->estimated_start_date=$tfRequest->hr_training_estimated_start_date;
    }
    if ($tfRequest->exist("hr_training_start_date")){
      $this->start_date=$tfRequest->hr_training_start_date;
    }
    if ($tfRequest->exist("hr_training_completion_estimated_date")){
      $this->completion_estimated_date=$tfRequest->hr_training_completion_estimated_date;
    }
    if ($tfRequest->exist("hr_training_completion_date")){
      $this->completion_date=$tfRequest->hr_training_completion_date;
    }
    if ($tfRequest->exist("hr_training_id_epic_priority")){
      $this->id_epic_priority=$tfRequest->hr_training_id_epic_priority;
    }
    if ($tfRequest->exist("hr_training_id_epic_status")){
      $this->id_epic_status=$tfRequest->hr_training_id_epic_status;
    }
    if ($tfRequest->exist("hr_training_status_date")){
      $this->status_date=$tfRequest->hr_training_status_date;
    }
    if ($tfRequest->exist("hr_training_progress")){
      $this->progress=$tfRequest->hr_training_progress;
    }
    if ($tfRequest->exist("hr_training_active")){
      $this->active=$tfRequest->hr_training_active;
    }
    if ($tfRequest->exist("hr_training_created_by")){
      $this->created_by=$tfRequest->hr_training_created_by;
    }
    if ($tfRequest->exist("hr_training_created_date")){
      $this->created_date=$tfRequest->hr_training_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_employee"]=array("type"=>"number",
                                  "value"=>$this->id_employee,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_position"]=array("type"=>"number",
                                  "value"=>$this->id_position,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_training_type"]=array("type"=>"number",
                                  "value"=>$this->id_training_type,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["name"]=array("type"=>"string",
                                  "value"=>$this->name,
                                  "length"=>50,
                                  "required"=>true);
    $this->validation["description"]=array("type"=>"string",
                                  "value"=>$this->description,
                                  "length"=>500,
                                  "required"=>true);
    $this->validation["hour"]=array("type"=>"number",
                                  "value"=>$this->hour,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["estimated_start_date"]=array("type"=>"date",
                                  "value"=>$this->estimated_start_date,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["start_date"]=array("type"=>"date",
                                  "value"=>$this->start_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["completion_estimated_date"]=array("type"=>"date",
                                  "value"=>$this->completion_estimated_date,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["completion_date"]=array("type"=>"date",
                                  "value"=>$this->completion_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_epic_priority"]=array("type"=>"number",
                                  "value"=>$this->id_epic_priority,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_epic_status"]=array("type"=>"number",
                                  "value"=>$this->id_epic_status,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["status_date"]=array("type"=>"datetime",
                                  "value"=>$this->status_date,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["progress"]=array("type"=>"number",
                                  "value"=>$this->progress,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdEmployee($value){
  $this->id_employee=$value;
  }
  public function getIdEmployee(){
  return $this->id_employee;
  }
  public function setIdPosition($value){
  $this->id_position=$value;
  }
  public function getIdPosition(){
  return $this->id_position;
  }
  public function setIdTrainingType($value){
  $this->id_training_type=$value;
  }
  public function getIdTrainingType(){
  return $this->id_training_type;
  }
  public function setName($value){
  $this->name=$value;
  }
  public function getName(){
  return $this->name;
  }
  public function setDescription($value){
  $this->description=$value;
  }
  public function getDescription(){
  return $this->description;
  }
  public function setHour($value){
  $this->hour=$value;
  }
  public function getHour(){
  return $this->hour;
  }
  public function setEstimatedStartDate($value){
  $this->estimated_start_date=$value;
  }
  public function getEstimatedStartDate(){
  return $this->estimated_start_date;
  }
  public function setStartDate($value){
  $this->start_date=$value;
  }
  public function getStartDate(){
  return $this->start_date;
  }
  public function setCompletionEstimatedDate($value){
  $this->completion_estimated_date=$value;
  }
  public function getCompletionEstimatedDate(){
  return $this->completion_estimated_date;
  }
  public function setCompletionDate($value){
  $this->completion_date=$value;
  }
  public function getCompletionDate(){
  return $this->completion_date;
  }
  public function setIdEpicPriority($value){
  $this->id_epic_priority=$value;
  }
  public function getIdEpicPriority(){
  return $this->id_epic_priority;
  }
  public function setIdEpicStatus($value){
  $this->id_epic_status=$value;
  }
  public function getIdEpicStatus(){
  return $this->id_epic_status;
  }
  public function setStatusDate($value){
  $this->status_date=$value;
  }
  public function getStatusDate(){
  return $this->status_date;
  }
  public function setProgress($value){
  $this->progress=$value;
  }
  public function getProgress(){
  return $this->progress;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO hr_training(id,
                               id_employee,
                               id_position,
                               id_training_type,
                               name,
                               description,
                               hour,
                               estimated_start_date,
                               start_date,
                               completion_estimated_date,
                               completion_date,
                               id_epic_priority,
                               id_epic_status,
                               status_date,
                               progress,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_employee==''?NULL:$this->id_employee,
                     $this->id_position==''?NULL:$this->id_position,
                     $this->id_training_type==''?NULL:$this->id_training_type,
                     $this->name==''?NULL:$this->name,
                     $this->description==''?NULL:$this->description,
                     $this->hour==''?NULL:$this->hour,
                     $this->estimated_start_date==''?NULL:$this->estimated_start_date,
                     $this->start_date==''?NULL:$this->start_date,
                     $this->completion_estimated_date==''?NULL:$this->completion_estimated_date,
                     $this->completion_date==''?NULL:$this->completion_date,
                     $this->id_epic_priority==''?NULL:$this->id_epic_priority,
                     $this->id_epic_status==''?NULL:$this->id_epic_status,
                     $this->status_date==''?NULL:$this->status_date,
                     $this->progress==''?NULL:$this->progress,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_employee!= $rs["id_employee"]){
          if ($this->updateable["id_employee"]){
            $set.=$set_aux."id_employee=?";
            $set_aux=",";
            $param[]=$this->id_employee==''?NULL:$this->id_employee;
          }else{
            $this->objError[]="The field (id_employee) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_position!= $rs["id_position"]){
          if ($this->updateable["id_position"]){
            $set.=$set_aux."id_position=?";
            $set_aux=",";
            $param[]=$this->id_position==''?NULL:$this->id_position;
          }else{
            $this->objError[]="The field (id_position) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_training_type!= $rs["id_training_type"]){
          if ($this->updateable["id_training_type"]){
            $set.=$set_aux."id_training_type=?";
            $set_aux=",";
            $param[]=$this->id_training_type==''?NULL:$this->id_training_type;
          }else{
            $this->objError[]="The field (id_training_type) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->name!= $rs["name"]){
          if ($this->updateable["name"]){
            $set.=$set_aux."name=?";
            $set_aux=",";
            $param[]=$this->name==''?NULL:$this->name;
          }else{
            $this->objError[]="The field (name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->description!= $rs["description"]){
          if ($this->updateable["description"]){
            $set.=$set_aux."description=?";
            $set_aux=",";
            $param[]=$this->description==''?NULL:$this->description;
          }else{
            $this->objError[]="The field (description) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->hour!= $rs["hour"]){
          if ($this->updateable["hour"]){
            $set.=$set_aux."hour=?";
            $set_aux=",";
            $param[]=$this->hour==''?NULL:$this->hour;
          }else{
            $this->objError[]="The field (hour) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->estimated_start_date!= $rs["estimated_start_date"]){
          if ($this->updateable["estimated_start_date"]){
            $set.=$set_aux."estimated_start_date=?";
            $set_aux=",";
            $param[]=$this->estimated_start_date==''?NULL:$this->estimated_start_date;
          }else{
            $this->objError[]="The field (estimated_start_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->start_date!= $rs["start_date"]){
          if ($this->updateable["start_date"]){
            $set.=$set_aux."start_date=?";
            $set_aux=",";
            $param[]=$this->start_date==''?NULL:$this->start_date;
          }else{
            $this->objError[]="The field (start_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->completion_estimated_date!= $rs["completion_estimated_date"]){
          if ($this->updateable["completion_estimated_date"]){
            $set.=$set_aux."completion_estimated_date=?";
            $set_aux=",";
            $param[]=$this->completion_estimated_date==''?NULL:$this->completion_estimated_date;
          }else{
            $this->objError[]="The field (completion_estimated_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->completion_date!= $rs["completion_date"]){
          if ($this->updateable["completion_date"]){
            $set.=$set_aux."completion_date=?";
            $set_aux=",";
            $param[]=$this->completion_date==''?NULL:$this->completion_date;
          }else{
            $this->objError[]="The field (completion_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_epic_priority!= $rs["id_epic_priority"]){
          if ($this->updateable["id_epic_priority"]){
            $set.=$set_aux."id_epic_priority=?";
            $set_aux=",";
            $param[]=$this->id_epic_priority==''?NULL:$this->id_epic_priority;
          }else{
            $this->objError[]="The field (id_epic_priority) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_epic_status!= $rs["id_epic_status"]){
          if ($this->updateable["id_epic_status"]){
            $set.=$set_aux."id_epic_status=?";
            $set_aux=",";
            $param[]=$this->id_epic_status==''?NULL:$this->id_epic_status;
          }else{
            $this->objError[]="The field (id_epic_status) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->status_date!= $rs["status_date"]){
          if ($this->updateable["status_date"]){
            $set.=$set_aux."status_date=?";
            $set_aux=",";
            $param[]=$this->status_date==''?NULL:$this->status_date;
          }else{
            $this->objError[]="The field (status_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->progress!= $rs["progress"]){
          if ($this->updateable["progress"]){
            $set.=$set_aux."progress=?";
            $set_aux=",";
            $param[]=$this->progress==''?NULL:$this->progress;
          }else{
            $this->objError[]="The field (progress) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE hr_training ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM hr_training
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
