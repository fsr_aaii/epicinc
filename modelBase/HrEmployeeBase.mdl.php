<?php
  class HrEmployeeBase extends TfEntity {
    protected $id;
    protected $id_person;
    protected $email_account;
    protected $hire_date;
    protected $id_development_level;
    protected $id_job_level;
    protected $id_job_level_next;
    protected $id_competence_a;
    protected $id_competence_b;
    protected $id_soft_skill_a;
    protected $id_soft_skill_b;
    protected $is_key_talent;
    protected $has_successor;
    protected $has_talent;
    protected $resignation_risk;
    protected $performance;
    protected $performance_date;
    protected $self_performance;
    protected $self_performance_date;
    protected $learning_agility;
    protected $learning_agility_date;
    protected $self_learning_agility;
    protected $self_learning_agility_date;
    protected $cr;
    protected $successor_name;
    protected $mobility;
    protected $area_interest;
    protected $termination_date;
    protected $id_termination_reason;
    protected $created_date;
    protected $created_by;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="hr_employee";
  }

  private function getAll(){

    $q="SELECT id,
               id_person,
               email_account,
               hire_date,
               id_development_level,
               id_job_level,
               id_job_level_next,
               id_competence_a,
               id_competence_b,
               id_soft_skill_a,
               id_soft_skill_b,
               is_key_talent,
               has_successor,
               has_talent,
               resignation_risk,
               performance,
               performance_date,
               self_performance,
               self_performance_date,
               learning_agility,
               learning_agility_date,
               self_learning_agility,
               self_learning_agility_date,
               cr,
               successor_name,
               mobility,
               area_interest,
               termination_date,
               id_termination_reason,
               created_date,
               created_by
          FROM hr_employee
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_person=$rs["id_person"];
    $this->email_account=$rs["email_account"];
    $this->hire_date=$rs["hire_date"];
    $this->id_development_level=$rs["id_development_level"];
    $this->id_job_level=$rs["id_job_level"];
    $this->id_job_level_next=$rs["id_job_level_next"];
    $this->id_competence_a=$rs["id_competence_a"];
    $this->id_competence_b=$rs["id_competence_b"];
    $this->id_soft_skill_a=$rs["id_soft_skill_a"];
    $this->id_soft_skill_b=$rs["id_soft_skill_b"];
    $this->is_key_talent=$rs["is_key_talent"];
    $this->has_successor=$rs["has_successor"];
    $this->has_talent=$rs["has_talent"];
    $this->resignation_risk=$rs["resignation_risk"];
    $this->performance=$rs["performance"];
    $this->performance_date=$rs["performance_date"];
    $this->self_performance=$rs["self_performance"];
    $this->self_performance_date=$rs["self_performance_date"];
    $this->learning_agility=$rs["learning_agility"];
    $this->learning_agility_date=$rs["learning_agility_date"];
    $this->self_learning_agility=$rs["self_learning_agility"];
    $this->self_learning_agility_date=$rs["self_learning_agility_date"];
    $this->cr=$rs["cr"];
    $this->successor_name=$rs["successor_name"];
    $this->mobility=$rs["mobility"];
    $this->area_interest=$rs["area_interest"];
    $this->termination_date=$rs["termination_date"];
    $this->id_termination_reason=$rs["id_termination_reason"];
    $this->created_date=$rs["created_date"];
    $this->created_by=$rs["created_by"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->hr_employee_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_hr_employee){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_hr_employee; 
    }

    if ($tfRequest->exist("hr_employee_id_person")){
      $this->id_person=$tfRequest->hr_employee_id_person;
    }
    if ($tfRequest->exist("hr_employee_email_account")){
      $this->email_account=$tfRequest->hr_employee_email_account;
    }
    if ($tfRequest->exist("hr_employee_hire_date")){
      $this->hire_date=$tfRequest->hr_employee_hire_date;
    }
    if ($tfRequest->exist("hr_employee_id_development_level")){
      $this->id_development_level=$tfRequest->hr_employee_id_development_level;
    }
    if ($tfRequest->exist("hr_employee_id_job_level")){
      $this->id_job_level=$tfRequest->hr_employee_id_job_level;
    }
    if ($tfRequest->exist("hr_employee_id_job_level_next")){
      $this->id_job_level_next=$tfRequest->hr_employee_id_job_level_next;
    }
    if ($tfRequest->exist("hr_employee_id_competence_a")){
      $this->id_competence_a=$tfRequest->hr_employee_id_competence_a;
    }
    if ($tfRequest->exist("hr_employee_id_competence_b")){
      $this->id_competence_b=$tfRequest->hr_employee_id_competence_b;
    }
    if ($tfRequest->exist("hr_employee_id_soft_skill_a")){
      $this->id_soft_skill_a=$tfRequest->hr_employee_id_soft_skill_a;
    }
    if ($tfRequest->exist("hr_employee_id_soft_skill_b")){
      $this->id_soft_skill_b=$tfRequest->hr_employee_id_soft_skill_b;
    }
    if ($tfRequest->exist("hr_employee_is_key_talent")){
      $this->is_key_talent=$tfRequest->hr_employee_is_key_talent;
    }
    if ($tfRequest->exist("hr_employee_has_successor")){
      $this->has_successor=$tfRequest->hr_employee_has_successor;
    }
    if ($tfRequest->exist("hr_employee_has_talent")){
      $this->has_talent=$tfRequest->hr_employee_has_talent;
    }
    if ($tfRequest->exist("hr_employee_resignation_risk")){
      $this->resignation_risk=$tfRequest->hr_employee_resignation_risk;
    }
    if ($tfRequest->exist("hr_employee_performance")){
      $this->performance=$tfRequest->hr_employee_performance;
    }
    if ($tfRequest->exist("hr_employee_performance_date")){
      $this->performance_date=$tfRequest->hr_employee_performance_date;
    }
    if ($tfRequest->exist("hr_employee_self_performance")){
      $this->self_performance=$tfRequest->hr_employee_self_performance;
    }
    if ($tfRequest->exist("hr_employee_self_performance_date")){
      $this->self_performance_date=$tfRequest->hr_employee_self_performance_date;
    }
    if ($tfRequest->exist("hr_employee_learning_agility")){
      $this->learning_agility=$tfRequest->hr_employee_learning_agility;
    }
    if ($tfRequest->exist("hr_employee_learning_agility_date")){
      $this->learning_agility_date=$tfRequest->hr_employee_learning_agility_date;
    }
    if ($tfRequest->exist("hr_employee_self_learning_agility")){
      $this->self_learning_agility=$tfRequest->hr_employee_self_learning_agility;
    }
    if ($tfRequest->exist("hr_employee_self_learning_agility_date")){
      $this->self_learning_agility_date=$tfRequest->hr_employee_self_learning_agility_date;
    }
    if ($tfRequest->exist("hr_employee_cr")){
      $this->cr=$tfRequest->hr_employee_cr;
    }
    if ($tfRequest->exist("hr_employee_successor_name")){
      $this->successor_name=$tfRequest->hr_employee_successor_name;
    }
    if ($tfRequest->exist("hr_employee_mobility")){
      $this->mobility=$tfRequest->hr_employee_mobility;
    }
    if ($tfRequest->exist("hr_employee_area_interest")){
      $this->area_interest=$tfRequest->hr_employee_area_interest;
    }
    if ($tfRequest->exist("hr_employee_termination_date")){
      $this->termination_date=$tfRequest->hr_employee_termination_date;
    }
    if ($tfRequest->exist("hr_employee_id_termination_reason")){
      $this->id_termination_reason=$tfRequest->hr_employee_id_termination_reason;
    }
    if ($tfRequest->exist("hr_employee_created_date")){
      $this->created_date=$tfRequest->hr_employee_created_date;
    }
    if ($tfRequest->exist("hr_employee_created_by")){
      $this->created_by=$tfRequest->hr_employee_created_by;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_person"]=array("type"=>"number",
                                  "value"=>$this->id_person,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["email_account"]=array("type"=>"string",
                                  "value"=>$this->email_account,
                                  "length"=>200,
                                  "required"=>false);
    $this->validation["hire_date"]=array("type"=>"date",
                                  "value"=>$this->hire_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_development_level"]=array("type"=>"number",
                                  "value"=>$this->id_development_level,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_job_level"]=array("type"=>"number",
                                  "value"=>$this->id_job_level,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_job_level_next"]=array("type"=>"number",
                                  "value"=>$this->id_job_level_next,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_competence_a"]=array("type"=>"number",
                                  "value"=>$this->id_competence_a,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_competence_b"]=array("type"=>"number",
                                  "value"=>$this->id_competence_b,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_soft_skill_a"]=array("type"=>"number",
                                  "value"=>$this->id_soft_skill_a,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_soft_skill_b"]=array("type"=>"number",
                                  "value"=>$this->id_soft_skill_b,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["is_key_talent"]=array("type"=>"string",
                                  "value"=>$this->is_key_talent,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["has_successor"]=array("type"=>"string",
                                  "value"=>$this->has_successor,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["has_talent"]=array("type"=>"string",
                                  "value"=>$this->has_talent,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["resignation_risk"]=array("type"=>"string",
                                  "value"=>$this->resignation_risk,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["performance"]=array("type"=>"number",
                                  "value"=>$this->performance,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["performance_date"]=array("type"=>"date",
                                  "value"=>$this->performance_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["self_performance"]=array("type"=>"number",
                                  "value"=>$this->self_performance,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["self_performance_date"]=array("type"=>"date",
                                  "value"=>$this->self_performance_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["learning_agility"]=array("type"=>"number",
                                  "value"=>$this->learning_agility,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["learning_agility_date"]=array("type"=>"date",
                                  "value"=>$this->learning_agility_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["self_learning_agility"]=array("type"=>"number",
                                  "value"=>$this->self_learning_agility,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["self_learning_agility_date"]=array("type"=>"date",
                                  "value"=>$this->self_learning_agility_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["cr"]=array("type"=>"string",
                                  "value"=>$this->cr,
                                  "length"=>1,
                                  "required"=>false);
    $this->validation["successor_name"]=array("type"=>"string",
                                  "value"=>$this->successor_name,
                                  "length"=>200,
                                  "required"=>false);
    $this->validation["mobility"]=array("type"=>"string",
                                  "value"=>$this->mobility,
                                  "length"=>4,
                                  "required"=>false);
    $this->validation["area_interest"]=array("type"=>"string",
                                  "value"=>$this->area_interest,
                                  "length"=>500,
                                  "required"=>false);
    $this->validation["termination_date"]=array("type"=>"date",
                                  "value"=>$this->termination_date,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["id_termination_reason"]=array("type"=>"number",
                                  "value"=>$this->id_termination_reason,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdPerson($value){
  $this->id_person=$value;
  }
  public function getIdPerson(){
  return $this->id_person;
  }
  public function setEmailAccount($value){
  $this->email_account=$value;
  }
  public function getEmailAccount(){
  return $this->email_account;
  }
  public function setHireDate($value){
  $this->hire_date=$value;
  }
  public function getHireDate(){
  return $this->hire_date;
  }
  public function setIdDevelopmentLevel($value){
  $this->id_development_level=$value;
  }
  public function getIdDevelopmentLevel(){
  return $this->id_development_level;
  }
  public function setIdJobLevel($value){
  $this->id_job_level=$value;
  }
  public function getIdJobLevel(){
  return $this->id_job_level;
  }
  public function setIdJobLevelNext($value){
  $this->id_job_level_next=$value;
  }
  public function getIdJobLevelNext(){
  return $this->id_job_level_next;
  }
  public function setIdCompetenceA($value){
  $this->id_competence_a=$value;
  }
  public function getIdCompetenceA(){
  return $this->id_competence_a;
  }
  public function setIdCompetenceB($value){
  $this->id_competence_b=$value;
  }
  public function getIdCompetenceB(){
  return $this->id_competence_b;
  }
  public function setIdSoftSkillA($value){
  $this->id_soft_skill_a=$value;
  }
  public function getIdSoftSkillA(){
  return $this->id_soft_skill_a;
  }
  public function setIdSoftSkillB($value){
  $this->id_soft_skill_b=$value;
  }
  public function getIdSoftSkillB(){
  return $this->id_soft_skill_b;
  }
  public function setIsKeyTalent($value){
  $this->is_key_talent=$value;
  }
  public function getIsKeyTalent(){
  return $this->is_key_talent;
  }
  public function setHasSuccessor($value){
  $this->has_successor=$value;
  }
  public function getHasSuccessor(){
  return $this->has_successor;
  }
  public function setHasTalent($value){
  $this->has_talent=$value;
  }
  public function getHasTalent(){
  return $this->has_talent;
  }
  public function setResignationRisk($value){
  $this->resignation_risk=$value;
  }
  public function getResignationRisk(){
  return $this->resignation_risk;
  }
  public function setPerformance($value){
  $this->performance=$value;
  }
  public function getPerformance(){
  return $this->performance;
  }
  public function setPerformanceDate($value){
  $this->performance_date=$value;
  }
  public function getPerformanceDate(){
  return $this->performance_date;
  }
  public function setSelfPerformance($value){
  $this->self_performance=$value;
  }
  public function getSelfPerformance(){
  return $this->self_performance;
  }
  public function setSelfPerformanceDate($value){
  $this->self_performance_date=$value;
  }
  public function getSelfPerformanceDate(){
  return $this->self_performance_date;
  }
  public function setLearningAgility($value){
  $this->learning_agility=$value;
  }
  public function getLearningAgility(){
  return $this->learning_agility;
  }
  public function setLearningAgilityDate($value){
  $this->learning_agility_date=$value;
  }
  public function getLearningAgilityDate(){
  return $this->learning_agility_date;
  }
  public function setSelfLearningAgility($value){
  $this->self_learning_agility=$value;
  }
  public function getSelfLearningAgility(){
  return $this->self_learning_agility;
  }
  public function setSelfLearningAgilityDate($value){
  $this->self_learning_agility_date=$value;
  }
  public function getSelfLearningAgilityDate(){
  return $this->self_learning_agility_date;
  }
  public function setCr($value){
  $this->cr=$value;
  }
  public function getCr(){
  return $this->cr;
  }
  public function setSuccessorName($value){
  $this->successor_name=$value;
  }
  public function getSuccessorName(){
  return $this->successor_name;
  }
  public function setMobility($value){
  $this->mobility=$value;
  }
  public function getMobility(){
  return $this->mobility;
  }
  public function setAreaInterest($value){
  $this->area_interest=$value;
  }
  public function getAreaInterest(){
  return $this->area_interest;
  }
  public function setTerminationDate($value){
  $this->termination_date=$value;
  }
  public function getTerminationDate(){
  return $this->termination_date;
  }
  public function setIdTerminationReason($value){
  $this->id_termination_reason=$value;
  }
  public function getIdTerminationReason(){
  return $this->id_termination_reason;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO hr_employee(id,
                               id_person,
                               email_account,
                               hire_date,
                               id_development_level,
                               id_job_level,
                               id_job_level_next,
                               id_competence_a,
                               id_competence_b,
                               id_soft_skill_a,
                               id_soft_skill_b,
                               is_key_talent,
                               has_successor,
                               has_talent,
                               resignation_risk,
                               performance,
                               performance_date,
                               self_performance,
                               self_performance_date,
                               learning_agility,
                               learning_agility_date,
                               self_learning_agility,
                               self_learning_agility_date,
                               cr,
                               successor_name,
                               mobility,
                               area_interest,
                               termination_date,
                               id_termination_reason,
                               created_date,
                               created_by)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_person==''?NULL:$this->id_person,
                     $this->email_account==''?NULL:$this->email_account,
                     $this->hire_date==''?NULL:$this->hire_date,
                     $this->id_development_level==''?NULL:$this->id_development_level,
                     $this->id_job_level==''?NULL:$this->id_job_level,
                     $this->id_job_level_next==''?NULL:$this->id_job_level_next,
                     $this->id_competence_a==''?NULL:$this->id_competence_a,
                     $this->id_competence_b==''?NULL:$this->id_competence_b,
                     $this->id_soft_skill_a==''?NULL:$this->id_soft_skill_a,
                     $this->id_soft_skill_b==''?NULL:$this->id_soft_skill_b,
                     $this->is_key_talent==''?NULL:$this->is_key_talent,
                     $this->has_successor==''?NULL:$this->has_successor,
                     $this->has_talent==''?NULL:$this->has_talent,
                     $this->resignation_risk==''?NULL:$this->resignation_risk,
                     $this->performance==''?NULL:$this->performance,
                     $this->performance_date==''?NULL:$this->performance_date,
                     $this->self_performance==''?NULL:$this->self_performance,
                     $this->self_performance_date==''?NULL:$this->self_performance_date,
                     $this->learning_agility==''?NULL:$this->learning_agility,
                     $this->learning_agility_date==''?NULL:$this->learning_agility_date,
                     $this->self_learning_agility==''?NULL:$this->self_learning_agility,
                     $this->self_learning_agility_date==''?NULL:$this->self_learning_agility_date,
                     $this->cr==''?NULL:$this->cr,
                     $this->successor_name==''?NULL:$this->successor_name,
                     $this->mobility==''?NULL:$this->mobility,
                     $this->area_interest==''?NULL:$this->area_interest,
                     $this->termination_date==''?NULL:$this->termination_date,
                     $this->id_termination_reason==''?NULL:$this->id_termination_reason,
                     $this->created_date==''?NULL:$this->created_date,
                     $this->created_by==''?NULL:$this->created_by);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_person!= $rs["id_person"]){
          if ($this->updateable["id_person"]){
            $set.=$set_aux."id_person=?";
            $set_aux=",";
            $param[]=$this->id_person==''?NULL:$this->id_person;
          }else{
            $this->objError[]="The field (id_person) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->email_account!= $rs["email_account"]){
          if ($this->updateable["email_account"]){
            $set.=$set_aux."email_account=?";
            $set_aux=",";
            $param[]=$this->email_account==''?NULL:$this->email_account;
          }else{
            $this->objError[]="The field (email_account) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->hire_date!= $rs["hire_date"]){
          if ($this->updateable["hire_date"]){
            $set.=$set_aux."hire_date=?";
            $set_aux=",";
            $param[]=$this->hire_date==''?NULL:$this->hire_date;
          }else{
            $this->objError[]="The field (hire_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_development_level!= $rs["id_development_level"]){
          if ($this->updateable["id_development_level"]){
            $set.=$set_aux."id_development_level=?";
            $set_aux=",";
            $param[]=$this->id_development_level==''?NULL:$this->id_development_level;
          }else{
            $this->objError[]="The field (id_development_level) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_job_level!= $rs["id_job_level"]){
          if ($this->updateable["id_job_level"]){
            $set.=$set_aux."id_job_level=?";
            $set_aux=",";
            $param[]=$this->id_job_level==''?NULL:$this->id_job_level;
          }else{
            $this->objError[]="The field (id_job_level) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_job_level_next!= $rs["id_job_level_next"]){
          if ($this->updateable["id_job_level_next"]){
            $set.=$set_aux."id_job_level_next=?";
            $set_aux=",";
            $param[]=$this->id_job_level_next==''?NULL:$this->id_job_level_next;
          }else{
            $this->objError[]="The field (id_job_level_next) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_competence_a!= $rs["id_competence_a"]){
          if ($this->updateable["id_competence_a"]){
            $set.=$set_aux."id_competence_a=?";
            $set_aux=",";
            $param[]=$this->id_competence_a==''?NULL:$this->id_competence_a;
          }else{
            $this->objError[]="The field (id_competence_a) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_competence_b!= $rs["id_competence_b"]){
          if ($this->updateable["id_competence_b"]){
            $set.=$set_aux."id_competence_b=?";
            $set_aux=",";
            $param[]=$this->id_competence_b==''?NULL:$this->id_competence_b;
          }else{
            $this->objError[]="The field (id_competence_b) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_soft_skill_a!= $rs["id_soft_skill_a"]){
          if ($this->updateable["id_soft_skill_a"]){
            $set.=$set_aux."id_soft_skill_a=?";
            $set_aux=",";
            $param[]=$this->id_soft_skill_a==''?NULL:$this->id_soft_skill_a;
          }else{
            $this->objError[]="The field (id_soft_skill_a) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_soft_skill_b!= $rs["id_soft_skill_b"]){
          if ($this->updateable["id_soft_skill_b"]){
            $set.=$set_aux."id_soft_skill_b=?";
            $set_aux=",";
            $param[]=$this->id_soft_skill_b==''?NULL:$this->id_soft_skill_b;
          }else{
            $this->objError[]="The field (id_soft_skill_b) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->is_key_talent!= $rs["is_key_talent"]){
          if ($this->updateable["is_key_talent"]){
            $set.=$set_aux."is_key_talent=?";
            $set_aux=",";
            $param[]=$this->is_key_talent==''?NULL:$this->is_key_talent;
          }else{
            $this->objError[]="The field (is_key_talent) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->has_successor!= $rs["has_successor"]){
          if ($this->updateable["has_successor"]){
            $set.=$set_aux."has_successor=?";
            $set_aux=",";
            $param[]=$this->has_successor==''?NULL:$this->has_successor;
          }else{
            $this->objError[]="The field (has_successor) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->has_talent!= $rs["has_talent"]){
          if ($this->updateable["has_talent"]){
            $set.=$set_aux."has_talent=?";
            $set_aux=",";
            $param[]=$this->has_talent==''?NULL:$this->has_talent;
          }else{
            $this->objError[]="The field (has_talent) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->resignation_risk!= $rs["resignation_risk"]){
          if ($this->updateable["resignation_risk"]){
            $set.=$set_aux."resignation_risk=?";
            $set_aux=",";
            $param[]=$this->resignation_risk==''?NULL:$this->resignation_risk;
          }else{
            $this->objError[]="The field (resignation_risk) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->performance!= $rs["performance"]){
          if ($this->updateable["performance"]){
            $set.=$set_aux."performance=?";
            $set_aux=",";
            $param[]=$this->performance==''?NULL:$this->performance;
          }else{
            $this->objError[]="The field (performance) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->performance_date!= $rs["performance_date"]){
          if ($this->updateable["performance_date"]){
            $set.=$set_aux."performance_date=?";
            $set_aux=",";
            $param[]=$this->performance_date==''?NULL:$this->performance_date;
          }else{
            $this->objError[]="The field (performance_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->self_performance!= $rs["self_performance"]){
          if ($this->updateable["self_performance"]){
            $set.=$set_aux."self_performance=?";
            $set_aux=",";
            $param[]=$this->self_performance==''?NULL:$this->self_performance;
          }else{
            $this->objError[]="The field (self_performance) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->self_performance_date!= $rs["self_performance_date"]){
          if ($this->updateable["self_performance_date"]){
            $set.=$set_aux."self_performance_date=?";
            $set_aux=",";
            $param[]=$this->self_performance_date==''?NULL:$this->self_performance_date;
          }else{
            $this->objError[]="The field (self_performance_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->learning_agility!= $rs["learning_agility"]){
          if ($this->updateable["learning_agility"]){
            $set.=$set_aux."learning_agility=?";
            $set_aux=",";
            $param[]=$this->learning_agility==''?NULL:$this->learning_agility;
          }else{
            $this->objError[]="The field (learning_agility) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->learning_agility_date!= $rs["learning_agility_date"]){
          if ($this->updateable["learning_agility_date"]){
            $set.=$set_aux."learning_agility_date=?";
            $set_aux=",";
            $param[]=$this->learning_agility_date==''?NULL:$this->learning_agility_date;
          }else{
            $this->objError[]="The field (learning_agility_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->self_learning_agility!= $rs["self_learning_agility"]){
          if ($this->updateable["self_learning_agility"]){
            $set.=$set_aux."self_learning_agility=?";
            $set_aux=",";
            $param[]=$this->self_learning_agility==''?NULL:$this->self_learning_agility;
          }else{
            $this->objError[]="The field (self_learning_agility) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->self_learning_agility_date!= $rs["self_learning_agility_date"]){
          if ($this->updateable["self_learning_agility_date"]){
            $set.=$set_aux."self_learning_agility_date=?";
            $set_aux=",";
            $param[]=$this->self_learning_agility_date==''?NULL:$this->self_learning_agility_date;
          }else{
            $this->objError[]="The field (self_learning_agility_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->cr!= $rs["cr"]){
          if ($this->updateable["cr"]){
            $set.=$set_aux."cr=?";
            $set_aux=",";
            $param[]=$this->cr==''?NULL:$this->cr;
          }else{
            $this->objError[]="The field (cr) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->successor_name!= $rs["successor_name"]){
          if ($this->updateable["successor_name"]){
            $set.=$set_aux."successor_name=?";
            $set_aux=",";
            $param[]=$this->successor_name==''?NULL:$this->successor_name;
          }else{
            $this->objError[]="The field (successor_name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->mobility!= $rs["mobility"]){
          if ($this->updateable["mobility"]){
            $set.=$set_aux."mobility=?";
            $set_aux=",";
            $param[]=$this->mobility==''?NULL:$this->mobility;
          }else{
            $this->objError[]="The field (mobility) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->area_interest!= $rs["area_interest"]){
          if ($this->updateable["area_interest"]){
            $set.=$set_aux."area_interest=?";
            $set_aux=",";
            $param[]=$this->area_interest==''?NULL:$this->area_interest;
          }else{
            $this->objError[]="The field (area_interest) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->termination_date!= $rs["termination_date"]){
          if ($this->updateable["termination_date"]){
            $set.=$set_aux."termination_date=?";
            $set_aux=",";
            $param[]=$this->termination_date==''?NULL:$this->termination_date;
          }else{
            $this->objError[]="The field (termination_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_termination_reason!= $rs["id_termination_reason"]){
          if ($this->updateable["id_termination_reason"]){
            $set.=$set_aux."id_termination_reason=?";
            $set_aux=",";
            $param[]=$this->id_termination_reason==''?NULL:$this->id_termination_reason;
          }else{
            $this->objError[]="The field (id_termination_reason) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE hr_employee ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM hr_employee
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
