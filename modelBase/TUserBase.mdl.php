<?php
  class TUserBase extends TfEntity {
    protected $id;
    protected $login;
    protected $photo;
    protected $active;
    protected $password;
    protected $password_date;
    protected $email_account;
    protected $name;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="t_user";
  }

  private function getAll(){

    $q="SELECT id,
               login,
               photo,
               active,
               password,
               password_date,
               email_account,
               name,
               created_by,
               created_date
          FROM t_user
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->login=$rs["login"];
    $this->photo=$rs["photo"];
    $this->active=$rs["active"];
    $this->password=$rs["password"];
    $this->password_date=$rs["password_date"];
    $this->email_account=$rs["email_account"];
    $this->name=$rs["name"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->t_user_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_t_user){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_t_user; 
    }

    if ($tfRequest->exist("t_user_login")){
      $this->login=$tfRequest->t_user_login;
    }
    if ($tfRequest->exist("t_user_photo")){
      $this->photo=$tfRequest->t_user_photo;
    }
    if ($tfRequest->exist("t_user_active")){
      $this->active=$tfRequest->t_user_active;
    }
    if ($tfRequest->exist("t_user_password")){
      $this->password=$tfRequest->t_user_password;
    }
    if ($tfRequest->exist("t_user_password_date")){
      $this->password_date=$tfRequest->t_user_password_date;
    }
    if ($tfRequest->exist("t_user_email_account")){
      $this->email_account=$tfRequest->t_user_email_account;
    }
    if ($tfRequest->exist("t_user_name")){
      $this->name=$tfRequest->t_user_name;
    }
    if ($tfRequest->exist("t_user_created_by")){
      $this->created_by=$tfRequest->t_user_created_by;
    }
    if ($tfRequest->exist("t_user_created_date")){
      $this->created_date=$tfRequest->t_user_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["login"]=array("type"=>"string",
                                  "value"=>$this->login,
                                  "length"=>30,
                                  "required"=>true);
    $this->validation["photo"]=array("type"=>"string",
                                  "value"=>$this->photo,
                                  "length"=>500,
                                  "required"=>true);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["password"]=array("type"=>"string",
                                  "value"=>$this->password,
                                  "length"=>500,
                                  "required"=>true);
    $this->validation["password_date"]=array("type"=>"date",
                                  "value"=>$this->password_date,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["email_account"]=array("type"=>"string",
                                  "value"=>$this->email_account,
                                  "length"=>500,
                                  "required"=>false);
    $this->validation["name"]=array("type"=>"string",
                                  "value"=>$this->name,
                                  "length"=>200,
                                  "required"=>false);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setLogin($value){
  $this->login=$value;
  }
  public function getLogin(){
  return $this->login;
  }
  public function setPhoto($value){
  $this->photo=$value;
  }
  public function getPhoto(){
  return $this->photo;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setPassword($value){
  $this->password=$value;
  }
  public function getPassword(){
  return $this->password;
  }
  public function setPasswordDate($value){
  $this->password_date=$value;
  }
  public function getPasswordDate(){
  return $this->password_date;
  }
  public function setEmailAccount($value){
  $this->email_account=$value;
  }
  public function getEmailAccount(){
  return $this->email_account;
  }
  public function setName($value){
  $this->name=$value;
  }
  public function getName(){
  return $this->name;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO t_user(id,
                               login,
                               photo,
                               active,
                               password,
                               password_date,
                               email_account,
                               name,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->login==''?NULL:$this->login,
                     $this->photo==''?NULL:$this->photo,
                     $this->active==''?NULL:$this->active,
                     $this->password==''?NULL:$this->password,
                     $this->password_date==''?NULL:$this->password_date,
                     $this->email_account==''?NULL:$this->email_account,
                     $this->name==''?NULL:$this->name,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->login!= $rs["login"]){
          if ($this->updateable["login"]){
            $set.=$set_aux."login=?";
            $set_aux=",";
            $param[]=$this->login==''?NULL:$this->login;
          }else{
            $this->objError[]="The field (login) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->photo!= $rs["photo"]){
          if ($this->updateable["photo"]){
            $set.=$set_aux."photo=?";
            $set_aux=",";
            $param[]=$this->photo==''?NULL:$this->photo;
          }else{
            $this->objError[]="The field (photo) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->password!= $rs["password"]){
          if ($this->updateable["password"]){
            $set.=$set_aux."password=?";
            $set_aux=",";
            $param[]=$this->password==''?NULL:$this->password;
          }else{
            $this->objError[]="The field (password) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->password_date!= $rs["password_date"]){
          if ($this->updateable["password_date"]){
            $set.=$set_aux."password_date=?";
            $set_aux=",";
            $param[]=$this->password_date==''?NULL:$this->password_date;
          }else{
            $this->objError[]="The field (password_date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->email_account!= $rs["email_account"]){
          if ($this->updateable["email_account"]){
            $set.=$set_aux."email_account=?";
            $set_aux=",";
            $param[]=$this->email_account==''?NULL:$this->email_account;
          }else{
            $this->objError[]="The field (email_account) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->name!= $rs["name"]){
          if ($this->updateable["name"]){
            $set.=$set_aux."name=?";
            $set_aux=",";
            $param[]=$this->name==''?NULL:$this->name;
          }else{
            $this->objError[]="The field (name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE t_user ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM t_user
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
