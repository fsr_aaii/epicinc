<?php
  class HrCoachingBase extends TfEntity {
    protected $id;
    protected $id_employee;
    protected $id_coaching;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="hr_coaching";
  }

  private function getAll(){

    $q="SELECT id,
               id_employee,
               id_coaching,
               active,
               created_by,
               created_date
          FROM hr_coaching
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_employee=$rs["id_employee"];
    $this->id_coaching=$rs["id_coaching"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->hr_coaching_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_hr_coaching){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_hr_coaching; 
    }

    if ($tfRequest->exist("hr_coaching_id_employee")){
      $this->id_employee=$tfRequest->hr_coaching_id_employee;
    }
    if ($tfRequest->exist("hr_coaching_id_coaching")){
      $this->id_coaching=$tfRequest->hr_coaching_id_coaching;
    }
    if ($tfRequest->exist("hr_coaching_active")){
      $this->active=$tfRequest->hr_coaching_active;
    }
    if ($tfRequest->exist("hr_coaching_created_by")){
      $this->created_by=$tfRequest->hr_coaching_created_by;
    }
    if ($tfRequest->exist("hr_coaching_created_date")){
      $this->created_date=$tfRequest->hr_coaching_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_employee"]=array("type"=>"number",
                                  "value"=>$this->id_employee,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_coaching"]=array("type"=>"number",
                                  "value"=>$this->id_coaching,
                                  "length"=>22,
                                  "required"=>false);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdEmployee($value){
  $this->id_employee=$value;
  }
  public function getIdEmployee(){
  return $this->id_employee;
  }
  public function setIdCoaching($value){
  $this->id_coaching=$value;
  }
  public function getIdCoaching(){
  return $this->id_coaching;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO hr_coaching(id,
                               id_employee,
                               id_coaching,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_employee==''?NULL:$this->id_employee,
                     $this->id_coaching==''?NULL:$this->id_coaching,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_employee!= $rs["id_employee"]){
          if ($this->updateable["id_employee"]){
            $set.=$set_aux."id_employee=?";
            $set_aux=",";
            $param[]=$this->id_employee==''?NULL:$this->id_employee;
          }else{
            $this->objError[]="The field (id_employee) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_coaching!= $rs["id_coaching"]){
          if ($this->updateable["id_coaching"]){
            $set.=$set_aux."id_coaching=?";
            $set_aux=",";
            $param[]=$this->id_coaching==''?NULL:$this->id_coaching;
          }else{
            $this->objError[]="The field (id_coaching) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE hr_coaching ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM hr_coaching
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
