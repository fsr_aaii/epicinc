<?php
  class CmPostBase extends TfEntity {
    protected $id;
    protected $id_category;
    protected $title;
    protected $image;
    protected $content;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="cm_post";
  }

  private function getAll(){

    $q="SELECT id,
               id_category,
               title,
               image,
               content,
               active,
               created_by,
               created_date
          FROM cm_post
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_category=$rs["id_category"];
    $this->title=$rs["title"];
    $this->image=$rs["image"];
    $this->content=$rs["content"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->cm_post_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_cm_post){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_cm_post; 
    }

    if ($tfRequest->exist("cm_post_id_category")){
      $this->id_category=$tfRequest->cm_post_id_category;
    }
    if ($tfRequest->exist("cm_post_title")){
      $this->title=$tfRequest->cm_post_title;
    }
    if ($tfRequest->exist("cm_post_image")){
      $this->image=$tfRequest->cm_post_image;
    }
    if ($tfRequest->exist("cm_post_content")){
      $this->content=$tfRequest->cm_post_content;
    }
    if ($tfRequest->exist("cm_post_active")){
      $this->active=$tfRequest->cm_post_active;
    }
    if ($tfRequest->exist("cm_post_created_by")){
      $this->created_by=$tfRequest->cm_post_created_by;
    }
    if ($tfRequest->exist("cm_post_created_date")){
      $this->created_date=$tfRequest->cm_post_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_category"]=array("type"=>"number",
                                  "value"=>$this->id_category,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["title"]=array("type"=>"string",
                                  "value"=>$this->title,
                                  "length"=>200,
                                  "required"=>true);
    $this->validation["image"]=array("type"=>"string",
                                  "value"=>$this->image,
                                  "length"=>500,
                                  "required"=>false);
    $this->validation["content"]=array("type"=>"string",
                                  "value"=>$this->content,
                                  "length"=>4500,
                                  "required"=>true);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdCategory($value){
  $this->id_category=$value;
  }
  public function getIdCategory(){
  return $this->id_category;
  }
  public function setTitle($value){
  $this->title=$value;
  }
  public function getTitle(){
  return $this->title;
  }
  public function setImage($value){
  $this->image=$value;
  }
  public function getImage(){
  return $this->image;
  }
  public function setContent($value){
  $this->content=$value;
  }
  public function getContent(){
  return $this->content;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO cm_post(id,
                               id_category,
                               title,
                               image,
                               content,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_category==''?NULL:$this->id_category,
                     $this->title==''?NULL:$this->title,
                     $this->image==''?NULL:$this->image,
                     $this->content==''?NULL:$this->content,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_category!= $rs["id_category"]){
          if ($this->updateable["id_category"]){
            $set.=$set_aux."id_category=?";
            $set_aux=",";
            $param[]=$this->id_category==''?NULL:$this->id_category;
          }else{
            $this->objError[]="The field (id_category) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->title!= $rs["title"]){
          if ($this->updateable["title"]){
            $set.=$set_aux."title=?";
            $set_aux=",";
            $param[]=$this->title==''?NULL:$this->title;
          }else{
            $this->objError[]="The field (title) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->image!= $rs["image"]){
          if ($this->updateable["image"]){
            $set.=$set_aux."image=?";
            $set_aux=",";
            $param[]=$this->image==''?NULL:$this->image;
          }else{
            $this->objError[]="The field (image) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->content!= $rs["content"]){
          if ($this->updateable["content"]){
            $set.=$set_aux."content=?";
            $set_aux=",";
            $param[]=$this->content==''?NULL:$this->content;
          }else{
            $this->objError[]="The field (content) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE cm_post ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM cm_post
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
