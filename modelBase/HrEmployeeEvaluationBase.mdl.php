<?php
  class HrEmployeeEvaluationBase extends TfEntity {
    protected $id;
    protected $id_employee;
    protected $id_position;
    protected $date;
    protected $id_evaluation_type;
    protected $evaluator_type;
    protected $created_by;
    protected $created_date;

  public function __construct(TfSession $tfs){ 
    $this->tfs = $tfs;
    $this->entity="hr_employee_evaluation";
  }

  private function getAll(){

    $q="SELECT id,
               id_employee,
               id_position,
               date,
               id_evaluation_type,
               evaluator_type,
               created_by,
               created_date
          FROM hr_employee_evaluation
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_employee=$rs["id_employee"];
    $this->id_position=$rs["id_position"];
    $this->date=$rs["date"];
    $this->id_evaluation_type=$rs["id_evaluation_type"];
    $this->evaluator_type=$rs["evaluator_type"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }

  protected function uiPopulate(TfRequest $tfRequest){ 

    $this->dbPopulate($tfRequest->hr_employee_evaluation_id);
      if ($this->initialState!=""){
      if ($this->initialState!=$tfRequest->is_hr_employee_evaluation){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfRequest->is_hr_employee_evaluation; 
    }

    if ($tfRequest->exist("hr_employee_evaluation_id_employee")){
      $this->id_employee=$tfRequest->hr_employee_evaluation_id_employee;
    }
    if ($tfRequest->exist("hr_employee_evaluation_id_position")){
      $this->id_position=$tfRequest->hr_employee_evaluation_id_position;
    }
    if ($tfRequest->exist("hr_employee_evaluation_date")){
      $this->date=$tfRequest->hr_employee_evaluation_date;
    }
    if ($tfRequest->exist("hr_employee_evaluation_id_evaluation_type")){
      $this->id_evaluation_type=$tfRequest->hr_employee_evaluation_id_evaluation_type;
    }
    if ($tfRequest->exist("hr_employee_evaluation_evaluator_type")){
      $this->evaluator_type=$tfRequest->hr_employee_evaluation_evaluator_type;
    }
    if ($tfRequest->exist("hr_employee_evaluation_created_by")){
      $this->created_by=$tfRequest->hr_employee_evaluation_created_by;
    }
    if ($tfRequest->exist("hr_employee_evaluation_created_date")){
      $this->created_date=$tfRequest->hr_employee_evaluation_created_date;
    }

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_employee"]=array("type"=>"number",
                                  "value"=>$this->id_employee,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_position"]=array("type"=>"number",
                                  "value"=>$this->id_position,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["date"]=array("type"=>"date",
                                  "value"=>$this->date,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_evaluation_type"]=array("type"=>"number",
                                  "value"=>$this->id_evaluation_type,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["evaluator_type"]=array("type"=>"string",
                                  "value"=>$this->evaluator_type,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdEmployee($value){
  $this->id_employee=$value;
  }
  public function getIdEmployee(){
  return $this->id_employee;
  }
  public function setIdPosition($value){
  $this->id_position=$value;
  }
  public function getIdPosition(){
  return $this->id_position;
  }
  public function setDate($value){
  $this->date=$value;
  }
  public function getDate(){
  return $this->date;
  }
  public function setIdEvaluationType($value){
  $this->id_evaluation_type=$value;
  }
  public function getIdEvaluationType(){
  return $this->id_evaluation_type;
  }
  public function setEvaluatorType($value){
  $this->evaluator_type=$value;
  }
  public function getEvaluatorType(){
  return $this->evaluator_type;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO hr_employee_evaluation(id,
                               id_employee,
                               id_position,
                               date,
                               id_evaluation_type,
                               evaluator_type,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_employee==''?NULL:$this->id_employee,
                     $this->id_position==''?NULL:$this->id_position,
                     $this->date==''?NULL:$this->date,
                     $this->id_evaluation_type==''?NULL:$this->id_evaluation_type,
                     $this->evaluator_type==''?NULL:$this->evaluator_type,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_employee!= $rs["id_employee"]){
          if ($this->updateable["id_employee"]){
            $set.=$set_aux."id_employee=?";
            $set_aux=",";
            $param[]=$this->id_employee==''?NULL:$this->id_employee;
          }else{
            $this->objError[]="The field (id_employee) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_position!= $rs["id_position"]){
          if ($this->updateable["id_position"]){
            $set.=$set_aux."id_position=?";
            $set_aux=",";
            $param[]=$this->id_position==''?NULL:$this->id_position;
          }else{
            $this->objError[]="The field (id_position) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->date!= $rs["date"]){
          if ($this->updateable["date"]){
            $set.=$set_aux."date=?";
            $set_aux=",";
            $param[]=$this->date==''?NULL:$this->date;
          }else{
            $this->objError[]="The field (date) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_evaluation_type!= $rs["id_evaluation_type"]){
          if ($this->updateable["id_evaluation_type"]){
            $set.=$set_aux."id_evaluation_type=?";
            $set_aux=",";
            $param[]=$this->id_evaluation_type==''?NULL:$this->id_evaluation_type;
          }else{
            $this->objError[]="The field (id_evaluation_type) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->evaluator_type!= $rs["evaluator_type"]){
          if ($this->updateable["evaluator_type"]){
            $set.=$set_aux."evaluator_type=?";
            $set_aux=",";
            $param[]=$this->evaluator_type==''?NULL:$this->evaluator_type;
          }else{
            $this->objError[]="The field (evaluator_type) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE hr_employee_evaluation ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM hr_employee_evaluation
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }


}
?>
