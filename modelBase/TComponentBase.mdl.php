<?php
  class TComponentBase extends TfEntity {
    protected $id;
    protected $id_component_type;
    protected $name;
    protected $description;
    protected $active;
    protected $created_by;
    protected $created_date;

  public function __construct($tfs){ 
    $this->tfs = $tfs;
    $this->entity="t_component";
  }

  private function getAll(){

    $q="SELECT id,
               id_component_type,
               name,
               description,
               active,
               created_by,
               created_date
          FROM t_component
         WHERE id=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    return $rs;
  }

  protected function dbPopulate($id){ 

    $this->id=$id;
    $rs = $this->getAll();
    $this->initialState=hash(HASH_KEY,json_encode($rs));
    $this->id_component_type=$rs["id_component_type"];
    $this->name=$rs["name"];
    $this->description=$rs["description"];
    $this->active=$rs["active"];
    $this->created_by=$rs["created_by"];
    $this->created_date=$rs["created_date"];

  }


  protected function uiPopulate(TfRequest $tfe){ 
    
    $this->dbPopulate($tfr->fi_rank_level_id);

    if ($this->initialState!=''){
      if ($this->initialState!=$tfr->is_fi_rank_level){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
    }else{
      $this->initialState=$tfr->is_fi_rank_level; 
    }

    if ($request->exist("fi_rank_level_name")){
      $this->name=$tfr->fi_rank_level_name;
    }
    if ($request->exist("fi_rank_level_id_parent")){
      $this->id_parent=$tfr->fi_rank_level_id_parent;
    }  
    if ($request->exist("fi_rank_level_active")){
      $this->active=$tfr->fi_rank_level_active;
    } 
    if ($request->exist("fi_rank_level_created_by")){
      $this->created_by=$tfr->fi_rank_level_created_by;
    } 
    if ($request->exist("fi_rank_level_created_date")){
      $this->created_date=$tfr->fi_rank_level_created_date;
    } 
    

  }

  public function setValidations(){
    $this->validation["id"]=array("type"=>"number",
                                  "value"=>$this->id,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["id_component_type"]=array("type"=>"number",
                                  "value"=>$this->id_component_type,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["name"]=array("type"=>"string",
                                  "value"=>$this->name,
                                  "length"=>255,
                                  "required"=>true);
    $this->validation["description"]=array("type"=>"string",
                                  "value"=>$this->description,
                                  "length"=>1000,
                                  "required"=>false);
    $this->validation["active"]=array("type"=>"string",
                                  "value"=>$this->active,
                                  "length"=>1,
                                  "required"=>true);
    $this->validation["created_by"]=array("type"=>"number",
                                  "value"=>$this->created_by,
                                  "length"=>22,
                                  "required"=>true);
    $this->validation["created_date"]=array("type"=>"datetime",
                                  "value"=>$this->created_date,
                                  "length"=>22,
                                  "required"=>true);

  $this->setAttrErrors();
  }

  public function setId($value){
  $this->id=$value;
  }
  public function getId(){
  return $this->id;
  }
  public function setIdComponentType($value){
  $this->id_component_type=$value;
  }
  public function getIdComponentType(){
  return $this->id_component_type;
  }
  public function setName($value){
  $this->name=$value;
  }
  public function getName(){
  return $this->name;
  }
  public function setDescription($value){
  $this->description=$value;
  }
  public function getDescription(){
  return $this->description;
  }
  public function setActive($value){
  $this->active=$value;
  }
  public function getActive(){
  return $this->active;
  }
  public function setCreatedBy($value){
  $this->created_by=$value;
  }
  public function getCreatedBy(){
  return $this->created_by;
  }
  public function setCreatedDate($value){
  $this->created_date=$value;
  }
  public function getCreatedDate(){
  return $this->created_date;
  }

  public function create(){
    $this->id = $this->sequence();
    $this->validate();
    if($this->valid){
      $q = "INSERT INTO t_component(id,
                               id_component_type,
                               name,
                               description,
                               active,
                               created_by,
                               created_date)
            VALUES (?,?,?,?,?,?,?)";

      $param = array($this->id==''?NULL:$this->id,
                     $this->id_component_type==''?NULL:$this->id_component_type,
                     $this->name==''?NULL:$this->name,
                     $this->description==''?NULL:$this->description,
                     $this->active==''?NULL:$this->active,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
      $this->objMsg[]="your record has been created";
      $rs=$this->getAll();
      $this->initialState=hash(HASH_KEY,json_encode($rs));
    }
   }

  public function update(){
    $this->validate();
    if($this->valid){
      $rs=$this->getAll();
      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){
        $this->objError[]="This record is blocked by another user, try later";
        $this->valid = false;
      }
      if($this->valid){
        unset($set);
        unset($q);
        $param = array();
        $set_aux=" SET ";

        if ($this->id!= $rs["id"]){
          if ($this->updateable["id"]){
            $set.=$set_aux."id=?";
            $set_aux=",";
            $param[]=$this->id==''?NULL:$this->id;
          }else{
            $this->objError[]="The field (id) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->id_component_type!= $rs["id_component_type"]){
          if ($this->updateable["id_component_type"]){
            $set.=$set_aux."id_component_type=?";
            $set_aux=",";
            $param[]=$this->id_component_type==''?NULL:$this->id_component_type;
          }else{
            $this->objError[]="The field (id_component_type) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->name!= $rs["name"]){
          if ($this->updateable["name"]){
            $set.=$set_aux."name=?";
            $set_aux=",";
            $param[]=$this->name==''?NULL:$this->name;
          }else{
            $this->objError[]="The field (name) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->description!= $rs["description"]){
          if ($this->updateable["description"]){
            $set.=$set_aux."description=?";
            $set_aux=",";
            $param[]=$this->description==''?NULL:$this->description;
          }else{
            $this->objError[]="The field (description) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->active!= $rs["active"]){
          if ($this->updateable["active"]){
            $set.=$set_aux."active=?";
            $set_aux=",";
            $param[]=$this->active==''?NULL:$this->active;
          }else{
            $this->objError[]="The field (active) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_by!= $rs["created_by"]){
          if ($this->updateable["created_by"]){
            $set.=$set_aux."created_by=?";
            $set_aux=",";
            $param[]=$this->created_by==''?NULL:$this->created_by;
          }else{
            $this->objError[]="The field (created_by) cannot be modified";
            $this->valid = false;
          }
        }
        if ($this->created_date!= $rs["created_date"]){
          if ($this->updateable["created_date"]){
            $set.=$set_aux."created_date=?";
            $set_aux=",";
            $param[]=$this->created_date==''?NULL:$this->created_date;
          }else{
            $this->objError[]="The field (created_date) cannot be modified";
            $this->valid = false;
          }
        }

        if ($this->valid){
          if (isset($set)){
            $q = "UPDATE t_component ".$set." WHERE id=?";
            $param[]=$this->id;
            $this->tfs->execute($q,$param);
            $this->objMsg[]="This record has been updated";
            $rs=$this->getAll();
            $this->initialState=hash(HASH_KEY,json_encode($rs));
          }else{
            $this->objMsg[]="This record don''t have data to update";
          }
        }
      }
    }
  }
  public function delete(){
    $q="DELETE FROM t_component
         WHERE id=?";
    $param = array($this->id);

    $this->tfs->execute($q,$param);
  }

}
?>
