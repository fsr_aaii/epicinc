<?php
  class HrDevelopmentLevel extends HrDevelopmentLevelBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "description"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",description \"option\"
            FROM hr_development_level
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT description description
            FROM hr_development_level
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.description
            FROM hr_development_level a";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
