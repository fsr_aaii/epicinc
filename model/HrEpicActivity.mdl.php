<?php
  class HrEpicActivity extends HrEpicActivityBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_epic"=>true,
                              "description"=>true,
                              "estimated_start_date"=>true,
                              "start_date"=>true,
                              "completion_estimated_date"=>true,
                              "completion_date"=>true,
                              "id_epic_priority"=>true,
                              "id_epic_status"=>true,
                              "status_date"=>true,
                              "progress"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }

  public function create(){
    parent::create();
    if($this->valid){
       $hrEpic = new HrEpic($this->tfs);
       $hrEpic->populatePk($this->id_epic); 
       $hrEpic->setGlobalProgress();
       $hrEpic->update();
       if(!$hrEpic->valid){
          $this->valid=false;
       }

    }
  }
  public function update(){
    parent::update();
    if($this->valid){
       $hrEpic = new HrEpic($this->tfs);
       $hrEpic->populatePk($this->id_epic); 
       $hrEpic->setGlobalProgress();
       $hrEpic->update();
       if(!$hrEpic->valid){
          $this->valid=false;
       }

    }
  }
  public function delete(){
    parent::delete();
    if($this->valid){
       $hrEpic = new HrEpic($this->tfs);
       $hrEpic->populatePk($this->id_epic); 
       $hrEpic->setGlobalProgress();
       $hrEpic->update();
       if(!$hrEpic->valid){
          $this->valid=false;
       }

    }
  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",description \"option\"
            FROM hr_epic_activity
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT description description
            FROM hr_epic_activity
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.description id_epic,
                 a.description,
                 a.estimated_start_date,
                 a.start_date,
                 a.completion_estimated_date,
                 a.completion_date,
                 c.name id_epic_priority,
                 d.name id_epic_status,
                 a.status_date,
                 a.progress,
                 a.active,
                 e.name created_by,
                 a.created_date
            FROM hr_epic_activity a,
                 hr_epic b,
                 hr_epic_priority c,
                 hr_epic_status d,
                 t_user e
           WHERE b.id = a.id_epic
           AND c.id = a.id_epic_priority
           AND d.id = a.id_epic_status
           AND e.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function listByEpic(TfSession $tfs,$idEpic){
    
    $q = "SELECT a.id,
                 a.description,
                 a.completion_estimated_date,
                 a.id_epic_priority,
                 a.id_epic_status,
                 d.name priority,
                 e.name status,
                 a.status_date,
                 a.progress
            FROM hr_epic_activity a,
                 hr_epic_priority d,
                 hr_epic_status e
           WHERE d.id = a.id_epic_priority
           AND e.id = a.id_epic_status
           AND a.id_epic = ?";
    $param = array($idEpic);
       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
