<?php
  class HrEmployee extends HrEmployeeBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_person"=>true,
                              "email_account"=>true,
                              "hire_date"=>true,
                              "id_development_level"=>true,
                              "id_job_level"=>true,
                              "id_job_level_next"=>true,
                              "id_competence_a"=>true,
                              "id_competence_b"=>true,
                              "id_soft_skill_a"=>true,
                              "id_soft_skill_b"=>true,
                              "is_key_talent"=>true,
                              "has_successor"=>true,
                              "has_talent"=>true,
                              "resignation_risk"=>true,
                              "performance"=>true,
                              "performance_date"=>true,
                              "self_performance"=>true,
                              "self_performance_date"=>true,
                              "learning_agility"=>true,
                              "learning_agility_date"=>true,
                              "self_learning_agility"=>true,
                              "self_learning_agility_date"=>true,
                              "cr"=>true,
                              "successor_name"=>true,
                              "mobility"=>true,
                              "area_interest"=>true,
                              "termination_date"=>true,
                              "id_termination_reason"=>true,
                              "created_date"=>true,
                              "created_by"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", email_account\"option\"
            FROM hr_employee
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM hr_employee
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_person,
                 a.email_account,
                 a.hire_date,
                 b.description id_development_level,
                 c.description id_job_level,
                 d.description id_job_level_next,
                 e.description id_competence_a,
                 f.description id_competence_b,
                 g.description id_soft_skill_a,
                 h.description id_soft_skill_b,
                 a.is_key_talent,
                 a.has_successor,
                 a.has_talent,
                 a.resignation_risk,
                 a.performance,
                 a.performance_date,
                 a.learning_agility,
                 a.learning_agility_date,
                 a.cr,
                 a.successor_name,
                 a.mobility,
                 a.area_interest,
                 a.termination_date,
                 a.id_termination_reason,
                 a.created_date,
                 i.name created_by
            FROM hr_employee a,
                 hr_development_level b,
                 hr_job_level c,
                 hr_job_level d,
                 hr_competence e,
                 hr_competence f,
                 hr_soft_skill g,
                 hr_soft_skill h,
                 t_user i
           WHERE b.id = a.id_development_level
           AND c.id = a.id_job_level
           AND d.id = a.id_job_level_next
           AND e.id = a.id_competence_a
           AND f.id = a.id_competence_b
           AND g.id = a.id_soft_skill_a
           AND h.id = a.id_soft_skill_b
           AND i.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
   
   public static function profile(TfSession $tfs,$idManager){

     $q = "SELECT a.id id_employee,
                  a.id_person,
                  a.email_account,
                  b.id id_employee_position,
                  b.id_position id_position,
                  c.nin,CONCAT(c.first_name,' ',c.last_name) name,
                  coalesce(c.photo,'../asset/images/blank-user.jpg') photo,
                  e.name business_title,      
                 TIMESTAMPDIFF(YEAR,a.hire_date,DATE(NOW())) AS tenure_year,        
                 TIMESTAMPDIFF(MONTH,a.hire_date,DATE(NOW()) ) AS tenure_month,        
                 TIMESTAMPDIFF(DAY,a.hire_date,DATE(NOW()) ) AS tenure_day,        
                 TIMESTAMPDIFF(YEAR,b.hire_date,DATE(NOW())) AS in_role_year,        
                 TIMESTAMPDIFF(MONTH,b.hire_date,DATE(NOW()) ) AS in_role_month,        
                 TIMESTAMPDIFF(DAY,b.hire_date,DATE(NOW()) ) AS in_role_day, 
               --  COALESCE(a.cr,'NYA') cr,  
                 COALESCE(a.successor_name,'NYA') successor_name,     
                 COALESCE(l.description,'NYA') job_level_next, 
                 COALESCE(dl.description,'NYA') development_moment,       
                 dl.id development_level,        
                 COALESCE(ca.description,'NYA') competence_a,        
                 COALESCE(cb.description,'NYA') competence_b, 
                 COALESCE(oa.description,'NYA') soft_skill_a,        
                 COALESCE(ob.description,'NYA') soft_skill_b,        
                 CASE WHEN a.resignation_risk = 'N' THEN 'Ningun'
                      WHEN a.resignation_risk = 'L' THEN 'Bajo'
                      WHEN a.resignation_risk = 'M' THEN 'Medio'
                      WHEN a.resignation_risk = 'H' THEN 'Alto'
                      ELSE 'NYA' END resignation_risk,        
                 d.criticality criticality, 
                 cr.id id_criticality_level,
                 cr.name criticality_level,
                 a.learning_agility,   
                 lr.id id_learning_agility_level,
                 lr.name learning_agility_level,
                 a.learning_agility_date,
                 a.self_learning_agility,   
                 lrs.id id_self_learning_agility_level,
                 lrs.name self_learning_agility_level,
                 a.self_learning_agility_date,
                 a.performance,
                 pr.id id_performance_level,
                 pr.name performance_level,
                 a.self_performance,
                 prs.id id_self_performance_level,
                 prs.name self_performance_level,
                 d.leadership_bench,
                 a.is_key_talent           
            FROM hr_employee a 
               LEFT OUTER JOIN hr_job_level l ON l.id= a.id_job_level_next
                 LEFT OUTER JOIN hr_development_level dl ON dl.id=a.id_development_level
                 LEFT OUTER JOIN hr_competence ca ON ca.id=a.id_competence_a 
                 LEFT OUTER JOIN hr_competence cb ON cb.id=a.id_competence_b    
                 LEFT OUTER JOIN hr_soft_skill oa ON oa.id=a.id_soft_skill_a 
                 LEFT OUTER JOIN hr_soft_skill ob ON ob.id=a.id_soft_skill_b                
                 LEFT OUTER JOIN hr_employee_performance_range pr ON COALESCE(a.performance,0) BETWEEN pr.min AND pr.max
                 LEFT OUTER JOIN hr_employee_performance_range prs ON COALESCE(a.self_performance,0) BETWEEN prs.min AND prs.max
                 LEFT OUTER JOIN hr_employee_learning_agility_range lr ON COALESCE(a.learning_agility,0) BETWEEN lr.min AND lr.max     
                 LEFT OUTER JOIN hr_employee_learning_agility_range lrs ON COALESCE(a.self_learning_agility,0) BETWEEN lrs.min AND lrs.max,                  
                 hr_employee_position b,
                 gl_natural_person c,
                 hr_position d LEFT OUTER JOIN hr_position_criticality_range cr ON COALESCE(d.criticality,0) BETWEEN cr.min AND cr.max,
                 hr_business_title e,
                 hr_employee_manager m
           WHERE e.id=d.id_business_title              
             AND d.id=b.id_position              
             AND c.id=a.id_person              
             AND b.termination_date IS NULL              
             AND b.id_employee = a.id              
             AND a.termination_date IS NULL              
             AND a.id=m.id_employee
             AND m.active = 'Y'
             AND m.id_manager = ?
             ORDER BY c.first_name,c.last_name";

    $param = array($idManager);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

   public static function myProfile(TfSession $tfs,$idEmployee){

     $q = "SELECT a.id id_employee,
                  a.id_person,
                  a.email_account,
                  b.id id_employee_position,
                  b.id_position id_position,
                  c.nin,CONCAT(c.first_name,' ',c.last_name) name,
                  coalesce(c.photo,'../asset/images/blank-user.jpg') photo,
                  e.name business_title,      
                 TIMESTAMPDIFF(YEAR,a.hire_date,DATE(NOW())) AS tenure_year,        
                 TIMESTAMPDIFF(MONTH,a.hire_date,DATE(NOW()) ) AS tenure_month,        
                 TIMESTAMPDIFF(DAY,a.hire_date,DATE(NOW()) ) AS tenure_day,        
                 TIMESTAMPDIFF(YEAR,b.hire_date,DATE(NOW())) AS in_role_year,        
                 TIMESTAMPDIFF(MONTH,b.hire_date,DATE(NOW()) ) AS in_role_month,        
                 TIMESTAMPDIFF(DAY,b.hire_date,DATE(NOW()) ) AS in_role_day, 
               --  COALESCE(a.cr,'NYA') cr,  
                 COALESCE(a.successor_name,'NYA') successor_name,     
                 COALESCE(l.description,'NYA') job_level_next, 
                 COALESCE(dl.description,'NYA') development_moment,       
                 dl.id development_level,        
                 COALESCE(ca.description,'NYA') competence_a,        
                 COALESCE(cb.description,'NYA') competence_b, 
                 COALESCE(oa.description,'NYA') soft_skill_a,        
                 COALESCE(ob.description,'NYA') soft_skill_b,        
                 CASE WHEN a.resignation_risk = 'N' THEN 'Ningun'
                      WHEN a.resignation_risk = 'L' THEN 'Bajo'
                      WHEN a.resignation_risk = 'M' THEN 'Medio'
                      WHEN a.resignation_risk = 'H' THEN 'Alto'
                      ELSE 'NYA' END resignation_risk,        
                 d.criticality criticality, 
                 cr.id id_criticality_level,
                 cr.name criticality_level,
                 a.learning_agility,   
                 lr.id id_learning_agility_level,
                 lr.name learning_agility_level,
                 a.learning_agility_date,
                 a.self_learning_agility,   
                 lrs.id id_self_learning_agility_level,
                 lrs.name self_learning_agility_level,
                 a.self_learning_agility_date,
                 a.performance,
                 pr.id id_performance_level,
                 pr.name performance_level,
                 a.self_performance,
                 prs.id id_self_performance_level,
                 prs.name self_performance_level,
                 d.leadership_bench,
                 a.is_key_talent           
            FROM hr_employee a 
               LEFT OUTER JOIN hr_job_level l ON l.id= a.id_job_level_next
                 LEFT OUTER JOIN hr_development_level dl ON dl.id=a.id_development_level
                 LEFT OUTER JOIN hr_competence ca ON ca.id=a.id_competence_a 
                 LEFT OUTER JOIN hr_competence cb ON cb.id=a.id_competence_b    
                 LEFT OUTER JOIN hr_soft_skill oa ON oa.id=a.id_soft_skill_a 
                 LEFT OUTER JOIN hr_soft_skill ob ON ob.id=a.id_soft_skill_b                
                 LEFT OUTER JOIN hr_employee_performance_range pr ON COALESCE(a.performance,0) BETWEEN pr.min AND pr.max
                 LEFT OUTER JOIN hr_employee_performance_range prs ON COALESCE(a.self_performance,0) BETWEEN prs.min AND prs.max
                 LEFT OUTER JOIN hr_employee_learning_agility_range lr ON COALESCE(a.learning_agility,0) BETWEEN lr.min AND lr.max     
                 LEFT OUTER JOIN hr_employee_learning_agility_range lrs ON COALESCE(a.self_learning_agility,0) BETWEEN lrs.min AND lrs.max,                  
                 hr_employee_position b,
                 gl_natural_person c,
                 hr_position d LEFT OUTER JOIN hr_position_criticality_range cr ON COALESCE(d.criticality,0) BETWEEN cr.min AND cr.max,
                 hr_business_title e
           WHERE e.id=d.id_business_title              
             AND d.id=b.id_position              
             AND c.id=a.id_person              
             AND b.termination_date IS NULL              
             AND b.id_employee = a.id              
             AND a.termination_date IS NULL              
             AND a.id = ?
             ORDER BY c.first_name,c.last_name";

    $param = array($idEmployee);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

   public static function shortList(TfSession $tfs,$idManager){

     $q = "SELECT a.id_person,
                  coalesce(c.photo,'../asset/images/blank-user.jpg') photo,
                  a.id id_employee,
                  a.email_account,
                  b.id id_employee_position,
                  c.nin,CONCAT(c.first_name,' ',c.last_name) name,
                  a.is_key_talent,a.hire_date hire_date_org,
                  b.hire_date hire_date_position,
                  f.name cost_center,e.name business_title,
                  a.cr   
            FROM hr_employee a,
                 hr_employee_position b,
                 gl_natural_person c,
                 hr_position d,
                 hr_business_title e,
                 t_functional_area f,
                 hr_employee_manager m
           WHERE f.id=d.id_cost_center
             AND e.id=d.id_business_title
             AND d.id=b.id_position
             AND c.id=a.id_person
             AND b.termination_date IS NULL
             AND b.id_employee = a.id
             AND a.termination_date IS NULL
             AND a.id=m.id_employee
             AND m.active = 'Y'
             AND m.id_manager = ?
             ORDER BY c.first_name,c.last_name";

    $param = array($idManager);
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function currentQuarter(TfSession $tfs){
     $q = "SELECT QUARTER(NOW()) q";
     list($rs) = $tfs->executeQuery($q);

     return $rs["q"];
  }  


  public static function listByCoaching(TfSession $tfs){
    $q = "SELECT a.id,coalesce(c.photo,'../asset/images/blank-user.jpg') photo,
                 a.email_account,
                 CONCAT(c.first_name,' ',c.last_name) name,
                 e.name business_title
            FROM hr_employee a,
                 hr_employee_position b,
                 gl_natural_person c,
                 hr_position d,
                 hr_business_title e
           WHERE e.id=d.id_business_title
             AND d.id=b.id_position
             AND c.id=a.id_person
             AND b.termination_date IS NULL
             AND b.id_employee = a.id
             AND a.termination_date IS NULL
           ORDER BY c.first_name,c.last_name
           LIMIT 10";

    $rs = $tfs->executeQuery($q);

    return $rs;
  }

}
?>
