<?php
  class CmPostLike extends CmPostLikeBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_post"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM cm_post_like
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM cm_post_like
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_post,
                 c.name created_by,
                 a.created_date
            FROM cm_post_like a,
                 cm_post b,
                 t_user c
           WHERE b.id = a.id_post
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
