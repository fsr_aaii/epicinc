<?php
  class HrCoaching extends HrCoachingBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_employee"=>true,
                              "id_coaching"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM hr_coaching
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM hr_coaching
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.successor_name id_employee,
                 a.id_coaching,
                 a.active,
                 d.name created_by,
                 a.created_date
            FROM hr_coaching a,
                 hr_employee b,
                 gl_person c,
                 t_user d
           WHERE b.id = a.id_employee
           AND c.id = a.id_coaching
           AND d.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function listByEmployeeQuarter(TfSession $tfs,$idEmployee,$quarter){
    $q = "SELECT f.id,coalesce(c.photo,'../asset/images/blank-user.jpg') photo,
                 a.email_account,
                 CONCAT(c.first_name,' ',c.last_name) name,
                 e.name business_title
            FROM hr_employee a,
                 hr_employee_position b,
                 gl_natural_person c,
                 hr_position d,
                 hr_business_title e,
                 hr_coaching f
           WHERE e.id=d.id_business_title
             AND d.id=b.id_position
             AND c.id=a.id_person
             AND b.termination_date IS NULL
             AND b.id_employee = a.id
             AND a.termination_date IS NULL
             AND a.id = f.id_coaching
             AND QUARTER(f.created_date)=?
             AND f.id_employee = ?
           ORDER BY c.first_name,c.last_name";

    $param = array($quarter,$idEmployee);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
