<?php
  class HrPosition extends HrPositionBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_cost_center"=>true,
                              "id_business_title"=>true,
                              "criticality"=>true,
                              "leadership_bench"=>true,
                              "evaluation_date"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", id\"option\"
            FROM hr_position
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM hr_position
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_cost_center,
                 a.id_business_title,
                 a.criticality,
                 a.leadership_bench,
                 a.evaluation_date,
                 b.name created_by,
                 a.created_date
            FROM hr_position a,
                 t_user b
           WHERE b.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
