<?php
  class HrPositionEvaluation extends HrPositionEvaluationBase {
  
  private $criticality;
  private $leadership;
  private $blanks;

  public function setCriticality($value){
  $this->criticality=$value;
  }
  public function getCriticality(){
  return $this->criticality;
  }
  public function setLeadership($value){
  $this->leadership=$value;
  }
  public function getLeadership(){
  return $this->leadership;
  }
  public function setBlanks($value){
  $this->blanks=$value;
  }
  public function getBlanks(){
  return $this->blanks;
  }

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_position"=>true,
                              "date"=>true,
                              "created_by"=>true,
                              "created_date"=>true,
                              "id_employee"=>true);

  }

  public function create(){
    parent::create();
    
    $q = "INSERT INTO hr_position_evaluation_criteria
          (id_position_evaluation,id_evaluation_criteria,date,created_by,created_date)
          SELECT d.id id_position_evaluation,c.id id_evaluation_criteria,d.date,d.created_by,d.created_date
            FROM hr_evaluation_type a,
                 hr_evaluation_group b,
                 hr_evaluation_criteria c,
                 hr_position_evaluation d
           WHERE d.id=?
             AND b.active='Y'
             AND c.id_evaluation_group=b.id
             AND b.active='Y'
             AND b.id_evaluation_type=a.id
             AND a.active='Y'
             AND a.id IN (1,2)
           ORDER BY a.id,b.id,c.id ";

      $param = array($this->id);
      $this->tfs->execute($q,$param);

  } 

  public function update(){
    parent::update();
    $this->resetInfoCriteria();
    if ($this->valid and $this->getBlanks()==0){ 
      $q = "UPDATE hr_position
               SET criticality=?,
                   leadership_bench=?,
                   evaluation_date=?
             WHERE id = ?
               AND (evaluation_date IS NULL OR evaluation_date <=?)";

      $param = array($this->criticality,$this->leadership,
                     $this->date,$this->id_position,$this->date);
       
      print_r($param);   
      $this->tfs->execute($q,$param);

      /*$q = "UPDATE hr_employee e
             INNER JOIN  hr_employee_position f 
                     ON f.id_employee = e.id 
                    AND f.termination_date IS NULL
                    AND f.id_position=?
               SET e.criticality=?,
                   e.criticality_date=?
             WHERE e.id = ?
               AND (e.criticality_date IS NULL OR e.criticality_date <=?)";

      $param = array($this->id_position,$this->criticality,$this->date,
                     $this->id_employee,$this->date);

      $this->tfs->execute($q,$param);    */

    }  
  }

   public function delete(){
    
    $q = "DELETE FROM hr_position_evaluation_criteria 
           WHERE id_position_evaluation = ?";

      $param = array($this->id);
      $this->tfs->execute($q,$param);         

    parent::delete();
    if ($this->valid){ 
      $q = "UPDATE hr_position j 
             INNER JOIN  (SELECT a.id_position,a.date,
                      SUM(CASE WHEN d.id_evaluation_type=1 THEN b.value ELSE 0 END) criticality,
                                SUM(CASE WHEN d.id_evaluation_type=2 THEN b.value ELSE 0 END) leadership_bench
                           FROM hr_position_evaluation a,
                                hr_position_evaluation_criteria b,
                                hr_evaluation_criteria c,
                                hr_evaluation_group d
                          WHERE d.id=c.id_evaluation_group
                            AND c.id=b.id_evaluation_criteria
                            AND b.id_position_evaluation=a.id
                            AND a.id=(SELECT MAX(id) id
                                        FROM hr_position_evaluation
                                       WHERE id_position=?
                                         AND id!=?)
                          GROUP BY a.id_position,a.date) e ON j.id=e.id_position
            SET j.criticality = e.criticality,
                j.leadership_bench = e.leadership_bench,
                j.evaluation_date = e.date";
      $param = array($this->id_position,$this->id);
      $this->tfs->execute($q,$param);        

      $q = "UPDATE hr_employee j 
             INNER JOIN  (SELECT a.id_employee,a.date,
                      SUM(CASE WHEN d.id_evaluation_type=1 THEN b.value ELSE 0 END) criticality,
                                SUM(CASE WHEN d.id_evaluation_type=2 THEN b.value ELSE 0 END) leadership_bench
                           FROM hr_position_evaluation a,
                                hr_position_evaluation_criteria b,
                                hr_evaluation_criteria c,
                                hr_evaluation_group d,
                                hr_employee_position f
                          WHERE f.termination_date IS NULL
                            AND f.id_position=a.id_position
                            AND f.id_employee=a.id_employee
                            AND d.id=c.id_evaluation_group
                            AND c.id=b.id_evaluation_criteria
                            AND b.id_position_evaluation=a.id
                            AND a.id=(SELECT MAX(id) id
                                        FROM hr_position_evaluation
                                       WHERE id_position=?
                                         AND id!=?)
                          GROUP BY a.id_employee,a.date) e ON j.id=e.id_employee          
            SET j.criticality = e.criticality,
                j.criticality_date = e.date";
      $param = array($this->id_position,$this->id);
      $this->tfs->execute($q,$param);         

    }  
  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM hr_position_evaluation
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM hr_position_evaluation
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_position,
                 a.date,
                 b.name created_by,
                 a.created_date,
                 a.id_employee
            FROM hr_position_evaluation a,
                 t_user b
           WHERE b.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  
  public static function listByPosition(TfSession $tfs,$idPosition){
    $q = "SELECT x.id,x.created_date,x.criticality,y.name criticality_level, x.leadership,z.name leadership_level   
            FROM (SELECT a.id,
                           a.date,
                           a.created_by,
                           a.created_date,
                           SUM(CASE WHEN d.id_evaluation_type=1 THEN b.value ELSE 0 END) criticality,
                           SUM(CASE WHEN d.id_evaluation_type=2 THEN b.value ELSE 0 END) leadership
                      FROM hr_position_evaluation a,
                           hr_position_evaluation_criteria b,
                           hr_evaluation_criteria c,
                           hr_evaluation_group d
                     WHERE d.id=c.id_evaluation_group
                       AND c.id=b.id_evaluation_criteria
                       AND b.id_position_evaluation=a.id
                       AND a.id_position = ?
                     GROUP BY a.id,a.date,a.created_by,a.created_date
                     ORDER BY a.created_date DESC) x LEFT JOIN  hr_position_criticality_range y ON  x.criticality BETWEEN y.min AND y.max
                                                     LEFT JOIN  hr_position_leadership_range z ON  x.leadership BETWEEN z.min AND z.max";
           
    $param = array($idPosition);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function listByPositionQuarter(TfSession $tfs,$idPosition,$quarter){
    $q = "SELECT x.id,x.created_date,x.criticality,y.name criticality_level,y.level id_criticality_level, 
                x.leadership,z.name leadership_level,z.level id_leadership_level   
            FROM (SELECT a.id,
                           a.date,
                           a.created_by,
                           a.created_date,
                           SUM(CASE WHEN d.id_evaluation_type=1 THEN b.value ELSE 0 END) criticality,
                           SUM(CASE WHEN d.id_evaluation_type=2 THEN b.value ELSE 0 END) leadership
                      FROM hr_position_evaluation a,
                           hr_position_evaluation_criteria b,
                           hr_evaluation_criteria c,
                           hr_evaluation_group d
                     WHERE d.id=c.id_evaluation_group
                       AND c.id=b.id_evaluation_criteria
                       AND b.id_position_evaluation=a.id
                       AND QUARTER(a.created_date)=?
                       AND a.id_position = ?
                     GROUP BY a.id,a.date,a.created_by,a.created_date) x LEFT JOIN  hr_position_criticality_range y ON  x.criticality BETWEEN y.min AND y.max
                                                     LEFT JOIN  hr_position_leadership_range z ON  x.leadership BETWEEN z.min AND z.max
          ORDER BY x.created_date ASC";
           
    $param = array($quarter,$idPosition);   
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public function resetInfoCriteria(){
    $q = "SELECT SUM(CASE WHEN d.id_evaluation_type=1 THEN b.value ELSE 0 END) criticality,
                 SUM(CASE WHEN d.id_evaluation_type=2 THEN b.value ELSE 0 END) leadership,
                 SUM(CASE WHEN b.value IS NULL THEN 1 ELSE 0 END) blank
            FROM hr_position_evaluation a,
                 hr_position_evaluation_criteria b,
                 hr_evaluation_criteria c,
                 hr_evaluation_group d
           WHERE d.id=c.id_evaluation_group
             AND c.id=b.id_evaluation_criteria
             AND b.id_position_evaluation=a.id
             AND a.id=?";
           
    $param = array($this->id);       
    list($rs) = $this->tfs->executeQuery($q,$param);

    $this->criticality=$rs["criticality"];
    $this->leadership=$rs["leadership"];
    $this->blanks=$rs["blanks"];
  }

  public static function criteria(TfSession $tfs){
    $q = "SELECT c.id,a.name 'type',b.name 'group',c.description criteria  
            FROM hr_evaluation_type a,
                 hr_evaluation_group b,
                 hr_evaluation_criteria c
           WHERE b.active='Y'
             AND c.id_evaluation_group=b.id
             AND b.active='Y'
             AND b.id_evaluation_type=a.id
             AND a.active='Y'
             AND a.id IN (1,2)
           ORDER BY a.id,b.id,c.id ";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }   

  public function getBusinessTitleDesc(){

    $q = "SELECT a.name description
            FROM hr_business_title a,
                 hr_position b
           WHERE a.id = b.id_business_title
             AND b.id=?";
    $param = array($this->id_position);
    list($rs) = $this->tfs->executeQuery($q,$param);

    return $rs["description"];
  }
}
?>
