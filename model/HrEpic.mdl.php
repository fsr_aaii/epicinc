<?php
  class HrEpic extends HrEpicBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_employee"=>true,
                              "id_position"=>true,
                              "description"=>true,
                              "estimated_start_date"=>true,
                              "start_date"=>true,
                              "completion_estimated_date"=>true,
                              "completion_date"=>true,
                              "id_epic_priority"=>true,
                              "id_epic_status"=>true,
                              "status_date"=>true,
                              "progress"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",description \"option\"
            FROM hr_epic
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT description description
            FROM hr_epic
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }

  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.successor_name id_employee,
                 a.id_position,
                 a.description,
                 a.estimated_start_date,
                 a.start_date,
                 a.completion_estimated_date,
                 a.completion_date,
                 d.name id_epic_priority,
                 e.name id_epic_status,
                 a.status_date,
                 a.progress,
                 a.active,
                 f.name created_by,
                 a.created_date
            FROM hr_epic a,
                 hr_employee b,
                 hr_position c,
                 hr_epic_priority d,
                 hr_epic_status e,
                 t_user f
           WHERE b.id = a.id_employee
           AND c.id = a.id_position
           AND d.id = a.id_epic_priority
           AND e.id = a.id_epic_status
           AND f.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

   public static function listByEmployeeQuarter(TfSession $tfs,$idEmployee,$quarter){
    
    $q = "SELECT a.id,
         a.description,
                 a.completion_estimated_date,
                 a.id_epic_priority,
                 a.id_epic_status,
                 d.name priority,
                 e.name status,
                 a.status_date,
                 a.progress,
                 count(*) activities
            FROM hr_epic a LEFT JOIN hr_epic_activity b ON b.id_epic = a.id ,
                 hr_epic_priority d,
                 hr_epic_status e
           WHERE d.id = a.id_epic_priority
           AND e.id = a.id_epic_status
           AND QUARTER(a.created_date)=?
           AND a.id_employee = ?
           GROUP BY a.id,a.description,
                 a.completion_estimated_date,
                 d.name,e.name,a.status_date,
                 a.progress";
    $param = array($quarter,$idEmployee);
       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function progressByEmployeeQuarter(TfSession $tfs,$idEmployee,$quarter){
    
    $q = "SELECT SUM(a.progress)/count(*) progress
            FROM hr_epic a 
           WHERE  QUARTER(a.created_date)=?
           AND a.id_employee = ?";
    
    $param = array($quarter,$idEmployee);
       
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["progress"];
  }

  public function setGlobalProgress(){
    
    $q = "SELECT sum(coalesce(b.progress,0)) /count(*) progress
            FROM hr_epic a LEFT JOIN hr_epic_activity b ON b.id_epic = a.id AND b.id_epic_status !=5
           WHERE a.id=?";

    $param = array($this->id);
       
    list($rs) = $this->tfs->executeQuery($q,$param);

    $this->progress = $rs["progress"];
  }

  public function setGlobalStatus(){
    
    $q = "SELECT sum(coalesce(b.progress,0)) /count(*) progress
            FROM hr_epic a LEFT JOIN hr_epic_activity b ON b.id_epic = a.id 
           WHERE a.id=?";

    $param = array($this->id);
       
    list($rs) = $this->tfs->executeQuery($q,$param);

    $this->progress = $rs["progress"];
  }
}
?>
