<?php
  class CmPostTag extends CmPostTagBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_post"=>true,
                              "id_tag"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM cm_post_tag
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM cm_post_tag
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_post,
                 c.name id_tag,
                 d.name created_by,
                 a.created_date
            FROM cm_post_tag a,
                 cm_post b,
                 cm_tag c,
                 t_user d
           WHERE b.id = a.id_post
           AND c.id = a.id_tag
           AND d.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
