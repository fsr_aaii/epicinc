<?php
  class HrEmployeeEvaluation extends HrEmployeeEvaluationBase {
  
  private $result;
  private $blanks; 

  public function setResult($value){
  $this->result=$value;
  }
  public function getResult(){
  return $this->result;
  }
  public function setBlanks($value){
  $this->blanks=$value;
  }
  public function getBlanks(){
  return $this->blanks;
  }

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_employee"=>true,
                              "id_position"=>true,
                              "date"=>true,
                              "id_evaluation_type"=>true,
                              "evaluator_type"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM hr_employee_evaluation
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public function create(){
    parent::create();
    
    $q = "INSERT INTO hr_employee_evaluation_criteria
          (id_employee_evaluation,id_evaluation_criteria,date,created_by,created_date)
          SELECT d.id id_employee_evaluation,c.id id_evaluation_criteria,d.date,d.created_by,d.created_date
            FROM hr_evaluation_type a,
                 hr_evaluation_group b,
                 hr_evaluation_criteria c,
                 hr_employee_evaluation d
           WHERE d.id=?
             AND b.active='Y'
             AND c.id_evaluation_group=b.id
             AND b.active='Y'
             AND b.id_evaluation_type=a.id
             AND a.active='Y'
             AND a.id =?
           ORDER BY a.id,b.id,c.id ";

      $param = array($this->id,$this->id_evaluation_type);
      $this->tfs->execute($q,$param);

  } 

  public function update(){
    parent::update();
    $this->resetInfoCriteria();
    if ($this->valid and $this->blanks==0){ 
      if ($this->id_evaluation_type==3){
        if ($this->evaluator_type=='M'){
          $q = "UPDATE hr_employee e INNER JOIN  hr_employee_position f 
                                                ON f.id_employee = e.id 
                                               AND f.termination_date IS NULL
                                               AND f.id_position=?
                 SET e.learning_agility=?,
                     e.learning_agility_date=?
               WHERE e.id = ?
                 AND (e.learning_agility_date IS NULL OR e.learning_agility_date <=?)"; 
        }else{
          $q = "UPDATE hr_employee e INNER JOIN  hr_employee_position f 
                                                  ON f.id_employee = e.id 
                                                 AND f.termination_date IS NULL
                                                 AND f.id_position=?
                   SET e.self_learning_agility=?,
                       e.self_learning_agility_date=?
                 WHERE e.id = ?
                   AND (e.learning_agility_date IS NULL OR e.learning_agility_date <=?)";
        }           
      }elseif ($this->id_evaluation_type==4){
        if ($this->evaluator_type=='M'){
          $q = "UPDATE hr_employee e INNER JOIN  hr_employee_position f 
                                                  ON f.id_employee = e.id 
                                                 AND f.termination_date IS NULL
                                                 AND f.id_position=?
                   SET e.performance=?,
                       e.performance_date=?
                 WHERE e.id = ?
                   AND (e.performance_date IS NULL OR e.performance_date <=?)";
        }else{
          $q = "UPDATE hr_employee e INNER JOIN  hr_employee_position f 
                                                  ON f.id_employee = e.id 
                                                 AND f.termination_date IS NULL
                                                 AND f.id_position=?
                   SET e.self_performance=?,
                       e.self_performance_date=?
                 WHERE e.id = ?
                   AND (e.performance_date IS NULL OR e.performance_date <=?)";
        }           
      }          

      $param = array($this->id_position,$this->result,$this->date,$this->id_employee,$this->date);

      $this->tfs->execute($q,$param);         
    }  
  } 

  public function delete(){
    
    $q = "DELETE FROM hr_employee_evaluation_criteria 
           WHERE id_employee_evaluation = ?";

      $param = array($this->id);
      $this->tfs->execute($q,$param);         

    parent::delete();
    if ($this->valid){ 
      if ($this->id_evaluation_type==3){
        if ($this->evaluator_type=='M'){
          $q = "UPDATE hr_employee j 
                   LEFT OUTER JOIN (SELECT a.id_employee,a.date evaluation_date,SUM(coalesce(b.value,0)) result
                                 FROM hr_employee_evaluation a,
                                      hr_employee_evaluation_criteria b,
                                      hr_evaluation_criteria c,
                                      hr_evaluation_group d
                                WHERE d.id=c.id_evaluation_group
                                  AND c.id=b.id_evaluation_criteria
                                  AND b.id_employee_evaluation=a.id
                                  AND a.id=(SELECT MAX(id) id
                                              FROM hr_employee_evaluation
                                             WHERE id_evaluation_type = ?
                                               AND id_employee=?
                                               AND evaluator_type='M'
                                               AND id!=?)
                                GROUP BY a.id_employee,a.date) e ON j.id=e.id_employee
                  SET j.learning_agility=e.result,
                      j.learning_agility_date=e.evaluation_date
              WHERE j.id=?";
        }else{
          $q = "UPDATE hr_employee j 
                 LEFT OUTER JOIN (SELECT a.id_employee,a.date evaluation_date,SUM(coalesce(b.value,0)) result
                               FROM hr_employee_evaluation a,
                                    hr_employee_evaluation_criteria b,
                                    hr_evaluation_criteria c,
                                    hr_evaluation_group d
                              WHERE d.id=c.id_evaluation_group
                                AND c.id=b.id_evaluation_criteria
                                AND b.id_employee_evaluation=a.id
                                AND a.id=(SELECT MAX(id) id
                                            FROM hr_employee_evaluation
                                           WHERE id_evaluation_type = ?
                                             AND id_employee=?
                                             AND evaluator_type='E'
                                             AND id!=?)
                              GROUP BY a.id_employee,a.date) e ON j.id=e.id_employee
                SET j.self_learning_agility=e.result,
                    j.self_learning_agility_date=e.evaluation_date
            WHERE j.id=?";
        }  
      } elseif ($this->id_evaluation_type==4){
        if ($this->evaluator_type=='M'){
          $q = "UPDATE hr_employee j 
               LEFT OUTER JOIN (SELECT a.id_employee,a.date evaluation_date,SUM(coalesce(b.value,0)) result
                                  FROM hr_employee_evaluation a,
                                  hr_employee_evaluation_criteria b,
                                  hr_evaluation_criteria c,
                                  hr_evaluation_group d
                            WHERE d.id=c.id_evaluation_group
                              AND c.id=b.id_evaluation_criteria
                              AND b.id_employee_evaluation=a.id
                              AND a.id=(SELECT MAX(id) id
                                          FROM hr_employee_evaluation
                                         WHERE id_evaluation_type = ?
                                           AND id_employee=?
                                           AND evaluator_type='M'
                                           AND id!=?)
                            GROUP BY a.id_employee,a.date) e ON j.id=e.id_employee
              SET j.performance=e.result,
                  j.performance_date=e.evaluation_date
              WHERE j.id=?";
        }else{
          $q = "UPDATE hr_employee j 
               LEFT OUTER JOIN (SELECT a.id_employee,a.date evaluation_date,SUM(coalesce(b.value,0)) result
                                  FROM hr_employee_evaluation a,
                                  hr_employee_evaluation_criteria b,
                                  hr_evaluation_criteria c,
                                  hr_evaluation_group d
                            WHERE d.id=c.id_evaluation_group
                              AND c.id=b.id_evaluation_criteria
                              AND b.id_employee_evaluation=a.id
                              AND a.id=(SELECT MAX(id) id
                                          FROM hr_employee_evaluation
                                         WHERE id_evaluation_type = ?
                                           AND id_employee=?
                                           AND evaluator_type='E'
                                           AND id!=?)
                            GROUP BY a.id_employee,a.date) e ON j.id=e.id_employee
              SET j.self_performance=e.result,
                  j.self_performance_date=e.evaluation_date
              WHERE j.id=?";
        }    
      }         
      $param = array($this->id_evaluation_type,$this->id_employee,$this->id,$this->id_employee);
      $this->tfs->execute($q,$param);         
    }  
  }

  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM hr_employee_evaluation
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_employee,
                 a.id_position,
                 a.date,
                 a.id_evaluation_type,
                 a.evaluator_type,
                 b.name created_by,
                 a.created_date
            FROM hr_employee_evaluation a,
                 t_user b
           WHERE b.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  
  public static function listByEmployee(TfSession $tfs,$idEvaluationType,$idEmployee){
    $q = "SELECT a.id,
                 a.date,
                 a.created_by,
                 a.created_date,
                 SUM(coalesce(b.value,0)) result,
                 SUM(CASE WHEN b.value IS NULL THEN 1 ELSE 0 END) blank
            FROM hr_employee_evaluation a,
                 hr_employee_evaluation_criteria b,
                 hr_evaluation_criteria c,
                 hr_evaluation_group d
           WHERE d.id_evaluation_type=?
             AND d.id=c.id_evaluation_group
             AND c.id=b.id_evaluation_criteria
             AND b.id_employee_evaluation=a.id
             AND a.id_employee=?
           GROUP BY a.id,a.date,a.created_by,a.created_date
           ORDER BY a.date DESC";
           
    $param = array($idEvaluationType,$idEmployee);
       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function listByEmployeeQuarter(TfSession $tfs,$idEvaluationType,$idEmployee,$evaluatorType,$quarter){
    if ($idEvaluationType==3){
      $q = "SELECT x.id,x.date,x.created_by,x.created_date,x.result,y.name level,y.level id_level,x.blank
              FROM (SELECT a.id,
                           a.date,
                           a.created_by,
                           a.created_date,
                           SUM(coalesce(b.value,0)) result,
                           SUM(CASE WHEN b.value IS NULL THEN 1 ELSE 0 END) blank
                      FROM hr_employee_evaluation a,
                           hr_employee_evaluation_criteria b,
                           hr_evaluation_criteria c,
                           hr_evaluation_group d
                     WHERE d.id_evaluation_type=?
                       AND d.id=c.id_evaluation_group
                       AND c.id=b.id_evaluation_criteria
                       AND b.id_employee_evaluation=a.id
                       AND QUARTER(a.created_date)=?
                       AND a.evaluator_type=?
                       AND a.id_employee=?
                     GROUP BY a.id,a.date,a.created_by,a.created_date) x LEFT JOIN  hr_employee_learning_agility_range y ON  x.result BETWEEN y.min AND y.max
              ORDER BY x.created_date DESC";
    }elseif ($idEvaluationType==4) {
      $q = "SELECT x.id,x.date,x.created_by,x.created_date,x.result,y.name level,y.level id_level,x.blank
              FROM (SELECT a.id,
                           a.date,
                           a.created_by,
                           a.created_date,
                           SUM(coalesce(b.value,0)) result,
                           SUM(CASE WHEN b.value IS NULL THEN 1 ELSE 0 END) blank
                      FROM hr_employee_evaluation a,
                           hr_employee_evaluation_criteria b,
                           hr_evaluation_criteria c,
                           hr_evaluation_group d
                     WHERE d.id_evaluation_type=?
                       AND d.id=c.id_evaluation_group
                       AND c.id=b.id_evaluation_criteria
                       AND b.id_employee_evaluation=a.id
                       AND QUARTER(a.created_date)=?
                       AND a.evaluator_type=?
                       AND a.id_employee=?
                     GROUP BY a.id,a.date,a.created_by,a.created_date) x LEFT JOIN  hr_employee_performance_range y ON  x.result BETWEEN y.min AND y.max
              ORDER BY x.created_date DESC";
    }          
           
    $param = array($idEvaluationType,$quarter,$evaluatorType,$idEmployee);
       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
  
  public function resetInfoCriteria(){
    $q = "SELECT SUM(coalesce(b.value,0)) result,
                 SUM(CASE WHEN b.value IS NULL THEN 1 ELSE 0 END) blank
            FROM hr_employee_evaluation a,
                 hr_employee_evaluation_criteria b,
                 hr_evaluation_criteria c,
                 hr_evaluation_group d
           WHERE d.id_evaluation_type=?
             AND d.id=c.id_evaluation_group
             AND c.id=b.id_evaluation_criteria
             AND b.id_employee_evaluation=a.id
             AND a.id=?";
           
    $param = array($this->id_evaluation_type,$this->id);       
    list($rs) = $this->tfs->executeQuery($q,$param);

    $this->result=$rs["result"];
    $this->blanks=$rs["blanks"];
  }

  public function getEmployeeName(){

    $q = "SELECT CONCAT(b.first_name,' ',b.last_name) description
            FROM hr_employee a,
                 gl_natural_person b
          WHERE b.id=a.id_person
             AND a.id=?";
    $param = array($this->id_employee);
    list($rs) = $this->tfs->executeQuery($q,$param);

    return $rs["description"];
  }

   public static function groupDetails(TfSession $tfs,$idEmployee,$date,$evaluatorType){
    $q = "SELECT d.name,SUM(b.value) total,COUNT(coalesce(b.value,0)) qty
            FROM hr_employee_evaluation a,
                 hr_employee_evaluation_criteria b,
                 hr_evaluation_criteria c,
                 hr_evaluation_group d
          WHERE d.id=c.id_evaluation_group
            AND c.id=b.id_evaluation_criteria
            AND b.value IS NOT NULL
            AND b.id_employee_evaluation = a.id
            AND a.id_evaluation_type=3
            AND a.evaluator_type = ?
            AND a.date=?
            AND a.id_employee=?
            GROUP BY d.name";
           
    $param = array($evaluatorType,$date,$idEmployee);

    $rs = $tfs->executeQuery($q,$param);
    return $rs;

  }
}
?>
