<?php
  class CmPostComment extends CmPostCommentBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_post"=>true,
                              "comment"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM cm_post_comment
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM cm_post_comment
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_post,
                 a.comment,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM cm_post_comment a,
                 cm_post b,
                 t_user c
           WHERE b.id = a.id_post
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function listByPost(TfSession $tfs,$idPost){
    $q = "SELECT a.comment,
                 CONCAT(d.first_name,' ',d.last_name) created_by,
                 a.created_date
            FROM cm_post_comment a,
                 hr_employee c,
                 gl_natural_person d
           WHERE d.id = c.id_person
             AND c.id = a.created_by
             AND a.id_post = ?
             AND a.active = 'Y'
             AND a.created_by !=? 
             ORDER BY  a.created_date  DESC";
   
    $param = array($idPost,$tfs->getUserId());
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function mineByPost(TfSession $tfs,$idPost){
    $q = "SELECT a.id,a.comment,
                 CONCAT(d.first_name,' ',d.last_name) created_by,
                 a.created_date
            FROM cm_post_comment a,
                 hr_employee c,
                 gl_natural_person d
           WHERE d.id = c.id_person
             AND c.id = a.created_by
             AND a.id_post = ?
             AND a.active = 'Y'
             AND a.created_by =? 
             ORDER BY  a.created_date  DESC";
   
    $param = array($idPost,$tfs->getUserId());
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
