<?php
  class TUser extends TUserBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "login"=>true,
                              "photo"=>true,
                              "active"=>true,
                              "password"=>true,
                              "password_date"=>true,
                              "email_account"=>true,
                              "name"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",name \"option\"
            FROM t_user
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT name description
            FROM t_user
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.login,
                 a.photo,
                 a.active,
                 a.password,
                 a.password_date,
                 a.email_account,
                 a.name,
                 b.name created_by,
                 a.created_date
            FROM t_user a,
                 t_user b
           WHERE b.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
