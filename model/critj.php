<?php
$html2.='                 <!--begin accordion-criticality-->
                                      <div id="accordion-criticality-'.$row["id_employee"].'" class="col-lg-12 container">
                                        <div class="my-epic-profile card">
                                          <!--begin accordion-criticality-header-->
                                          <div class="col-12 card-header" id="heading-criticality-'.$row["id_employee"].'">
                                            <div class="col-11 container"> 
                                              <a href="#" class="section '.$quarter[3]["accordion"].' collapsed" data-toggle="collapse" data-target="#collapse-criticality-'.$row["id_employee"].'" aria-expanded="true" aria-controls="collapse-criticality-'.$row["id_employee"].'" data-form-action="form-action-criticality-'.$row["id_employee"].'"> Evalua esta posicion</a>
                                            </div>
                                            <div class="col-1 container text-right">';
            if ($quarter[3]["accordion"]=='active'){                                
              $html2.='                       <a id="form-action-criticality-'.$row["id_employee"].'" name="form-action-criticality-'.$row["id_employee"].'" class="btn btn-epic-2 rounded-circle" alt="New" title="New" data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPositionEvaluation" data-tf-data="'.$tfResponse->encrypt($tfData).'" data-tf-action="AI" onclick="TfRequest.do(this);" style="display: none;">            
                                                 <i class="bx bx-plus"></i>
                                                </a>';
            } 
            $html2.='                      </div>
                                          </div>
                                          <!--end accordion-criticality-header-->
                                          <!--begin accordion-criticality-content-->
                                          <div id="collapse-criticality-'.$row["id_employee"].'" class="collapsed collapse" aria-labelledby="heading-criticality-'.$row["id_employee"].'" data-parent="#accordion-criticality-'.$row["id_employee"].'">';  
              if (count($criticalityEvaluationList)==0){
                 $html2.='                 No existen evaluaciones en este trimestre';
              }else{
                $html2.='                   <!--Begin row epic-->
                                            <div class="row epic">
                                              <div class="col-lg-3 col-container text-left">
                                              </div>
                                              <div class="col-lg-4 col-container epic-text-sm text-left">
                                                Criticality
                                              </div> 
                                              <div class="col-lg-4 col-container epic-text-sm text-left">
                                                Leadership Bank
                                              </div>                              
                                              <div class="col-lg-1 col-container epic-text-sm text-right">
                                              </div>';     
                $i=0;
                foreach ($criticalityEvaluationList as $c){
                $i++;
                $tfData = array();
                $tfData["hr_position_evaluation_id"] = $c["id"];
                $html2.='           <div class="col-lg-3 col-container text-left">
                                      <div class="h7 m-0">#'.$i.' Test</div>
                                      <div class="text-muted h6">'.$c["created_date"].'</div>
                                    </div>
                                    <div class="col-lg-4 col-container text-left">
                                      <div class="epic-bottom">
                                        <span class="badge badge-pill badge-secondary">'.$c["criticality"].'</span>
                                        <span class="badge badge-pill c3-'.$c["id_criticality_level"].'">'.$c["criticality_level"].'</span>
                                      </div>
                                    </div>                  
                                    <div class="col-lg-4 col-container text-left">
                                      <div class="epic-bottom">
                                        <span class="badge badge-pill badge-secondary">'.$c["leadership"].'</span>
                                        <span class="badge badge-pill c3-'.$c["id_leadership_level"].'">'.$c["leadership_level"].'</span>
                                      </div>
                                    </div>             
                                    <div class="col-lg-1 col-container text-right">';
                  if ($quarter[3]["accordion"]=='active'){                                
                    $html2.='              <a data-tf-task-id="'.$tfs->getTaskId().'" data-tf-controller="HrPositionEvaluation" data-tf-action="AE" data-tf-data="'.$tfResponse->encrypt($tfData).' "onclick="TfRequest.do(this);">
                           <span class="far fa-pencil"></span>
                         </a>';
                  }         
                  $html2.='               </div> 
                                </div>
                                <!--End row epic-->';
                }                

              }                           
              $html2.='                   </div>
                                          <!--end accordion-criticality-content-->
                                        </div>
                                      </div>
                                      <!--end accordion-criticality-->';           
                                      ?>