<?php
  class CmPost extends CmPostBase {

  protected $tags;  

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_category"=>true,
                              "image"=>true,
                              "title"=>true,
                              "content"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true,
                              "tags"=>true);

  }

  protected function dbPopulate($id){ 
    parent::dbPopulate($id);

     $q="SELECT GROUP_CONCAT(a.id_tag) tags
          FROM cm_post_tag a 
         WHERE a.id_post=?";

    $param = array($this->id);
    list($rs) = $this->tfs->executeQuery($q,$param);
    $this->tags=$rs["tags"];
  }

  protected function uiPopulate($request){ 

    parent::uiPopulate($request);
    $this->tags=$request->get("cm_post_tags");

  }

   public function setValidations(){
    $this->validation["tags"]=array("type"=>"string",
                                  "value"=>$this->tags,
                                  "length"=>2000,
                                  "required"=>true);
    parent::setValidations(); 
  }

  public function setTags($value){
    $this->tags=$value;
  }
  public function getTags(){
    return $this->tags;
  }
  

  public function create(){
    parent::create();
    if($this->valid){
      
      $t=$this->tags;
      if (!isset($t) OR $t==''){
        $t="'0'";
      }else{  
        $t="'".str_replace (",","','",$t)."'";
      } 

       $q = "INSERT INTO cm_post_tag(
                               id_post,
                               id_tag,
                               created_by,
                               created_date)
            SELECT ?,i.id,?,? FROM cm_tag i WHERE i.id IN ({$t})";

      $param = array($this->id==''?NULL:$this->id,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date);
      $this->tfs->execute($q,$param);
    }
  }  
  
  public function update(){
    parent::update();
    if($this->valid){
      $t=$this->tags;
      if (!isset($t) OR $t==''){
        $t="'0'";
      }else{  
        $t="'".str_replace (",","','",$t)."'";
      } 
      
      $q = "DELETE FROM cm_post_tag
               WHERE id_tag NOT IN ({$t})
                 AND id_post = ?";
       
       $param = array($this->id);
       $this->tfs->execute($q,$param); 

       $q = "INSERT INTO cm_post_tag(
                               id_post,
                               id_tag,
                               created_by,
                               created_date)
            SELECT ?,i.id,?,? 
              FROM cm_tag i 
             WHERE i.id IN ({$t})
               AND NOT EXISTS (SELECT 1
                                 FROM cm_post_tag d
                      WHERE d.id_tag = i.id
                                  AND d.id_post = ?) ";

      $param = array($this->id==''?NULL:$this->id,
                     $this->created_by==''?NULL:$this->created_by,
                     $this->created_date==''?NULL:$this->created_date,
                     $this->id==''?NULL:$this->id);
      $this->tfs->execute($q,$param);      

      $this->objMsg=array();
      $this->objMsg[]="This record has been updated";   
      
    }
  }  
  public function delete(){
    $q = "DELETE FROM cm_post_tag
           WHERE id_post = ?";

    $param = array($this->id);
    $this->tfs->execute($q,$param); 
    parent::delete();
  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM cm_post
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM cm_post
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 d.name category,
                 COUNT(DISTINCT b.id) likes,
                 COUNT(DISTINCT c.id) comment,
                 a.title,
                 a.active,
                 a.created_date
            FROM cm_post a LEFT JOIN cm_post_like b ON b.id_post = a.id
                           LEFT JOIN cm_post_comment c ON c.id_post = a.id AND c.active='Y',
                 cm_category d
           WHERE d.id = a.id_category
           AND a.created_by = ?
           GROUP BY a.id,d.name,a.title,a.active,a.created_date";
    
    $param = array($tfs->getUserId());
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function profile(TfSession $tfs){
    $q = "  SELECT coalesce(c.photo,'../asset/images/blank-user.png') photo,
                   CONCAT(c.first_name,' ',c.last_name) name,
                   f.email_account,
                   x.business_title
              FROM gl_natural_person c,
                   hr_employee f LEFT JOIN (SELECT g.id_employee,i.name business_title
                                              FROM hr_employee_position g,
                                                   hr_position h,
                                                   hr_business_title i
                                             WHERE i.id = h.id_business_title
                                               AND h.id = g.id_position 
                                               AND g.termination_date IS NULL) x
                                        ON x.id_employee = f.id 
             WHERE c.id = f.id_person 
               AND f.id = ?";
    
    $param = array($tfs->getUserId());
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs;
  }
  
  public static function dataListPage(TfSession $tfs,$in){
    $q = "  SELECT a.id,COUNT(DISTINCT d.id) likes,COUNT(DISTINCT e.id) likeme,
                  COUNT(DISTINCT p.id) commented,
                   y.tag,
                   a.id_category,
                   b.name category,
                   a.image,
                   a.title,
                   a.content,
                   a.created_date,
                   coalesce(c.photo,'../asset/images/blank-user.png') photo,
                   CONCAT(c.first_name,' ',c.last_name) name,
                   f.email_account,
                   x.business_title
              FROM cm_post a LEFT JOIN cm_post_like d ON d.id_post = a.id
                             LEFT JOIN cm_post_like e ON e.id_post = a.id AND e.created_by = ?
                             LEFT JOIN cm_post_comment p ON p.id_post = a.id AND p.created_by = ? AND p.active='Y'
                             LEFT JOIN (SELECT j.id_post,GROUP_CONCAT(k.name) tag
                                         FROM cm_post_tag j,
                                              cm_tag k
                                        WHERE k.id=j.id_tag
                                        GROUP BY j.id_post) y ON y.id_post = a.id,
                   cm_category b,
                   gl_natural_person c,
                   hr_employee f LEFT JOIN (SELECT g.id_employee,i.name business_title
                                              FROM hr_employee_position g,
                                                   hr_position h,
                                                   hr_business_title i
                                             WHERE i.id = h.id_business_title
                                               AND h.id = g.id_position 
                                               AND g.termination_date IS NULL) x
                                        ON x.id_employee = f.id 
             WHERE c.id = f.id_person 
               AND b.id = a.id_category
               AND f.id = a.created_by
               AND a.id IN ("."'".str_replace(",", "','", $in)."'".")
            GROUP BY a.id,id_category,b.name,a.title,a.content,a.created_date,photo,name,
                     f.email_account,x.business_title";
    
    $param = array($tfs->getUserId(),$tfs->getUserId());
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
public static function index(TfSession $tfs){
    $q = "SELECT a.id,999999999999 idx,a.created_date
            FROM cm_post a 
           WHERE DATE(a.created_date) = CURDATE() 
            AND a.active='Y'
          UNION ALL
          SELECT a.id,(5000-DATEDIFF(NOW(),a.created_date))+(COUNT(DISTINCT b.id)*2)+COUNT(DISTINCT c.id) idx,a.created_date
            FROM cm_post a LEFT JOIN cm_post_like b ON b.id_post = a.id
                           LEFT JOIN cm_post_comment c ON c.id_post = a.id AND c.active='Y'
           WHERE DATE(a.created_date) != CURDATE() 
            AND a.active='Y'
           GROUP BY a.id
           ORDER BY 2 DESC,3 DESC";
           
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function indexBySearch(TfSession $tfs,$search){
    $q = " SELECT w.id,999999999999 idx,w.created_date
  FROM (SELECT a.id,COUNT(DISTINCT d.id) likes,
       CONCAT(c.first_name,' ',c.last_name,' ',x.business_title,' ',
              f.email_account,' ',b.name,' ',a.title,' ',a.content,' ',y.tag,' ',z.comment) content,
                   z.commented,
                   a.created_date                   
              FROM cm_post a LEFT JOIN cm_post_like d ON d.id_post = a.id
                             LEFT JOIN (SELECT p.id_post,COUNT(DISTINCT p.id) commented,
                                               GROUP_CONCAT(p.comment) comment
                                           FROM cm_post_comment p 
                      WHERE p.active='Y'
                                          GROUP BY p.id_post) z ON z.id_post = a.id
                             LEFT JOIN (SELECT j.id_post,GROUP_CONCAT(k.name) tag
                                         FROM cm_post_tag j,
                                              cm_tag k
                                        WHERE k.id=j.id_tag
                                        GROUP BY j.id_post) y ON y.id_post = a.id,
                   cm_category b,
                   gl_natural_person c,
                   hr_employee f LEFT JOIN (SELECT g.id_employee,i.name business_title
                                              FROM hr_employee_position g,
                                                   hr_position h,
                                                   hr_business_title i
                                             WHERE i.id = h.id_business_title
                                               AND h.id = g.id_position 
                                               AND g.termination_date IS NULL) x
                                        ON x.id_employee = f.id 
             WHERE c.id = f.id_person 
               AND b.id = a.id_category
               AND f.id = a.created_by
               AND DATE(a.created_date) = CURDATE()
            GROUP BY a.id) w
     WHERE REPLACE(UPPER(w.content),'Ñ','0N1') LIKE REPLACE(UPPER(CONCAT('%',REPLACE(?,' ','%'),'%')),'Ñ','0N1') 
           UNION ALL
     SELECT w.id,(5000-DATEDIFF(NOW(),w.created_date))+(w.likes*2)+w.commented idx,w.created_date
  FROM (SELECT a.id,COUNT(DISTINCT d.id) likes,
       CONCAT(c.first_name,' ',c.last_name,' ',x.business_title,' ',
              f.email_account,' ',b.name,' ',a.title,' ',a.content,' ',y.tag,' ',z.comment) content,
                   z.commented,
                   a.created_date                   
              FROM cm_post a LEFT JOIN cm_post_like d ON d.id_post = a.id
                             LEFT JOIN (SELECT p.id_post,COUNT(DISTINCT p.id) commented,
                                               GROUP_CONCAT(p.comment) comment
                                           FROM cm_post_comment p 
                      WHERE p.active='Y'
                                          GROUP BY p.id_post) z ON z.id_post = a.id
                             LEFT JOIN (SELECT j.id_post,GROUP_CONCAT(k.name) tag
                                         FROM cm_post_tag j,
                                              cm_tag k
                                        WHERE k.id=j.id_tag
                                        GROUP BY j.id_post) y ON y.id_post = a.id,
                   cm_category b,
                   gl_natural_person c,
                   hr_employee f LEFT JOIN (SELECT g.id_employee,i.name business_title
                                              FROM hr_employee_position g,
                                                   hr_position h,
                                                   hr_business_title i
                                             WHERE i.id = h.id_business_title
                                               AND h.id = g.id_position 
                                               AND g.termination_date IS NULL) x
                                        ON x.id_employee = f.id 
             WHERE c.id = f.id_person 
               AND b.id = a.id_category
               AND f.id = a.created_by
               AND DATE(a.created_date) != CURDATE()
            GROUP BY a.id) w
     WHERE REPLACE(UPPER(w.content),'Ñ','0N1') LIKE REPLACE(UPPER(CONCAT('%',REPLACE(?,' ','%'),'%')),'Ñ','0N1')       
    ORDER BY 2 DESC,3 DESC";
    
    $param = array($search,$search);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function indexByCategory(TfSession $tfs,$idCategory){
    $q = "SELECT a.id,(5000-DATEDIFF(NOW(),a.created_date))+(COUNT(DISTINCT b.id)*2)+COUNT(DISTINCT c.id) idx
            FROM cm_post a LEFT JOIN cm_post_like b ON b.id_post = a.id
                           LEFT JOIN cm_post_comment c ON c.id_post = a.id AND c.active='Y'
           WHERE a.active='Y'
            AND  a.id_category = ?
           GROUP BY a.id
           ORDER BY idx DESC";
    
    $param = array($idCategory);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
  
  public static function indexByLike(TfSession $tfs){
    $q = "SELECT a.id,(5000-DATEDIFF(NOW(),a.created_date))+(COUNT(DISTINCT b.id)*2)+COUNT(DISTINCT c.id) idx
            FROM cm_post a INNER JOIN cm_post_like b ON b.id_post = a.id  AND b.created_by = ?
                           LEFT JOIN cm_post_comment c ON c.id_post = a.id AND c.active='Y'
           WHERE a.active='Y'
           GROUP BY a.id
           ORDER BY idx DESC";
    
    $param = array($tfs->getUserId());
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function indexByComment(TfSession $tfs){
    $q = "SELECT a.id,(5000-DATEDIFF(NOW(),a.created_date))+(COUNT(DISTINCT b.id)*2)+COUNT(DISTINCT c.id) idx
            FROM cm_post a LEFT JOIN cm_post_like b ON b.id_post = a.id
                           INNER JOIN cm_post_comment c ON c.id_post = a.id AND c.active='Y' AND c.created_by = ?
           WHERE a.active='Y'
           GROUP BY a.id
           ORDER BY idx DESC";
    
    $param = array($tfs->getUserId());       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function indexByTag(TfSession $tfs,$idTag){
    $q = "SELECT a.id,(5000-DATEDIFF(NOW(),a.created_date))+(COUNT(DISTINCT b.id)*2)+COUNT(DISTINCT c.id) idx
            FROM cm_post a LEFT JOIN cm_post_like b ON b.id_post = a.id
                           LEFT JOIN cm_post_comment c ON c.id_post = a.id AND c.active='Y',
                 cm_post_tag d          
           WHERE a.active='Y'
             AND a.id = d.id_post 
             AND d.id_tag = ?
           GROUP BY a.id
           ORDER BY idx DESC";
    
    $param = array($idTag);       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }

 public static function timeAgo($date) {
     $timestamp = strtotime($date); 
     
     $strTime = array("second", "minute", "hour", "day", "month", "year");
     $length = array("60","60","24","30","12","10");

     $currentTime = time();
     if($currentTime >= $timestamp) {
      $diff     = time()- $timestamp;
      for($i = 0; $diff >= $length[$i] && $i < count($length)-1; $i++) {
      $diff = $diff / $length[$i];
      }

      $diff = round($diff);
      return $diff . " " . $strTime[$i] . "(s) ago ";
     }
  }  

  public static function qtyByCategory(TfSession $tfs){
    $q = "SELECT a.id,a.name,count(b.id) qty
            FROM epic.cm_category a LEFT JOIN epic.cm_post b ON b.active = 'Y' AND b.id_category = a.id 
           WHERE a.active = 'Y'
           GROUP BY a.id,a.name
           ORDER BY a.name";
                     
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function qtyByLike(TfSession $tfs){
    $q = "SELECT COUNT(DISTINCT a.id) qty
            FROM cm_post a,
                 cm_post_like b  
           WHERE a.active='Y'
             AND a.id = b.id_post
             AND b.created_by = ?";
                     
    $param = array($tfs->getUserId());
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["qty"];
  }
  public static function qtyByComment(TfSession $tfs){
    $q = "SELECT COUNT(DISTINCT a.id) qty
            FROM cm_post a,
                 cm_post_comment b  
           WHERE a.active='Y'
             AND a.id = b.id_post
             AND b.active='Y'
             AND b.created_by = ?";
                     
    $param = array($tfs->getUserId());
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["qty"];
  }
}
?>
