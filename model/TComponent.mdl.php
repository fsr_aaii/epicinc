<?php
  class TComponent extends TComponentBase {
  
  private $schema;
  private $columns=array();
  private $entityName;
  private $className;
  private $varName;
  private $numberType=array('decimal','bigint','int','float','tinyint','double');
  private $tag=array('name','description');
  private $alias=array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r');


  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_component_type"=>true,
                              "name"=>true,
                              "description"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  
  private function underscoreToCamelCase($string, $capitalizeFirstCharacter = TRUE) {

    $str = str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));

    if (!$capitalizeFirstCharacter) {
        $str[0] = strtolower($str[0]);
    }

    return $str;
  }

  private function underscoreToTitle($string) {

    $str = ucwords(str_replace('_', ' ', $string));
    return $str;
  }

  public function populateName($schema,$name){
   
    $this->entityName = $name;
    $this->className = $this->underscoreToCamelCase($name,TRUE);
    $this->varName = $this->underscoreToCamelCase($name,FALSE);

    $q = "SELECT id
            FROM t_component
           WHERE name=?";

    $param = array($this->className);
    list($rs) = $this->tfs->executeQuery($q,$param);

    if (isset($rs["id"]) and $rs["id"]!=''){
      $this->dbPopulate($rs["id"]);
      $this->schema=$schema;
      $this->setValidations();
    }else{
      $this->id_component_type=1;
      $this->name=$this->className;
      $this->description='Controller for entity: '.$name;
      $this->schema=$schema;
      $this->active="Y";
      $this->created_by=$this->tfs->getUserId();
      $this->created_date=date("Y-m-d H:i:s");
      $this->create();
      $this->gSequence();

    }
    $this->allEntityColumns();


     
  }
  public function gController(){
    foreach ($this->columns as $c){
      if ($c['column_name'] == 'created_by'){
        $audit.='      $'.$this->varName.'->setCreatedBy($tfs->getUserId());'.PHP_EOL;
      }elseif ($c['column_name'] == 'created_date') {
        $audit.='      $'.$this->varName.'->setCreatedDate(date("Y-m-d H:i:s"));'.PHP_EOL;
      } 
    }
    
    $file.='<?php'.PHP_EOL;
    $file.='  if ($tfRequestAction=="AL"){'.PHP_EOL;
    $file.='     $'.$this->varName.'List='.$this->className.'::dataList($tfs);'.PHP_EOL;
    $file.='  }else{'.PHP_EOL;
    $file.='     $'.$this->varName.' = new '.$this->className.'($tfs);'.PHP_EOL;
    $file.='     $'.$this->varName.'->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); '.PHP_EOL; 
    $file.='  }'.PHP_EOL;  
    //$file.='  $viewTitle = "'.$this->name.'";'.PHP_EOL;
    $file.='  switch ($tfRequestAction){'.PHP_EOL;
    $file.='    case "AN":'.PHP_EOL;
    $file.='      break;'.PHP_EOL;
    $file.='    case "AL":'.PHP_EOL;
    $file.='      break;'.PHP_EOL;
    $file.='    case "AC":'.PHP_EOL;
    $file.='      $tfs->setSessionReadonly(TRUE);'.PHP_EOL;
    $file.='      break; '.PHP_EOL;
    $file.='    case "AE":'.PHP_EOL;
    $file.='      break; '.PHP_EOL;
    $file.='    case "AI":'.PHP_EOL;
    $file.=$audit;
    $file.='      $'.$this->varName.'->setValidations();'.PHP_EOL;
    $file.='      $'.$this->varName.'->create();'.PHP_EOL;
    $file.='      if ($'.$this->varName.'->isValid()){ '.PHP_EOL;
    $file.='        $tfs->checkTrans();'.PHP_EOL;  
    $file.='        $tfs->swapTrail("'.$this->className.'","AE",tfRequest::encrypt(array("'.$this->entityName.'_id" => $'.$this->varName.'->getId())),2);'.PHP_EOL;
    $file.='        $tfRequestAction="AE";'.PHP_EOL;
    $file.='      }'.PHP_EOL;
    $file.='      break;'.PHP_EOL;
    $file.='    case "AA":'.PHP_EOL;
    $file.='      $'.$this->varName.'->update();'.PHP_EOL;
    $file.='      if ($'.$this->varName.'->isValid()){ '.PHP_EOL;
    $file.='        $tfs->checkTrans();'.PHP_EOL;
    $file.='      }'.PHP_EOL;
    $file.='        $tfs->swapTrail("'.$this->className.'","AE",tfRequest::encrypt(array("'.$this->entityName.'_id" => $'.$this->varName.'->getId())),2);'.PHP_EOL;
    $file.='        $tfRequestAction="AE";'.PHP_EOL;
    $file.='      break;'.PHP_EOL;
    $file.='    case "AB":'.PHP_EOL;
    $file.='      $'.$this->varName.'->delete();'.PHP_EOL;
    $file.='      $tfs->checkTrans();'.PHP_EOL;
    $file.='      $tfs->backTrail();'.PHP_EOL; 
    $file.='      break;'.PHP_EOL;
    $file.='  }'.PHP_EOL;
    $file.='  if ($tfs->toRedirect()){'.PHP_EOL;
    $file.='    $tfs->redirect();'.PHP_EOL;
    $file.='  }else{'.PHP_EOL;
    $file.='    if  ($tfRequestAction=="AL"){'.PHP_EOL;
    $file.='      require("view/'.$this->className.'.rvw.php");'.PHP_EOL;
    $file.='    }else{'.PHP_EOL;
    $file.='      require("view/'.$this->className.'.vw.php");'.PHP_EOL;
    $file.='    } '.PHP_EOL;  
    $file.='  }'.PHP_EOL;
    $file.='?>'.PHP_EOL;

      
    return $file;
  }

  public function gCallbackFn(){
    foreach ($this->columns as $c){
      if (isset($c['referenced_table_name']) and $c['referenced_table_name']!=''){
        $case.='    case "'.$this->name.'_'.$c['column_name'].'":'.PHP_EOL;
        $case.='      $id=$tfRequest->id;'.PHP_EOL;
        $case.='      $'.$c['referenced_table_name'].' = '.$c['referenced_table_name'].'::selectOptions(TfSession $tfs);'.PHP_EOL;
        $case.='      echo TfWidget::selectStructure($'.$c['referenced_table_name'].',$id);'.PHP_EOL;
        $case.='      break;'.PHP_EOL;
        $case.='    case "'.$this->name.'_'.$c['column_name'].'_results":'.PHP_EOL;
        $case.='      $filter=strtoupper($tfRequest->get("filter"));'.PHP_EOL;
        $case.='      $id=$tfRequest->get("id");'.PHP_EOL;
        $case.='      $'.$c['referenced_table_name'].' = '.$c['referenced_table_name'].'::selectOptionsFilter(TfSession $tfs,$id,$filter);'.PHP_EOL;
        $case.='      if (count($'.$c['referenced_table_name'].')==0){'.PHP_EOL;
        $case.='        $html=\'<option value="">Select a option<\option>\';'.PHP_EOL;
        $case.='      }else{'.PHP_EOL;
        $case.='        $html.=TfWidget::selectStructure($'.$c['referenced_table_name'].',"");'.PHP_EOL;
        $case.='      }'.PHP_EOL;
        $case.='      echo $html;'.PHP_EOL;
        $case.='      break;'.PHP_EOL;
            
        $require.='  require_once ("modelExtend/'.$c['referenced_table_name'].'.mdlx.php");'.PHP_EOL;
      }
    }  
    $file='';
    $file.='<?php'.PHP_EOL;
    $file.='  require_once ("modelExtend/'.$this->name.'.mdlx.php");'.PHP_EOL;
    $file.=$require.PHP_EOL;
    $file.='  $widget= $tfRequest->get("widget");'.PHP_EOL.PHP_EOL;
    $file.='  switch ($widget){'.PHP_EOL;
    $file.=$case;
    $file.='    default:'.PHP_EOL;
    $file.='      echo "";'.PHP_EOL;
    $file.='      break; '.PHP_EOL;
    $file.='  }'.PHP_EOL;
    $file.='?>'.PHP_EOL;
    return $file;
  }

  public function gJSView(){
    $file.='function '.$this->varName.'Rules(){'.PHP_EOL;
    $file.='  $("#'.$this->entityName.'_form").validate();'.PHP_EOL;
    foreach ($this->columns as $c){
      if ($c['column_name']!='created_date' AND $c['column_name']!='created_by' ){
        $comma='';
        $file.='  $("#'.$this->entityName.'_'.$c['column_name'].'").rules("add", {'.PHP_EOL;
        if ($c['notnull']== 'NO'){
          $file.=$comma.'    required:true';
          $comma=','.PHP_EOL;
        }
          
         if (($c['data_type']== 'date') AND $c['column_name']!='created_date'){
          
          $file.=$comma.'    isDate:true';
          $datepicker.='$("#'.$this->entityName.'_'.$c['column_name'].'").datepicker({'.PHP_EOL;
          $datepicker.='  uiLibrary: \'bootstrap4\','.PHP_EOL;
          $datepicker.='  format: \'yyyy-mm-dd\','.PHP_EOL;
          $datepicker.='  }).on(\'change\', function(e) {'.PHP_EOL;
          $datepicker.='  $(this).valid();'.PHP_EOL;
          $datepicker.='});'.PHP_EOL;
          $mask.='  $("#'.$this->entityName.'_'.$c['column_name'].'").css(\'vertical-align\',\'top\');'.PHP_EOL;
          $mask.='  $("#'.$this->entityName.'_'.$c['column_name'].'").mask(\'y999-m9-d9\');'.PHP_EOL;
          $comma=','.PHP_EOL;          
        }elseif  (in_array($c['data_type'],$this->numberType)) {
          if ($c['data_length']== '0'){
            $file.=$comma.'    digits:true';
          }else{
            $file.=$comma.'    number:true';
          }       
          $comma=','.PHP_EOL;       
        }

        $select2='';
        if (isset($c['referenced_table_name']) and $c['referenced_table_name']!=''){ 
          $select2.='  $("#'.$this->entityName.'_'.$c['column_name'].'").select2({'.PHP_EOL;
          $select2.='    minimumResultsForSearch: -1'.PHP_EOL;
          $select2.='  });'.PHP_EOL;
        }

        $file.=$comma.'    maxlength:'.$c['data_length'];
        $file.=PHP_EOL.'  '.'});'.PHP_EOL;
      }  
    }  
    
    $file.=PHP_EOL;
    $file.='}'.PHP_EOL.PHP_EOL;
    $file.=$datepicker.PHP_EOL;
    $file.='$(document).ready(function(){'.PHP_EOL;
    $file.='  '.$this->varName.'Rules();'.PHP_EOL;
    $file.=$select2.PHP_EOL;
    $file.=$mask.PHP_EOL;
    $file.='})'.PHP_EOL;
    return $file;
  }

  public function gJSDataTable(){
    
    $file='';
    $file.='$(document).ready(function() {'.PHP_EOL;
    $file.='  $("#'.$this->entityName.'_dt").DataTable({'.PHP_EOL;
    $file.='    pageLength: 8,'.PHP_EOL;
    $file.='    info:false,'.PHP_EOL;
    $file.='    paging:true,'.PHP_EOL;
    $file.='    lengthChange:false,'.PHP_EOL;
    $file.='    dom: \'frtip\','.PHP_EOL;
    $file.='    columnDefs: ['.PHP_EOL;
    $file.='      { width: "3em", targets: -1 },'.PHP_EOL;
    $file.='      { orderable: false, targets: -1 }'.PHP_EOL;
    $file.='    ],'.PHP_EOL;
    $file.='  });'.PHP_EOL;
    $file.='});'.PHP_EOL;
    return $file;
  }

  public function gModelBase(){
    foreach ($this->columns as $c){
      $attr.='    protected $'.$c['column_name'].';'.PHP_EOL;
      $getAllSelect.=','.PHP_EOL.'               '.$c['column_name'];
      $setAttrDB.='    $this->'.$c['column_name'].'=$rs["'.$c['column_name'].'"];'.PHP_EOL;
      
      $setAttrUI.='    if ($tfRequest->exist("'.$this->entityName.'_'.$c['column_name'].'")){'.PHP_EOL;
      $setAttrUI.='      $this->'.$c['column_name'].'=$tfRequest->'.$this->entityName.'_'.$c['column_name'].';'.PHP_EOL;
      $setAttrUI.='    }'.PHP_EOL;

      if (in_array($c['data_type'],$this->numberType)) {
        $setValidate.='    $this->validation["'.$c['column_name'].'"]=array("type"=>"number",'.PHP_EOL; 
      }elseif ($c['data_type']=='datetime') {
        $setValidate.='    $this->validation["'.$c['column_name'].'"]=array("type"=>"datetime",'.PHP_EOL; 
      }elseif ($c['data_type']=='date') {
        $setValidate.='    $this->validation["'.$c['column_name'].'"]=array("type"=>"date",'.PHP_EOL; 
      }else{
        $setValidate.='    $this->validation["'.$c['column_name'].'"]=array("type"=>"string",'.PHP_EOL; 
      }
      $setValidate.='                                  "value"=>$this->'.$c['column_name'].','.PHP_EOL; 
      $setValidate.='                                  "length"=>'.$c['data_length'].','.PHP_EOL; 
      if ($c['notnull']== 'NO'){
        $setValidate.='                                  "required"=>true);'.PHP_EOL;
      }else{
        $setValidate.='                                  "required"=>false);'.PHP_EOL;
      }
      
      $setget.='  public function set'.$this->underscoreToCamelCase($c['column_name']).'($value){'.PHP_EOL;
      $setget.='  $this->'.$c['column_name'].'=$value;'.PHP_EOL;
      $setget.='  }'.PHP_EOL;
      $setget.='  public function get'.$this->underscoreToCamelCase($c['column_name']).'(){'.PHP_EOL;
      $setget.='  return $this->'.$c['column_name'].';'.PHP_EOL;
      $setget.='  }'.PHP_EOL;
       
      $insertColumns.=','.PHP_EOL.'                               '.$c['column_name'];
      $insertValues.=',?';
      $insertParam.=','.PHP_EOL.'                     $this->'.$c['column_name'].'==\'\'?NULL:$this->'.$c['column_name']; 

      $updateSet.='        if ($this->'.$c['column_name'].'!= $rs["'.$c['column_name'].'"]){'.PHP_EOL;
      $updateSet.='          if ($this->updateable["'.$c['column_name'].'"]){'.PHP_EOL;
      $updateSet.='            $set.=$set_aux."'.$c['column_name'].'=?";'.PHP_EOL;
      $updateSet.='            $set_aux=",";'.PHP_EOL;
      $updateSet.='            $param[]=$this->'.$c['column_name'].'==\'\'?NULL:$this->'.$c['column_name'].';'.PHP_EOL;
      $updateSet.='          }else{'.PHP_EOL;
      $updateSet.='            $this->objError[]="The field ('.$c['column_name'].') cannot be modified";'.PHP_EOL;
      $updateSet.='            $this->valid = false;'.PHP_EOL;
      $updateSet.='          }'.PHP_EOL;
      $updateSet.='        }'.PHP_EOL;
    }  
    $file='';
    $file.='<?php'.PHP_EOL;
    $file.='  class '.$this->className.'Base extends TfEntity {'.PHP_EOL;
    $file.='    protected $id;'.PHP_EOL;
    $file.=$attr.PHP_EOL;
    $file.='  public function __construct(TfSession $tfs){ '.PHP_EOL; 
    $file.='    $this->tfs = $tfs;'.PHP_EOL;
    $file.='    $this->entity="'.$this->entityName.'";'.PHP_EOL;
    $file.='  }'.PHP_EOL.PHP_EOL; 
    $file.='  private function getAll(){'.PHP_EOL.PHP_EOL;
    $file.='    $q="SELECT id'.$getAllSelect.PHP_EOL; 
    $file.='          FROM '.$this->entityName.PHP_EOL; 
    $file.='         WHERE id=?";'.PHP_EOL.PHP_EOL; 
    $file.='    $param = array($this->id);'.PHP_EOL; 
    $file.='    list($rs) = $this->tfs->executeQuery($q,$param);'.PHP_EOL;  
    $file.='    return $rs;'.PHP_EOL;
    $file.='  }'.PHP_EOL.PHP_EOL;
    $file.='  protected function dbPopulate($id){ '.PHP_EOL.PHP_EOL;
    $file.='    $this->id=$id;'.PHP_EOL; 
    $file.='    $rs = $this->getAll();'.PHP_EOL; 
    $file.='    $this->initialState=hash(HASH_KEY,json_encode($rs));'.PHP_EOL; 
    $file.=$setAttrDB.PHP_EOL; 
    $file.='  }'.PHP_EOL.PHP_EOL;  
    $file.='  protected function uiPopulate(TfRequest $tfRequest){ '.PHP_EOL.PHP_EOL;
    $file.='    $this->dbPopulate($tfRequest->'.$this->entityName.'_id);'.PHP_EOL; 
    $file.='      if ($this->initialState!=""){'.PHP_EOL; 
    $file.='      if ($this->initialState!=$tfRequest->is_'.$this->entityName.'){'.PHP_EOL; 
    $file.='        $this->objError[]="This record is blocked by another user, try later";'.PHP_EOL; 
    $file.='        $this->valid = false;'.PHP_EOL; 
    $file.='      }'.PHP_EOL; 
    $file.='    }else{'.PHP_EOL; 
    $file.='      $this->initialState=$tfRequest->is_'.$this->entityName.'; '.PHP_EOL; 
    $file.='    }'.PHP_EOL.PHP_EOL;  
    $file.=$setAttrUI.PHP_EOL;
    $file.='  }'.PHP_EOL.PHP_EOL;  
    $file.='  public function setValidations(){'.PHP_EOL; 
    $file.='    $this->validation["id"]=array("type"=>"number",'.PHP_EOL; 
    $file.='                                  "value"=>$this->id,'.PHP_EOL; 
    $file.='                                  "length"=>22,'.PHP_EOL; 
    $file.='                                  "required"=>true);'.PHP_EOL; 
    $file.=$setValidate.PHP_EOL;
    $file.='  $this->setAttrErrors();'.PHP_EOL;
    $file.='  }'.PHP_EOL.PHP_EOL;
    $file.='  public function setId($value){'.PHP_EOL;
    $file.='  $this->id=$value;'.PHP_EOL;
    $file.='  }'.PHP_EOL;
    $file.='  public function getId(){'.PHP_EOL;
    $file.='  return $this->id;'.PHP_EOL;
    $file.='  }'.PHP_EOL;
    $file.=$setget.PHP_EOL;
    $file.='  public function create(){'.PHP_EOL;
    $file.='    $this->id = $this->sequence();'.PHP_EOL;
    $file.='    $this->validate();'.PHP_EOL;
    $file.='    if($this->valid){'.PHP_EOL;
    $file.='      $q = "INSERT INTO '.$this->entityName.'(id'.$insertColumns.')'.PHP_EOL;
    $file.='            VALUES (?'.$insertValues.')";'.PHP_EOL.PHP_EOL;
    $file.='      $param = array($this->id==\'\'?NULL:$this->id'.$insertParam.');'.PHP_EOL;
    $file.='      $this->tfs->execute($q,$param);'.PHP_EOL;
    $file.='      $this->objMsg[]="your record has been created";'.PHP_EOL;
    $file.='      $rs=$this->getAll();'.PHP_EOL;
    $file.='      $this->initialState=hash(HASH_KEY,json_encode($rs));'.PHP_EOL;
    $file.='    }'.PHP_EOL;
    $file.='   }'.PHP_EOL.PHP_EOL;
    $file.='  public function update(){'.PHP_EOL;
    $file.='    $this->validate();'.PHP_EOL;
    $file.='    if($this->valid){'.PHP_EOL;
    $file.='      $rs=$this->getAll();'.PHP_EOL;
    $file.='      if ($this->initialState!=hash(HASH_KEY,json_encode($rs))){'.PHP_EOL;
    $file.='        $this->objError[]="This record is blocked by another user, try later";'.PHP_EOL;
    $file.='        $this->valid = false;'.PHP_EOL;
    $file.='      }'.PHP_EOL;
    $file.='      if($this->valid){'.PHP_EOL;
    $file.='        unset($set);'.PHP_EOL;
    $file.='        unset($q);'.PHP_EOL;
    $file.='        $param = array();'.PHP_EOL;
    $file.='        $set_aux=" SET ";'.PHP_EOL.PHP_EOL;
    $file.='        if ($this->id!= $rs["id"]){'.PHP_EOL;
    $file.='          if ($this->updateable["id"]){'.PHP_EOL;
    $file.='            $set.=$set_aux."id=?";'.PHP_EOL;
    $file.='            $set_aux=",";'.PHP_EOL;
    $file.='            $param[]=$this->id==\'\'?NULL:$this->id;'.PHP_EOL;
    $file.='          }else{'.PHP_EOL;
    $file.='            $this->objError[]="The field (id) cannot be modified";'.PHP_EOL;
    $file.='            $this->valid = false;'.PHP_EOL;
    $file.='          }'.PHP_EOL;
    $file.='        }'.PHP_EOL;
    $file.=$updateSet.PHP_EOL;
    $file.='        if ($this->valid){'.PHP_EOL;
    $file.='          if (isset($set)){'.PHP_EOL;
    $file.='            $q = "UPDATE '.$this->entityName.' ".$set." WHERE id=?";'.PHP_EOL;
    $file.='            $param[]=$this->id;'.PHP_EOL; 
    $file.='            $this->tfs->execute($q,$param);'.PHP_EOL;
    $file.='            $this->objMsg[]="This record has been updated";'.PHP_EOL;
    $file.='            $rs=$this->getAll();'.PHP_EOL;
    $file.='            $this->initialState=hash(HASH_KEY,json_encode($rs));'.PHP_EOL;
    $file.='          }else{'.PHP_EOL;
    $file.='            $this->objMsg[]="This record don\'\'t have data to update";'.PHP_EOL;
    $file.='          }'.PHP_EOL;
    $file.='        }'.PHP_EOL;
    $file.='      }'.PHP_EOL;
    $file.='    }'.PHP_EOL;
    $file.='  }'.PHP_EOL;
    $file.='  public function delete(){'.PHP_EOL;
    $file.='    $q="DELETE FROM '.$this->entityName.PHP_EOL;
    $file.='         WHERE id=?";'.PHP_EOL;
    $file.='    $param = array($this->id);'.PHP_EOL;
    $file.=PHP_EOL;
    $file.='    $this->tfs->execute($q,$param);'.PHP_EOL;
    $file.='  }';
    $file.=PHP_EOL.PHP_EOL;
    $file.='}'.PHP_EOL;
    $file.='?>'.PHP_EOL;
    return $file;
  }
  
  public function gModel(){
    foreach ($this->columns as $c){
      $updateable.=','.PHP_EOL.'                              "'.$c['column_name'].'"=>true';
      if  (in_array($c['column_name'],$this->tag)) {
        $tag=$c['column_name'];
      }  
    }  
    $file='';
    $file.='<?php'.PHP_EOL;
    $file.='  class '.$this->className.' extends '.$this->className.'Base {'.PHP_EOL.PHP_EOL;
    $file.='  public function __construct(TfSession $tfs){ '.PHP_EOL;
    $file.='    parent::__construct($tfs);'.PHP_EOL;
    $file.='    $this->updateable = array("id"=>false'.$updateable.');'.PHP_EOL.PHP_EOL; 
    $file.='  }'.PHP_EOL;  
    $file.='  public static function selectOptions(TfSession $tfs){ '.PHP_EOL;
    $file.='    $q = "SELECT id \"value\",'.$tag.' \"option\"'.PHP_EOL;
    $file.='            FROM '.$this->entityName.PHP_EOL;
    $file.='           ORDER BY 2";'.PHP_EOL;
    $file.='    $rs = $tfs->executeQuery($q);'.PHP_EOL.PHP_EOL;
    $file.='    return $rs;'.PHP_EOL;
    $file.='  }'.PHP_EOL;  
    $file.='  public static function description(TfSession $tfs,$id){ '.PHP_EOL;
    $file.='    $q = "SELECT '.$tag.' description'.PHP_EOL;
    $file.='            FROM '.$this->entityName.PHP_EOL;
    $file.='           WHERE id=?";'.PHP_EOL;
    $file.='    $param = array($id);'.PHP_EOL;
    $file.='    list($rs) = $tfs->executeQuery($q,$param);'.PHP_EOL.PHP_EOL;
    $file.='    return $rs["description"];'.PHP_EOL;
    $file.='  }'.PHP_EOL;  
    $file.='  public static function dataList(TfSession $tfs){'.PHP_EOL;
    $file.='    $q = "'.$this->gDatatableList().'";'.PHP_EOL;    
    $file.='    $rs = $tfs->executeQuery($q);'.PHP_EOL.PHP_EOL;
    $file.='    return $rs;'.PHP_EOL;
    $file.='  }'.PHP_EOL;
    $file.='}'.PHP_EOL;
    $file.='?>'.PHP_EOL;

    return $file;
  }

  public function gDatatableList(){
    $i=0;
    $select='SELECT a.id';
    $from='            FROM '.$this->entityName.' a';
    $where='';
    $and='           WHERE ';
    foreach ($this->columns as $c){
      if (isset($c['referenced_table_name']) and $c['referenced_table_name']!=''){
        $i++;
        $desc=$this->FKColumnDesc($c['referenced_table_name']);
        if (isset($desc) and $desc!=''){
          $select.=','.PHP_EOL.'                 '.$this->alias["$i"].'.'.$desc.' '.$c['column_name'];
        }else{
          $select.=','.PHP_EOL.'                 a.'.$c['column_name'];
        }  
        $from.=','.PHP_EOL.'                 '.$c['referenced_table_name'].' '.$this->alias["$i"]; 
        $where.=PHP_EOL.$and.$this->alias["$i"].'.id = a.'.$c['column_name'];
        $and='           AND ';
      }elseif ($c['column_name']=="created_by"){
        $i++;
        $select.=','.PHP_EOL.'                 '.$this->alias["$i"].'.name created_by';
        $from.=','.PHP_EOL.'                 '.'t_user '.$this->alias["$i"];
        $where.=PHP_EOL.$and.$this->alias["$i"].'.id = a.'.$c['column_name'];
        $and='           AND ';
      }else{
        $select.=','.PHP_EOL.'                 a.'.$c['column_name'];
      }
    }
    $select.=PHP_EOL.$from.$where;
    return $select;

  }

  public function gDTView(){
    $th='';
    $tr='';
    foreach ($this->columns as $c){

      if ($c['column_name'] == 'created_by'){
        $th.='             <th class="none">Created by</th>'.PHP_EOL; 
      }elseif ($c['column_name'] == 'created_date') {
        $th.='             <th class="none">Created date</th>'.PHP_EOL; 
      }else{
        $th.='             <th class="all">'.$this->underscoreToTitle($c['column_name']).'</th>'.PHP_EOL;  
      } 
      $tr.='            <td>\'.$row["'.$c['column_name'].'"].\'</td>'.PHP_EOL;  
    }
    $file='';
    
    $file.='<?php'.PHP_EOL;
    $file.='  if (!is_object($tfs)){'.PHP_EOL;
    $file.='    throw new TfException("The server cannot process your request",7102,400);'.PHP_EOL;
    $file.='  }'.PHP_EOL.PHP_EOL;
    $file.='  $html=\'<div class="row">'.PHP_EOL;
    $file.='           <div class="mx-auto col-lg-8">'.PHP_EOL;
    $file.='            <div class="col-6 title">'.$this->underscoreToTitle($this->entityName).'</div>'.PHP_EOL;
    $file.='             <div class="col-6 text-right action">'.PHP_EOL;
    $file.='               <a class="button" role="button" data-tf-task-id="\'.$tfs->getTaskId().\'" data-tf-controller="'.$this->className.'" data-tf-action="AN" onclick="TfRequest.do(this);">'.PHP_EOL;
    $file.='                 <span>Add</span><div class="icon"><span class="far fa-plus"></span></div>'.PHP_EOL;
    $file.='               </a>'.PHP_EOL;
    $file.='               <a class="button" role="button" data-tf-table="#'.$this->entityName.'_dt" data-tf-file="'.$this->underscoreToTitle($this->entityName).'" onclick="TfExport.excel(this);">'.PHP_EOL;
    $file.='                 <span>Excel</span><div class="icon"><span class="far fa-download"></span></div>'.PHP_EOL;
    $file.='               </a>'.PHP_EOL;
    $file.='             </div>'.PHP_EOL;
    $file.='       <table id="'.$this->entityName.'_dt" class="display responsive" style="width:100%">'.PHP_EOL;
    $file.='         <thead>'.PHP_EOL;
    $file.='          <tr>'.PHP_EOL;
    $file.=$th;
    $file.='             <th class="all dt-right"></th>'.PHP_EOL;
    $file.='           </tr>'.PHP_EOL;
    $file.='         </thead>'.PHP_EOL;
    $file.='         <tbody>\';'.PHP_EOL;
    $file.='   foreach ($'.$this->varName.'List as $row){'.PHP_EOL;
    $file.='    $tfData["'.$this->entityName.'_id"] = $row["id"];'.PHP_EOL;
    $file.='    $html.=\'   <tr>'.PHP_EOL;
    $file.=$tr;
    $file.='                 <td class="dt-right">'.PHP_EOL;
    $file.='                   <a data-tf-task-id="\'.$tfs->getTaskId().\'" data-tf-controller="'.$this->className.'" data-tf-action="AE" data-tf-data="\'.$tfResponse->encrypt($tfData).\'" onclick="TfRequest.do(this);">'.PHP_EOL;
    $file.='                     <span class="far fa-pencil"></span>'.PHP_EOL;
    $file.='                   </a>'.PHP_EOL;
    $file.='                 </td>\';  '.PHP_EOL;
    $file.='   }'.PHP_EOL;
    $file.='   $html.=\'</tr>'.PHP_EOL;
    $file.='           </tbody>'.PHP_EOL;
    $file.='          </table>'.PHP_EOL;
    $file.='         </div>'.PHP_EOL;
    $file.='        </div>\';'.PHP_EOL;
    $file.=' echo $html;'.PHP_EOL;
    $file.='?>'.PHP_EOL;
    //$file.=' <script type="text/javascript" src="../javascript/'.$this->name.'.rep.js"></script>'.PHP_EOL;

    return $file;

  }

  

  public function gView(){
   $i=0;
   $hidden=''; 
   foreach ($this->columns as $c){ 
    if ($c['column_name']=='created_by'){
      $created_by='$audit=\'<div class="col-lg-12 container">'.PHP_EOL;
      $created_by.='          <span class="created_by">Created  by \'.TUser::description($tfs,$'.$this->varName.'->getCreatedBy()).\'  on \'.$'.$this->varName.'->getCreatedDate().\'</span>'.PHP_EOL;
      $created_by.='        </div>\';'.PHP_EOL;
      $created_by_show="'.\$audit.'".PHP_EOL;
      //$hidden='<input type="hidden" id="'.$this->entityName.'_created_by" name="'.$this->entityName.'_created_by" maxlength="22" value="\'.$'.$this->varName.'->getCreatedBy().\'">'.PHP_EOL;
      //$hidden.='<input type="hidden" id="'.$this->entityName.'_created_date" name="'.$this->entityName.'_created_date" maxlength="22" value="\'.$'.$this->varName.'->getCreatedDate().\'">'.PHP_EOL;
    }elseif ($c['column_name']=='created_date') {
       # code...
    }else{
      $i++; 
      $form.='      <div class="col-lg-12 container">'.PHP_EOL;
      $form.='       <label for="'.$this->entityName.'_'.$c['column_name'].'" class="control-label">'.$this->underscoreToTitle($c['column_name']).':</label>'.PHP_EOL;
      if (isset($c['referenced_table_name']) and $c['referenced_table_name']!=''){ 
        $form.='        <select  id="'.$this->entityName.'_'.$c['column_name'].'" name="'.$this->entityName.'_'.$c['column_name'].'" class="'.$this->entityName.'_'.$c['column_name'].' form-control" tabindex="'.$i.'">'.PHP_EOL;
        $form.='      <option value="">Select a option</option>\'.'.PHP_EOL;
        $form.='      TfWidget::selectStructure('.$this->underscoreToCamelCase($c['referenced_table_name']).'::selectOptions($tfs),$'.$this->varName.'->get'.$this->underscoreToCamelCase($c['column_name']).'()).'.PHP_EOL;
        $form.='\'      </select>'.PHP_EOL;
      }elseif ($c['data_length']>=500 OR $c['data_type']=="TEXT"){
             $form.='        <textarea id="'.$this->entityName.'_'.$c['column_name'].'" name="'.$this->entityName.'_'.$c['column_name'].'" class="'.$this->entityName.'_'.$c['column_name'].' form-control" rows="3" >\'.$'.$this->varName.'->get'.$this->underscoreToCamelCase($c['column_name']).'().\'</textarea>'.PHP_EOL;
      }elseif ($c['column_name']=='active') {
         $form.='        <select  id="'.$this->entityName.'_'.$c['column_name'].'" name="'.$this->entityName.'_'.$c['column_name'].'" class="'.$this->entityName.'_'.$c['column_name'].' form-control" tabindex="'.$i.'">'.PHP_EOL;
        $form.='      <option value="">Select a option</option>\'.'.PHP_EOL;
        $form.='      TfWidget::selectStructureYN($'.$this->varName.'->get'.$this->underscoreToCamelCase($c['column_name']).'(),\'Y\').'.PHP_EOL;
        $form.='\'      </select>'.PHP_EOL;
      }else{
           $form.='        <input type="text" id="'.$this->entityName.'_'.$c['column_name'].'" name="'.$this->entityName.'_'.$c['column_name'].'" class="'.$this->entityName.'_'.$c['column_name'].' form-control"  maxlength="'.$c['data_length'].'"  value="\'.$'.$this->varName.'->get'.$this->underscoreToCamelCase($c['column_name']).'().\'"  tabindex="'.$i.'"/>'.PHP_EOL;        
      }
      $form.='      <label for="'.$this->entityName.'_'.$c['column_name'].'" class="error">\'.$'.$this->varName.'->getAttrError("'.$c['column_name'].'").\'</label>'.PHP_EOL;
      $form.='      </div>'.PHP_EOL;
    }

   
   }
 
    
   $file='';    
   $file.='<?php'.PHP_EOL;
   $file.='  if (!is_object($tfs)){'.PHP_EOL;
   $file.='    header("Location: ../403.html");'.PHP_EOL;
   $file.='    exit();'.PHP_EOL;
   $file.='  }'.PHP_EOL.PHP_EOL;
      
   $file.='  switch ($tfRequestAction){'.PHP_EOL;
   $file.='   case "AN":'.PHP_EOL;
   $file.='   case "AI":'.PHP_EOL;
   $file.='    $buttons =\'<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#'.$this->entityName.'_form" data-tf-task-id="\'.$tfs->getTaskId().\'" data-tf-controller="'.$this->varName.'" data-tf-action="AI" onclick="TfRequest.do(this,true);">Guardar</a>\';'.PHP_EOL;
   $file.='    break;'.PHP_EOL;
   $file.='   case "AE":'.PHP_EOL;
   $file.='   case "AB":'.PHP_EOL;
   $file.='   case "AA":'.PHP_EOL;
   $file.=$created_by;
   $file.='    $buttons =\'<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#'.$this->entityName.'_form" data-tf-task-id="\'.$tfs->getTaskId().\'" data-tf-controller="'.$this->varName.'" data-tf-action="AA" onclick="TfRequest.do(this,true);">Guardar</a>\';'.PHP_EOL;
  $file.='    $buttons.=\'<a class="btn btn-epic-xl-primary" role="button" data-tf-form="#'.$this->entityName.'_form" data-tf-task-id="\'.$tfs->getTaskId().\'" data-tf-controller="'.$this->varName.'" data-tf-action="AB" onclick="TfRequest.do(this,true);">Borrar</a>\';'.PHP_EOL;
   $file.='    break;'.PHP_EOL;
   $file.='  }'.PHP_EOL;   
   $file.='  foreach ($'.$this->varName.'->getObjError() as $oe) {'.PHP_EOL;
   $file.='    $objAlerts.=TfWidget::alertDangerTemplate($oe);'.PHP_EOL;
   $file.='  }'.PHP_EOL;
   $file.='  foreach ($'.$this->varName.'->getObjMsg() as $om) {'.PHP_EOL;
   $file.='    $objAlerts.=TfWidget::alertSuccessTemplate($om);'.PHP_EOL;
   $file.='  }'.PHP_EOL;
   $file.='  $html = \'<div class="row">'.PHP_EOL;
   $file.='  <div class="mx-auto card col-lg-10 form-frame shadow mb-4">'.PHP_EOL;
   $file.='    <form id="'.$this->entityName.'_form" name="'.$this->entityName.'_form" method="post" onsubmit="return false" class="form-horizontal info-panel">'.PHP_EOL;
   $file.='      <fieldset>'.PHP_EOL;
   $file.='      <div class="col-lg-12 container epic-title xl mb-5">'.$this->underscoreToTitle($this->entityName).'</div>'.PHP_EOL;
   $file.='      <div class="col-lg-12 container">\'.$objAlerts.\'</div>'.PHP_EOL;
   $file.='      <div class="col-lg-12 container">'.PHP_EOL;
   $file.='         <input type="hidden" id="is_'.$this->entityName.'" name="is_'.$this->entityName.'" value="\'.$'.$this->varName.'->getInitialState().\'">'.PHP_EOL;
   $file.='         <input type="hidden" id="'.$this->entityName.'_id" name="'.$this->entityName.'_id" maxlength="22" value="\'.$'.$this->varName.'->getId().\'">'.PHP_EOL;
   $file.=$hidden.PHP_EOL;
   $file.='      </div>'.PHP_EOL;
   $file.=$form.PHP_EOL;
   $file.=$created_by_show.PHP_EOL;
   $file.='     <div class="col-lg-12 container text-right keypad">\'.$buttons.\'</div>'.PHP_EOL;
   $file.='   </fieldset>'.PHP_EOL;
   $file.='  </form>'.PHP_EOL;
   $file.=' </div>'.PHP_EOL;
   $file.='</div>\';'.PHP_EOL;
   $file.='  echo $html;'.PHP_EOL;
   $file.='?>'.PHP_EOL;

    return $file;

  }


  public function gAuditTrigger(){
    $separator_a=';';
    $separator_b=',\'";"\',';
    $a='"id"';  
    $b='NEW.id';
    $g='OLD.id';   
    $d='   IF NEW.id!=OLD.id THEN'.PHP_EOL;
    $d.='     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,\'"id"\');'.PHP_EOL;
    $d.='     SET p_new_value = CONCAT(p_new_value,v_separator,\'"\', NEW.id,\'"\');'.PHP_EOL;
    $d.='     SET p_old_value = CONCAT(p_old_value,v_separator,\'"\', OLD.id,\'"\');'.PHP_EOL;
    $d.='     SET v_separator=\';\';'.PHP_EOL;
    $d.='   END IF;'.PHP_EOL;
    
    foreach ($this->columns as $c){
      $a.=$separator_a.'"'.$c["column_name"].'"';  
      $b.=$separator_b.'NEW.'.$c["column_name"];
      $g.=$separator_b.'OLD.'.$c["column_name"];   
      $d.='   IF NEW.'.$c["column_name"].'!=OLD.'.$c["column_name"].' THEN'.PHP_EOL;
      $d.='     SET p_affected_columns = CONCAT(p_affected_columns,v_separator,\'"'.$c["column_name"].'"\');'.PHP_EOL;
      $d.='     SET p_new_value = CONCAT(p_new_value,v_separator,\'"\', NEW.'.$c["column_name"].',\'"\');'.PHP_EOL;
      $d.='     SET p_old_value = CONCAT(p_old_value,v_separator,\'"\', OLD.'.$c["column_name"].',\'"\');'.PHP_EOL;
      $d.='     SET v_separator=\';\';'.PHP_EOL;
      $d.='   END IF;'.PHP_EOL;
    }  
    $file='';
    $file.='DELIMITER ;;'.PHP_EOL;
    $file.='CREATE OR REPLACE TRIGGER '.$this->name.'_aui_trg BEFORE INSERT ON '.$this->name.' FOR EACH ROW'.PHP_EOL;
    $file.='  BEGIN'.PHP_EOL.PHP_EOL;
    $file.='   DECLARE p_new_value TEXT; '.PHP_EOL;
    $file.='   DECLARE p_affected_columns TEXT;'.PHP_EOL;
    $file.='   SET p_affected_columns = \''.$a.'\';'.PHP_EOL; 
    $file.='   SET p_new_value = CONCAT(\'"\','.$b.',\'"\');'.PHP_EOL;  
    $file.='   CALL sp_audit_d(\'INSERT\',\''.$this->name.'\',NEW.id,p_affected_columns,NULL,p_new_value);'.PHP_EOL;
    $file.='  END;;'.PHP_EOL;
    $file.='DELIMITER ;'.PHP_EOL.PHP_EOL;
    $file.='DELIMITER ;;'.PHP_EOL;
    $file.='CREATE OR REPLACE TRIGGER '.$this->name.'_aud_trg BEFORE DELETE ON '.$this->name.' FOR EACH ROW'.PHP_EOL;
    $file.='  BEGIN'.PHP_EOL.PHP_EOL;
    $file.='   DECLARE p_old_value TEXT; '.PHP_EOL;
    $file.='   DECLARE p_affected_columns TEXT;'.PHP_EOL;
    $file.='   SET p_affected_columns = \''.$a.'\';'.PHP_EOL; 
    $file.='   SET p_old_value = CONCAT(\'"\','.$g.',\'"\');'.PHP_EOL;  
    $file.='   CALL sp_audit_d(\'DELETE\',\''.$this->name.'\',OLD.id,p_affected_columns,p_old_value,NULL);'.PHP_EOL;
    $file.='  END;;'.PHP_EOL;
    $file.='DELIMITER ;'.PHP_EOL.PHP_EOL;
    $file.='DELIMITER ;;'.PHP_EOL;
    $file.='CREATE OR REPLACE TRIGGER '.$this->name.'_auu_trg BEFORE UPDATE ON '.$this->name.' FOR EACH ROW'.PHP_EOL;
    $file.='  BEGIN'.PHP_EOL.PHP_EOL;
    $file.='   DECLARE p_old_value TEXT;'.PHP_EOL;
    $file.='   DECLARE p_new_value TEXT;'.PHP_EOL;
    $file.='   DECLARE p_affected_columns TEXT;'.PHP_EOL;
    $file.='   DECLARE v_separator VARCHAR(1);'.PHP_EOL;
    $file.='   SET v_separator = \'\';'.PHP_EOL;
    $file.='   SET p_old_value = \'\';'.PHP_EOL;
    $file.='   SET p_new_value = \'\';'.PHP_EOL;
    $file.='   SET p_affected_columns = \'\';'.PHP_EOL;
    $file.=$d.PHP_EOL;
    $file.='   CALL sp_audit_d(\'UPDATE\',\''.$this->name.'\',NEW.id,p_affected_columns,p_old_value,p_new_value);'.PHP_EOL;
    $file.='  END;;'.PHP_EOL;
    $file.='DELIMITER ;'.PHP_EOL.PHP_EOL;

    return $file;
  }
  
  private function gSequence(){
     $q = "INSERT INTO t_sequence (table_name, current_value, created_by, created_date) VALUES(?,0,?,?)";
     $param=array($this->entityName,$this->tfs->getUserId(),date("Y-m-d H:i:s") ); 
     $this->tfs->execute($q,$param);
  }  
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",name \"option\"
            FROM t_component
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
   public static function allEntities(TfSession $tfs){ 
       $q="SELECT CONCAT(table_schema,'.',table_name) \"value\", CONCAT(table_schema,'.',table_name) \"option\"
            FROM information_schema.tables
           WHERE table_schema = ?
           ORDER BY table_schema,table_name"; 
        
        $param=array($tfs->getDatabase());
        $entities = $tfs->executeQuery($q,$param);   
        return $entities;
  }
   
  private function allEntityColumns(){ 
       $q="SELECT c.ordinal_position, 
          c.column_name,
          c.data_type,
          c.column_comment,
          CASE WHEN c.character_maximum_length IS NULL THEN 22 ELSE coalesce(c.character_maximum_length,0) END AS data_length,
          c.numeric_scale as data_scale,
          c.is_nullable as notnull,
          referenced_table_name,
          constraint_name,
          referenced_table_name,
          referenced_column_name
     FROM information_schema.columns c 
          LEFT JOIN information_schema.key_column_usage f 
                 ON f.table_schema =f.referenced_table_schema
                AND f.table_name !=f.referenced_table_name
                AND f.table_schema = c.table_schema
                AND f.table_name = c.table_name
                AND f.column_name = c.column_name
    WHERE c.column_name !='id' 
      AND c.table_schema = ?
      AND c.table_name = ?
    ORDER BY c.ordinal_position"; 
        
        $param=array($this->schema,$this->entityName);
        $this->columns = $this->tfs->executeQuery($q,$param); 

  }

  private function FKColumnDesc($referenced_table_name){ 
   $q="SELECT c.column_name
         FROM information_schema.columns c 
        WHERE c.table_schema = ?
          AND c.table_name = ?
          AND (position('name' in c.column_name)>0 OR
               position('desc' in c.column_name)>0 OR
               position('description' in c.column_name)>0)
      LIMIT 1"; 
    
    $param=array($this->schema,$referenced_table_name);
    list($rs) = $this->tfs->executeQuery($q,$param);  

    return $rs["column_name"];

  }




  public static function selectOptionsFilter(TfSession $tfs,$value,$filter){ 
    $filter_mask = "%".html_entity_decode(str_replace(" ","%",$filter))."%";
    if (isset($value)&&($value!='')){
      if (isset($filter)){
         $q = "SELECT id \"value\",name \"option\"
                 FROM t_component
                WHERE (TRANSLATE(UPPER(name),'ÁÉÍÓÚÀÈÌÒÙ','AEIOUAEIOU') LIKE  TRANSLATE(UPPER(?),'ÁÉÍÓÚÀÈÌÒÙ','AEIOUAEIOU'))
                UNION
               SELECT id \"value\",name \"option\"
                 FROM t_component
                WHERE id = ?";
         $param = array($filter_mask,$value);
      }else{
         $q = "SELECT id \"value\",name \"option\"
                 FROM t_component
                WHERE id = ?";
         $param = array($value);
      }
    }else{
      $q = "SELECT id \"value\",name \"option\"
              FROM t_component
             WHERE (TRANSLATE(UPPER(name),'ÁÉÍÓÚÀÈÌÒÙ','AEIOUAEIOU') LIKE  TRANSLATE(UPPER(?),'ÁÉÍÓÚÀÈÌÒÙ','AEIOUAEIOU'))";
      $param = array($filter_mask);
    }
    $rs = $tfs->executeQuery($q,$param);
    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT name description
            FROM t_component
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.name id_component_type,
                 a.name,
                 a.description,
                 a.active,
                 c.name created_by,
                 a.created_date
            FROM t_component a,
                 t_component_type b,
                 t_user c
           WHERE c.id=a.created_by
             AND b.id = a.id_component_type";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function entityInfo(TfSession $tfs,$table_name){ 
    $q = "SELECT CONCAT(table_schema,'.',table_name) table_name,table_comment 
             FROM information_schema.tables
           WHERE CONCAT(table_schema,'.',table_name) = ?";
    $param = array($table_name);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs;
  }

  public static function entityColumnsInfo(TfSession $tfs,$table_name){ 
       $q="SELECT c.ordinal_position, 
          c.column_name,
          c.data_type,
          c.column_comment,
          CASE WHEN c.character_maximum_length IS NULL THEN 22 ELSE coalesce(c.character_maximum_length,0) END AS data_length,
          c.numeric_scale as data_scale,
          c.is_nullable as is_nullable,
          constraint_name,
          referenced_table_name,
          referenced_column_name
     FROM information_schema.columns c 
          LEFT JOIN information_schema.key_column_usage f 
                 ON f.table_schema =f.referenced_table_schema
                AND f.table_name !=f.referenced_table_name
                AND f.table_schema = c.table_schema
                AND f.table_name = c.table_name
                AND f.column_name = c.column_name
    WHERE CONCAT(c.table_schema,'.',c.table_name) = ?
    ORDER BY c.ordinal_position"; 
        
        $param=array($table_name);
        $columns = $tfs->executeQuery($q,$param);  
        return $columns;

  }

}
?>
