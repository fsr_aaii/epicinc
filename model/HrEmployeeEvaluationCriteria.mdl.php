<?php
  class HrEmployeeEvaluationCriteria extends HrEmployeeEvaluationCriteriaBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_employee_evaluation"=>true,
                              "id_evaluation_criteria"=>true,
                              "value"=>true,
                              "date"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\", \"option\"
            FROM hr_employee_evaluation_criteria
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT  description
            FROM hr_employee_evaluation_criteria
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.id_employee_evaluation,
                 a.id_evaluation_criteria,
                 a.value,
                 a.date,
                 c.name created_by,
                 a.created_date
            FROM hr_employee_evaluation_criteria a,
                 hr_employee_evaluation b,
                 t_user c
           WHERE b.id = a.id_employee_evaluation
           AND c.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

  public static function listByEmployeeEvaluation(TfSession $tfs,$idEmployeeEvaluation){
    $q = "SELECT a.id,
                 t.name 'type',b.name 'group',c.description criteria, 
                 a.value,
                 a.date,
                 a.created_by,
                 a.created_date
            FROM hr_employee_evaluation_criteria a,
                 hr_evaluation_type t,
                 hr_evaluation_group b,
                 hr_evaluation_criteria c
           WHERE t.id=b.id_evaluation_type
             AND b.id=c.id_evaluation_group
             AND c.id = a.id_evaluation_criteria
             AND a.id_employee_evaluation =?
             ORDER BY t.id,b.id,c.id";
     $param = array($idEmployeeEvaluation);        
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
