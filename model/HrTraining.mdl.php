<?php
  class HrTraining extends HrTrainingBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "id_employee"=>true,
                              "id_position"=>true,
                              "id_training_type"=>true,
                              "name"=>true,
                              "description"=>true,
                              "hour"=>true,
                              "estimated_start_date"=>true,
                              "start_date"=>true,
                              "completion_estimated_date"=>true,
                              "completion_date"=>true,
                              "id_epic_priority"=>true,
                              "id_epic_status"=>true,
                              "status_date"=>true,
                              "progress"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",description \"option\"
            FROM hr_training
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT description description
            FROM hr_training
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 b.successor_name id_employee,
                 a.id_position,
                 d.name id_training_type,
                 a.name,
                 a.description,
                 a.estimated_start_date,
                 a.start_date,
                 a.completion_estimated_date,
                 a.completion_date,
                 e.name id_epic_priority,
                 f.name id_epic_status,
                 a.status_date,
                 a.progress,
                 a.active,
                 g.name created_by,
                 a.created_date
            FROM hr_training a,
                 hr_employee b,
                 hr_position c,
                 hr_training_type d,
                 hr_epic_priority e,
                 hr_epic_status f,
                 t_user g
           WHERE b.id = a.id_employee
           AND c.id = a.id_position
           AND d.id = a.id_training_type
           AND e.id = a.id_epic_priority
           AND f.id = a.id_epic_status
           AND g.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }

   public static function listByEmployeeQuarter(TfSession $tfs,$idEmployee,$idTrainingType,$quarter){
    
    $q = "SELECT a.id,
                 a.name,
                 a.description,
                 a.hour,
                 a.completion_estimated_date,
                 a.id_epic_priority,
                 a.id_epic_status,
                 e.name priority,
                 f.name status,
                 a.status_date,
                 a.progress
            FROM hr_training a,
                 hr_epic_priority e,
                 hr_epic_status f
           WHERE e.id = a.id_epic_priority
           AND f.id = a.id_epic_status
           AND QUARTER(a.created_date)=?
           AND a.id_training_type = ?
           AND a.id_employee = ?";
    $param = array($quarter,$idTrainingType,$idEmployee);
       
    $rs = $tfs->executeQuery($q,$param);

    return $rs;
  }
}
?>
