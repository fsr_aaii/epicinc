<?php
  class HrDisc extends HrDiscBase {

  public function __construct(TfSession $tfs){ 
    parent::__construct($tfs);
    $this->updateable = array("id"=>false,
                              "name"=>true,
                              "active"=>true,
                              "created_by"=>true,
                              "created_date"=>true);

  }
  public static function selectOptions(TfSession $tfs){ 
    $q = "SELECT id \"value\",name \"option\"
            FROM hr_disc
           ORDER BY 2";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
  public static function description(TfSession $tfs,$id){ 
    $q = "SELECT name description
            FROM hr_disc
           WHERE id=?";
    $param = array($id);
    list($rs) = $tfs->executeQuery($q,$param);

    return $rs["description"];
  }
  public static function dataList(TfSession $tfs){
    $q = "SELECT a.id,
                 a.name,
                 a.active,
                 b.name created_by,
                 a.created_date
            FROM hr_disc a,
                 t_user b
           WHERE b.id = a.created_by";
    $rs = $tfs->executeQuery($q);

    return $rs;
  }
}
?>
