<!DOCTYPE html>
<html lang="en">

<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/png" href="<?php echo TUICH_URL_ROOT.'/asset/images/shortcut.png';?>">
  <title>Guaramo</title>

  <link rel="stylesheet" type="text/css" href="<?php echo TUICH_URL_ROOT.'/vendor/bootstrap-4.1.3/css/bootstrap.min.css';?>">
  <link rel="stylesheet" type="text/css" href="<?php echo TUICH_URL_ROOT.'/asset/css/style.css';?>">

  <style>
* {
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}

body {
  padding: 0;
  margin: 0;
  text-align: left;
}



#message .message {
  left: 50%;
}

.message {
  width: 100%;
  line-height: 1.4;
  padding: 0 80px;
  text-align: left;
}

.message-code {
  text-align: center;
}


.message-code img{
  width:12rem;
  margin-top: 30px;
  margin-bottom: 10px;
}

.message-code .code{
  font-family: 'Lato', sans-serif;
  font-size: 1.2rem;
  font-weight: bold;
  color: #1fbad6;
  margin-top: 0px;
  margin-bottom: 25px;
}




.message .body {
  font-family: 'Lato', sans-serif;
  font-size: 1rem;
  font-weight: 400;
  color: #a9a9a9;
  margin-top: 0px;
  margin-bottom: 25px;
}

.message .body p{
  font-family: 'Lato', sans-serif;
  font-size: 1rem;
  font-weight: 400;
  color: #a9a9a9;
  margin-top: 0px;
  margin-bottom: 25px;
  text-align: left;
}

.message .body pre{
  text-align: left;
}

  </style>


</head>

<body>
  <div id="message" class="col-lg-8">
    <div class="message" class="col-lg-12">
      <div class="message-code">
        <img src="<?php echo TUICH_URL_ROOT.'/asset/images/logo.png';?>"  alt="gtk">
        <div class="code"><?php echo $t->getCode();?> Exception</div>
      </div>
      
      <div class="body"><?php echo $t->getMessage();?></div>   
      <div class="body"><?php echo $t->getFile()." on line ".$t->getLine();?></div>  
      
    </div>
  </div>
</body>

</html>



