function tfValidate(b){
  var a;
  a=$(b).valid();

  return a;
};


function tfParseDate(s) {
  var b = s.split(/\D/);
  return new Date(b[2], --b[1], b[0]);
};


function confirmDelete(){ 
    if (confirm('Are you sure you want to save this information?')){ 
      return true;
    }else{
    return false; 
  }
} 
function confirmUpdate(){ 
    if (confirm('Are you sure you want to update this information?')){ 
      return true;
    }else{
    return false; 
  }
} 


 $.validator.setDefaults({
        ignore: []
    });
 
$.validator.addMethod("isRifN",function (value, element) {
 return this.optional(element) || value.match(/^[VvEe]-\d{8}-[0-9]$/);
},
"The field does not match the format V-00000000-0");

$.validator.addMethod("isRifJ",function (value, element) {
 return this.optional(element) || value.match(/^[JG]-\d{8}-[0-9]$/);
},
"The field does not match the format J-00000000-0");


$.validator.addMethod("isDateTime",function (value, element) {
  if (tfParseDate(value)) {
    return this.optional(element) || value.match(/^[0-9]{4}-[0-1][0-9]-[0-3][0-9] [0-1][0-9]:[0-5][0-9]:[0-5][0-9]$/);
  }else{
  return false;   
  }
},"Not match the format yyyy-mm-dd hh:mi:ss");

$.validator.addMethod("dateTime",function (value, element) {
  if (tfParseDate(value)) {
    return this.optional(element) || value.match(/^[0-9]{4}-[0-1][0-9]-[0-3][0-9] [0-1][0-9]:[0-5][0-9]$/);
  }else{
  return false;   
  }
},"Not match the format yyyy-mm-dd hh:mi");

$.validator.addMethod("isDate",function (value, element) {
  if (tfParseDate(value)) {
    return this.optional(element) || value.match(/^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/);
  }else{
  return false;   
  }
},"Not match the format yyyy-mm-dd");


$.validator.addMethod("dateLessEqualThanNow",function(value, element) {
  var now = new Date();
  now.setHours(0,0,0,0); 
  return this.optional(element) || tfParseDate(value) <= now;
  
},"The date must be less than or equal a now");

$.validator.addMethod("dateGreaterEqualThanNow",function(value, element) {
  var now = new Date();
  now.setHours(0,0,0,0); 
  return this.optional(element) || tfParseDate(value) >= now;
  
},"The date must be greater than or equal a now");


$.validator.addMethod("dateLessThanNow",function(value, element) {
  var now = new Date();
  now.setHours(0,0,0,0); 
  return this.optional(element) || tfParseDate(value) < now;
  
},"The date must be less than now");

$.validator.addMethod("dateGreaterThanNow",function(value, element) {
  var now = new Date();
  now.setHours(0,0,0,0); 
  return this.optional(element) || tfParseDate(value) > now;
  
},"The date must be greater than now");


$.validator.addMethod("dateLessEqualThan",function(value, element, params) {

  return this.optional(element) || tfParseDate(value) <= tfParseDate($(params).val());
  
},"The date must be less than or equal next date");


$.validator.addMethod("dateGreaterEqualThan",function(value, element, params) {
                            
  return this.optional(element) || this.optional(element) || tfParseDate(value) >= tfParseDate($(params).val());
  
},"The date must be greater than or equal previous date");


$.validator.addMethod("dateLessThan",function(value, element, params) {
  
  var date = $(params).val();
  if (tfParseDate(date) == 'Invalid Date') {
    return true;
  }else{
    return this.optional(element) || tfParseDate(value) < tfParseDate($(params).val());
  } 
  
},"The date must be less than next date");


$.validator.addMethod("dateGreaterThan",function(value, element, params) {
                            
  var date = $(params).val();
  if (tfParseDate(date) == 'Invalid Date') {
    return true;
  }else{
    return this.optional(element) || tfParseDate(value) > tfParseDate(date);
  }  
},"The date must be greater than previous date");


/**
 * Custom validator for contains at least one lower-case letter
 */
$.validator.addMethod("atLeastOneLowercaseLetter", function (value, element) {
    return this.optional(element) || /[a-z]+/.test(value);
}, "Must have at least one lowercase letter");
 
/**
 * Custom validator for contains at least one upper-case letter.
 */
$.validator.addMethod("atLeastOneUppercaseLetter", function (value, element) {
    return this.optional(element) || /[A-Z]+/.test(value);
}, "Must have at least one uppercase letter");
 
/**
 * Custom validator for contains at least one number.
 */
$.validator.addMethod("atLeastOneNumber", function (value, element) {
    return this.optional(element) || /[0-9]+/.test(value);
}, "Must have at least one number");
 
/**
 * Custom validator for contains at least one symbol.
 */
$.validator.addMethod("atLeastOneSymbol", function (value, element) {
    return this.optional(element) || /[!@#$%^&*()]+/.test(value);
}, "Must have at least one symbol");


$.validator.addMethod("time24", function(value, element) {
    if (!/^\d{2}:\d{2}:\d{2}$/.test(value)) return false;
    var parts = value.split(':');
    if (parts[0] > 23 || parts[1] > 59 || parts[2] > 59) return false;
    return true;
}, "Invalid time format.");

$(function(){
$.mask.definitions['n'] = "[0-9]";
$.mask.definitions['d'] = "[0-3]";
$.mask.definitions['m'] = "[0-1]";
$.mask.definitions['y'] = "[1-2]";
$.mask.definitions['R'] = "[JjGgCc]";
$.mask.definitions['r'] = "[VvEePp]";
$.mask.definitions['~'] = "([0-9])?";
$.mask.definitions['h'] = "[0-2]";
$.mask.definitions['i'] = "[0-5]";
$.mask.definitions['s'] = "[0-5]";
 });



