class TfRequest{
	
	static login(jsonObject){
		let JDATA = new Object();
		let cryptography  = new TfCryptography();
          $.ajax({
		    data: {"request-data": cryptography.encryptRSA(jsonObject)},
		    url: window.location.href+"/api/auth",    
		    type: 'POST',
		    async : false,
			 success:function(response){
			    try {
			    	//console.log(response);
			        JDATA=JSON.parse(response);  
			    } catch (e) {
			    	console.log('Exception ');
			    }     
		                
			},
			error: function(response) {	
				JDATA=JSON.parse(response.responseText);  
		    }
	    });

	    return JDATA;
	}
  
    static nav(){
    	let jsonObject = new Object();
		let JDATA = new Object();
        let cryptography  = new TfCryptography();
        window.RANDOM_PUBLIC_KEY = cryptography.randomPublicKey(8);
	    jsonObject.randomPublicKey = window.RANDOM_PUBLIC_KEY;
         $.ajax({
		    data: {"request-data": cryptography.encryptRSA(jsonObject)},
		    url: window.location.href+"/api/nav",    
		    type: 'POST',
		    async : false,
			 success:function(response){
			 	let JDATA=JSON.parse(response);
			 	let cryptography  = new TfCryptography();
			 	window.TF_TRAIL_ID = cryptography.decryptResponse(JDATA.tfTrailId.value, window.RANDOM_PUBLIC_KEY);
			    $('#main-menu-ul').html(cryptography.decryptResponse(JDATA.tfMenu.value, window.RANDOM_PUBLIC_KEY));

		                
			},
			error: function(response) {	
				JDATA=JSON.parse(response.responseText);  
		    }
	    });

	    return JDATA;
	}

	

	static menu(element){
		$('#tf-container').addClass("spinner");
		$('.menu-link').removeClass("active");  
	    $('#'+element.id).addClass("active"); 
	    sessionStorage.setItem('profile', 'null');
	    sessionStorage.setItem('quarter', 'null');
	    sessionStorage.setItem('section', 'null');
	    //console.log(element.id); 
		window.TF_TASK_ID = element.dataset.tfTaskId;
		let url=window.location.href+"/"+window.TF_TRAIL_ID+"/view/"+element.dataset.tfTaskId+"/"+element.dataset.tfController+"/"+element.dataset.tfAction;
         let data;
        $.ajax({
		    data: {"request-data": data},
		    url: url,    
		    type: 'POST',
		    async : false,
			success:function(response){	
				 try {
			    	$('#tf-container').removeClass("spinner");   
			        let JDATA=JSON.parse(response);
			        let cryptography  = new TfCryptography();
                    $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, window.RANDOM_PUBLIC_KEY));
                
			    } catch (e) {
			    	console.log(e);
			    	console.log(JDATA.tfContent.value);
			    	console.log(window.RANDOM_PUBLIC_KEY);
			    }     
			}
	    });
	} 
	
	static post(element){
	
		let url=window.location.href+"/"+window.TF_TRAIL_ID+"/view/"+element.dataset.tfTaskId+"/"+element.dataset.tfController+"/"+element.dataset.tfAction;
        let data = element.dataset.tfData;
        let form = element.dataset.tfForm;
        let go = true;
        
        if (typeof data == 'undefined') {
          let jsonObject = new Object();
          if (typeof form != 'undefined') {
          	if ($(form).valid()){
             jsonObject = $(form).serializeJSON();   
            }else{
            	go = false;
            } 
          }
          let cryptography  = new TfCryptography();
         
	      data = cryptography.encryptJSON(jsonObject,window.RANDOM_PUBLIC_KEY);
        }
        //console.log(data);
        if (go){
            $.ajax({
			    data: {"request-data": data},
			    url: url,    
			    type: 'POST',
			    async : false,
				success:function(response){	
				   let JDATA=JSON.parse(response);
			       let cryptography  = new TfCryptography();
                   $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, window.RANDOM_PUBLIC_KEY));
				}
	        });
        }
    }

   static fn(jsonObject){
	
		let url=window.location.href+"/fn/"+jsonObject.tfTaskId+"/"+jsonObject.tfController+"/"+jsonObject.tfFnName;
        let cryptography  = new TfCryptography();
         
	    let data = cryptography.encryptJSON(jsonObject.tfData,window.RANDOM_PUBLIC_KEY);
        var r;
        $.ajax({
		    data: {"request-data": data},
		    url: url,    
		    type: 'POST',
		    async : false,
			success:function(response){	
			   let JDATA=JSON.parse(response);
               r = cryptography.decryptResponse(JDATA.tfContent.value, window.RANDOM_PUBLIC_KEY);        
			}
	    });

	    return r;
        
    }

    static redirect(tfTaskId,tfController,tfAction,tfData){
	
		let url=window.location.href+"/"+window.TF_TRAIL_ID+"/view/"+tfTaskId+"/"+tfController+"/"+tfAction;
        $.ajax({
		    data: {"request-data": tfData},
		    url: url,    
		    type: 'POST',
		    async : false,
			success:function(response){	
			   $('#tf-container').removeClass("spinner");   
			 let JDATA=JSON.parse(response);
		    let cryptography  = new TfCryptography();
            //console.log(cryptography.decryptResponse(JDATA.tfContent.value, window.RANDOM_PUBLIC_KEY));
            if (JDATA.hasOwnProperty('tfContent')){
                $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, window.RANDOM_PUBLIC_KEY));
			   }else{
			   	TfRequest.home();
			   } 


			}
	     });
    }

    static home(){
	
		let url=window.location.href+"/"+window.TF_TRAIL_ID+"/home";
        $.ajax({
		    data: {"request-data": ""},
		    url: url,    
		    type: 'POST',
		    async : false,
			success:function(response){	
			   let JDATA=JSON.parse(response);
		       let cryptography  = new TfCryptography();
               $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, window.RANDOM_PUBLIC_KEY));
			}
	     });
    }

    static refresh(){
	
		let url=window.location.href+"/"+window.TF_TRAIL_ID+"/refresh";
        $.ajax({
		    data: {"request-data": ""},
		    url: url,    
		    type: 'POST',
		    async : false,
			success:function(response){	
			   //console.log(response); 	
			   let JDATA=JSON.parse(response);
		       let cryptography  = new TfCryptography();
               $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, window.RANDOM_PUBLIC_KEY));
			}
	     });
    }

    static previous(){
	
		let url=window.location.href+"/"+window.TF_TRAIL_ID+"/previous";
        $.ajax({
		    data: {"request-data": ""},
		    url: url,    
		    type: 'POST',
		    async : false,
			success:function(response){	
			   //console.log(response); 	
			   let JDATA=JSON.parse(response);
		       let cryptography  = new TfCryptography();
               $('#tf-container').html(cryptography.decryptResponse(JDATA.tfContent.value, window.RANDOM_PUBLIC_KEY));
			}
	     });
    }

    static logout(){
	
		let url=window.location.href+"/"+window.TF_TRAIL_ID+"/logout";
        $.ajax({
		    data: {"request-data": ""},
		    url: url,    
		    type: 'POST',
		    async : false,
			success:function(response){	
			   window.history.back();
			}
	     });
    }
    
    static do(element,confirm=false){
		if (confirm){
		   jConfirm('Are you sure you want to process this information?','Continue?',function(r){
		   	   if(r){
		   	   	TfRequest.post(element);
		   	   }
		   });
		}else{
          TfRequest.post(element);
		}
	}

    static api(url,jsonObject){
		let JDATA = new Object();
        let cryptography  = new TfCryptography();
         $.ajax({
		    data: {"request-data": cryptography.encryptJSON(jsonObject)},
		    url: url,    
		    type: 'POST',
		    async : false,
			 success:function(response){
			    try {
			    	 JDATA=JSON.parse(response);  
                
			        $.each(JDATA, function(i, item) {
			        	if (JDATA[i].encrypt){
	                       JDATA[i].value=cryptography.decryptResponse(JDATA[i].value,window.RANDOM_PUBLIC_KEY);
			        	}
	               
	                });
			    } catch (e) {
			    	console.log('Exception 69');
			    }     
		                
			},
			error: function(response) {	
				JDATA=JSON.parse(response.responseText);  
		    }
	    });

	    return JDATA;
	}
	
}