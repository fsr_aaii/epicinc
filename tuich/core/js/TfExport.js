class TfExport{
	
	static excel(element){
    let dt = new Date();
    TableToExcel.convert(document.querySelector(element.dataset.tfTable),element.dataset.tfFile+" "+dt.getFullYear()+dt.getMonth()+dt.getDate()+dt.getHours()+dt.getMinutes());
  }  
}  