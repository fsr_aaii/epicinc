<?php

class TfResponse {

  private $content = array();    
  private $encryption;
  private $random_public_key;

  public function __construct(){ 
    header_remove();
    header("Cache-Control: no-transform,public,max-age=300,s-maxage=900"); 
    $this->content["status"]=200;
    $this->encryption = new Encryption();  
  }  
  public function setRandomPublicKey($value){
    $this->random_public_key = $value;
  }  
  public function set($key,$value,$encrypt=false){
    if ($encrypt){
      $this->content[$key]["encrypt"]="true";
      if (is_array($value)){
        $this->content[$key]["array"]="true";
        $this->content[$key]["value"]=$this->encryption->encrypt(json_encode($value,true),$this->random_public_key);
      }else{
        $this->content[$key]["value"]=$this->encryption->encrypt($value,$this->random_public_key);
      }      
    }else{
        $this->content[$key]["array"]="false";
        $this->content[$key]["encrypt"]="false";
        $this->content[$key]["value"]=$value;
    }
  }

  public function encrypt(array $request){ 
    return $this->encryption->encrypt(json_encode($request,true),$this->random_public_key);
  }  
  public function return($code=200){
    http_response_code($code);
    echo json_encode($this->content,true);    
  }

  public static function call($filePath){
    if (is_file($filePath)){
      http_response_code(200);
      require($filePath);
    }else{
      throw new TfException("Not view found",7201,404);    
    }
       
  }
  
  public static function controller($tfRequestController,$tfRequestAction){
    $path=TF_REQUIRE_PATH . '/controller/'.$tfRequestController.'.ctrl.php';
    if (is_file($path)){
      require($path);
    }else{
      throw new TfException("Not controller found",7201,404);    
    }
       
  }

  public static function view($tfRequestView){
    $path=TF_REQUIRE_PATH . '/view/'.$tfRequestView.'.html';
    if (is_file($path)){
      http_response_code(200);
      require($path);
    }else{
      throw new TfException("Not view found",7201,404);    
    }
       
  }
  

  private function recursive(&$item, $key) {
    $item = $this->encryption->encrypt($item,$this->random_public_key);
  } 


   public function obEncrypt($buffering) {
    $response["data"] = $this->encryption->encrypt($buffering,$this->random_public_key);
    return json_encode($response,true);
  } 

  public function exception($t){
    if (is_a($t,'TfException')){
      http_response_code($t->getHttpCode());
    }else{
      http_response_code(500);
    }
    $this->content["status"]=http_response_code();
    $this->content["exception"]["code"]=$t->getCode();
    $this->content["exception"]["message"]=$t->getMessage();
    $this->content["exception"]["file"]=pathinfo($t->getFile(), PATHINFO_FILENAME);           
    $this->content["exception"]["line"]=$t->getLine();      
   
    echo json_encode($this->content,false);     
  }
 
}
