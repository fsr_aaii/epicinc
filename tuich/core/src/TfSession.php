<?php

class TfSession extends TfConnect { 

  private $id;
  private $task_id;
  private $action;
  private $breadcrumb; 
  private $breadcrumb_trail_id;
  private $random_public_key;

  private $session_id;
  private $authentication_type;
  private $functional_area_code;
  private $functional_area_id;
  private $user_id;
  private $user_login;
  private $user_password;
  private $user_password_date;
  private $user_name;
  private $user_email_account;


  private $sessionParameters;
  private $sessionRedirect=array();
  private $sessionReadonly=false;
  
  
  private $JWTChangePwd; 

  public function __construct($database){      
    parent::__construct($database);
  }  

  public function start(){ 
    $this->connect();
    $this->authentication();
    if ($this->state=='Start'){
      $this->setSessionVariable();
    }
  }
  
  private function authentication(){ 

    $this->session_id=session_id();

    switch ($this->authentication_type){
      case "Login":
         $q = "SELECT u.id,u.login,u.name,u.email_account,
                      u.password,u.password_date,f.id id_functional_area
                  FROM t_user u,
                       t_user_role c,
                       t_functional_area_role r,
                       t_functional_area f
                 WHERE f.code=?
                   AND f.active='Y'
                   AND f.id=r.id_functional_area
                   AND r.active='Y'
                   AND r.id=c.id_functional_area_role
                   AND c.active='Y' 
                   AND c.id_user = u.id
                   AND u.login=?
                   AND u.active='Y'";             
        
        $param = array($this->functional_area_code,$this->user_login);

        list($rs) = $this->executeQuery($q,$param);       
        
        if (isset($rs["id"])&&$rs["id"]!=""){
           $this->user_id = $rs["id"];  
           $this->user_login = $rs["login"];
           $this->user_email_account = $rs["email_account"];
           $this->user_name = $rs["name"]; 
           $this->functional_area_id = $rs["id_functional_area"];
           $this->user_password = $rs["password"]; 
           $this->user_password_date = $rs["password_date"]; 


           $this->id=$this->sequence();
               
           $q = "INSERT INTO t_session(id,id_session,id_user,
                                                  id_user_device,
                                                  id_http_user_agent,start_timestamp,
                                                  type,breadcrumb,
                                                  id_functional_area)
                       VALUES (?,?,?,?,?,?,'W',?,?)";


           $param = array($this->id,$this->session_id,
                          $this->user_id,$_SERVER["REMOTE_ADDR"],
                          $_SERVER["HTTP_USER_AGENT"],date("Y/m/d"),
                          json_encode(array()),
                          $this->functional_area_id);
          
           $this->execute($q,$param);
       
        }else{
          throw new TfException("The username or password is incorrect",'TF001',401); 
        }      
       break;
      case "Password":
          $q = "SELECT u.id,u.login,u.name,u.email_account,f.id id_functional_area
                  FROM t_user u,
                       t_user_role c,
                       t_functional_area_role r,
                       t_functional_area f
                 WHERE f.code=?
                   AND f.active='Y'
                   AND f.id=r.id_functional_area
                   AND r.active='Y'
                   AND r.id=c.id_functional_area_role
                   AND c.active='Y' 
                   AND c.id_user = u.id 
                   AND u.password=?
                   AND u.login=?
                   AND u.active='Y'";

          $param = array($this->functional_area_code,$this->user_password,$this->user_login);
          
          list($rs) = $this->executeQuery($q,$param);
        if (isset($rs["id"])&&$rs["id"]!=""){
           $this->user_id = $rs["id"];  
           $this->user_login = $rs["login"];
           $this->user_email_account = $rs["email_account"];
           $this->user_name = $rs["name"]; 
           $this->functional_area_id =$rs["id_functional_area"];
            
           $q = "SELECT s.id,s.id_user,u.login,u.name,
                           s.breadcrumb,u.email_account
                      FROM t_session s,
                           t_user u
                     WHERE u.active='Y'
                       AND u.id=s.id_user
                       AND s.id_session = ?
                       AND s.id_user_device = ?
                       AND s.id_http_user_agent = ?
                       AND s.start_timestamp = ?
                       AND s.id_user = ?
                       AND s.id_functional_area = ?
                       AND s.logout = 'N'
                       AND s.type = 'W'";

         
            $param=array($this->session_id,$_SERVER["REMOTE_ADDR"],
                     $_SERVER["HTTP_USER_AGENT"],date("Y/m/d"),
                     $this->user_id,$this->functional_area_id);

            list($rs) = $this->executeQuery($q,$param);   
            if (is_array($rs)){
              $this->id = $rs["id"];
              $this->breadcrumb = json_decode($rs["breadcrumb"],true); 
            }else{
             

               $this->id=$this->sequence();
               
               $q = "INSERT INTO t_session(id,id_session,id_user,
                                                  id_user_device,
                                                  id_http_user_agent,start_timestamp,
                                                  type,breadcrumb,
                                                  id_functional_area)
                       VALUES (?,?,?,?,?,?,'W',?,?)";


               $param = array($this->id,$this->session_id,
                              $this->user_id,$_SERVER["REMOTE_ADDR"],
                              $_SERVER["HTTP_USER_AGENT"],date("Y/m/d"),
                              json_encode(array()),
                              $this->functional_area_id);
          
               $this->execute($q,$param);
            } 
          }else{
            throw new TfException("The username or password is incorrect",7100,401); 
          }   
      break;
      case "Token":  
        $q = "SELECT u.id,u.login,u.name,u.email_account,
                      s.breadcrumb,f.id id_functional_area,
                      s.random_public_key
                  FROM t_user u,
                       t_user_role c,
                       t_functional_area_role r,
                       t_functional_area f,
                       t_session s
                 WHERE s.id_session = ?
                   AND s.id_user_device = ?
                   AND s.id_http_user_agent = ?
                   AND s.start_timestamp = ?
                   AND s.id_functional_area = f.id
                   AND s.logout = 'N'
                   AND s.type = 'W'
                   AND s.id_user=u.id
                   AND s.id_user = u.id
                   AND s.id = ?
                   AND f.code=?
                   AND f.active='Y'
                   AND f.id=r.id_functional_area
                   AND r.active='Y'
                   AND r.id=c.id_functional_area_role
                   AND c.active='Y' 
                   AND c.id_user = u.id
                   AND u.active='Y'";              
         
        $param=array($this->session_id,$_SERVER["REMOTE_ADDR"],
                     $_SERVER["HTTP_USER_AGENT"],date("Y/m/d"),
                     $this->id,$this->functional_area_code);
        
        list($rs) = $this->executeQuery($q,$param);                    

        if (count($rs)>0){
          $this->user_id = $rs["id"]; 
          $this->user_login = $rs["login"];
          $this->user_name = $rs["name"]; 
          $this->user_email_account = $rs["email_account"];
          $this->breadcrumb = json_decode($rs["breadcrumb"],true); 
          $this->functional_area_id =$rs["id_functional_area"];
          $this->random_public_key = $rs["random_public_key"];

        }else{
          throw new TfException("Invalid Access Tokens",7101,401); 
        }

      break;
      case "jwt":
         $jwt = JWT::decode($this->JWTChangePwd,TUICH_SECRET, array('HS256'));
         

         $this->user_id=$jwt->data->tsui;
         $this->user_login=$jwt->data->tsul;         
         $this->user_password=$jwt->data->tsup;
         $this->user_password_date=$jwt->data->tsupd;
         $this->user_email_account=$jwt->data->tsum;


         $q = "SELECT u.id,u.login,u.name
                     FROM t_user u
                     WHERE u.id=?
                       AND u.login=?
                       AND u.password=?
                       AND u.password_date=?
                       AND u.email_account=?
                       AND u.active='Y'";
        
        $param = array($this->user_id,$this->user_login,$this->user_password,
                       $this->user_password_date,$this->user_email_account);
         
        list($rs) = $this->executeQuery($q,$param);       


        if (isset($rs["id"])&&$rs["id"]!=""){
            $this->user_name = $rs["name"];   
         }else{
            $this->state='Failed';
         }

       break;
       
    }


  }  

   private function setSessionVariable(){    
    $q="SET @t_session_id = ?,
            @session_id = ?,
            @user_id = ?,
            @user_login = ?,
            @task_id = ?,
            @current_page = ?,
            @client_ip = ?";

    $param=array($this->id,$this->php_session_id,
                 $this->user_id,$this->user_login,
                 $this->task_id,$this->currentPage(),
                 $this->clientIP());
    $this->execute($q,$param);      
  } 
 
public function menu(){

   $q ="SELECT DISTINCT m.menu,coalesce(m.tag,t.name) task,
                t.id task_id,
                c.name controller,
                tc.action,
                LOWER(t.id_task_function) class
                  FROM t_user_role rdu,
                       t_functional_area_role rd,
                       t_functional_area l,
                       t_role r,
                       t_process_task_role ptrd,
                       t_process_task pt,
                       t_process p,
                       t_module_process ap,
                       t_task_component tc,
                       t_task t LEFT JOIN (SELECT mt.id_task,m.name menu,m.sorting m_sorting,
                                                  mt.sorting t_sorting,mt.tag
                                            FROM t_menu m,
                                                 t_menu_task mt
                                           WHERE mt.active = 'Y'
                                             AND mt.id_menu = m.id
                                             AND m.active='Y') m ON m.id_task =t.id,
                       t_component c,
                       t_module a
                  WHERE a.active='Y'
                   AND a.id=ap.id_module
                   AND t.active='Y'
                   AND t.id_task_function != 'S'
                   AND t.id = tc.id_task
                   AND c.active='Y'
                   AND c.id = tc.id_component
                   AND tc.active='Y'
                   AND tc.is_main = 'Y'
                   AND tc.id_task = pt.id_task
                   AND ap.active='Y'
                   AND ap.id_process = p.id
                   AND p.active='Y'
                   AND p.id = pt.id_process
                   AND pt.active='Y'
                   AND pt.id = ptrd.id_process_task
                   AND ptrd.active='Y'
                   AND ptrd.id_role = r.id
                   AND r.active = 'Y'
                   AND r.id = rd.id_role
                   AND rd.active = 'Y'
                   AND l.code=?
                   AND l.active='Y'
                   AND l.id=rd.id_functional_area
                   AND rd.id = rdu.id_functional_area_role
                   AND rdu.active = 'Y' 
                   AND rdu.id_user = ?
                ORDER BY coalesce(m.m_sorting,t.sorting),CASE WHEN t.id_task_function = 'T' THEN 100
                    WHEN t.id_task_function = 'R' THEN 200 
                    ELSE 300 END + m.t_sorting";
    
  $param = array($this->functional_area_code,$this->user_id);  
  $rs = $this->executeQuery($q,$param); 
  
  
 $html=''; 

  foreach ($rs as $item) {
    $html.='<li class="nav-item">
               <a id="menu_'.$item["task_id"].'" name="menu_'.$item["task_id"].'" class="nav-link menu-link" aria-selected="false" data-tf-task-id="'.$item["task_id"].'" data-tf-controller="'.$item["controller"].'" data-tf-action="'.$item["action"].'"  onclick="TfRequest.menu(this);" >'.$item["task"].'</a> 
            </li>';
  }
      

  return $html;


  
  }


  public function access($tfRequestController,$ctrl=true){

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

	    if(isset($_SERVER["HTTP_REFERER"])){
        $host_referer = $_SERVER["HTTP_REFERER"];
		    $http = substr($host_referer,0,strpos($host_referer,"//")+2);
		    $host=$http.$_SERVER["HTTP_HOST"];
		    $host_referer = substr($host_referer,0,strlen($host));
		    
        if ($host_referer!=$host) {
          throw new TfException("The server cannot process your request",7102,400);   
		    }		
	  	}else{
        throw new TfException("The server cannot process your request",7102,400);    
		  }
	  }else{
	    throw new TfException("The server cannot process your request",7102,400);   
	  }	
    $this->permission($this->task_id,$tfRequestController);
    
    if ($ctrl){
       $path= TF_REQUIRE_PATH.'/controller/'.$tfRequestController.'.ctrl.php';
    }else{
       $path= TF_REQUIRE_PATH.'/function/'.$tfRequestController.'.fn.php';
    }
    
    if (is_file($path)){
      if ($ctrl){
         return 'controller/'.$tfRequestController.'.ctrl.php';
      }else{
        return 'function/'.$tfRequestController.'.fn.php';
      }   
    }else{
      throw new TfException("Not controller found",7201,404);    
    } 

  }	

private function permission($tfRequestTaskId,$tfRequestController){ 
    
  $q = "SELECT c.id,r.readonly
          FROM t_user_role rdu,
               t_functional_area_role rd,
               t_functional_area l,
               t_role r,
               t_process_task_role ptrd,
               t_process_task pt,
               t_process p,
               t_module_process ap,
               t_task_component tc,
               t_task t,
               t_component c,
               t_module a
         WHERE c.active = 'Y'
           AND a.id=ap.id_module
           AND t.active='Y'
           AND t.id = tc.id_task
           AND c.name = ?
           AND c.active = 'Y' 
           AND c.id = tc.id_component
           AND tc.active = 'Y' 
           AND tc.id_task = pt.id_task
           AND ap.active = 'Y' 
           AND ap.id_process = p.id
           AND p.active = 'Y' 
           AND p.id = pt.id_process
           AND pt.active = 'Y' 
           AND pt.id_task = ?
           AND pt.id = ptrd.id_process_task
           AND ptrd.active = 'Y' 
           AND ptrd.id_role = r.id
           AND r.active = 'Y'
           AND r.id = rd.id_role
           AND l.code=?
           AND l.active = 'Y' 
           AND l.id=rd.id_functional_area
           AND rd.active = 'Y' 
           AND rd.id = rdu.id_functional_area_role
           AND rdu.active = 'Y' 
           AND rdu.id_user = ?
         LIMIT 1";

   $param = array($tfRequestController,$tfRequestTaskId,
                  $this->functional_area_code,$this->user_id);  
   
   list($rs) = $this->executeQuery($q,$param);
   
   if (count($rs)>0){
     if($rs["readonly"]=='Y'){
        $this->sessionReadonly=true;   
     }     
   }else{
     throw new TfException("Your user does not have permission for this task",7103,401);
   }
   
  }
  

  public function openTrail(){
    $ix=count($this->breadcrumb);
    if (!isset($ix)||$ix==0){
      $this->breadcrumb_trail_id=1;
    }else{
      $this->breadcrumb_trail_id=$ix+1;
    }
    $this->breadcrumb[$this->breadcrumb_trail_id]=array();
    $q = "UPDATE t_session SET breadcrumb = ? WHERE id=?";
    $param = array(json_encode($this->breadcrumb),$this->id);
    $this->execute($q,$param);        
                
  }
  public function setTrail(){   
    $q = "UPDATE t_session SET breadcrumb = ? WHERE id=?";
    $param = array(json_encode($this->breadcrumb),$this->id);
    $this->execute($q,$param);   
  }
  public function toBeTrail($tfRequestController,$tfRequestAction,$tfRequestData){
    
    $tfRequest = array("tfRequestTaskId"=>$this->task_id,
                       "tfRequestController"=>$tfRequestController,
                       "tfRequestAction"=>$tfRequestAction,
                       "tfRequestData"=>$tfRequestData);

    $tfPrevious =  array_pop($this->breadcrumb[$this->breadcrumb_trail_id]);
    if (isset($tfPrevious)){
      if ($tfPrevious==$tfRequest) {
        array_push($this->breadcrumb[$this->breadcrumb_trail_id], $tfRequest);    
      }else{
        array_push($this->breadcrumb[$this->breadcrumb_trail_id], $tfPrevious);  
        array_push($this->breadcrumb[$this->breadcrumb_trail_id], $tfRequest); 
      }
    }else{
       array_push($this->breadcrumb[$this->breadcrumb_trail_id],$tfRequest);  
    }
    $this->setTrail();  
  }
  
 
  public function backTrail($remove = FALSE){  
    if ($remove){
      $a=array_pop($this->breadcrumb[$this->breadcrumb_trail_id]); 
    }
    $current =  array_pop($this->breadcrumb[$this->breadcrumb_trail_id]);
    $previous = array_pop($this->breadcrumb[$this->breadcrumb_trail_id]);
    if (isset($previous)){
      $r=$previous;
    }else{
      $r=$current;
    }
    $this->setTrail();

    $this->sessionRedirect=$r;

    return $r;
  }

   public function refresh(){  
    $current =  array_pop($this->breadcrumb[$this->breadcrumb_trail_id]);
    echo '<script>TfRequest.redirect("'.$current["tfRequestTaskId"].'","'.$current["tfRequestController"].'","'.$current["tfRequestAction"].'","'.$current["tfRequestData"].'")</script>';
    
  }
  
  public function previous(){  
    $current =  array_pop($this->breadcrumb[$this->breadcrumb_trail_id]);
    $previous =  array_pop($this->breadcrumb[$this->breadcrumb_trail_id]);
    if($previous==""){
      $previous= $current;
    }
    $this->setTrail();
    echo '<script>TfRequest.redirect("'.$previous["tfRequestTaskId"].'","'.$previous["tfRequestController"].'","'.$previous["tfRequestAction"].'","'.$previous["tfRequestData"].'")</script>';
    
  }

  public function logout(){
    $q = "UPDATE t_session SET logout = 'Y' WHERE id=?";
    $param = array($this->id);
    $this->execute($q,$param);   
    session_destroy();
    session_start(); 
    session_regenerate_id();
  }

    public function swapTrail($tfRequestController,$tfRequestAction,$tfRequestData,$pop=1){ 

    $tfRequest = array("tfRequestTaskId"=>$this->task_id,
                        "tfRequestController"=>$tfRequestController,
                        "tfRequestAction"=>$tfRequestAction,
                        "tfRequestData"=>$tfRequestData);
    
    for ($x = 1; $x <= $pop; $x++) {
     $a=array_pop($this->breadcrumb[$this->breadcrumb_trail_id]); 
    }
    
    array_push($this->breadcrumb[$this->breadcrumb_trail_id], $tfRequest); 
    $this->setTrail();
   }
   
   public function printTrail(){
    print_r($this->breadcrumb[$this->breadcrumb_trail_id]);
   }

  

  


   


 


  public function currentPage() {
     $url = 'http';
     $https="off";
     if (array_key_exists("HTTPS", $_SERVER)){
       $https="on";    
     }
     if ($https == "on") {$url .= "s";}
     $url .= "://";
     if ($_SERVER["SERVER_PORT"] != "80") {
      $url .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
     } else {
      $url .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
     }
     return $url;
  }
  
  public  function toRedirect(){   
    if (count($this->sessionRedirect)>0){
      return TRUE;
    }else{
      return FALSE;
    }  
  }

   public function redirect(){
    echo '<script>TfRequest.redirect("'.$this->sessionRedirect["tfRequestTaskId"].'","'.$this->sessionRedirect["tfRequestController"].'","'.$this->sessionRedirect["tfRequestAction"].'","'.$this->sessionRedirect["tfRequestData"].'")</script>';
  }
  
  public function getId(){
     return $this->id;
  }
  public function setId($value){
     $this->id=$value;
  }
  public function getTaskId(){
     return $this->task_id;
  }
  public function setTaskId($value){
     $this->task_id=$value;
  }
  public function getUserId(){
     return $this->user_id;
  }
  public function setUserId($value){
     $this->user_id=$value;
  }
  public function getUserLogin(){
     return $this->user_login;
  }
  public function setUserLogin($value){
     $this->user_login=$value;
  }
  public function getUserName(){
     return $this->user_name;
  }
  public function getUserPassword(){
     return $this->user_password;
  }
  public function setUserPassword($value){
     $this->user_password=$value;
  }

  public function getAuthenticationType(){
     return $this->authentication_type;
  }
  public function setAuthenticationType($value){
     $this->authentication_type=$value;
  }
  public function getFunctionalAreaCode(){    
    return $this->functional_area_code;
  }
  public function setFunctionalAreaCode($value){
      $this->functional_area_code=$value;
  }
  
  public function setTrailId($value){    
    $this->breadcrumb_trail_id = $value;
  }
  public function getTrailId(){    
    return $this->breadcrumb_trail_id;
  }
  
  public function setRandomPublicKey($value){
    $this->random_public_key = $value;
    $q = "UPDATE t_session SET random_public_key = ? WHERE id=?";
    $param = array($value,$this->id);
    $this->execute($q,$param);   
  } 

  public function getRandomPublicKey(){
    return $this->random_public_key;
  } 
  public function changePassword($value){

    $q = "UPDATE t_user
             SET password=?,
                 password_date=?
           WHERE login=?";
    
    $param = array($value,date("Y-m-d H:i:s"),$this->user_login);
    $this->Execute($q,$param);
  } 

 
  public function getJWTChangePwd(){
     return $this->JWTChangePwd;
  }
  public function setJWTChangePwd($value){
     $this->JWTChangePwd=$value;
  }
  
  public  function JWTChangePwd(){
    $time = time();      
    $token = array('iat'  => $time,
                   'data' => array('tsul'=>$this->user_login,
                                   'tsui'=>$this->user_id,
                                   'tsup'=>$this->user_password,
                                   'tsupd'=>$this->user_password_date,
                                   'tsum'=>$this->user_email_account));
  

    $this->JWTChangePwd=JWT::encode($token,TUICH_SECRET);
  }
  

  private function sequence(){
        $q="SELECT current_value
              FROM t_sequence
             WHERE table_name='t_session'";

        list($rs) = $this->executeQuery($q);
       
        $nextval=$rs["current_value"]+1;
        $q = "UPDATE t_sequence SET current_value=? WHERE table_name='t_session'";
        $param = array($nextval);
        
        $this->execute($q,$param);
        return $nextval;
    }
  public static function ClientIP()
  {
      if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
        return $_SERVER['HTTP_X_FORWARDED_FOR'];    
      }else{
        return $_SERVER['REMOTE_ADDR'];    
      }
      
  }
  
}