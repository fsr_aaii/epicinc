<?php

class TfRequest {

  private $data = array();  

  public function __construct($request,$randomPublicKey=""){ 
    
    if ($request!=""){
        $decrypted = "";
        if ($randomPublicKey==""){
          if (openssl_private_decrypt(base64_decode($request), $decrypted, TF_RSA_PVK)) {
              $this->data=json_decode(utf8_decode($decrypted),true);        
              array_walk_recursive($this->data,$this->set);
              
          }else{
             throw new Exception("Failed to decrypt data ",930); 
          }
        }else{
          $encryption = new Encryption(); 
      
         $this->data=json_decode($encryption->decrypt($request, $randomPublicKey),true);
    
  
        }  
    }
  }  

  private function set(&$item, $key) {
    $item = mysql_real_escape_string(htmlspecialchars(strip_tags($item)));
  } 
  
  public function get($key) {
    if (array_key_exists($key, $this->data)){
      return $this->data[$key];
    }
  }


  public function __set($name, $value)
  {
        $this->data[$name] = mysql_real_escape_string(htmlspecialchars(strip_tags($value)));
  }

  public function __get($name)
  {
      if (array_key_exists($name, $this->data)) {
          return $this->data[$name];
      }
      return null;
  }

  public function getAll() {
   return $this->data;
   
  }
  
  public function exist($key) {
    return array_key_exists($key, $this->data);
  }

  public static function encrypt(array $request){ 
    $encrypted = "";
    if (openssl_public_encrypt(json_encode($request,true), $encrypted, TF_RSA_PBK)) {
       return base64_encode($encrypted);
    }else{
           throw new Exception("Failed to encrypt data ",940); 
    }
  }  
  
 
}
