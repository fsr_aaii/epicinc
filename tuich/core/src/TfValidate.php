<?php
class TfValidate{ 

  public static function validate(array $param){
      switch ($param["type"]){
			case "string":
			  return TfValidate::isString($param["value"],$param["required"],$param["length"]);
			  break;						 
			case "number":
			  return TfValidate::isNumber($param["value"],$param["required"],$param["length"]);
			  break;
			case "date":
			  return TfValidate::isDate($param["value"],$param["required"]);
			  break;
      case "datetime":
        return TfValidate::isDateTime($param["value"],$param["required"]);
        break;  
	  }
  }
   
  public static function isAlpha($value, $allow = ''){
    if (preg_match('/^[a-zA-Z' . $allow . ']+$/', $value)){
      $is=0;
    } else {
      $is=4;
    }
    return $is;
  }

  public static function isAlphaNumeric($value){
    if (preg_match("/^[A-Za-z0-9 ]+$/", $value)){
      $is=0;
    }
    else {
      $is=5;
    }
    return $is;
  }

  public static function isADate($value){
   if (DateTime::createFromFormat('Y-m-d', $value) !== false){
     return 0;
   }
   return 6;
  }

  public static function isADateTime($value){
    if (DateTime::createFromFormat('Y-m-d H:i:s', $value) !== false){
     return 0;
    }
    return 6;
  }

 public static function isEmail($email){
    $pattern = "/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+/";
    if (preg_match($pattern, $email)){
      $is=0;
    }
    else {
      $is=7;
    }
    return $is;
  }

 public static function isEmpty($value){
    if (strlen($value) < 1 || is_null($value)) {
      $is=0;
    } else {
      $is=8;
    }
    return $is;
  }

  public static function isInternetURL($value){
    if (preg_match("/^http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?$/i", $value))
    {
      $is=0;
    } else {
      $is=9;
    }
  }

  public static function isIPAddress($value){
    $pattern = "/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/i";
    if (preg_match($pattern, $value)){
      $is=0;
    }
    else {
      $is=10;
    }
  }

  public static function isTooLong($value, $maxLength) {
    if (strlen($value) > $maxLength) {
      $is=13;
    }
    else {
      $is=0;
    }
    return $is;
  }

  public static function isTooShort($value, $minLength) {
    if (strlen($value) < $minLength) {
      $is=14;
    }
    else {
      $is=0;
    }
    return $is;
  }
  
  public static function isUnsignedNumber($number){
    if (preg_match("/^\+?[0-9]+$/", $number)){
      $is=0;
    }
    else {
      $is=15;
    }
    return $is;
  }

  public static function isRif($value, $required=FALSE)
  {
      $aux = TfValidate::hasValue($value);
      if($aux==0)
      {
        $rif = array();
        $numbers = array(4,3,2,7,6,5,4,3,2);
        if(strlen($value)==10)
        {
          for($i = 0; $i<10; $i++)
          {
            array_push($rif, substr($value, $i, 1));
          }
          $rif[0]=0;
          if(substr($value, 0,1)=='V'||substr($value, 0,1)=='v')
          {
            $rif[0] = 1;
          }
          if(substr($value,0,1)=='E'||substr($value,0,1)=='e')
          {
            $rif[0] = 2;
          }
          
          for ($i=0; $i < 10 ; $i++) 
          { 
            if($i!=9)
            {
              $rif[$i] = $rif[$i] * $numbers[$i];
              $sumRif= $sumRif + $arr[$i];
            }
          }
          $intRif = intval($sumRif/11);

          $residue = $sumRif - ($intRif * 11);

          $validationDigit = 11 - $residue;
          
          if($validationDigit > 9)
          {
            $validationDigit = 0;
          }
          if($validationDigit == $rif[9])
          {
            $is=0;
          }
        }
        else
        {
          $is=1;
        }
      }
      else
      {
        if($required===FALSE)
        {
          $is=0;
        }
        else
        {
          $is = $aux;
        }
      }
      return $is;
  }
  
   
  public static function isDate($value,$required=FALSE){
    $aux=TfValidate::hasValue($value);
    if($aux==0){
      $aux=TfValidate::isADate($value);
      if($aux==0){
        $is=0;
      }else{
        $is=$aux;
      }
    }else{
      if($required===FALSE){
        $is=0;
      }else{
        $is=$aux;
      }
    }
    return $is;
  }

  public static function isDateTime($value,$required=FALSE){
    $aux=TfValidate::hasValue($value);
    if($aux==0){
      $aux=TfValidate::isADateTime($value);
      if($aux==0){
        $is=0;
      }else{
        $is=$aux;
      }
    }else{
      if($required===FALSE){
        $is=0;
      }else{
        $is=$aux;
      }
    }
    return $is;
  }
  

 public static function hasValue($value){
    if (strlen($value) < 1) {
      $is=3;
    }
    else {
      $is=0;
    }
    return $is;
  }
  
  public static function isANumber($number){

    if(is_numeric($number)){
      $is=0;
    }
    else {
      $is=11;
    }
    return $is;
  }
  
  public static function checkLength($value, $maxLength, $minLength = 0){
    if (!(mb_strlen($value,'UTF8') > $maxLength) && !(mb_strlen($value,'UTF8') < $minLength)) {
      $is=0;
    } else {
      $is=1;
    }
    return $is;
  }
  
  public static function isNumber($number,$required=FALSE,$maxLength=-1){


    $aux=TfValidate::hasValue($number);
   
    if($aux==0){
      $aux=TfValidate::isANumber($number);

      if($aux==0){
        if($maxLength>-1){
          $aux=TfValidate::checkLength($number, $maxLength);
          if($aux==0){
            $is=0;
          }else{
            $is=$aux;
          }
        }else{
          $is=0;
        }
      }else{
        $is=$aux;
      }
    }else{
      if($required===FALSE){
        $is=0;
      }
    }
    return $is;
}	
   
   public static function isString($string,$required=FALSE,$maxLength=-1){
    $aux=TfValidate::hasValue($string);
    if($aux==0){
        if($maxLength>-1){
          $aux=TfValidate::checkLength($string, $maxLength);
          if($aux==0){
            $is=0;
          }
          else{
            $is=$aux;
          }
        }
        else{
          $is=0;
        }
    }
    else{
      if($required===FALSE){
        $is=0;
      }
      else{
        $is=$aux;
      }
    }
    return $is;
  }
   
    
}
?>