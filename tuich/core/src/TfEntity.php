<?php

 class TfEntity {
    
    protected $tfs;
    protected $entity;
    protected $initialState;
    protected $validation = array();
    protected $updateable = array();
    protected $objError   = array();
    protected $attrError  = array();
    protected $objMsg     = array();
    protected $valid = TRUE;

    public function setInitialState($value){
	    $this->initialState=$value; 
	}
	public function getInitialState(){
	    return $this->initialState; 
	}

    public function setValidations(){

	}
	public function setValidation($attr,$array){
	    if (is_array($array)){
	      $this->validation[$attr]=$array;
	    }
	}
	public function getValidation($attr){
	    return $this->validation[$attr];
	}
	public function setValidationProperty($attr,$property,$value){
	    $this->validation[$attr][$property]=$value;
	}

	public function setUpdateable($attr,$value){
	    $this->updateable[$attr]=$value;
	    
	}
	public function getUpdateable($attr){
	    return $this->updateable[$attr];
	}

	
	public function setObjError($value){
	    $this->objError[]=$value;
	}
	public function getObjError(){
	    return $this->objError;
	}

	
	public function setAttrErrors(){
	   foreach($this->validation as $key => $value) {
         $this->attrError[$key]=0;	
       }    
	}
    public function setAttrError($attr,$value){
	    $this->attrError[$attr]=$value;	    
	}
	public function getAttrError($attr){
		global $tuichValidateMessage;
	    return $tuichValidateMessage[$this->attrError[$attr]];
	}
    
    public function getAttrErrors(){
	   return $this->attrError;    
	}

	public function getErrors(){
	    return count($this->objError)+array_sum($this->attrError); 
	}

	 public function setValid($value){
	 	$this->valid=$value;

	}

	public function populate($request,$fromDB=TRUE){
        if ($fromDB){
        	$this->dbPopulate($request->get($this->entity."_id"));
    	}else{
    		$this->uiPopulate($request);
    	}
    	$this->setValidations();
    }
    public function populatePk($id){
        $this->dbPopulate($id);
    	$this->setValidations();
    }

    protected function dbPopulate($id){

	}
    protected function uiPopulate($request){

    }

    public function setObjMsg($value){
	    $this->objMsg[]=$value;
	}
	public function getObjMsg(){
	    return $this->objMsg;
	}

    private function getAll(){
    	
    }
    public function create(){

    }
    public function update(){

    }
    public function delete(){

	}

    public function sequence(){
        $q="SELECT current_value
              FROM t_sequence
             WHERE table_name=?
               FOR UPDATE";

        $param = array($this->entity);
        list($rs) = $this->tfs->executeQuery($q,$param);
       
        $nextval=$rs["current_value"]+1;
        $q = "UPDATE t_sequence SET current_value=? WHERE table_name=?";
        $param = array($nextval,$this->entity);
        $this->tfs->execute($q,$param);
        return $nextval;
    }

    public static function sequenceByTable($tfs,$table_name){
        $q="SELECT current_value
              FROM t_sequence
             WHERE table_name=?
               FOR UPDATE";

        $param = array($table_name);
        list($rs) = $tfs->executeQuery($q,$param);
       
        $nextval=$rs["current_value"]+1;
        $q = "UPDATE t_sequence SET current_value=? WHERE table_name=?";
        $param = array($nextval,$table_name);
        $tfs->execute($q,$param);
        return $nextval;
    }

    public function audit(){

    }

    public function isValid(){
	    return $this->valid; 
	}

    protected function attrValidate(){
    	foreach($this->validation as $key => $values) {
            
         $this->attrError[$key]=TfValidate::validate($values);	
        }
        if(array_sum($this->attrError)!=0){
	      $this->valid = FALSE;
	    }
    }
    protected function objValidate(){

    }
	protected function validate(){
	    $this->attrValidate();
	    $this->objValidate();
	}

   


 }
?>