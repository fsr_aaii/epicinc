<?php
class TfWidget{ 

  public static function compare($value1, $value2, $caseSensitive = false){
    if ($caseSensitive) {
      $is=($value1 ==  $value2 ? 0 : 2);
    } 
    else {
      if (strtoupper($value1) ==  strtoupper($value2)) {
        $is=0;
      } else {
        $is=2;
      }
    }
    return $is;
  }

 
   public static function selectStructure($selectValues,$value,$maxlen=65){    
    if (is_array($selectValues)){
		foreach($selectValues as $l){
		  if (strlen($l["option"])>$maxlen){
		   $option= substr($l["option"],0,$maxlen)."...";
		   $title=' title="'.$l["option"].'"';
		  }else{
		   $option= $l["option"];
		   $title='';
		  }
		  if ($l["value"]==$value){
		    $options.="<option".$title." value=\"".$l["value"]."\" selected=\"selected\">".$option."</option>";
		  }else{
			$options.="<option".$title." value=\"".$l["value"]."\">".$option."</option>";
		  }
		}
	}		  
	return $options;
  }

  public static function  datalistStructure($selectValues,$value,$maxlen=65){    
    if (is_array($selectValues)){
    foreach($selectValues as $l){
      if (strlen($l["option"])>$maxlen){
       $option= substr($l["option"],0,$maxlen)."...";
       $title=' title="'.$l["option"].'"';
      }else{
       $option= $l["option"];
       $title='';
      }
      if ($l["value"]==$value){
        $options.="<option".$title." value=\"".$l["value"]."\" selected=\"selected\">";
      }else{
      $options.="<option".$title." value=\"".$l["value"]."\">";
      }
    }
  }     
  return $options;
  }
  
  public static function selectStructureYN($value,$valueDefault){    
    if($value=='Y'){
	  $options.='<option value="Y" selected="selected">Yes</option>';
	}elseif($value=="N"){
	  $options.='<option value="Y">Yes</option>';
	}else{
	  if($valueDefault=='Y'){
	    $options.='<option value="Y" selected="selected">Yes</option>';
	  }else{
	    $options.='<option value="Y">Yes</option>';
	  }	
	}
	if($value=='N'){
	  $options.='<option value="N" selected="selected">No</option>';
	}elseif($value=="Y"){
	  $options.='<option value="N">No</option>';
	}else{
	  if($valueDefault=='N'){
	    $options.='<option value="N" selected="selected">No</option>';
	  }else{
	    $options.='<option value="N">No</option>';
	  }	
	}		  
	return $options;
  }
  public static function bsf($amount){   
    if (isset($amount)){
      $format=number_format($amount, 2,',', '.').' Bs.F.';
    }else{
      $format='0,00 Bs.F.';
    }  
  

	return $format;
  }	 
  
  public static function amount($amount){   
    if (isset($amount)){
      $format=number_format($amount, 2,'.', ',');
    }else{
      $format='0.00';
    } 
	  return $format;
  }	 
  
  public static function dateFormat($date){   
    if (isset($date)&&$date!=""){
	  $format=substr($date,0,10);
	}
	return $format;
  }	 
  
   public static function dateTimeFormat($date){   
	  
    if (isset($date)&&$date!=""){
	  if (strlen($date)<19){
	    $format=substr($date,0,10).' 00:00:00';
	  }else{
	    $format=substr($date,0,19);
	  }
	}  
	return $format;
  } 
  

 //devuelve la cantidad de dias de acuerdo a las hora especificacda
 public static function hour2Days($horas){
      $dias=floor($horas/24);
      return $dias;
 }

//devuelve la diferencia de dias en horas
  public static function dateDiff($fecha_ini, $fecha_fin)
  {

    if($fecha_ini !='' && $fecha_ini!=NULL && $fecha_fin !='' && $fecha_fin!=NULL){
    
        
       $fechaenteraini = explode(" ",$fecha_ini);
        $date_ini = explode("/",$fechaenteraini[0]);
        $time_ini = explode(":",$fechaenteraini[1]);
        $fechaenterafin = explode(" ",$fecha_fin);
        $date_fin = explode("/",$fechaenterafin[0]);
        $time_fin = explode(":",$fechaenterafin[1]);
       
       
        $hora_ini = $time_ini[0];
        $minuto_ini = $time_ini[1];
        $segundo_ini = $time_ini[2];
        $mes_ini = $date_ini[1];
        $dia_ini = $date_ini[0];
        $anno_ini = $date_ini[2];
        
        $hora_fin = $time_fin[0];
        $minuto_fin = $time_fin[1];
        $segundo_fin = $time_fin[2];
        $mes_fin = $date_fin[1];
        $dia_fin = $date_fin[0];
        $anno_fin = $date_fin[2];
        
      
        $fecha_inicial= mktime($hora_ini,$minuto_ini,$segundo_ini,$mes_ini,$dia_ini,$anno_ini); //Hora inicial
        $fecha_final=mktime($hora_fin,$minuto_fin,$segundo_fin,$mes_fin,$dia_fin,$anno_fin); //Hora final
        
        
        $horas_totales=$fecha_final-$fecha_inicial; 
        $horasdiferencia=($horas_totales/60)/60;

      
    }else{
      $horasdiferencia=NULL;
    }
        
    return $horasdiferencia;
    
  }

  public static function formateaHorasMinutos($horas){
    
          
      $horaformateada=TfWidget::generaHora($horas).':'.TfWidget::generaMinuto($horas);
      return $horaformateada;
  }

  public static function generaHora($horas){
    
      $hora = floor($horas);        
      return $hora;
  }

  public static function generaMinuto($horas){
      $hora = floor($horas);    
      $minutos = round(($horas -$hora)*60,0);
      
      return $minutos;

  }

  public static function horaFormateada($fecha_ini, $fecha_fin){
    
    $horas=TfWidget::dateDiff($fecha_ini, $fecha_fin);
    $hora_formateada=TfWidget::formateaHorasMinutos($horas);
    return $hora_formateada;

  }

  public static function fechaFormateada($fecha){
    
    if($fecha !='' && $fecha!=NULL){
    
      $fechaTiempo = explode(" ",$fecha);
      $fecha = explode("/",$fechaTiempo[0]);
      $tiempo = explode(":",$fechaTiempo[1]);
        
      $hora    = $tiempo[0];
      $minuto  = $tiempo[1];
      $segundo = $tiempo[2];
        
      $mes = $fecha[1];
      $dia = $fecha[0];
      $anno = $fecha[2];      
      
      $fechaFormateada= mktime($hora,$minuto,$segundo,$mes,$dia,$anno);
      
      
    }else{
      $fechaFormateada=NULL;
    }
        
    return $fechaFormateada;

  }

  public static function numberToStrTiempo($tiempo){
    $hora = floor($tiempo);
    $str_hora = str_pad($hora,2,"0", STR_PAD_LEFT);
    $str_minuto = str_pad(($tiempo-$hora)*100,2,"0", STR_PAD_RIGHT);
    
    return $str_hora.":". $str_minuto.":00"; 
  }

  public function enIntervaloFecha($fecha,$fechaI,$fechaF){
      if (($fecha>=$fechaI) &&($fecha<=$fechaF)){
      return TRUE;
    }else{
      return FALSE; 
    }
  }

  public function numberToHour($numero){
    $horas=($numero/60)/60;
    $hora_formateada=TfWidget::formateaHorasMinutos($horas);
    return $hora_formateada;      
  }

  public static function alertDangerTemplate($alert){
    if ($alert!=""){
      return ' <div class="notice notice-danger">
                    <span class="far fa-bell"></span> '.$alert.'
                </div>';        
    }else{
      return '';
    }
  }  

   public static function alertSuccessTemplate($alert){
    if ($alert!=""){     
     return ' <div class="notice notice-success">
                    <span class="far fa-bell"></span> '.$alert.'
                </div>'; ;
    }else{
      return '';
    }
  }

   public static function alertInfoTemplate($alert){
    if ($alert!=""){
      return ' <div class="notice notice-info">
                    <span class="far fa-bell"></span> '.$alert.'
                </div>';         
    }else{
      return '';
    }
  }
  

   public static function alertWarningTemplate($alert){
    if ($alert!=""){
       return ' <div class="notice notice-warning">
                    <span class="far fa-bell"></span> '.$alert.'
                </div>';     
    }else{
      return '';
    }
  }  
  

  public static function years($tus){ 
    $q = " SELECT 2017+id \"value\", 2017+id \"option\" 
             FROM `gl_week` WHERE id <= YEAR(NOW()) - 2017
           ORDER BY id";
    $rs = $tus->executeQuery($q);

    return $rs;
  }

  public static function months(){ 

    return Array ( "0" => Array ( "value" => 1 ,"option" => "January" ) ,"1" => Array ( "value" => 2 ,"option" => "February" ) ,
                   "2" => Array ( "value" => 3 ,"option" => "March" ) ,"3" => Array ( "value" => 4 ,"option" => "April" ) ,
                   "4" => Array ( "value" => 5 ,"option" => "May" ) ,"5" => Array ( "value" => 6 ,"option" => "June" ) ,
                   "6" => Array ( "value" => 7 ,"option" => "July" ) ,"7" => Array ( "value" => 8 ,"option" => "August" ) ,
                   "8" => Array ( "value" => 9 ,"option" => "September" ) ,"9" => Array ( "value" => 10 ,"option" => "October" ) ,
                   "10" => Array ( "value" => 11 ,"option" => "November") ,"11" => Array ( "value" => 12 ,"option" => "December" ) );
  }

  public static function weeks($tus){ 
    $q = "SELECT id \"value\",
                 id \"option\"
            FROM gl_week
           ORDER BY 2";
    $rs = $tus->executeQuery($q);

    return $rs;
  }

public static function unitTimeDays(){ 

    return Array ( "0" => Array ( "value" => "d" ,"option" => "Day" ) ,"1" => Array ( "value" => "w" ,"option" => "Week" ) ,
                   "2" => Array ( "value" => "m" ,"option" => "Month" ) ,"3" => Array ( "value" => "q" ,"option" => "Quarter" ) ,
                   "4" => Array ( "value" => "s" ,"option" => "Semester" ) ,"5" => Array ( "value" => "y" ,"option" => "Year" ));
  }
  
  public static function strRandom($length){
   $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
   return substr(str_shuffle($permitted_chars), 0, $length);
  }

  public static function country($code){
    $countries =  array('AF' => 'Afghanistan',
                        'AX' => 'Aland Islands',
                        'AL' => 'Albania',
                        'DZ' => 'Algeria',
                        'AS' => 'American Samoa',
                        'AD' => 'Andorra',
                        'AO' => 'Angola',
                        'AI' => 'Anguilla',
                        'AQ' => 'Antarctica',
                        'AG' => 'Antigua And Barbuda',
                        'AR' => 'Argentina',
                        'AM' => 'Armenia',
                        'AW' => 'Aruba',
                        'AU' => 'Australia',
                        'AT' => 'Austria',
                        'AZ' => 'Azerbaijan',
                        'BS' => 'Bahamas',
                        'BH' => 'Bahrain',
                        'BD' => 'Bangladesh',
                        'BB' => 'Barbados',
                        'BY' => 'Belarus',
                        'BE' => 'Belgium',
                        'BZ' => 'Belize',
                        'BJ' => 'Benin',
                        'BM' => 'Bermuda',
                        'BT' => 'Bhutan',
                        'BO' => 'Bolivia',
                        'BA' => 'Bosnia And Herzegovina',
                        'BW' => 'Botswana',
                        'BV' => 'Bouvet Island',
                        'BR' => 'Brazil',
                        'IO' => 'British Indian Ocean Territory',
                        'BN' => 'Brunei Darussalam',
                        'BG' => 'Bulgaria',
                        'BF' => 'Burkina Faso',
                        'BI' => 'Burundi',
                        'KH' => 'Cambodia',
                        'CM' => 'Cameroon',
                        'CA' => 'Canada',
                        'CV' => 'Cape Verde',
                        'KY' => 'Cayman Islands',
                        'CF' => 'Central African Republic',
                        'TD' => 'Chad',
                        'CL' => 'Chile',
                        'CN' => 'China',
                        'CX' => 'Christmas Island',
                        'CC' => 'Cocos (Keeling) Islands',
                        'CO' => 'Colombia',
                        'KM' => 'Comoros',
                        'CG' => 'Congo',
                        'CD' => 'Congo, Democratic Republic',
                        'CK' => 'Cook Islands',
                        'CR' => 'Costa Rica',
                        'CI' => 'Cote D\'Ivoire',
                        'HR' => 'Croatia',
                        'CU' => 'Cuba',
                        'CY' => 'Cyprus',
                        'CZ' => 'Czech Republic',
                        'DK' => 'Denmark',
                        'DJ' => 'Djibouti',
                        'DM' => 'Dominica',
                        'DO' => 'Dominican Republic',
                        'EC' => 'Ecuador',
                        'EG' => 'Egypt',
                        'SV' => 'El Salvador',
                        'GQ' => 'Equatorial Guinea',
                        'ER' => 'Eritrea',
                        'EE' => 'Estonia',
                        'ET' => 'Ethiopia',
                        'FK' => 'Falkland Islands (Malvinas)',
                        'FO' => 'Faroe Islands',
                        'FJ' => 'Fiji',
                        'FI' => 'Finland',
                        'FR' => 'France',
                        'GF' => 'French Guiana',
                        'PF' => 'French Polynesia',
                        'TF' => 'French Southern Territories',
                        'GA' => 'Gabon',
                        'GM' => 'Gambia',
                        'GE' => 'Georgia',
                        'DE' => 'Germany',
                        'GH' => 'Ghana',
                        'GI' => 'Gibraltar',
                        'GR' => 'Greece',
                        'GL' => 'Greenland',
                        'GD' => 'Grenada',
                        'GP' => 'Guadeloupe',
                        'GU' => 'Guam',
                        'GT' => 'Guatemala',
                        'GG' => 'Guernsey',
                        'GN' => 'Guinea',
                        'GW' => 'Guinea-Bissau',
                        'GY' => 'Guyana',
                        'HT' => 'Haiti',
                        'HM' => 'Heard Island & Mcdonald Islands',
                        'VA' => 'Holy See (Vatican City State)',
                        'HN' => 'Honduras',
                        'HK' => 'Hong Kong',
                        'HU' => 'Hungary',
                        'IS' => 'Iceland',
                        'IN' => 'India',
                        'ID' => 'Indonesia',
                        'IR' => 'Iran, Islamic Republic Of',
                        'IQ' => 'Iraq',
                        'IE' => 'Ireland',
                        'IM' => 'Isle Of Man',
                        'IL' => 'Israel',
                        'IT' => 'Italy',
                        'JM' => 'Jamaica',
                        'JP' => 'Japan',
                        'JE' => 'Jersey',
                        'JO' => 'Jordan',
                        'KZ' => 'Kazakhstan',
                        'KE' => 'Kenya',
                        'KI' => 'Kiribati',
                        'KR' => 'Korea',
                        'KW' => 'Kuwait',
                        'KG' => 'Kyrgyzstan',
                        'LA' => 'Lao People\'s Democratic Republic',
                        'LV' => 'Latvia',
                        'LB' => 'Lebanon',
                        'LS' => 'Lesotho',
                        'LR' => 'Liberia',
                        'LY' => 'Libyan Arab Jamahiriya',
                        'LI' => 'Liechtenstein',
                        'LT' => 'Lithuania',
                        'LU' => 'Luxembourg',
                        'MO' => 'Macao',
                        'MK' => 'Macedonia',
                        'MG' => 'Madagascar',
                        'MW' => 'Malawi',
                        'MY' => 'Malaysia',
                        'MV' => 'Maldives',
                        'ML' => 'Mali',
                        'MT' => 'Malta',
                        'MH' => 'Marshall Islands',
                        'MQ' => 'Martinique',
                        'MR' => 'Mauritania',
                        'MU' => 'Mauritius',
                        'YT' => 'Mayotte',
                        'MX' => 'Mexico',
                        'FM' => 'Micronesia, Federated States Of',
                        'MD' => 'Moldova',
                        'MC' => 'Monaco',
                        'MN' => 'Mongolia',
                        'ME' => 'Montenegro',
                        'MS' => 'Montserrat',
                        'MA' => 'Morocco',
                        'MZ' => 'Mozambique',
                        'MM' => 'Myanmar',
                        'NA' => 'Namibia',
                        'NR' => 'Nauru',
                        'NP' => 'Nepal',
                        'NL' => 'Netherlands',
                        'AN' => 'Netherlands Antilles',
                        'NC' => 'New Caledonia',
                        'NZ' => 'New Zealand',
                        'NI' => 'Nicaragua',
                        'NE' => 'Niger',
                        'NG' => 'Nigeria',
                        'NU' => 'Niue',
                        'NF' => 'Norfolk Island',
                        'MP' => 'Northern Mariana Islands',
                        'NO' => 'Norway',
                        'OM' => 'Oman',
                        'PK' => 'Pakistan',
                        'PW' => 'Palau',
                        'PS' => 'Palestinian Territory, Occupied',
                        'PA' => 'Panama',
                        'PG' => 'Papua New Guinea',
                        'PY' => 'Paraguay',
                        'PE' => 'Peru',
                        'PH' => 'Philippines',
                        'PN' => 'Pitcairn',
                        'PL' => 'Poland',
                        'PT' => 'Portugal',
                        'PR' => 'Puerto Rico',
                        'QA' => 'Qatar',
                        'RE' => 'Reunion',
                        'RO' => 'Romania',
                        'RU' => 'Russian Federation',
                        'RW' => 'Rwanda',
                        'BL' => 'Saint Barthelemy',
                        'SH' => 'Saint Helena',
                        'KN' => 'Saint Kitts And Nevis',
                        'LC' => 'Saint Lucia',
                        'MF' => 'Saint Martin',
                        'PM' => 'Saint Pierre And Miquelon',
                        'VC' => 'Saint Vincent And Grenadines',
                        'WS' => 'Samoa',
                        'SM' => 'San Marino',
                        'ST' => 'Sao Tome And Principe',
                        'SA' => 'Saudi Arabia',
                        'SN' => 'Senegal',
                        'RS' => 'Serbia',
                        'SC' => 'Seychelles',
                        'SL' => 'Sierra Leone',
                        'SG' => 'Singapore',
                        'SK' => 'Slovakia',
                        'SI' => 'Slovenia',
                        'SB' => 'Solomon Islands',
                        'SO' => 'Somalia',
                        'ZA' => 'South Africa',
                        'GS' => 'South Georgia And Sandwich Isl.',
                        'ES' => 'Spain',
                        'LK' => 'Sri Lanka',
                        'SD' => 'Sudan',
                        'SR' => 'Suriname',
                        'SJ' => 'Svalbard And Jan Mayen',
                        'SZ' => 'Swaziland',
                        'SE' => 'Sweden',
                        'CH' => 'Switzerland',
                        'SY' => 'Syrian Arab Republic',
                        'TW' => 'Taiwan',
                        'TJ' => 'Tajikistan',
                        'TZ' => 'Tanzania',
                        'TH' => 'Thailand',
                        'TL' => 'Timor-Leste',
                        'TG' => 'Togo',
                        'TK' => 'Tokelau',
                        'TO' => 'Tonga',
                        'TT' => 'Trinidad And Tobago',
                        'TN' => 'Tunisia',
                        'TR' => 'Turkey',
                        'TM' => 'Turkmenistan',
                        'TC' => 'Turks And Caicos Islands',
                        'TV' => 'Tuvalu',
                        'UG' => 'Uganda',
                        'UA' => 'Ukraine',
                        'AE' => 'United Arab Emirates',
                        'GB' => 'United Kingdom',
                        'US' => 'United States',
                        'UM' => 'United States Outlying Islands',
                        'UY' => 'Uruguay',
                        'UZ' => 'Uzbekistan',
                        'VU' => 'Vanuatu',
                        'VE' => 'Venezuela',
                        'VN' => 'Viet Nam',
                        'VG' => 'Virgin Islands, British',
                        'VI' => 'Virgin Islands, U.S.',
                        'WF' => 'Wallis And Futuna',
                        'EH' => 'Western Sahara',
                        'YE' => 'Yemen',
                        'ZM' => 'Zambia',
                        'ZW' => 'Zimbabwe');
    return $countries[strtoupper($code)];
  }
    
  public static function qty($qty){   
    if ($qty==0){
      return '0';
    }else{
      $format=number_format($qty, 0,',', '.');
      return $format;
    }
    
  }  
}
?>