<?php
class TfException extends Exception
{
    private $http_code;

    public function __construct($message, $code = 0,$httpCode) { 
        parent::__construct($message, $code, $previous);
        $this->http_code=$httpCode;
    }

    public function getHttpCode(){
        return $this->http_code;
    }

}
