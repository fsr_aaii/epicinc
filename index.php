<?php 
session_start(); 

require_once("include/system.inc.php");  

$router = new AltoRouter();
$router->setBasePath(TF_BASE_PATH);

$router->map( 'GET', '/[a:tfa]', function($tfFunctionalArea) {
  TfResponse::call(__DIR__ . '/login.html');
});

$router->map( 'POST', '/[a:tfa]/api/auth', function($tfFunctionalArea) {
  $tfResponse = new TfResponse();
  try{
      $tfRequest = new TfRequest($_POST['request-data']);
      unset($_POST);
      
      $t_user =  $tfRequest->get("t_user");

      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setUserLogin($t_user["login"]);
      $tfs->setUserPassword(md5($t_user["password"]));
      $tfs->setAuthenticationType("Password");

      $tfs->start();
      $tfResponse->set("tfSessionToken",$tfs->getId(),FALSE);
      $tfs->close(); 
      $tfResponse->return(); 
            
  }catch(Throwable $t){
      $tfResponse->exception($t);
    }
  
});

$router->map( 'GET', '/[a:tli]/[a:tst]', function($tfFunctionalArea,$tfSessionToken) {
  try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setAuthenticationType("Token");
       
      $tfs->start();
      TfResponse::call(__DIR__ . '/home.html');  
      $tfs->close(); 
            
  }catch(Throwable $t){
      if (is_file(__DIR__ .'/'.$t->getCode().'.html')){
       require __DIR__ .'/'.$t->getCode().'.html';
     }else{
       //require __DIR__ .'/7999.html';
       print_r($t);
     } 
  }

}); 

$router->map( 'POST', '/[a:tfa]/[a:tst]/api/nav', function($tfFunctionalArea,$tfSessionToken) {
 $tfResponse = new TfResponse();
  try{
      $tfRequest = new TfRequest($_POST['request-data']);
      unset($_POST);
      $tfResponse->setRandomPublicKey($tfRequest->get("randomPublicKey"));
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setAuthenticationType("Token");
      $tfs->start();
      $tfs->setRandomPublicKey($tfRequest->get("randomPublicKey"));
      $tfs->openTrail();
      $tfResponse->set("tfTrailId",$tfs->getTrailId(),true);
      $tfResponse->set("tfMenu",$tfs->menu(),true);
      $tfs->close(); 
      $tfResponse->return(); 
            
  }catch(Throwable $t){
      $tfResponse->exception($t);
  }
  
});

$router->map( 'POST', '/[a:tfa]/[a:tst]/[a:tsb]/view/[a:trt]/[:trc]/[a:tra]', function($tfFunctionalArea,$tfSessionToken,$tfTrailId,$tfRequestTaskId,$tfRequestController,$tfRequestAction) {
  $tfResponse = new TfResponse();
  ob_start();
  try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTaskId($tfRequestTaskId);
      $tfs->setTrailId($tfTrailId);
      $tfs->setAuthenticationType("Token");             
      $tfs->start();      
      $tfRequest = new TfRequest($_POST['request-data'],$tfs->getRandomPublicKey());
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());
      $path=$tfs->access($tfRequestController);
      $tfs->toBeTrail($tfRequestController,$tfRequestAction,$_POST['request-data']);
      require($path);
      $tfs->close();
      $content = ob_get_contents();
  ob_end_clean();
  $tfResponse->set("tfContent",$content,true);
  $tfResponse->return(); 

            
  }catch(Throwable $t){      
    /* if (is_file(__DIR__ .'/'.$t->getCode().'.html')){
       require __DIR__ .'/'.$t->getCode().'.html';
     }else{
       //require __DIR__ .'/7999.html';
       print_r($t);
     } */
     print_r($t);
     $content = ob_get_contents();
     ob_end_clean();
     $tfResponse->set("tfContent",$content,true);
     $tfResponse->return(); 

     exit();
  }
});

$router->map( 'POST', '/[a:tfa]/[a:tst]/fn/[a:trt]/[:trc]/[a:tra]', function($tfFunctionalArea,$tfSessionToken,$tfRequestTaskId,$tfRequestController,$tfFnName) {
  $tfResponse = new TfResponse();
  ob_start();
  try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTaskId($tfRequestTaskId);
      $tfs->setAuthenticationType("Token");             
      $tfs->start();      
      $tfRequest = new TfRequest($_POST['request-data'],$tfs->getRandomPublicKey());
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());
      $path=$tfs->access($tfRequestController,false);
      require($path);
      $tfs->close();
      $content = ob_get_contents();
      ob_end_clean();
      $tfResponse->set("tfContent",$content,true);
      $tfResponse->return(); 

            
  }catch(Throwable $t){      
     print_r($t);
     $content = ob_get_contents();
     ob_end_clean();
     $tfResponse->set("tfContent",$content,true);
     $tfResponse->return(); 

     exit();
  }
});


$router->map( 'POST', '/[a:tfa]/[a:tst]/[a:tsb]/home', function($tfFunctionalArea,$tfSessionToken,$tfTrailId) {
  ob_start(); 
  $tfResponse = new TfResponse();
   try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTaskId(81);
      $tfs->setTrailId($tfTrailId);
      $tfs->setAuthenticationType("Token");             
      $tfs->start();      
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());
      $tfs->toBeTrail($tfRequestController,$tfRequestAction,$_POST['request-data']);
      $tfRequestAction="HOME";
      require('controller/CmPost.ctrl.php');
      $tfs->close();
      $content = ob_get_contents();
  ob_end_clean();
  $tfResponse->set("tfContent",$content,true);
  $tfResponse->return(); 

            
  }catch(Throwable $t){      
    /* if (is_file(__DIR__ .'/'.$t->getCode().'.html')){
       require __DIR__ .'/'.$t->getCode().'.html';
     }else{
       //require __DIR__ .'/7999.html';
       print_r($t);
     } */
     print_r($t);
     $content = ob_get_contents();
     ob_end_clean();
     $tfResponse->set("tfContent",$content,true);
     $tfResponse->return(); 

     exit();
  }
});

$router->map( 'POST', '/[a:tfa]/[a:tst]/[a:tsb]/refresh', function($tfFunctionalArea,$tfSessionToken,$tfTrailId) {
  ob_start();
  $tfResponse = new TfResponse();
   try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTrailId($tfTrailId);
      $tfs->setAuthenticationType("Token");             
      $tfs->start();      
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());
      //$tfs->toBeTrail($tfRequestController,$tfRequestAction,$_POST['request-data']);
      $tfs->refresh();
      $tfs->close();
      $content = ob_get_contents();
  ob_end_clean();
  $tfResponse->set("tfContent",$content,true);
  $tfResponse->return(); 

            
  }catch(Throwable $t){      
    /* if (is_file(__DIR__ .'/'.$t->getCode().'.html')){
       require __DIR__ .'/'.$t->getCode().'.html';
     }else{
       //require __DIR__ .'/7999.html';
       print_r($t);
     } */
     print_r($t);
     $content = ob_get_contents();
     ob_end_clean();
     $tfResponse->set("tfContent",$content,true);
     $tfResponse->return(); 

     exit();
  }
});

$router->map( 'POST', '/[a:tfa]/[a:tst]/[a:tsb]/previous', function($tfFunctionalArea,$tfSessionToken,$tfTrailId) {
  ob_start();
  $tfResponse = new TfResponse();
   try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTrailId($tfTrailId);
      $tfs->setAuthenticationType("Token");             
      $tfs->start();      
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());
      //$tfs->toBeTrail($tfRequestController,$tfRequestAction,$_POST['request-data']);
      $tfs->previous();
      $tfs->close();
      $content = ob_get_contents();
  ob_end_clean();
  $tfResponse->set("tfContent",$content,true);
  $tfResponse->return(); 

            
  }catch(Throwable $t){      
    /* if (is_file(__DIR__ .'/'.$t->getCode().'.html')){
       require __DIR__ .'/'.$t->getCode().'.html';
     }else{
       //require __DIR__ .'/7999.html';
       print_r($t);
     } */
     print_r($t);
     $content = ob_get_contents();
     ob_end_clean();
     $tfResponse->set("tfContent",$content,true);
     $tfResponse->return(); 

     exit();
  }
});

$router->map( 'POST', '/[a:tfa]/[a:tst]/[a:tsb]/logout', function($tfFunctionalArea,$tfSessionToken,$tfTrailId) {

  $tfResponse = new TfResponse();
   try{
      
      $tfs = new TfSession(TF_DATABASE[$_SERVER["SERVER_NAME"]]);
      $tfs->setFunctionalAreaCode($tfFunctionalArea);
      $tfs->setId($tfSessionToken);
      $tfs->setTrailId($tfTrailId);
      $tfs->setAuthenticationType("Token");             
      $tfs->start();      
      $tfResponse->setRandomPublicKey($tfs->getRandomPublicKey());
      $tfs->logout();
      $tfs->close();
  $tfResponse->set("tfContent","",false);
  $tfResponse->return(); 

            
  }catch(Throwable $t){      
    /* if (is_file(__DIR__ .'/'.$t->getCode().'.html')){
       require __DIR__ .'/'.$t->getCode().'.html';
     }else{
       //require __DIR__ .'/7999.html';
       print_r($t);
     } */

     $tfResponse->set("tfContent",$t,true);
     $tfResponse->return(); 

     exit();
  }
});

$match = $router->match();
if( $match && is_callable( $match['target'] ) ) {
	call_user_func_array( $match['target'], $match['params']); 
} else {
	//require __DIR__ . '/404.php';
  echo "401";
}
?>



