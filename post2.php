  <style type="text/css">
  	.post-profile-photo{
  		width: 4.7rem;
  	}

.morecontent .more {
    display: none;
}

.more-comment{
	display: none;
}
.morelink {
    display: inline;
    color: #E64B9A;
    cursor: pointer;

}

.post-created-by{
	font-size: .75rem;
	text-transform: uppercase;
}
.post-business-title{
	font-size: .75rem;
}
.post-email-account{
	font-size: .65rem;
	font-style: italic;
}
.post-ago{
	font-size: .65rem;
}
.post-title{
	text-transform: uppercase;
}
.post-comment{
	font-size: .75rem;
	display: block;
	margin-bottom: .25rem;
}
.more-comment .morelink{
  display: inline;
}
.all-comment{
   font-size: .75rem;
   display: block;
   cursor: pointer;
}
.all-comment-hide{
	display: none;
}


.likes {
    font-size: .75rem;
    font-style: italic;
    padding: .25rem 0;
    margin-bottom: 0;
}

 .post-page.active{
       display: block;
     }
     .post-page{
       display: none;
     }

     .more, .more-comment, .morecontent{
     	font-size: .75rem;
     }
.post .feedback{
	font-size: 1.25rem;
	margin-bottom: 0;
}    
.feedback .like{
	color:#EA4335;
} 
.modal {
	top:6rem;
	z-index: 99999;
}
  </style>
  <?php 
 
    $postIndex=CmPost::index($tfs);
    
        
    $postLength=count($postIndex); 
    $pages= intdiv($postLength,5);
    $pageExtra =$postLength-($pages*5);
    $pagination = array();

    $pagination[1]= implode(",",array_column(array_slice($postIndex,(0), 5),"id"));
    $show=CmPost::dataListPage($tfs,$pagination[1]);
	$htmlPage='<div id="post-page-1" class="post-page full active">';
    
    foreach ($show as $s) {
       	$htmlPage.='   <!--Begin Blog Post -->
			       <div class="card shadow mb-4">  
			            <div class="card-body">
			                <div>
			                    <div class="card-header post">
			                        <span class="badge badge-pill c3-3 float-right m-2">'.$s["category"].'</span>
			                        <div class="d-flex justify-content-between align-items-center">
			                            <div class="d-flex justify-content-between align-items-center">
			                                <div class="mr-2">
			                                    <img class="rounded-circle post-profile-photo" src="'.$s["photo"].'" alt="">
			                                </div>
			                                <div class="ml-2">
			                                    <div class="post-created-by m-0">'.$s["name"].'</div>
			                                    <div class="post-business-title text-muted m-0">'.$s["business_title"].'</div>
			                                    <div class="post-email-account text-muted m-0">'.$s["email_account"].'</div>
			                                    <div class="post-ago text-muted mt-1">
			                                        <i class="bx bx-clock-o"></i>Hace '.CmPost::timeAgo($s["created_date"]).'
			                                    </div>
			                                </div>
			                            </div>
			                       </div>
			                    </div>
			                    <div class="card-body">
			                        <div class="post-title text-muted mb-2">'.$s["title"].'</div>
			                        <div>';
			                        $tags = explode(",",$s["tag"]);
			                        foreach ($tags as $t) {
                                        $htmlPage.='<span class="badge badge-pill c3-3">'.$t.'</span>';
			                        }
			             $htmlPage.=' </div>';  
                         
                         $length = strlen($s["content"]);
			             if ($length>225){
			                $htmlPage.=' <span class="card-text more">'.substr($s["content"],0,225).'...</span>
			                <span class="morecontent">
                             <span class="card-text more">'.$s["content"].'</span>
                             <span class="morelink" onclick="more(this);">  Mostrar mas</span>
                          </span>';
			             }else{
                            $htmlPage.=' <span class="card-text more">'.$s["content"].'</span>';
			             }        
			              $htmlPage.='</div>
			                    <div class="card-body post">
			                        <p class="feedback">';
			                if ($s["likeme"]>0){
                                $htmlPage.='<span class="like" data-id-post="'.$s["id"].'" onclick="like(this);"><i class="bx bxs-heart like"></i></span>';
			                }else{
			                	$htmlPage.='<span class="no-like" data-id-post="'.$s["id"].'" onclick="like(this);"><i class="bx bx-heart"></i></span>';   
			                }        
			                 $htmlPage.='   <span data-toggle="modal" data-target="#exampleModal" class="no-comment" data-id-post="'.$s["id"].'" onclick="comment(this);"><i class="bx bx-comment"></i></span>
			                        </p> 
			                        <p class="likes">
			                        	<b id="likes-'.$s["id"].'">'.$s["likes"].' Me gusta</b>
			                        </p>';
                            
                            $postComments = CmPostComment::listByPost($tfs,$s["id"]);
                            $comment = $postComments[0];
                            
                            $length = strlen($comment["created_by"])+strlen($comment["comment"]);
                    
                            if ($length>105){ 
                                $htmlPage.='<span id="one-comment-'.$s["id"].'" class="card-text more"><b>'.$comment["created_by"].' </b>'.substr($comment["comment"],0,105-strlen($comment["created_by"])).'...</span>
                                 <span class="morecontent">
                                    <span class="card-text more-comment" ><b>'.$comment["created_by"].' </b>'.$comment["comment"].'</span>
                                    <span id="link-comment-'.$s["id"].'" class="morelink" onclick="more(this);">  Mostrar mas</span>
                                  </span>';

                            }else{
                            	$htmlPage.='<span id="one-comment-'.$s["id"].'" class="card-text more"><b>'.$comment["created_by"].' </b>'.$comment["comment"].'</span>';
                            }
                            $htmlPage.='<div id="all-comment-'.$s["id"].'" style="display:none;">';
                            foreach ($postComments as $pc) {
                              $htmlPage.='<span class="post-comment"><b>'.$pc["created_by"].' </b>'.$pc["comment"].'</span>';
                            }    
                            $htmlPage.='</div>
                                <span class="all-comment light" data-id-comment="'.$s["id"].'" onclick="allComment(this)"><span class="verb"> Mostrar </span>'.count($postComments).' comentario(s)</span>    
			                    </div>
			                </div>
			            </div>
			        </div>';
    }
    $htmlPage.='</div>';

     for ($x = 1; $x <= ($pages-1); $x++) {
	  $pagination[$x+1]= implode(",",array_column(array_slice($postIndex,(5*$x), 5),"id"));
	  $htmlPage.='<div id="post-page-'.($x+1).'" class="post-page"></div>';
	}	
	if ($pageExtra>0){
     $pagination[$x+1]= implode(",",array_column(array_slice($postIndex,(5*$x), $pageExtra),"id"));
     $htmlPage.='<div id="post-page-'.($x+1).'" class="post-page"></div>';

	}
    
    $paginationJSON = json_encode($pagination,true);


    $postJs='<script type="text/javascript">


	$(function() {

	
    $("#light-pagination").pagination({
        items: '.$postLength.',
        itemsOnPage: 5,
        useAnchors:false,
        cssStyle: "light-theme",
        onPageClick : function(pageNumber){
        	let index = '.$paginationJSON.';	
            $(".post-page").removeClass("active");
            if (!$("#post-page-"+pageNumber).hasClass("full")){
              let JDATA = new Object();
              JDATA.tfTaskId = 81;
              JDATA.tfController = "CmPost"; 
              JDATA.tfFnName = "pagination";	
              JDATA.tfData = new Object();
              JDATA.tfData.posts=index[pageNumber];
              $("#post-page-"+pageNumber).html(TfRequest.fn(JDATA));
              $("#post-page-"+pageNumber).addClass("full");
            }  
            
            $("#post-page-"+pageNumber).addClass("active");
          }
	    });
	});
    </script>'; 

    //$postPage = array_column(array_slice($postIndex, 0, 10),"id"); 
   

   // $show=CmPost::dataListPage($tfs,implode(",",$posts));
        
    $html='  <!-- Begin dynamic content -->
			 <div class="row">
			   <!-- Begin left content -->
			    <div class="col-8">
			    '.$htmlPage.'
			    <div id="light-pagination" class="pagination light-theme simple-pagination"></div>
			    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-epic-xl-primary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-epic-xl-primary">Publicar</button>
      </div>
    </div>
  </div>
</div> '; 
       
     
    $html.='</div>
		    <!-- End left content --> 
		    <!-- Begin right content -->
		    <div class="col-4">
		        <!-- Search Widget -->
		        <div class="card">
		            <h5 class="card-header" ><i class="bx bx-search"></i> Buscar</h5>
		            <div class="card-body">
		                <div class="input-group">
		                    <input type="text" class="form-control" placeholder="Buscar...">
		                    <span class="input-group-append">
		                        <button class="btn btn-epic" type="button">Go!</button>
		                    </span>
		                </div>
		            </div>
		        </div>
		    </div>
		    <!-- End right content -->
		</div>  
		<!-- End dynamic content -->';
       //print_r($show);
       //
    echo $html.$postJs;
  ?>

