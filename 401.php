<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/png" href="asset/images/shortcut.png">
  <title>Guaramo</title>

  <!-- Google font -->
  <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

  <style>
* {
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}

body {
  padding: 0;
  margin: 0;
  background: #2F4858;
}

#message {
  position: relative;
  height: 100vh;
}

#message .message {
  position: absolute;
  left: 50%;
  top: 50%;
  -webkit-transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
}

.message {
  max-width: 767px;
  width: 100%;
  line-height: 1.4;
  padding: 110px 40px;
  text-align: center;
}

.message .message-code {
  position: relative;
  height: 6rem;
}


.message h2 {
  font-family: 'Lato', sans-serif;
  font-size: 1.4rem;
  font-weight: 400;
  color: #ffffff;
  margin-top: 0px;
  margin-bottom: 25px;
}

.message h4 {
  font-family: 'Lato', sans-serif;
  font-size: 1rem;
  font-weight: 400;
  color: #09091a;
  margin-top: 0px;
  margin-bottom: 25px;
}

img{
width:10rem;
}


  </style>


</head>

<body>


  <div id="message">
    <div class="message">
      <div class="message-code">
      <img src="<?php echo TUICH_URL_ROOT.'/asset/images/logo.png';?>"  alt="GTK">
      </div>
      <h2>401 Unauthorized</h2>
      <h4>The request has not been applied because it lacks valid authentication credentials.</h4>   

      
      
    </div>
  </div>

</body>

</html>
