<?php
  if ($tfRequestAction=="AL"){
     $hrEpicStatusList=HrEpicStatus::dataList($tfs);
  }else{
     $hrEpicStatus = new HrEpicStatus($tfs);
     $hrEpicStatus->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $hrEpicStatus->setCreatedBy($tfs->getUserId());
      $hrEpicStatus->setCreatedDate(date("Y-m-d H:i:s"));
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrEpicStatus->create();
      if ($hrEpicStatus->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("HrEpicStatus","AE",'{"hr_epic_status_id":"'.$hrEpicStatus->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrEpicStatus->update();
      if ($hrEpicStatus->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrEpicStatus","AE",'{"hr_epic_status_id":"'.$hrEpicStatus->getId().'"}');
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrEpicStatus->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrEpicStatus.rvw.php");
    }else{
      require("view/HrEpicStatus.vw.php");
    } 
  }
?>
