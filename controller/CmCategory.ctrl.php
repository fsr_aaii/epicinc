<?php
  if ($tfRequestAction=="AL"){
     $cmCategoryList=CmCategory::dataList($tfs);
  }else{
     $cmCategory = new CmCategory($tfs);
     $cmCategory->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $cmCategory->setCreatedBy($tfs->getUserId());
      $cmCategory->setCreatedDate(date("Y-m-d H:i:s"));
      $cmCategory->setValidations();
      $cmCategory->create();
      if ($cmCategory->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("CmCategory","AE",tfRequest::encrypt(array("cm_category_id" => $cmCategory->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $cmCategory->update();
      if ($cmCategory->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CmCategory","AE",tfRequest::encrypt(array("cm_category_id" => $cmCategory->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $cmCategory->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CmCategory.rvw.php");
    }else{
      require("view/CmCategory.vw.php");
    } 
  }
?>
