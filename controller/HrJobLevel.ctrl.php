<?php
  if ($tfRequestAction=="AL"){
     $hrJobLevelList=HrJobLevel::dataList($tfs);
  }else{
     $hrJobLevel = new HrJobLevel($tfs);
     $hrJobLevel->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrJobLevel->create();
      if ($hrJobLevel->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("HrJobLevel","AE",'{"hr_job_level_id":"'.$hrJobLevel->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrJobLevel->update();
      if ($hrJobLevel->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrJobLevel","AE",'{"hr_job_level_id":"'.$hrJobLevel->getId().'"}');
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrJobLevel->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrJobLevel.rvw.php");
    }else{
      require("view/HrJobLevel.vw.php");
    } 
  }
?>
