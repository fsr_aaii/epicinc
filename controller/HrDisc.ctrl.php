<?php
  if ($tfRequestAction=="AL"){
     $hrDiscList=HrDisc::dataList($tfs);
  }else{
     $hrDisc = new HrDisc($tfs);
     $hrDisc->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $hrDisc->setCreatedBy($tfs->getUserId());
      $hrDisc->setCreatedDate(date("Y-m-d H:i:s"));
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrDisc->create();
      if ($hrDisc->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("HrDisc","AE",'{"hr_disc_id":"'.$hrDisc->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrDisc->update();
      if ($hrDisc->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrDisc","AE",'{"hr_disc_id":"'.$hrDisc->getId().'"}');
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrDisc->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrDisc.rvw.php");
    }else{
      require("view/HrDisc.vw.php");
    } 
  }
?>
