<?php
  if ($tfRequestAction=="AL"){
     $tUserList=TUser::dataList($tfs);
  }else{
     $tUser = new TUser($tfs);
     $tUser->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $tUser->setCreatedBy($tfs->getUserId());
      $tUser->setCreatedDate(date("Y-m-d H:i:s"));
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $tUser->create();
      if ($tUser->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("TUser","AE",'{"t_user_id":"'.$tUser->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $tUser->update();
      if ($tUser->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("TUser","AE",'{"t_user_id":"'.$tUser->getId().'"}');
        $tfRequestAction="AE";
      break;
    case "AB":
      $tUser->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/TUser.rvw.php");
    }else{
      require("view/TUser.vw.php");
    } 
  }
?>
