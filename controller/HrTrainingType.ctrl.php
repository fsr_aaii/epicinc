<?php
  if ($tfRequestAction=="AL"){
     $hrTrainingTypeList=HrTrainingType::dataList($tfs);
  }else{
     $hrTrainingType = new HrTrainingType($tfs);
     $hrTrainingType->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrTrainingType->setCreatedBy($tfs->getUserId());
      $hrTrainingType->setCreatedDate(date("Y-m-d H:i:s"));
      $hrTrainingType->setValidations();
      $hrTrainingType->create();
      if ($hrTrainingType->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("HrTrainingType","AE",tfRequest::encrypt(array("hr_training_type_id" => $hrTrainingType->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrTrainingType->update();
      if ($hrTrainingType->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrTrainingType","AE",tfRequest::encrypt(array("hr_training_type_id" => $hrTrainingType->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrTrainingType->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrTrainingType.rvw.php");
    }else{
      require("view/HrTrainingType.vw.php");
    } 
  }
?>
