<?php
  if ($tfRequestAction=="AL"){
     $hrPositionEvaluationCriteriaList=HrPositionEvaluationCriteria::dataList($tfs);
  }else{
     $hrPositionEvaluationCriteria = new HrPositionEvaluationCriteria($tfs);
     $hrPositionEvaluationCriteria->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $hrPositionEvaluationCriteria->setCreatedBy($tfs->getUserId());
      $hrPositionEvaluationCriteria->setCreatedDate(date("Y-m-d H:i:s"));
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrPositionEvaluationCriteria->create();
      if ($hrPositionEvaluationCriteria->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("HrPositionEvaluationCriteria","AE",'{"hr_position_evaluation_criteria_id":"'.$hrPositionEvaluationCriteria->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrPositionEvaluationCriteria->update();
      if ($hrPositionEvaluationCriteria->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrPositionEvaluationCriteria","AE",'{"hr_position_evaluation_criteria_id":"'.$hrPositionEvaluationCriteria->getId().'"}');
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrPositionEvaluationCriteria->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrPositionEvaluationCriteria.rvw.php");
    }else{
      require("view/HrPositionEvaluationCriteria.vw.php");
    } 
  }
?>
