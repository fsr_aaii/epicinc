<?php
  if ($tfRequestAction=="AL"){
     $hrInsightsDiscoveryList=HrInsightsDiscovery::dataList($tfs);
  }else{
     $hrInsightsDiscovery = new HrInsightsDiscovery($tfs);
     $hrInsightsDiscovery->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrInsightsDiscovery->create();
      if ($hrInsightsDiscovery->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("HrInsightsDiscovery","AE",'{"hr_insights_discovery_id":"'.$hrInsightsDiscovery->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrInsightsDiscovery->update();
      if ($hrInsightsDiscovery->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrInsightsDiscovery","AE",'{"hr_insights_discovery_id":"'.$hrInsightsDiscovery->getId().'"}');
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrInsightsDiscovery->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrInsightsDiscovery.rvw.php");
    }else{
      require("view/HrInsightsDiscovery.vw.php");
    } 
  }
?>
