<?php
  if ($tfRequestAction=="AL"){
     $hrDevelopmentLevelList=HrDevelopmentLevel::dataList($tfs);
  }else{
     $hrDevelopmentLevel = new HrDevelopmentLevel($tfs);
     $hrDevelopmentLevel->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrDevelopmentLevel->create();
      if ($hrDevelopmentLevel->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("HrDevelopmentLevel","AE",'{"hr_development_level_id":"'.$hrDevelopmentLevel->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrDevelopmentLevel->update();
      if ($hrDevelopmentLevel->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrDevelopmentLevel","AE",'{"hr_development_level_id":"'.$hrDevelopmentLevel->getId().'"}');
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrDevelopmentLevel->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrDevelopmentLevel.rvw.php");
    }else{
      require("view/HrDevelopmentLevel.vw.php");
    } 
  }
?>
