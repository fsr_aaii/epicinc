<?php
  if ($tfRequestAction=="HOME"){    
    $postIndex=CmPost::index($tfs);  

  }elseif ($tfRequestAction=="LC"){
    $postIndex=CmPost::indexByCategory($tfs,$tfRequest->cm_post_id_category);  
  }elseif ($tfRequestAction=="LT"){
     $postIndex=CmPost::indexByTag($tfs,$tfRequest->cm_post_id_tag);  
  }elseif ($tfRequestAction=="LS"){
     $postIndex=CmPost::indexBySearch($tfs,$tfRequest->cm_post_search);  
  }elseif ($tfRequestAction=="LL"){
     $postIndex=CmPost::indexByLike($tfs);  
  }elseif ($tfRequestAction=="LCM"){
     $postIndex=CmPost::indexByComment($tfs);  
  }elseif ($tfRequestAction=="AL"){
    $cmPostList=CmPost::dataList($tfs);
  }else{  
     $cmPost = new CmPost($tfs);
     $cmPost->populate($tfRequest,in_array($tfRequestAction,array("AC","AE","AP","AR"))); 
     $tags = json_encode(CmTag::selectOptions($tfs),true);
     $profile = CmPost::profile($tfs);
  }
  switch ($tfRequestAction){
    case "AN":
    $cmPost->setActive('N');
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $cmPost->setCreatedBy($tfs->getUserId());
      $cmPost->setCreatedDate(date("Y-m-d H:i:s"));

      $filename=$_FILES['cm_post_image_img']['name'];
        if (isset($filename)) {
          $ext = pathinfo($filename, PATHINFO_EXTENSION);
          if ($_FILES['cm_post_image_img']['error']==0) {
            $allowed =  array('gif','png' ,'jpg','jpeg','bmp');           
            if(!in_array(strtolower($ext),$allowed) ) {
              $objAlerts.=TfWidget::alertDangerTemplate("Image extension not allowed,only are allowed: gif, png, jpg, jpeg, bmp");
            }else{
              $new_filename=date("mdY")."-".TfWidget::strRandom(4)."-".date("His").".".$ext;
              move_uploaded_file($_FILES['cm_post_image_img']['tmp_name'],"upload/post/".$new_filename);
              $cmPost->setImage("../upload/post/".$new_filename);
            }                   
          }
        }




      $cmPost->setValidations();
      $cmPost->create();
      if ($cmPost->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("CmPost","AE",tfRequest::encrypt(array("cm_post_id" => $cmPost->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $filename=$_FILES['cm_post_image_img']['name'];
        if (isset($filename)) {
          $ext = pathinfo($filename, PATHINFO_EXTENSION);
          if ($_FILES['cm_post_image_img']['error']==0) {
            $allowed =  array('gif','png' ,'jpg','jpeg','bmp');           
            if(!in_array(strtolower($ext),$allowed) ) {
              $objAlerts.=TfWidget::alertDangerTemplate("Image extension not allowed,only are allowed: gif, png, jpg, jpeg, bmp");
            }else{
              $new_filename=date("mdY")."-".TfWidget::strRandom(4)."-".date("His").".".$ext;
              move_uploaded_file($_FILES['cm_post_image_img']['tmp_name'],"upload/post/".$new_filename);
              $cmPost->setImage("../upload/post/".$new_filename);
            }                   
          }
        }

      $cmPost->update();
      if ($cmPost->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CmPost","AE",tfRequest::encrypt(array("cm_post_id" => $cmPost->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $cmPost->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
    case "AP":
      $cmPost->setActive('Y');
      $cmPost->update();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
    case "AR":
      $cmPost->setActive('N');
      $cmPost->update();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;    
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  (in_array($tfRequestAction,array("HOME","LC","LT","LS","LL","LCM"))){
      $postLength=count($postIndex); 
      $pages= intdiv($postLength,5);
      $pageExtra =$postLength-($pages*5);
      $pagination = array();

      $showIndex = array_column(array_slice($postIndex,0, 5),"id");
      $pagination[1]= implode(",", $showIndex);
      $aux=CmPost::dataListPage($tfs,$pagination[1]);

      foreach ($showIndex as $i) {
        $show[]= $aux[array_search($i, array_column($aux, 'id'))];
      }

      $dataListCategory=CmPost::qtyByCategory($tfs);
      $dataListTag=CmTag::selectOptions($tfs);
      require("view/CmPostHome.vw.php");
    }elseif  ($tfRequestAction=="AL"){
      require("view/CmPost.rvw.php");
    }else{
      require("view/CmPost.vw.php");
    } 
  }
?>
