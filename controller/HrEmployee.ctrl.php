<?php
 
  if ($tfRequestAction=="AL"){
     $hrEmployeeList=HrEmployee::dataList($tfs);
  }else{
     $hrEmployee = new HrEmployee($tfs);
     $hrEmployee->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
     $glNaturalPerson = new GlNaturalPerson($tfs);
     $glNaturalPerson->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $hrEmployee->setCreatedDate(date("Y-m-d H:i:s"));
      $hrEmployee->setCreatedBy($tfs->getUserId());
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrEmployee->create();
      if ($hrEmployee->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("HrEmployee","AE",'{"hr_employee_id":"'.$hrEmployee->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
    print_r($_FILES);

      $hrEmployee->update();
      if ($hrEmployee->isValid()){ 

        $filename=$_FILES['gl_natural_person_photo_img']['name'];
        if (isset($filename)) {
          $ext = pathinfo($filename, PATHINFO_EXTENSION);
          if ($_FILES['gl_person_photo_img']['error']==0) {
            $allowed =  array('gif','png' ,'jpg','jpeg','bmp');           
            if(!in_array(strtolower($ext),$allowed) ) {
              $objAlerts.=TfWidget::alertDangerTemplate("Image extension not allowed,only are allowed: gif, png, jpg, jpeg, bmp");
            }else{
              $new_filename=date("mdY")."-".TfWidget::strRandom(4)."-".date("His").".".$ext;
              move_uploaded_file($_FILES['gl_natural_person_photo_img']['tmp_name'],"upload/user/".$new_filename);
              $glNaturalPerson->setPhoto("../upload/user/".$new_filename);
            }                   
          }
        }
        $glNaturalPerson->update();
        if ($glNaturalPerson->isValid()){ 
          $tfs->checkTrans();
        } 

      }
      $tfs->swapTrail("HrEmployee","AE",'{"hr_employee_id":"'.$hrEmployee->getId().'"}');
      $tfRequestAction="AE";
      break;
    case "AB":
      $hrEmployee->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrEmployee.rvw.php");
    }else{
      require("view/HrEmployee.vw.php");
    } 
  }
?>
