<?php
  if ($tfRequestAction=="AL"){
     $hrEpicPriorityList=HrEpicPriority::dataList($tfs);
  }else{
     $hrEpicPriority = new HrEpicPriority($tfs);
     $hrEpicPriority->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $hrEpicPriority->setCreatedBy($tfs->getUserId());
      $hrEpicPriority->setCreatedDate(date("Y-m-d H:i:s"));
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrEpicPriority->create();
      if ($hrEpicPriority->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("HrEpicPriority","AE",'{"hr_epic_priority_id":"'.$hrEpicPriority->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrEpicPriority->update();
      if ($hrEpicPriority->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrEpicPriority","AE",'{"hr_epic_priority_id":"'.$hrEpicPriority->getId().'"}');
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrEpicPriority->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrEpicPriority.rvw.php");
    }else{
      require("view/HrEpicPriority.vw.php");
    } 
  }
?>
