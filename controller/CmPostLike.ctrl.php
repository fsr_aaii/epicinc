<?php
  if ($tfRequestAction=="AL"){
     $cmPostLikeList=CmPostLike::dataList($tfs);
  }else{
     $cmPostLike = new CmPostLike($tfs);
     $cmPostLike->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $cmPostLike->setCreatedBy($tfs->getUserId());
      $cmPostLike->setCreatedDate(date("Y-m-d H:i:s"));
      $cmPostLike->setValidations();
      $cmPostLike->create();
      if ($cmPostLike->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("CmPostLike","AE",tfRequest::encrypt(array("cm_post_like_id" => $cmPostLike->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $cmPostLike->update();
      if ($cmPostLike->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CmPostLike","AE",tfRequest::encrypt(array("cm_post_like_id" => $cmPostLike->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $cmPostLike->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CmPostLike.rvw.php");
    }else{
      require("view/CmPostLike.vw.php");
    } 
  }
?>
