<?php
  if ($tfRequestAction=="AL"){
     $hrCompetenceList=HrCompetence::dataList($tfs);
  }else{
     $hrCompetence = new HrCompetence($tfs);
     $hrCompetence->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrCompetence->create();
      if ($hrCompetence->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("HrCompetence","AE",'{"hr_competence_id":"'.$hrCompetence->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrCompetence->update();
      if ($hrCompetence->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrCompetence","AE",'{"hr_competence_id":"'.$hrCompetence->getId().'"}');
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrCompetence->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrCompetence.rvw.php");
    }else{
      require("view/HrCompetence.vw.php");
    } 
  }
?>
