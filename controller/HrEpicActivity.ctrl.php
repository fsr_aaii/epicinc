<?php
  if ($tfRequestAction=="AL"){
     $hrEpicActivityList=HrEpicActivity::dataList($tfs);
  }else{
     $hrEpicActivity = new HrEpicActivity($tfs);
     $hrEpicActivity->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $hrEpicActivity->setIdEpicPriority(1);      
      $hrEpicActivity->setIdEpicStatus(1);
      $hrEpicActivity->setProgress("0");

      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrEpicActivity->setCreatedBy($tfs->getUserId());
      $hrEpicActivity->setCreatedDate(date("Y-m-d H:i:s"));
      $hrEpicActivity->setStatusDate(date("Y-m-d H:i:s"));
      $hrEpicActivity->setActive("Y");
      $hrEpicActivity->setValidations();
      $hrEpicActivity->create();
      if ($hrEpicActivity->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("HrEpicActivity","AE",$tfResponse->encrypt(array("hr_epic_activity_id" => $hrEpicActivity->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrEpicActivity->update();
      if ($hrEpicActivity->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrEpicActivity","AE",$tfResponse->encrypt(array("hr_epic_activity_id" => $hrEpicActivity->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrEpicActivity->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrEpicActivity.rvw.php");
    }else{
      require("view/HrEpicActivity.vw.php");
    } 
  }
?>
