<?php
  if ($tfRequestAction=="AL"){
     $hrCoachingList=HrCoaching::dataList($tfs);
  }else{
     $hrCoaching = new HrCoaching($tfs);
     $hrCoaching->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrCoaching->setCreatedBy($tfs->getUserId());
      $hrCoaching->setCreatedDate(date("Y-m-d H:i:s"));
      $hrCoaching->setValidations();
      $hrCoaching->create();
      if ($hrCoaching->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("HrCoaching","AE",tfRequest::encrypt(array("hr_coaching_id" => $hrCoaching->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrCoaching->update();
      if ($hrCoaching->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrCoaching","AE",tfRequest::encrypt(array("hr_coaching_id" => $hrCoaching->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrCoaching->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrCoaching.rvw.php");
    }else{
      require("view/HrCoaching.vw.php");
    } 
  }
?>
