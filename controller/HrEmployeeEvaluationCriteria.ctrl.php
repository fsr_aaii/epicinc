<?php
  if ($tfRequestAction=="AL"){
     $hrEmployeeEvaluationCriteriaList=HrEmployeeEvaluationCriteria::dataList($tfs);
  }else{
     $hrEmployeeEvaluationCriteria = new HrEmployeeEvaluationCriteria($tfs);
     $hrEmployeeEvaluationCriteria->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $hrEmployeeEvaluationCriteria->setCreatedBy($tfs->getUserId());
      $hrEmployeeEvaluationCriteria->setCreatedDate(date("Y-m-d H:i:s"));
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrEmployeeEvaluationCriteria->create();
      if ($hrEmployeeEvaluationCriteria->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("HrEmployeeEvaluationCriteria","AE",'{"hr_employee_evaluation_criteria_id":"'.$hrEmployeeEvaluationCriteria->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrEmployeeEvaluationCriteria->update();
      if ($hrEmployeeEvaluationCriteria->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrEmployeeEvaluationCriteria","AE",'{"hr_employee_evaluation_criteria_id":"'.$hrEmployeeEvaluationCriteria->getId().'"}');
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrEmployeeEvaluationCriteria->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrEmployeeEvaluationCriteria.rvw.php");
    }else{
      require("view/HrEmployeeEvaluationCriteria.vw.php");
    } 
  }
?>
