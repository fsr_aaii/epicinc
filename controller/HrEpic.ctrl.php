<?php

  if ($tfRequestAction=="AL"){
     $hrEpicList=HrEpic::dataList($tfs);
  }else{
     $hrEpic = new HrEpic($tfs);
     $hrEpic->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $hrEpic->setIdEpicPriority(1);      
      $hrEpic->setIdEpicStatus(1);
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrEpic->setCreatedBy($tfs->getUserId());
      $hrEpic->setCreatedDate(date("Y-m-d H:i:s"));
      $hrEpic->setStatusDate(date("Y-m-d H:i:s"));
      $hrEpic->setProgress("0");
      $hrEpic->setActive("Y");
      $hrEpic->setValidations();
      $hrEpic->create();
      if ($hrEpic->isValid()){        
        $tfs->checkTrans();
        $tfs->swapTrail("HrEpic","AE",$tfResponse->encrypt(array('hr_epic_id' => $hrEpic->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrEpic->update();
      if ($hrEpic->isValid()){ 
        $tfs->checkTrans();
      }
      $tfs->swapTrail("HrEpic","AE",$tfResponse->encrypt(array('hr_epic_id' => $hrEpic->getId())),2);
      $tfRequestAction="AE";
      break;
    case "AB":
      $hrEpic->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrEpic.rvw.php");
    }else{
      require("view/HrEpic.vw.php");
    } 
  }
?>
