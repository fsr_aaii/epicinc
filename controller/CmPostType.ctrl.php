<?php
  if ($tfRequestAction=="AL"){
     $cmPostTypeList=CmPostType::dataList($tfs);
  }else{
     $cmPostType = new CmPostType($tfs);
     $cmPostType->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $cmPostType->setCreatedBy($tfs->getUserId());
      $cmPostType->setCreatedDate(date("Y-m-d H:i:s"));
      $cmPostType->setValidations();
      $cmPostType->create();
      if ($cmPostType->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("CmPostType","AE",tfRequest::encrypt(array("cm_post_type_id" => $cmPostType->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $cmPostType->update();
      if ($cmPostType->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CmPostType","AE",tfRequest::encrypt(array("cm_post_type_id" => $cmPostType->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $cmPostType->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CmPostType.rvw.php");
    }else{
      require("view/CmPostType.vw.php");
    } 
  }
?>
