<?php
  if ($tfRequestAction=="AL"){
     $glNaturalPersonList=GlNaturalPerson::dataList($tfs);
  }else{
     $glNaturalPerson = new GlNaturalPerson($tfs);
     $glNaturalPerson->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $glNaturalPerson->setCreatedBy($tfs->getUserId());
      $glNaturalPerson->setCreatedDate(date("Y-m-d H:i:s"));
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $glNaturalPerson->create();
      if ($glNaturalPerson->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("GlNaturalPerson","AE",'{"gl_natural_person_id":"'.$glNaturalPerson->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $glNaturalPerson->update();
      if ($glNaturalPerson->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("GlNaturalPerson","AE",'{"gl_natural_person_id":"'.$glNaturalPerson->getId().'"}');
        $tfRequestAction="AE";
      break;
    case "AB":
      $glNaturalPerson->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/GlNaturalPerson.rvw.php");
    }else{
      require("view/GlNaturalPerson.vw.php");
    } 
  }
?>
