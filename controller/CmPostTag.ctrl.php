<?php
  if ($tfRequestAction=="AL"){
     $cmPostTagList=CmPostTag::dataList($tfs);
  }else{
     $cmPostTag = new CmPostTag($tfs);
     $cmPostTag->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $cmPostTag->setCreatedBy($tfs->getUserId());
      $cmPostTag->setCreatedDate(date("Y-m-d H:i:s"));
      $cmPostTag->setValidations();
      $cmPostTag->create();
      if ($cmPostTag->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("CmPostTag","AE",tfRequest::encrypt(array("cm_post_tag_id" => $cmPostTag->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $cmPostTag->update();
      if ($cmPostTag->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CmPostTag","AE",tfRequest::encrypt(array("cm_post_tag_id" => $cmPostTag->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $cmPostTag->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CmPostTag.rvw.php");
    }else{
      require("view/CmPostTag.vw.php");
    } 
  }
?>
