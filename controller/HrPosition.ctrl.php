<?php
  if ($tfRequestAction=="AL"){
     $hrPositionList=HrPosition::dataList($tfs);
  }else{
     $hrPosition = new HrPosition($tfs);
     $hrPosition->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrPosition->setCreatedBy($tfs->getUserId());
      $hrPosition->setCreatedDate(date("Y-m-d H:i:s"));
      $hrPosition->setValidations();
      $hrPosition->create();
      if ($hrPosition->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("HrPosition","AE",tfRequest::encrypt(array("hr_position_id" => $hrPosition->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrPosition->update();
      if ($hrPosition->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrPosition","AE",tfRequest::encrypt(array("hr_position_id" => $hrPosition->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrPosition->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrPosition.rvw.php");
    }else{
      require("view/HrPosition.vw.php");
    } 
  }
?>
