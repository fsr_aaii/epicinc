<?php
  if ($tfRequestAction=="AL"){
     $hrPositionEvaluationList=HrPositionEvaluation::dataList($tfs);
  }else{
     $hrPositionEvaluation = new HrPositionEvaluation($tfs);
     $hrPositionEvaluation->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $hrPositionEvaluation->setCreatedBy($tfs->getfserId());
      $hrPositionEvaluation->setCreatedDate(date("Y-m-d H:i:s"));
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrPositionEvaluation->setDate(date("Y-m-d"));
      $hrPositionEvaluation->setCreatedBy($tfs->getUserId());
      $hrPositionEvaluation->setCreatedDate(date("Y-m-d H:i:s"));
      $hrPositionEvaluation->setValidations();
      $hrPositionEvaluation->create();
      if ($hrPositionEvaluation->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("HrPositionEvaluation","AE",'{"hr_position_evaluation_id":"'.$hrPositionEvaluation->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrPositionEvaluationCriteriaList=HrPositionEvaluationCriteria::listByPositionEvaluation($tfs,$hrPositionEvaluation->getId()); 
      $affected_rows=0;
      foreach ($hrPositionEvaluationCriteriaList as $row){
        $hrPositionEvaluationCriteria = new HrPositionEvaluationCriteria($tfs);
        $hrPositionEvaluationCriteria->populatePk($row["id"]);
        $hrPositionEvaluationCriteria->setValue($tfRequest->get("hr_position_evaluation_criteria_".$row["id"]));
        $hrPositionEvaluationCriteria->update();
        if (!$hrPositionEvaluationCriteria->isValid()){ 
          break;
        }else{
          if (in_array("This record has been updated", $hrPositionEvaluationCriteria->getObjMsg())) {
           $affected_rows++;
          }  
        }
      }
      if ($affected_rows>0){
        $hrPositionEvaluation->update();
      }
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);  
      break;
    case "AB":
      $hrPositionEvaluation->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrPositionEvaluation.rvw.php");
    }else{
      $hrPositionEvaluationCriteriaList = HrPositionEvaluationCriteria::listByPositionEvaluation($tfs,$hrPositionEvaluation->getId());
      require("view/HrPositionEvaluation.vw.php");
    } 
  }
?>
