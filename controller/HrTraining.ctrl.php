<?php
  if ($tfRequestAction=="AL"){
     $hrTrainingList=HrTraining::dataList($tfs);
  }else{
     $hrTraining = new HrTraining($tfs);
     $hrTraining->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $hrTraining->setIdEpicPriority(3);      
      $hrTraining->setIdEpicStatus(1);
      $hrTraining->setProgress("0");
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrTraining->setCreatedBy($tfs->getUserId());
      $hrTraining->setCreatedDate(date("Y-m-d H:i:s"));
      $hrTraining->setStatusDate(date("Y-m-d H:i:s"));
       $hrTraining->setActive('Y');
      $hrTraining->setValidations();
      $hrTraining->create();
      if ($hrTraining->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("HrTraining","AE",tfRequest::encrypt(array("hr_training_id" => $hrTraining->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrTraining->update();
      if ($hrTraining->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrTraining","AE",tfRequest::encrypt(array("hr_training_id" => $hrTraining->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrTraining->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrTraining.rvw.php");
    }else{
      require("view/HrTraining.vw.php");
    } 
  }
?>
