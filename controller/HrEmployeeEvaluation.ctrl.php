<?php
  if ($tfRequestAction=="AL"){
     $hrEmployeeEvaluationList=HrEmployeeEvaluation::dataList($tfs);
  }else{
     $hrEmployeeEvaluation = new HrEmployeeEvaluation($tfs);
     $hrEmployeeEvaluation->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      $hrEmployeeEvaluation->setCreatedBy($tfs->getUserId());
      $hrEmployeeEvaluation->setCreatedDate(date("Y-m-d H:i:s"));
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrEmployeeEvaluation->setDate(date("Y-m-d"));
      $hrEmployeeEvaluation->setCreatedBy($tfs->getUserId());
      $hrEmployeeEvaluation->setCreatedDate(date("Y-m-d H:i:s"));
      $hrEmployeeEvaluation->setValidations();
      $hrEmployeeEvaluation->create();
      if ($hrEmployeeEvaluation->isValid()){ 
        $tfs->checkTrans();
        $tfData["hr_employee_evaluation_id"]=$hrEmployeeEvaluation->getId();
        $tfs->swapTrail("HrEmployeeEvaluation","AE",$tfResponse->encrypt($tfData));
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrEmployeeEvaluationCriteriaList=HrEmployeeEvaluationCriteria::listByEmployeeEvaluation($tfs,$hrEmployeeEvaluation->getId());
      $affected_rows=0;
      foreach ($hrEmployeeEvaluationCriteriaList as $row){
        $hrEmployeeEvaluationCriteria = new HrEmployeeEvaluationCriteria($tfs);
        $hrEmployeeEvaluationCriteria->populatePk($row["id"]);
        $hrEmployeeEvaluationCriteria->setValue($tfRequest->get("hr_employee_evaluation_criteria_".$row["id"]));
        $hrEmployeeEvaluationCriteria->update();
        if (!$hrEmployeeEvaluationCriteria->isValid()){ 
          break;
        }else{
          if (in_array("This record has been updated", $hrEmployeeEvaluationCriteria->getObjMsg())) {
           $affected_rows++;
          }  
        }
      }
      if ($affected_rows>0){
        $hrEmployeeEvaluation->update();
      }
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);  
      break;
    case "AB":
      $hrEmployeeEvaluation->delete();
      $tfs->checkTrans();
      $tfs->backTrail(TRUE);
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    $hrEmployeeEvaluationCriteriaList=HrEmployeeEvaluationCriteria::listByEmployeeEvaluation($tfs,$hrEmployeeEvaluation->getId()); 
      if ($hrEmployeeEvaluation->getIdEvaluationType()==3){
        require("view/HrEmployeeEvaluation3.vw.php");
      }elseif ($hrEmployeeEvaluation->getIdEvaluationType()==4) {
        require("view/HrEmployeeEvaluation4.vw.php");
      }else{
      require("view/HrEmployeeEvaluation.vw.php");
    } 
  }
?>
