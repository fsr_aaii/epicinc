<?php
  if ($tfRequestAction=="AL"){
     $cmTagList=CmTag::dataList($tfs);
  }else{
     $cmTag = new CmTag($tfs);
     $cmTag->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $cmTag->setCreatedBy($tfs->getUserId());
      $cmTag->setCreatedDate(date("Y-m-d H:i:s"));
      $cmTag->setValidations();
      $cmTag->create();
      if ($cmTag->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("CmTag","AE",tfRequest::encrypt(array("cm_tag_id" => $cmTag->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $cmTag->update();
      if ($cmTag->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CmTag","AE",tfRequest::encrypt(array("cm_tag_id" => $cmTag->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $cmTag->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CmTag.rvw.php");
    }else{
      require("view/CmTag.vw.php");
    } 
  }
?>
