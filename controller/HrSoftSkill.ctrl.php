<?php
  if ($tfRequestAction=="AL"){
     $hrSoftSkillList=HrSoftSkill::dataList($tfs);
  }else{
     $hrSoftSkill = new HrSoftSkill($tfs);
     $hrSoftSkill->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $hrSoftSkill->create();
      if ($hrSoftSkill->isValid()){ 
        $tfs->checkTrans();
        $tfs->checkTrans();
        $tfs->swapTrail("HrSoftSkill","AE",'{"hr_soft_skill_id":"'.$hrSoftSkill->getId().'"}');
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $hrSoftSkill->update();
      if ($hrSoftSkill->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("HrSoftSkill","AE",'{"hr_soft_skill_id":"'.$hrSoftSkill->getId().'"}');
        $tfRequestAction="AE";
      break;
    case "AB":
      $hrSoftSkill->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/HrSoftSkill.rvw.php");
    }else{
      require("view/HrSoftSkill.vw.php");
    } 
  }
?>
