<?php
  if ($tfRequestAction=="AL"){
     $cmPostCommentList=CmPostComment::dataList($tfs);
  }else{
     $cmPostComment = new CmPostComment($tfs);
     $cmPostComment->populate($tfRequest,in_array($tfRequestAction,array("AC","AE"))); 
  }
  switch ($tfRequestAction){
    case "AN":
      break;
    case "AL":
      break;
    case "AC":
      $tfs->setSessionReadonly(TRUE);
      break; 
    case "AE":
      break; 
    case "AI":
      $cmPostComment->setCreatedBy($tfs->getUserId());
      $cmPostComment->setCreatedDate(date("Y-m-d H:i:s"));
      $cmPostComment->setValidations();
      $cmPostComment->create();
      if ($cmPostComment->isValid()){ 
        $tfs->checkTrans();
        $tfs->swapTrail("CmPostComment","AE",tfRequest::encrypt(array("cm_post_comment_id" => $cmPostComment->getId())),2);
        $tfRequestAction="AE";
      }
      break;
    case "AA":
      $cmPostComment->update();
      if ($cmPostComment->isValid()){ 
        $tfs->checkTrans();
      }
        $tfs->swapTrail("CmPostComment","AE",tfRequest::encrypt(array("cm_post_comment_id" => $cmPostComment->getId())),2);
        $tfRequestAction="AE";
      break;
    case "AB":
      $cmPostComment->delete();
      $tfs->checkTrans();
      $tfs->backTrail();
      break;
  }
  if ($tfs->toRedirect()){
    $tfs->redirect();
  }else{
    if  ($tfRequestAction=="AL"){
      require("view/CmPostComment.rvw.php");
    }else{
      require("view/CmPostComment.vw.php");
    } 
  }
?>
